<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

// Include classes
include_once('tbs_class.php'); // Load the TinyButStrong template engine
include_once('tbs_plugin_opentbs.php'); // Load the OpenTBS plugin

// prevent from a PHP configuration problem when using mktime() and date()
if (version_compare(PHP_VERSION,'5.1.0')>=0) {
	if (ini_get('date.timezone')=='') {
		date_default_timezone_set('UTC');
	}
}

// Initialize the TBS instance
$TBS = new clsTinyButStrong; // new instance of TBS
$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load the OpenTBS plugin

// Retrouve les champs à remplacer
require("./compte.php");
setlocale (LC_TIME, 'fr_FR.utf8','fra');
$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
mysql_select_db($baseSYGALE,$link);
$req2 = "SELECT * FROM operations LEFT JOIN collectivites ON collectivites.code_insee=operations.code_insee WHERE operations.id=".$_GET["id"];
$result2=mysql_query($req2,$link);
$nom_coll="";
while ($ligne2=mysql_fetch_assoc($result2))
{
	$num_ope = $ligne2["numero"];
	$categorie_ope = $ligne2["libelle"];
	$libelle_ope = $ligne2["categorie"];
	if ($ligne2["nom"]){$nom_coll = $ligne2["nom"];}else{$nom_coll="";}
}

$template = '../report/'.$_GET["type"].'/'.$_GET["file"];
$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

$output_file_name = str_replace('.', '_'.date('Y-m-d').'.', $template);
$TBS->Show(OPENTBS_DOWNLOAD, $output_file_name);
?>