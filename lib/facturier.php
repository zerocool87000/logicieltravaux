<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(isset($_POST['action']) && !empty($_POST['action'])) {
	$action = $_POST['action'];
	switch($action) {
		case 'lstcollectivite' : lst_collectivite();break;
		case 'lstentreprise' : lst_entreprise();break;
		case 'lstcategorie' : lst_categorie();break;
		case 'lstnature' : lst_nature();break;
		case 'lsthistory' : lst_history($_POST["idproj"]);break;
		case 'lstfacturierfacture' : lst_facturierfacture($_POST["page"],$_POST["login"],$_POST["chp"],$_POST["val"],$_POST["criteres"],$_POST["tabcrit"]);break;
		case 'addprojet' : add_projet($_POST["idcat"]);break;
		case 'delprojet' : del_projet($_POST["idproj"]);break;
		case 'attributionprojet' : attribution_projet($_POST["idproj"]);break;
		case 'detailprojet' : detail_projet($_POST["id"]);break;
		case 'detailprojlocalisation' : detailproj_localisation($_POST["idope"],$_POST["insee"],$_POST["entite"]);break;
		case 'detailgeneprojet' : detailgene_projet($_POST["idproj"]);break;
		case 'detailprogrammation' : detail_programmation($_POST["idproj"]);break;
		case 'updateprojet' : update_projet($_POST["idproj"],$_POST["chp"],$_POST["val"]);break;
		case 'updateaps' : update_aps($_POST["idproj"],$_POST["chp"],$_POST["val"]);break;
		case 'blah' : blah();break;
		// ...etc...
	}
}

function add_projet($idcat){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT cat_libelle, cat_codenumero, cat_AUT FROM categorie WHERE cat_id=".$idcat;
	$result2=mysql_query($req2,$link);
	while ($lignecat=mysql_fetch_assoc($result2))
	{
		$lib_cat = $lignecat["cat_libelle"];
		$code_cat = $lignecat["cat_codenumero"];
		$aut_cat = $lignecat["cat_AUT"];
	}
	$req2="SELECT proj_numero FROM projets ORDER BY proj_numero DESC LIMIT 1";
	$result2=mysql_query($req2,$link);
	while ($ligneproj=mysql_fetch_assoc($result2))
	{
		$tabnumope = explode("-",$ligneproj["proj_numero"]);
	}
	$newref = date("y")."-PRO-";
	if ($tabnumope[0]==date("y")){
		$compteur = $tabnumope[2]+1;
	}else{
		$compteur = 1;
	}
	$compt = strlen($compteur);
	$diff = 4 - $compt;
	for ($i=0;$i<$diff;$i++){
		$newref .= "0";
	}
	$newref .= $compteur;
	/* Insert l'op�ration */
	$req2="INSERT INTO projets (proj_numero,proj_datecreate,proj_categorie,proj_chargeaffaire) VALUES ('".$newref."',NOW(),'".$lib_cat."','".$_COOKIE["login"]."')";
	$result2=mysql_query($req2,$link);
	$idproj = mysql_insert_id($link);
	/* Ajout APS */
	$req2="INSERT INTO projets_aps (projaps_idproj) VALUES ('".$idproj."')";
	$result2=mysql_query($req2,$link);
	$req2="INSERT INTO historique (hist_idocc,hist_dateevent,hist_libelleevent,hist_user,hist_event,hist_natureevent) VALUES ('".$idproj."',NOW(),'Cr�ation projet','".$_COOKIE["login"]."','create','PROJ')";
	$result2=mysql_query($req2,$link);
}
function update_projet($idproj,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	switch ($chp){
		case "proj_datedemande":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "proj_entite":
			$req2="UPDATE projets SET proj_codeepci=0, proj_codeinsee='' WHERE proj_id=".$idproj;
			$result2=mysql_query($req2,$link);
			break;
		case "proj_idtva":
			$req2="UPDATE projets SET ".$chp."='".utf8_decode($val)."' WHERE proj_id=".$idproj;
			$result2=mysql_query($req2,$link);	
			$req="SELECT tva.tva_taux FROM projets JOIN tva ON tva.tva_id=projets.proj_idtva WHERE projets.proj_id=".$idproj;
			$result=mysql_query($req,$link);
			$ligne=mysql_fetch_assoc($result);
			$tva = $ligne["tva_taux"];
			$reqaps="SELECT * FROM projets_aps WHERE projets_aps.projaps_idproj=".$idproj;
			$resultaps=mysql_query($reqaps,$link);
			while ($ligneaps=mysql_fetch_assoc($resultaps))
			{
				$montant_ope = $ligneaps["projaps_MontantETU"]+$ligneaps["projaps_MontantBT"]+$ligneaps["projaps_MontantBT2"]+$ligneaps["projaps_MontantHT"]+$ligneaps["projaps_MontantPoste"]+$ligneaps["projaps_MontantMatECL"]+$ligneaps["projaps_MontantResECL"]+$ligneaps["projaps_MontantTEL"]+$ligneaps["projaps_MontantAutre"];
			}
			$montant_tva = $montant_ope * $tva/100;
			$montant_ttc = $montant_ope + $montant_tva;
			$req2="UPDATE projets_aps SET projaps_MontantHTOPE='".$montant_ope."',projaps_MontantTVA='".$montant_tva."',projaps_MontantTTCOPE='".$montant_ttc."' WHERE projaps_idproj=".$idproj;
			$result2=mysql_query($req2,$link);	
			break;
	}
	$req2="UPDATE projets SET ".$chp."='".utf8_decode($val)."' WHERE proj_id=".$idproj;
	$result2=mysql_query($req2,$link);	
}
function del_projet($idproj){
	require("./compte.php");
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	/* Suppression de l'op�ration */
	$sqlope = "DELETE FROM projets WHERE proj_id=".$idproj;
	$resulope=mysql_query($sqlope,$link);
}
function attribution_projet($idproj){
	require("./compte.php");
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT * FROM projets WHERE proj_id=".$idproj;
	$result2=mysql_query($req2,$link);
	while ($ligneproj=mysql_fetch_assoc($result2))
	{
		$demande_proj=$ligneproj["proj_datedemande"];
		$entite_proj=$ligneproj["proj_entite"];
		$codeepci_proj=$ligneproj["proj_codeepci"];
		$codeinsee_proj=$ligneproj["proj_codeinsee"];
		$codeinsee2_proj=$ligneproj["proj_codeinsee2"];
		$libelle_proj=$ligneproj["proj_libelle"];
		$lieudit_proj=$ligneproj["proj_lieudit"];
		$nature_proj=$ligneproj["proj_nature"];
		$anneeprog_proj=$ligneproj["proj_anneeprog"];
		$trimprog_proj=$ligneproj["proj_trimprog"];
		$cat_proj=$ligneproj["proj_categorie"];
	}
	$req2="SELECT * FROM projets_aps WHERE projaps_idproj=".$idproj;
	$result2=mysql_query($req2,$link);
	while ($ligneproj=mysql_fetch_assoc($result2))
	{
		$Montant_BT=$ligneproj["projaps_MontantBT"];
		$Montant_HT=$ligneproj["projaps_MontantHT"];
		$Montant_Poste=$ligneproj["projaps_MontantPoste"];
		$Montant_TEL=$ligneproj["projaps_MontantTEL"];
		$Montant_ECL=$ligneproj["projaps_MontantECL"];
		$Montant_OPE=$ligneproj["projaps_MontantOPE"];
	}
	$req2="SELECT cat_libelle, cat_codenumero, cat_AUT FROM categorie WHERE cat_libelle='".$cat_proj."'";
	$result2=mysql_query($req2,$link);
	while ($lignecat=mysql_fetch_assoc($result2))
	{
		$lib_cat = $lignecat["cat_libelle"];
		$code_cat = $lignecat["cat_codenumero"];
		$aut_cat = $lignecat["cat_AUT"];
	}
	$req2="SELECT ope_numero FROM operations WHERE ope_categorie='".$lib_cat."' ORDER BY ope_numero DESC LIMIT 1";
	$result2=mysql_query($req2,$link);
	while ($ligneope=mysql_fetch_assoc($result2))
	{
		$tabnumope = explode("-",$ligneope["ope_numero"]);
	}
	$newref = date("y")."-".$code_cat."-";
	if ($tabnumope[0]==date("y")){
		$compteur = $tabnumope[2]+1;
	}else{
		$compteur = 1;
	}
	$compt = strlen($compteur);
	$diff = 3 - $compt;
	for ($i=0;$i<$diff;$i++){
		$newref .= "0";
	}
	$newref .= $compteur;
	/* Insert l'op�ration */
	$req2="INSERT INTO operations (ope_numero,ope_datecreate,ope_datedemande,ope_entite,ope_codeepci,ope_codeinsee,ope_codeinsee2,ope_libelle,ope_lieudit,ope_nature,ope_anneeprog,ope_trimprog,ope_categorie,ope_chargeaffaire) VALUES ('".$newref."',NOW(),'".$demande_proj."','".$entite_proj."','".$codeepci_proj."','".$codeinsee_proj."','".$codeinsee2_proj."','".$libelle_proj."','".$lieudit_proj."','".$nature_proj."','".$anneeprog_proj."','".$trimprog_proj."','".$cat_proj."','".$_COOKIE["login"]."')";
	$result2=mysql_query($req2,$link);
	$idope = mysql_insert_id($link);
	/* Ajout APS */
	$req2="INSERT INTO ope_aps (opeaps_idope,opeaps_MontantBT,opeaps_MontantHT,opeaps_MontantPoste,opeaps_MontantTEL,opeaps_MontantECL,opeaps_MontantOPE) VALUES ('".$idope."','".$Montant_BT."','".$Montant_HT."','".$Montant_Poste."','".$Montant_TEL."','".$Montant_ECL."','".$Montant_OPE."')";
	$result2=mysql_query($req2,$link);
	$req2="INSERT INTO historique (hist_idocc,hist_dateevent,hist_libelleevent,hist_user,hist_event,hist_natureevent) VALUES ('".$idope."',NOW(),'Cr�ation op�ration','".$_COOKIE["login"]."','create','TVX')";
	$result2=mysql_query($req2,$link);
	/* Insert les jalons */
	$reqjal="SELECT jal_id FROM jalon WHERE jal_activite in ('','ETU','CTO','".$code_cat."') ORDER BY jal_ordre ASC";
	$resultjal=mysql_query($reqjal,$link);
	while ($lignejal=mysql_fetch_assoc($resultjal))
	{
		$req2="INSERT INTO ope_jalon (opejalon_idope,opejalon_idjalon) VALUES ('".$idope."','".$lignejal["id"]."')";
		$result2=mysql_query($req2,$link);
	}
	/* Cr�ation des coordinations */
	$reqcoord="SELECT coord_id, coord_libelle FROM coordination";
	$resultcoord=mysql_query($reqcoord,$link);
	while ($lignecoord=mysql_fetch_assoc($resultcoord))
	{
		$req2="INSERT INTO ope_coord (opecoord_idope,opecoord_idcoord,opecoord_valeur) VALUES ('".$idope."','".$lignecoord["coord_id"]."','0')";
		$result2=mysql_query($req2,$link);
	}
	/* Cr�ation des crit�res */
	$reqcrit="SELECT critere_id, critere_libelle FROM critere";
	$resultcrit=mysql_query($reqcrit,$link);
	while ($lignecrit=mysql_fetch_assoc($resultcrit))
	{
		$req2="INSERT INTO ope_critere (opecritere_idope,opecritere_idcritere,opecritere_valeur) VALUES ('".$idope."','".$lignecrit["critere_id"]."','0')";
		$result2=mysql_query($req2,$link);
	}
	if ($aut_cat==1){
		/* Cr�ation de l'autorisation */
		$num_aut = $newref."-AUT-1";
		$req2="INSERT INTO autorisation (auto_idope,auto_type,auto_numero) VALUES ('".$idope."','74','".$num_aut."')";
		$result2=mysql_query($req2,$link);
	}
	/* Cr�ation des prestations obligatoires */
	$reqnat="SELECT nat_libelle,nat_codenumero FROM nature WHERE nat_oblig='1'";
	$resultnat=mysql_query($reqnat,$link);
	while ($lignenat=mysql_fetch_assoc($resultnat))
	{
		$newref_cht = $newref."-".$lignenat["nat_codenumero"]."-1";
		$req2="INSERT INTO chantiers (cht_idope,cht_numero,cht_datecreate,cht_nature,cht_categorie) VALUES ('".$idope."','".$newref_cht."',NOW(),'".$lignenat["nat_libelle"]."','".$lib_cat."')";
		$result2=mysql_query($req2,$link);
		$idcht = mysql_insert_id($link);
		$req2="INSERT INTO apd (id_cht) VALUES ('".$idcht."')";
		$result2=mysql_query($req2,$link);
		$sql = "SHOW TABLES FROM $baseSYGALE";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_row($result)) {
			if ($row[0]==strtolower($lignenat["code_numero"])){
				$req2="INSERT INTO ".$row[0]." (id_cht) VALUES ('".$idcht."')";
				$result2=mysql_query($req2,$link);
				break;
			}
		}
		$req2="INSERT INTO historique (hist_idocc,hist_dateevent,hist_libelleevent,hist_user,hist_e4vent,hist_natureevent) VALUES ('".$idope."',NOW(),'Cr�ation prestation ".$lignenat["libelle"]."','".$_COOKIE["login"]."','create','TVX')";
		$result2=mysql_query($req2,$link);
	}
	$sqlproj = "DELETE FROM projets WHERE proj_id=".$idproj;
	$resulproj=mysql_query($sqlproj,$link);
}
function lst_collectivite(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT coll_nom, coll_codeinsee, coll_type FROM collectivites WHERE coll_type in ('COMMUNE','EPCI') ORDER BY coll_type,coll_nom ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une collectivit�</option>';
	if ($row2!=0)
	{
		$c=0;
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			if ($c==0){echo '<option class="lstcomoptionfilter" disabled><b>-------- '.$ligne2["coll_type"].' --------</b></option>';$type_coll=$ligne2["coll_type"];}
			if ($type_coll!=$ligne2["type"]){echo '<option class="lstcomoptionfilter" disabled><b>-------- '.$ligne2["coll_type"].' --------</b></option>';$type_coll=$ligne2["coll_type"];}
			echo '<option value="'.$ligne2["coll_codeinsee"].'">'.$ligne2["coll_nom"].'</option>';
			$c++;
		}
	}
}
function lst_entreprise(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT ent_nom, ent_id FROM entreprises ORDER BY ent_nom ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une entrepise</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["ent_id"].'">'.$ligne2["ent_nom"].'</option>';
		}
	}
}
function lst_categorie(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT cat_libelle, cat_id FROM categorie ORDER BY cat_libelle ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une cat�gorie</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["cat_id"].'">'.$ligne2["cat_libelle"].'</option>';
		}
	}
}
function lst_nature(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT nat_libelle, nat_id FROM nature ORDER BY nat_libelle ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une nature</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["nat_id"].'">'.$ligne2["nat_libelle"].'</option>';
		}
	}
}

function lst_facturierfacture($page,$loginca,$chp,$val,$criteres,$tabcrit){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	
	/* Recherche des droits de l'utilisateur */
	$retour_droit=mysql_query('SELECT user.user_droit FROM user where user.user_login="'.$loginca.'"');
	$donnees_droit=mysql_fetch_assoc($retour_droit);
	$droit=$donnees_droit['user_droit'];

	$messagesParPage=10;
	if ($criteres!=""){
		//$retour_total=mysql_query('SELECT COUNT(*) AS total FROM projets where '.$chp.' like "%'.$val.'%"');
		$retour_total=mysql_query('SELECT COUNT(*) AS total FROM factures where '.stripslashes(utf8_decode($criteres)));
		$donnees_total=mysql_fetch_assoc($retour_total);
	}else{
		$retour_total=mysql_query('SELECT COUNT(*) AS total FROM factures');
		$donnees_total=mysql_fetch_assoc($retour_total);
	}
	$total=$donnees_total['total'];
	$nombreDePages=ceil($total/$messagesParPage);
	if(isset($page))
	{
		$pageActuelle=intval($page);

		if($pageActuelle>$nombreDePages)
		{
			$pageActuelle=$nombreDePages;
		}
	}else{
		$pageActuelle=1;
	}

	$premiereEntree=($pageActuelle-1)*$messagesParPage;

	if ($criteres!=""){
		$delimiter = ",";
		$tab_crit = explode($delimiter,$tabcrit);
		for ($i=0;$i<count($tab_crit);$i++){
			$tabtemp = explode(":",$tab_crit[$i]);
			switch($tabtemp[0]){
				case "fact_datecreate":
					$date = $tabtemp[1];
					break;
				case "fact_idtype":
					$type = $tabtemp[1];
					break;
				case "fact_numero":
					$numero = $tabtemp[1];
					break;
				case "fact_codeinsee":
					$codeinsee = $tabtemp[1];
					break;
			}
		}
		//$req2="SELECT projets.proj_numero, projets.proj_libelle, projets.proj_lieudit, projets.proj_categorie, projets.proj_chargeaffaire, projets.proj_anneeprog, projets.proj_id, collectivites.coll_nom, user.user_initial FROM projets LEFT JOIN collectivites ON collectivites.coll_codeinsee=projets.proj_codeinsee LEFT JOIN user ON user.user_login=projets.proj_chargeaffaire WHERE ".$chp." LIKE '%".$val."%' ORDER BY projets.proj_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
		$req2="SELECT factures.fact_numero, factures.fact_datecreate, factures.fact_montantht, factures.fact_montantttc, factures.fact_id, collectivites.coll_nom, lexique.lex_libelle FROM factures LEFT JOIN collectivites ON collectivites.coll_codeinsee=factures.fact_codeinsee LEFT JOIN lexique ON lexique.lex_id=factures.fact_idtype WHERE ".stripslashes(utf8_decode($criteres))." ORDER BY factures.fact_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
	}else{
		$req2="SELECT factures.fact_numero, factures.fact_datecreate, factures.fact_montantht, factures.fact_montantttc, factures.fact_id, collectivites.coll_nom, lexique.lex_libelle FROM factures LEFT JOIN collectivites ON collectivites.coll_codeinsee=factures.fact_codeinsee LEFT JOIN lexique ON lexique.lex_id=factures.fact_idtype ORDER BY factures.fact_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
	}
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	$compteope = 1;
	if ($pageActuelle>1){
		echo "<div><div data-tooltip='Pr�c�dent' class='forward btnpager'><i class='fa fa-caret-left' aria-hidden='true'></i></div>";
	}
	echo "<div class='boxpager'>".$pageActuelle." / ".$nombreDePages."</div>";
	if ($pageActuelle<$nombreDePages){
		echo "<div data-tooltip='Suivant' class='next btnpager'><i class='fa fa-caret-right' aria-hidden='true'></i></div></div>";
	}
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th width='15%'></th><th class='text-center' width='10%'>Date facture</th><th class='text-center' width='10%'>Type facture</th><th class='text-center' width='10%'>Num�ro</th><th class='text-center' width='20%'>Commune</th><th class='text-center' width='10%'>Montant HT (�)</th><th class='text-center' width='10%'>Montant TTC (�)</th></tr></thead>";
	echo "<tbody><tr><td></td><td><input data-chp='fact_datecreate' class='form-control input-sm text-center chpfact_filter' value='".$date."'></td>";
	echo "<td><select data-chp='fact_idtype' class='form-control input-sm chpfact_filter'><option value=''>-- Type de facture --</option>";
	$reqcat="SELECT lex_id, lex_libelle FROM lexique WHERE lex_codelexique='TYPE_FACT'";
	$resultcat=mysql_query($reqcat,$link);
	while ($lignecat=mysql_fetch_assoc($resultcat))
	{
		if ($type==$lignecat["lex_id"]){$selected="selected";}else{$selected="";}
		echo "<option value='".$lignecat["lex_id"]."' ".$selected.">".$lignecat["lex_libelle"]."</option>";
	}
	echo "</select></td>";
	echo "<td><input data-chp='fact_numero' class='form-control input-sm text-center chpfact_filter' value='".$numero."'></td>";
	echo "<td><select data-chp='fact_codeinsee' class='form-control input-sm chpfact_filter'><option value=''>-- Collectivit�s --</option>";
	$reqcat="SELECT coll_nom, coll_codeinsee FROM collectivites WHERE coll_type='COMMUNE'";
	$resultcat=mysql_query($reqcat,$link);
	while ($lignecat=mysql_fetch_assoc($resultcat))
	{
		if ($codeinsee==$lignecat["coll_codeinsee"]){$selected="selected";}else{$selected="";}
		echo "<option value='".$lignecat["coll_codeinsee"]."' ".$selected.">".$lignecat["coll_nom"]."</option>";
	}
	echo "</select></td>";
	echo "<td></td>";
	echo "<td></td>";
	echo "</tr></tbody>";
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo "<tr><td align='center' width='15%'><button data-tooltip='Visualiser' data-id='".$ligne2["fact_id"]."' class='btn btn-sm btn-default visuproj'><i class='fa fa-eye'></i></button> <button data-tooltip='Imprimer' data-id='".$ligne2["fact_id"]."' class='btn btn-sm btn-default attrproj'><i class='fas fa-print'></i></button> <button data-tooltip='Supprimer' data-id='".$ligne2["fact_id"]."' class='btn btn-sm btn-default delfact'><i class='fa fa-trash'></i></button></td>";
			echo "<td align='center' width='10%'>".strftime("%d/%m/%Y",strtotime($ligne2["fact_datecreate"]))."</td>";
			echo "<td align='center' width='10%'>".$ligne2["lex_libelle"]."</td>";
			echo "<td align='center' width='10%'>".$ligne2["fact_numero"]."</td><td width='20%'>".$ligne2["coll_nom"]."</td><td align='right' width='10%'>".$ligne2["fact_montantht"]."</td><td align='right' width='10%'>".$ligne2["fact_montantttc"]."</td>";
			echo "</tr>";
		}
	}else{
		echo "<tr><td colspan='8' align='center'><b>Aucune facture</b></td></tr>";
	}
	echo "</tbody>";
	echo "</table>";
}

function detail_projet($id){
	require("./compte.php");
	require("./print.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT proj_numero, proj_libelle, proj_categorie, proj_id FROM projets WHERE proj_id=".$id;
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{

			$reqcat="SELECT cat_libelle, cat_codenumero, cat_AUT FROM categorie WHERE cat_libelle='".$ligne2["proj_categorie"]."'";
			$resultcat=mysql_query($reqcat,$link);
			while ($lignecat=mysql_fetch_assoc($resultcat))
			{
				$aut_cat = $lignecat["cat_AUT"];
				$code_cat = $lignecat["cat_codenumero"];
			}
			$panel=1;
			echo "<div class='col-md-12'>";
			echo "<div class='portlet'><div class='portlet-title'><i class='fa fa-cogs' aria-hidden='true'></i> <b>PROJET</b> ".$ligne2["proj_categorie"]." - ".$ligne2["proj_numero"]." - ".$ligne2["proj_libelle"]."</div><div class='portlet-content' id='detprojet'>";
			echo "<div class='row'>";

			echo '<div class="col-sm-12">';
			echo "<button id='back_lstprojets' class='btn btn-default btn-sm' data-idproj='".$ligne2["proj_id"]."'><i class='fa fa-reply'></i> Projets</button>";
			echo '</div><br><br>';

			echo '<div class="col-sm-12">';
			echo '<div class="panel-group" id="accordion">';
			/* G�n�ralit�s */
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h2 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-file" aria-hidden="true"></i> G�n�ralit�s</a>';
			echo '</h2>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detailgene_projet">';
				detailgene_projet($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Programmation */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="far fa-chart-bar"></i> Programmation (APS)</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detprogrammation">';
				detail_programmation($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Etats */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="far fa-file-alt" aria-hidden="true"></i> Etats</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detdocprojet">';
				lst_print_contextuel("Travaux/PROJET",$id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Historique */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-history" aria-hidden="true"></i> Historique</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="lsthistory">';
				lst_history($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			
			echo '</div>';
			echo '</div>';
		}
	}
}
function lst_history($idproj){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqhist="SELECT hist_dateevent, hist_libelleevent, hist_event, user_nom, user_prenom FROM historique LEFT JOIN user ON historique.hist_user=user.user_login WHERE hist_idocc=".$idproj." AND hist_natureevent='PROJ' ORDER BY hist_dateevent DESC";
	$resulthist=mysql_query($reqhist,$link);
	$rowhist=mysql_num_rows($resulthist);
	if ($rowhist!=0)
	{
		echo "<div class='col-sm-12'>";
		while ($lignehist=mysql_fetch_assoc($resulthist))
		{
			switch($lignehist["hist_event"]){
				case "create":
					$icon = "magic";
					break;
				case "delete":
					$icon = "trash";
					break;
			}
			//echo "<div class='row'><i class='fa fa-".$icon."'></i> ".$lignehist["hist_libelleevent"]." le ".strftime("%d/%m/%Y %T",strtotime($lignehist["hist_dateevent"]))." par ".$lignehist["hist_user"]."</div>";
			echo '<div class="media"><div class="media-left"><a href="#"><img src="../images/cogs_hist.png" class="media-object img-rounded" alt="Sample Image"></a></div>';
			echo '<div class="media-body"><h5 class="media-heading"><b>'.$lignehist["user_nom"].' '.$lignehist["user_prenom"].' </b><small><i>Effectu� le '.strftime("%d/%m/%Y %T",strtotime($lignehist["hist_dateevent"])).'</i></small></h5>';
			echo '<p><i class="fa fa-'.$icon.'"></i> '.$lignehist["hist_libelleevent"].'</p>';
			echo '</div></div>';
		}
		echo "</div>";
	}
}
function detailgene_projet($idproj){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM projets WHERE proj_id=".$idproj;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$datecreate = strftime("%d/%m/%Y",strtotime($ligne["proj_datecreate"]));
		$datedemande = strftime("%d/%m/%Y",strtotime($ligne["proj_datedemande"]));
		$entite = $ligne["proj_entite"];
		$charge_affaire = $ligne["proj_chargeaffaire"];
		$epci = $ligne["proj_codeepci"];
		$insee = $ligne["proj_codeinsee"];
		$insee2 = $ligne["proj_codeinsee2"];
		$numero = $ligne["proj_numero"];
		$libelle = $ligne["proj_libelle"];
		$lieudit = $ligne["proj_lieudit"];
		$categorie = $ligne["proj_categorie"];
		$moa = $ligne["proj_moa"];
		$moe = $ligne["proj_moe"];
		$nature = $ligne["proj_nature"];
		$proj_tva = $ligne["proj_idtva"];
	}
	if ($charge_affaire==$_COOKIE["login"]){$disabled="";}else{$disabled="disabled";}
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">G�n�ralit�s</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="proj_datecreate">Date cr�ation:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="proj_datecreate" name="proj_datecreate" value="'.$datecreate.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="proj_datedemande">Date demande:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_datedemande" id="proj_datedemande" name="proj_datedemande" value="'.$datedemande.'" '.$disabled.'>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="proj_ca">Charg� d\'affaire:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_chargeaffaire" id="proj_ca" name="proj_ca" disabled>';
		echo '<option value="">S�lectionnez un charg� d\'affaire</option>';
		$req="SELECT user_login,user_nom,user_prenom FROM user";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["user_login"]==$charge_affaire){$selected = "selected";}else{$selected = "";}
			echo '<option value="'.$ligne["user_login"].'" '.$selected.'>'.$ligne["user_nom"].' '.$ligne["user_prenom"].'</option>';
		}
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="proj_tva">TVA:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_idtva" id="proj_tva" name="proj_tva">';
		echo '<option value="">S�lectionnez une tva</option>';
		$req="SELECT tva_id,tva_taux FROM tva";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["tva_id"]==$proj_tva){$selected = "selected";}else{$selected = "";}
			echo '<option value="'.$ligne["tva_id"].'" '.$selected.'>'.$ligne["tva_taux"].'</option>';
		}
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="proj_moa">MOA:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="proj_moa" name="proj_moa" value="'.$moa.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="proj_moe">MOE:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="proj_moe" name="proj_moe" value="'.$moe.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="proj_categorie">Cat�gorie:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="proj_categorie" name="proj_categorie" value="'.$categorie.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="proj_nature">Nature:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_nature" id="proj_nature" name="proj_nature" '.$disabled.'>';
		echo '<option value="0">Choisir une nature de travaux</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='NAT_OPE'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_libelle"]==$nature){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_libelle"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="proj_refsehv">R�f�rence SEHV:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="proj_refsehv" name="proj_refsehv" value="'.$numero.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="proj_libelle">Libell�:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_libelle" id="proj_libelle" name="proj_libelle" value="'.$libelle.'" '.$disabled.'>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="proj_lieudit">Lieu-dit:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_lieudit" id="proj_lieudit" name="proj_lieudit" value="'.$lieudit.'" '.$disabled.'>';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-map-marker-alt"></i> Localisation</div>';
	echo '<div class="panel-body" id="detailproj_localisation">';
		detailproj_localisation($idproj,$insee,$insee2,$entite,$epci,$disabled);
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function detailproj_localisation($idproj,$insee,$insee2,$entite,$epci,$disabled){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="proj_entite">Origine demande:</label>';
	echo '<div class="col-sm-7">';
	echo '<select class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_entite" id="proj_entite" name="proj_entite" '.$disabled.'>';
	echo '<option value="-1">S�lectionnez une entit�</option>';
	if ($entite=="EPCI"){echo '<option value="EPCI" selected>EPCI</option>';}else{echo '<option value="EPCI">EPCI</option>';}
	if ($entite=="COMMUNE"){echo '<option value="COMMUNE" selected>Commune</option>';}else{echo '<option value="COMMUNE">Commune</option>';}
	echo '</select>';
	echo '</div>';
	echo '</div>';
	switch ($entite){
		case "COMMUNE":
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="proj_commune">Commune:</label>';
			echo '<div class="col-sm-7">';
			echo '<select class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_codeinsee" id="proj_commune" name="proj_commune" '.$disabled.'>';
			echo '<option value="-1">S�lectionnez une commune</option>';
			if ($epci!=0){
				$req="SELECT * FROM collectivites WHERE coll_idpere=".$epci;
			}else{
				$req="SELECT * FROM collectivites WHERE coll_type='COMMUNE'";
			}
			$result=mysql_query($req,$link);
			$code_epci="";
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["coll_codeinsee"]==$insee){$selected="selected";$code_epci=$ligne["coll_idpere"];}else{$selected="";}
				echo '<option value="'.$ligne["coll_codeinsee"].'" '.$selected.'>'.$ligne["coll_nom"].'</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="proj_epci">EPCI:</label>';
			echo '<div class="col-sm-7">';
			if ($disabled==""){if ($entite!="EPCI"){$disabled="disabled";}else{$disabled="";}}
			echo '<select class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_codeepci" id="proj_epci" name="proj_epci" '.$disabled.'>';
			echo '<option value="-1">S�lectionnez un EPCI</option>';
			$req="SELECT * FROM collectivites WHERE coll_type='EPCI'";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($epci!=0){
					if ($ligne["coll_id"]==$epci){$selected="selected";}else{$selected="";}
				}else{
					if ($ligne["coll_id"]==$code_epci){$selected="selected";}else{$selected="";}
				}
				echo '<option value="'.$ligne["coll_id"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
		break;
		case "EPCI":
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="proj_epci">EPCI:</label>';
			echo '<div class="col-sm-7">';
			if ($disabled==""){if ($entite!="EPCI"){$disabled="disabled";}else{$disabled="";}}
			echo '<select class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_codeepci" id="proj_epci" name="proj_epci" '.$disabled.'>';
			echo '<option value="-1">S�lectionnez un EPCI</option>';
			$req="SELECT * FROM collectivites WHERE coll_type='EPCI'";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($epci!=0){
					if ($ligne["coll_id"]==$epci){$selected="selected";}else{$selected="";}
				}else{
					if ($ligne["coll_id"]==$code_epci){$selected="selected";}else{$selected="";}
				}
				echo '<option value="'.$ligne["coll_id"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="proj_commune">Commune:</label>';
			echo '<div class="col-sm-7">';
			echo '<select class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_codeinsee" id="proj_commune" name="proj_commune" '.$disabled.'>';
			echo '<option value="-1">S�lectionnez une commune</option>';
			if ($epci!=0){
				$req="SELECT * FROM collectivites WHERE coll_idpere=".$epci;
			}else{
				$req="SELECT * FROM collectivites WHERE coll_type='COMMUNE'";
			}
			$result=mysql_query($req,$link);
			$code_epci="";
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["coll_codeinsee"]==$insee){$selected="selected";$code_epci=$ligne["coll_idpere"];}else{$selected="";}
				echo '<option value="'.$ligne["coll_codeinsee"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
		break;
	}
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="proj_commune2">Commune 2:</label>';
	echo '<div class="col-sm-7">';
	echo '<select class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_codeinsee2" id="proj_commune2" name="proj_commune2" '.$disabled.'>';
	echo '<option value="-1">S�lectionnez une commune</option>';
	$req="SELECT * FROM collectivites WHERE coll_type='COMMUNE'";
	$result=mysql_query($req,$link);
	$code_epci="";
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["coll_codeinsee"]==$insee2){$selected="selected";$code_epci=$ligne["coll_idpere"];}else{$selected="";}
		echo '<option value="'.$ligne["coll_codeinsee"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
	}
	echo '</select>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="proj_ste">STE:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="proj_ste" name="proj_ste" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="proj_insee">Code insee:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="proj_insee" name="proj_insee" value="'.$insee.'" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="proj_cp">Code postal:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="proj_cp" name="proj_cp" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="proj_statcoll">Statut collectivit� (U/R):</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="proj_statcoll" name="proj_statcoll" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="proj_eligface">Eligibilit� FACE:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="proj_eligface" name="proj_eligface" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="proj_adhecl">Adh�rente ECL:</label>';
	echo '<div class="col-sm-2">';
	echo '<input type="text" class="form-control input-sm" id="proj_adhecl" name="proj_adhecl" disabled>';
	echo '</div>';
	echo '<label class="control-label col-sm-3" for="proj_adhesp">Adh�rente ESP:</label>';
	echo '<div class="col-sm-2">';
	echo '<input type="text" class="form-control input-sm" id="proj_adhesp" name="proj_adhesp" disabled>';
	echo '</div>';
	echo '</div>';
	echo '</form>';
}
function detail_programmation($idproj){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<div class="col-sm-12" style="margin-bottom:20px;">';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="far fa-clock"></i> Programmation</div>';
	echo '<div class="panel-body">';
		$reqproj="SELECT projets.proj_anneeprog,projets.proj_trimprog,projets.proj_identprog,tva.tva_taux,projets.proj_longdeposefs1,projets.proj_longdeposefs2,projets.proj_longdeposetorsade,projets.proj_deltau,projets.proj_idtype FROM projets JOIN tva ON tva.tva_id=projets.proj_idtva WHERE projets.proj_id=".$idproj;
		$resultproj=mysql_query($reqproj,$link);
		while ($ligneproj=mysql_fetch_assoc($resultproj))
		{
			$annee_prog = $ligneproj["proj_anneeprog"];
			$trim_prog = $ligneproj["proj_trimprog"];
			$ident_prog = $ligneproj["proj_identprog"];
			$tva = $ligneproj["tva_taux"];
			$longdeposefs1 = $ligneproj["proj_longdeposefs1"];
			$longdeposefs2 = $ligneproj["proj_longdeposefs2"];
			$longdeposetorsade = $ligneproj["proj_longdeposetorsade"];
			$deltau = $ligneproj["proj_deltau"];
			$id_type = $ligneproj["proj_idtype"];
		}
		echo "<label class='col-sm-7'>Ann�e programmation:</label><div class='col-sm-4'><input id='proj_anneeprog' data-idproj='".$idproj."' data-chp='proj_anneeprog' class='form-control input-sm text-center modifchpprojet' value='".$annee_prog."'></div>";
		echo "<div class='col-sm-12'>";
		for ($i=1;$i<5;$i++){
			if ($i==$trim_prog){$checked='checked';}else{$checked='';}
			echo "<label class='radio col-sm-12'><input data-idproj='".$idproj."' data-chp='proj_trimprog' type='radio' name='optradio' class='modifchpprojet' value='".$i."' ".$checked.">Trimestre ".$i."</label>";
		}
		echo "</div>";
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-wrench"></i> Donn�es Techniques</div>';
	echo '<div class="panel-body">';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="proj_entreprise">Entreprise: </label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_identprog" id="proj_entreprise" name="proj_entreprise">';
			echo '<option value="0">Choisir une entreprise</option>';
			$req="SELECT entreprises.ent_id, entreprises.ent_nom FROM entreprises";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["ent_id"]==$ident_prog){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["ent_id"].'" '.$selected.'>'.$row["ent_nom"].'</option>';
			}
			echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="proj_longdeposefs1">Long. d�pose FS1:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_longdeposefs1" id="proj_longdeposefs1" name="proj_longdeposefs1" value="'.$longdeposefs1.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="proj_longdeposefs2">Long. d�pose FS2:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_longdeposefs2" id="proj_longdeposefs2" name="proj_longdeposefs2" value="'.$longdeposefs2.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="proj_longdeposetorsade">Long. d�pose Torsad�:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_longdeposetorsade" id="proj_longdeposetorsade" name="proj_longdeposetorsade" value="'.$longdeposetorsade.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="proj_deltau">&#916 U/U (%):</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_deltau" id="proj_deltau" name="proj_deltau" value="'.$deltau.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="proj_idtype">Type: </label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpprojet" data-idproj="'.$idproj.'" data-chp="proj_idtype" id="proj_idtype" name="proj_idtype">';
			echo '<option value="0">Choisir un type</option>';
			$req="SELECT lexique.lex_id, lexique.lex_libelle FROM lexique WHERE lexique.lex_codelexique='TYPE_PROJET'";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["lex_id"]==$id_type){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["lex_id"].'" '.$selected.'>'.$row["lex_libelle"].'</option>';
			}
			echo '</select>';
		echo '</div>';
		echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '</div>';
	/* APS */
	echo '<div class="col-sm-12">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="far fa-money-bill-alt" aria-hidden="true"></i> APS</div>';
	echo '<div class="panel-body">';
		$montant_ope = 0;
		echo '<table class="table table-sm table-bordered table-responsive table-striped table-hover">';
		echo '<thead><tr><th>Libell�</th><th>Prog.</th><th>Qte/mL</th><th>P.U.</th><th>Montants HT</th><th>Montants TTC</th></tr></thead>';
		echo '<tbody>';
		$reqaps="SELECT * FROM projets_aps WHERE projets_aps.projaps_idproj=".$idproj;
		$resultaps=mysql_query($reqaps,$link);
		while ($ligneaps=mysql_fetch_assoc($resultaps))
		{
			echo '<tr><td>Etude</td>';
			echo '<td width="25%"><select class="form-control input-sm update_aps" data-idproj="'.$idproj.'" data-chp="projaps_idprogETU" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["projaps_idprogETU"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_qteETU" value="'.$ligneaps["projaps_qteETU"].'"></td><td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_puETU" value="'.$ligneaps["projaps_puETU"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantHTETU" value="'.$ligneaps["projaps_MontantHTETU"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantTTCETU" value="'.$ligneaps["projaps_MontantTTCETU"].'"></td></tr>';
			echo '<tr><td>Electricit� BT 1</td>';
			echo '<td width="25%"><select class="form-control input-sm update_aps" data-idproj="'.$idproj.'" data-chp="projaps_idprogBT" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["projaps_idprogBT"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_qteBT" value="'.$ligneaps["projaps_qteBT"].'"></td><td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_puBT" value="'.$ligneaps["projaps_puBT"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantHTBT" value="'.$ligneaps["projaps_MontantHTBT"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantTTCBT" value="'.$ligneaps["projaps_MontantTTCBT"].'"></td></tr>';
			echo '<tr><td>Electricit� BT 2</td>';
			echo '<td width="20%"><select class="form-control input-sm update_aps" data-idproj="'.$idproj.'" data-chp="projaps_idprogBT2" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["projaps_idprogBT2"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_qteBT2" value="'.$ligneaps["projaps_qteBT2"].'"></td><td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_puBT2" value="'.$ligneaps["projaps_puBT2"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantHTBT2" value="'.$ligneaps["projaps_MontantHTBT2"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantTTCBT2" value="'.$ligneaps["projaps_MontantTTCBT2"].'"></td></tr>';
			echo '<tr><td>Electricit� HT</td>';
			echo '<td width="25%"><select class="form-control input-sm update_aps" data-idproj="'.$idproj.'" data-chp="projaps_idprogHT" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["projaps_idprogHT"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_qteHTA" value="'.$ligneaps["projaps_qteHTA"].'"></td><td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_puHTA" value="'.$ligneaps["projaps_puHTA"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantHTHTA" value="'.$ligneaps["projaps_MontantHTHTA"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantTTCHTA" value="'.$ligneaps["projaps_MontantTTCHTA"].'"></td></tr>';
			echo '<tr><td>Poste</td>';
			echo '<td width="25%"><select class="form-control input-sm update_aps" data-idproj="'.$idproj.'" data-chp="projaps_idprogPoste" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["projaps_idprogPoste"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_qtePoste" value="'.$ligneaps["projaps_qtePoste"].'"></td><td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_puPoste" value="'.$ligneaps["projaps_puPoste"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantHTPoste" value="'.$ligneaps["projaps_MontantHTPoste"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantTTCPoste" value="'.$ligneaps["projaps_MontantTTCPoste"].'"></td></tr>';
			echo '<tr><td>Eclairage Public (Mat�riel)</td>';
			echo '<td width="25%"><select class="form-control input-sm update_aps" data-idproj="'.$idproj.'" data-chp="projaps_idprogMatECL" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["projaps_idprogMatECL"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_qteMatECL" value="'.$ligneaps["projaps_qteMatECL"].'"></td><td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_puMatECL" value="'.$ligneaps["projaps_puMatECL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantHTMatECL" value="'.$ligneaps["projaps_MontantHTMatECL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantTTCMatECL" value="'.$ligneaps["projaps_MontantTTCMatECL"].'"></td></tr>';
			echo '<tr><td>Eclairage Public (R�seau)</td>';
			echo '<td width="25%"><select class="form-control input-sm update_aps" data-idproj="'.$idproj.'" data-chp="projaps_idprogResECL" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["projaps_idprogResECL"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_qteResECL" value="'.$ligneaps["projaps_qteResECL"].'"></td><td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_puResECL" value="'.$ligneaps["projaps_puResECL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantHTResECL" value="'.$ligneaps["projaps_MontantHTResECL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantTTCResECL" value="'.$ligneaps["projaps_MontantTTCResECL"].'"></td></tr>';
			echo '<tr><td>T�l�communication</td>';
			echo '<td width="25%"><select class="form-control input-sm update_aps" data-idproj="'.$idproj.'" data-chp="projaps_idprogTEL" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["projaps_idprogTEL"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_qteTEL" value="'.$ligneaps["projaps_qteTEL"].'"></td><td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_puTEL" value="'.$ligneaps["projaps_puTEL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantHTTEL" value="'.$ligneaps["projaps_MontantHTTEL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantTTCTEL" value="'.$ligneaps["projaps_MontantTTCTEL"].'"></td></tr>';
			echo '<tr><td>Autre</td>';
			echo '<td width="25%"><select class="form-control input-sm update_aps" data-idproj="'.$idproj.'" data-chp="projaps_idprogAutre" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["projaps_idprogAutre"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_qteAutre" value="'.$ligneaps["projaps_qteAutre"].'"></td><td width="12%"><input class="form-control input-sm text-center update_aps" data-idproj="'.$idproj.'" data-chp="projaps_puAutre" value="'.$ligneaps["projaps_puAutre"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantHTAutre" value="'.$ligneaps["projaps_MontantHTAutre"].'"></td><td width="15%"><input class="form-control input-sm text-right update_aps" data-idproj="'.$idproj.'" data-chp="projaps_MontantTTCAutre" value="'.$ligneaps["projaps_MontantTTCAutre"].'"></td></tr>';
			echo '<tr><td colspan="4"><b>Total Op�ration</b></td><td class="text-right"><b>'.$ligneaps["projaps_MontantHTOPE"].' �</b></td><td class="text-right"><b>'.$ligneaps["projaps_MontantTTCOPE"].' �</b></td></tr>';
		}
		echo '</tbody>';
		echo '</table>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function update_aps($idproj,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE projets_aps SET ".$chp."='".$val."' WHERE projaps_idproj=".$idproj;
	$result2=mysql_query($req2,$link);	
	$req="SELECT tva.tva_taux FROM projets JOIN tva ON tva.tva_id=projets.proj_idtva WHERE projets.proj_id=".$idproj;
	$result=mysql_query($req,$link);
	$ligne=mysql_fetch_assoc($result);
	$tva = $ligne["tva_taux"];
	$reqaps="SELECT * FROM projets_aps WHERE projets_aps.projaps_idproj=".$idproj;
	$resultaps=mysql_query($reqaps,$link);
	while ($ligneaps=mysql_fetch_assoc($resultaps))
	{
		$montant_ope=0;
		$tab = array("ETU","BT","BT2","HTA","Poste","MatECL","ResECL","TEL","Autre");
		for ($i=0;$i<count($tab);$i++){
			$chpqte="projaps_qte".$tab[$i];
			$chppu="projaps_pu".$tab[$i];
			$chpht="projaps_MontantHT".$tab[$i];
			$chpttc="projaps_MontantTTC".$tab[$i];
			if ($ligneaps[$chpqte]!=0 || $ligneaps[$chppu]!=0){$ht = $ligneaps[$chpqte]*$ligneaps[$chppu];$req2="UPDATE projets_aps SET ".$chpht."='".$ht."' WHERE projaps_idproj=".$idproj;$result2=mysql_query($req2,$link);}else{$ht=$ligneaps[$chpht];}
			$ttc=$ht+($ht*$tva/100);
			$req2="UPDATE projets_aps SET ".$chpttc."='".$ttc."' WHERE projaps_idproj=".$idproj;$result2=mysql_query($req2,$link);
			$montant_ope = $montant_ope + $ligneaps[$chpht];
		}
	}
	$montant_tva = $montant_ope * $tva/100;
	$montant_ttc = $montant_ope + $montant_tva;
	$req2="UPDATE projets_aps SET projaps_MontantHTOPE='".$montant_ope."',projaps_MontantTTCOPE='".$montant_ttc."' WHERE projaps_idproj=".$idproj;
	$result2=mysql_query($req2,$link);	
}