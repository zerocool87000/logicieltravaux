<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(isset($_POST['action']) && !empty($_POST['action'])) {
	$action = $_POST['action'];
	switch($action) {
		case 'lstoperationdemos' : lst_operationdemos($_POST["idbc"]);break;
		case 'lstchantieropedemos' : lst_chantieropedemos($_POST["idope"],$_POST["tabcht"]);break;
		case 'lstchantieropedembc' : lst_chantieropedembc($_POST["idope"],$_POST["tabcht"]);break;
		case 'lstchantieropedembctemp' : lst_chantieropedembctemp($_POST["tabcht"]);break;
		case 'lstchantieropedemostemp' : lst_chantieropedemostemp($_POST["tabcht"]);break;
		case 'lsttypeos' : lst_typeos();break;
		case 'lstdemandebc' : lst_demandebc();break;
		case 'lstoperation' : lst_operation($_POST["page"],$_POST["login"],$_POST["chp"],$_POST["val"],$_POST["criteres"],$_POST["tabcrit"]);break;
		case 'addoperation' : add_operation($_POST["idcat"]);break;
		case 'deloperation' : del_operation($_POST["idope"]);break;
		case 'addchantier' : add_chantier($_POST["idope"],$_POST["idnat"]);break;
		case 'delchantier' : del_chantier($_POST["idcht"]);break;
		case 'financecht' : finance_cht($_POST["idanneeprog"],$_POST["idprog"]);break;
		case 'adddemandebc' : add_demandebc($_POST["numconvep"],$_POST["dateconvep"],$_POST["ident"],$_POST["delai"],$_POST["tabcht"]);break;
		case 'adddemandeos' : add_demandeos($_POST["idope"],$_POST["idbc"],$_POST["montantttc"],$_POST["typeos"],$_POST["ident"],$_POST["delai"],$_POST["tabcht"]);break;
		case 'lstcollectivite' : lst_collectivite();break;
		case 'lstentreprise' : lst_entreprise();break;
		case 'lstcategorie' : lst_categorie();break;
		case 'lstnature' : lst_nature();break;
		case 'lsthistory' : lst_history($_POST["idope"]);break;
		case 'detailoperation' : detail_operation($_POST["id"]);break;
		case 'detailopelocalisation' : detailope_localisation($_POST["idope"],$_POST["insee"],$_POST["entite"]);break;
		case 'detailchantier' : detail_chantier($_POST["id"]);break;
		case 'updatechantier' : update_chantier($_POST["idcht"],$_POST["chp"],$_POST["val"]);break;
		case 'detailgeneoperation' : detailgene_operation($_POST["idope"]);break;
		case 'detailgenechantier' : detailgene_chantier($_POST["idcht"]);break;
		case 'lstsstraitantcht' : lst_sstraitantcht($_POST["idcht"]);break;
		case 'addsstraitantcht' : add_sstraitantcht($_POST["idcht"]);break;
		case 'delsstraitantcht' : del_sstraitantcht($_POST["id"]);break;
		case 'updatesstraitantcht' : update_sstraitantcht($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		case 'detailjalon' : detail_jalon($_POST["idope"]);break;
		case 'detailautorisation' : detail_autorisation($_POST["idope"]);break;
		case 'detailpctoperation' : detail_pctoperation($_POST["idope"]);break;
		case 'detailcoordination' : detail_coordination($_POST["idope"]);break;
		case 'detailprogrammation' : detail_programmation($_POST["idope"]);break;
		case 'updateapsope' : update_apsope($_POST["idope"],$_POST["chp"],$_POST["val"]);break;
		case 'detailbcoperation' : detailbc_operation($_POST["idope"]);break;
		case 'checkdestinataireauto' : check_destinataireauto($_POST["idauto"],$_POST["iddest"],$_POST["val"]);break;
		case 'updatedestinataireauto' : update_destinataireauto($_POST["idauto"],$_POST["iddest"],$_POST["chp"],$_POST["val"]);break;
		case 'addvalauto' : add_valauto($_POST["idope"]);break;
		case 'delvalauto' : del_valauto($_POST["idvalauto"]);break;
		case 'updatevalauto' : update_valauto($_POST["idvalauto"],$_POST["chp"],$_POST["val"]);break;
		case 'lstvalorisationauto' : lst_valorisationauto($_POST["idope"]);break;
		case 'updatedatejalonoperation' : updatedatejalon_operation($_POST["idope"],$_POST["idjal"],$_POST["val"]);break;
		case 'updatecoordoperation' : updatecoord_operation($_POST["idope"],$_POST["idcoord"],$_POST["val"]);break;
		case 'updatecritereoperation' : updatecritere_operation($_POST["idope"],$_POST["idcritere"],$_POST["val"]);break;
		case 'updateoperation' : update_operation($_POST["idope"],$_POST["chp"],$_POST["val"]);break;
		/* APD */
		case 'lstarticlebord' : lst_article_bord($_POST["idbord"],$_POST["idapd"]);break;
		case 'lstvalapd' : lst_val_apd($_POST["idapd"]);break;
		case 'addvalapd' : add_valapd($_POST["idapd"],$_POST["idart"]);break;
		case 'delvalapd' : del_valapd($_POST["idvalapd"]);break;
		case 'updatevalapd' : update_valapd($_POST["idvalapd"],$_POST["chp"],$_POST["val"]);break;
		case 'updatetauxapd' : update_tauxapd($_POST["idapd"],$_POST["chp"],$_POST["val"],$_POST["type"]);break;
		/* B�n�ficiaires */
		case 'detailopebeneficiaire' : detail_opebeneficiaire($_POST["idope"]);break;
		case 'addopebeneficiaire' : add_opebeneficiaire($_POST["idope"]);break;
		case 'delopebeneficiaire' : del_opebeneficiaire($_POST["id"]);break;
		case 'updateopebeneficiaire' : update_opebeneficiaire($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		/* PCT */
		case 'lstbordpct' : lst_bordpct($_POST["page"],$_POST["login"],$_POST["chp"],$_POST["val"]);break;
		case 'detailbordpct' : detail_bordpct($_POST["id"]);break;
		case 'blah' : blah();break;
		/* DT */
		case 'createemprisedt' : create_emprisedt($_POST["idope"],$_POST["emprise"]);break;
		// ...etc...
	}
}

function update_operation($idope,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	switch ($chp){
		case "datedemande":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "entite":
			$req2="UPDATE operations SET ope_codeepci=0, ope_codeinsee='' WHERE ope_id=".$idope;
			$result2=mysql_query($req2,$link);
			break;
	}
	$req2="UPDATE operations SET ".$chp."='".utf8_decode($val)."' WHERE ope_id=".$idope;
	$result2=mysql_query($req2,$link);	
}

function lst_demandebc(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT demande_bc.numero, demande_bc.date_create, statut_demandebc.libelle, demande_bc.id FROM demande_bc JOIN statut_demandebc ON statut_demandebc.id=demande_bc.id_statut WHERE demande_bc.login='".$_COOKIE["login"]."' ORDER BY demande_bc.date_create ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center'></th><th>Num�ro</th><th>Date</th><th>Statut</th></tr></thead>";
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo "<tr class='showcht' style='cursor:zoom-in;'><td align='center' width='5%'><i class='fa fa-envelope'></i></td><td>".$ligne2["numero"]."</td><td width='8%'>".strftime("%d/%m/%Y",strtotime($ligne2["date_create"]))."</td><td width='20%'>".$ligne2["libelle"]."</td></tr>";
			$reqcht="SELECT chantiers.cht_numero FROM chantiers JOIN demandebc_chantiers ON chantiers.cht_id=demandebc_chantiers.id_cht WHERE demandebc_chantiers.id_demandebc=".$ligne2["id"];
			$resultcht=mysql_query($reqcht,$link);
			echo '<tr><td colspan="2">Prestations associ�es</td><td colspan="2">';
			$c=1;
			while ($lignecht=mysql_fetch_assoc($resultcht))
			{
				if ($c!=1){echo "<br>";}
				echo "<i class='fa fa-bolt'></i> ".$lignecht["cht_numero"];
				$c++;
			}
			echo '</td></tr>';
		}
	}else{
		echo '<tr><td class="text-center" colspan="4"><b>Aucune demande</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function lst_operationdemos($idbc){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT chantiers.cht_numero, chantiers.cht_id, chantiers.cht_idanneeprog, programme.prog_libelle as programme, annee_prog.anneeprog_annee, apd.tot_ttc FROM chantiers JOIN apd ON apd.id_cht=chantiers.cht_id JOIN bc_cht ON bc_cht.bccht_idcht=chantiers.cht_id  LEFT JOIN annee_prog ON annee_prog.anneeprog_id=chantiers.cht_idanneeprog LEFT JOIN programme ON programme.prog_id=annee_prog.anneeprog_idprog WHERE bc_cht.bccht_idbc=".$idbc;
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center'></th><th>Prestations</th><th>APD</th><th>Financement</th></tr></thead>";
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne=mysql_fetch_assoc($result2))
		{
			echo "<tr><td align='center' width='5%'><input type='checkbox' class='chkchtdemandeos' data-id='".$ligne["cht_id"]."'></td><td>".$ligne["cht_numero"]."</td><td class='text-center' width='20%'>".number_format($ligne["tot_ttc"], 2, ',', ' ')."</td><td class='text-center' width='20%'>".$ligne["programme"]." ".$ligne["anneeprog_annee"]."</td></tr>";
		}
		$disabled = "";
	}else{
		echo "<tr><td colspan='4' align='center'><b>Aucune prestation</b></td></tr>";
		$disabled = "disabled";
	}
	echo "</tbody>";
	echo "</table>";
}
function lst_chantieropedemos($idope,$tabcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT ope_id FROM operations WHERE ope_numero='".$idope."'";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne=mysql_fetch_assoc($result2))
		{
			$idope= $ligne["ope_id"];
		}
		$req2="SELECT chantiers.cht_numero, chantiers.cht_id, chantiers.cht_idanneeprog, programme.prog_libelle as programme, annee_prog.anneeprog_annee, apd.tot_ttc FROM chantiers JOIN apd ON apd.id_cht=chantiers.cht_id LEFT JOIN annee_prog ON annee_prog.anneeprog_id=chantiers.cht_idanneeprog LEFT JOIN programme ON programme.prog_id=annee_prog.anneeprog_idprog WHERE NOT EXISTS (select * from demandebc_chantiers where chantiers.cht_id=demandebc_chantiers.id_cht) AND chantiers.cht_idope='".$idope."'";
		//$req2="SELECT chantiers.cht_numero, chantiers.cht_id, chantiers.cht_programme, chantiers.cht_anneeprog, apd.tot_ttc FROM chantiers LEFT JOIN apd ON apd.id_cht=chantiers.cht_id WHERE NOT EXISTS (select * from bc_cht where chantiers.cht_id=bc_cht.bccht_idcht) AND chantiers.cht_idope='".$idope."'";
		$result2=mysql_query($req2,$link);
		$row2=mysql_num_rows($result2);
		$delimiter = ",";
		$tab_cht = explode($delimiter,$tabcht);
		echo "<table class='table table-responsive table-bordered table-sm'>";
		echo "<thead><tr><th class='text-center'></th><th>Prestations</th><th width='20%'>APD (TTC)</th><th width='20%'>Financement</th></tr></thead>";
		echo "<tbody>";
		if ($row2!=0)
		{
			while ($ligne=mysql_fetch_assoc($result2))
			{
				for ($i=0;$i<count($tab_cht);$i++){
					if ($ligne["cht_id"]==$tab_cht[$i]){$checked="checked";break;}else{$checked="";}
				}
				if ($ligne["cht_idanneeprog"]!=0){$finance = $ligne["programme"]." ".$ligne["anneeprog_annee"];}else{$finance = "<i class='fa fa-exclamation-triangle text-danger'></i>";}
				if ($ligne["tot_ttc"]!=0){$apd = number_format($ligne["tot_ttc"], 2, ',', ' ');}else{$apd = "<i class='fa fa-exclamation-triangle text-danger'></i>";}
				if ($ligne["cht_idanneeprog"]!=0 && $ligne["tot_ttc"]!=0){$td1 = "<input type='checkbox' class='chkchtdemandeos' data-id='".$ligne["cht_id"]."' ".$checked.">";$error="";}else{$td1="";$error='class="bg-danger"';}
				echo "<tr ".$error."><td align='center' width='5%'>".$td1."</td><td>".$ligne["cht_numero"]."</td><td class='text-center' width='20%'>".$apd."</td><td class='text-center' width='20%'>".$finance."</td></tr>";
			}
			$disabled = "";
		}else{
			echo "<tr><td colspan='4' align='center'><b>Aucune prestation</b></td></tr>";
			$disabled = "disabled";
		}
		echo "</tbody>";
		echo "</table>";
	}else{
		echo "<div class='alert alert-danger text-center'><strong><i class='fa fa-exclamation-triangle' aria-hidden='true'></i></strong> l'op�ration n'existe pas!!</div>";
	}
}
function lst_chantieropedembc($idope,$tabcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT ope_id FROM operations WHERE ope_numero='".$idope."'";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne=mysql_fetch_assoc($result2))
		{
			$idope= $ligne["ope_id"];
		}
		$req2="SELECT chantiers.cht_numero, chantiers.cht_id, chantiers.cht_idanneeprog, programme.prog_libelle as programme, annee_prog.anneeprog_annee, apd.tot_ttc FROM chantiers JOIN apd ON apd.id_cht=chantiers.cht_id LEFT JOIN annee_prog ON annee_prog.anneeprog_id=chantiers.cht_idanneeprog LEFT JOIN programme ON programme.prog_id=annee_prog.anneeprog_idprog WHERE NOT EXISTS (select * from demandebc_chantiers where chantiers.cht_id=demandebc_chantiers.id_cht) AND chantiers.cht_idope='".$idope."'";
		$result2=mysql_query($req2,$link);
		$row2=mysql_num_rows($result2);
		$delimiter = ",";
		$tab_cht = explode($delimiter,$tabcht);
		echo "<table class='table table-responsive table-bordered table-sm'>";
		echo "<thead><tr><th class='text-center'></th><th>Prestations</th><th width='20%'>APD (TTC)</th><th width='20%'>Financement</th></tr></thead>";
		echo "<tbody>";
		if ($row2!=0)
		{
			while ($ligne=mysql_fetch_assoc($result2))
			{
				for ($i=0;$i<count($tab_cht);$i++){
					if ($ligne["cht_id"]==$tab_cht[$i]){$checked="checked";break;}else{$checked="";}
				}
				if ($ligne["cht_idanneeprog"]!=0){$finance = $ligne["programme"]." ".$ligne["anneeprog_annee"];}else{$finance = "<i class='fa fa-exclamation-triangle text-danger'></i>";}
				if ($ligne["tot_ttc"]!=0){$apd = number_format($ligne["tot_ttc"], 2, ',', ' ');}else{$apd = "<i class='fa fa-exclamation-triangle text-danger'></i>";}
				if ($ligne["cht_idanneeprog"]!=0 && $ligne["tot_ttc"]!=0){$td1 = "<input type='checkbox' class='chkchtdemandebc' data-id='".$ligne["cht_id"]."' ".$checked.">";$error="";}else{$td1="";$error='class="bg-danger"';}
				echo "<tr ".$error."><td align='center' width='5%'>".$td1."</td><td>".$ligne["cht_numero"]."</td><td class='text-center' width='20%'>".$apd."</td><td class='text-center' width='20%'>".$finance."</td></tr>";
			}
			$disabled = "";
		}else{
			echo "<tr><td colspan='4' align='center'><b>Aucune prestation</b></td></tr>";
			$disabled = "disabled";
		}
		echo "</tbody>";
		echo "</table>";
	}else{
		echo "<div class='alert alert-danger text-center'><strong><i class='fa fa-exclamation-triangle' aria-hidden='true'></i></strong> l'op�ration n'existe pas!!</div>";
	}
}
function lst_chantieropedemostemp($tabcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT chantiers.cht_numero, chantiers.cht_id, chantiers.cht_idanneeprog, programme.prog_libelle as programme, annee_prog.anneeprog_annee, apd.tot_ttc FROM chantiers JOIN apd ON apd.id_cht=chantiers.cht_id LEFT JOIN annee_prog ON annee_prog.anneeprog_id=chantiers.cht_idanneeprog LEFT JOIN programme ON programme.prog_id=annee_prog.anneeprog_idprog WHERE chantiers.cht_id in (".$tabcht.")";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center'></th><th>Prestations</th><th>APD</th><th>Financement</th></tr></thead>";
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne=mysql_fetch_assoc($result2))
		{
			echo "<tr><td align='center' width='5%'></td><td>".$ligne["cht_numero"]."</td><td class='text-center' width='20%'>".number_format($ligne["tot_ttc"], 2, ',', ' ')."</td><td class='text-center' width='20%'>".$ligne["programme"]." ".$ligne["anneeprog_annee"]."</td></tr>";
		}
		$disabled = "";
	}else{
		echo "<tr><td colspan='4' align='center'><b>Aucune prestation</b></td></tr>";
		$disabled = "disabled";
	}
	echo "</tbody>";
	echo "</table>";
}
function lst_chantieropedembctemp($tabcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT chantiers.cht_numero, chantiers.cht_id, chantiers.cht_idanneeprog, programme.prog_libelle as programme, annee_prog.anneeprog_annee, apd.tot_ttc FROM chantiers JOIN apd ON apd.id_cht=chantiers.cht_id LEFT JOIN annee_prog ON annee_prog.anneeprog_id=chantiers.cht_idanneeprog LEFT JOIN programme ON programme.prog_id=annee_prog.anneeprog_idprog WHERE chantiers.cht_id in (".$tabcht.")";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center'></th><th>Prestations</th><th>APD</th><th>Financement</th></tr></thead>";
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne=mysql_fetch_assoc($result2))
		{
			echo "<tr><td align='center' width='5%'></td><td>".$ligne["cht_numero"]."</td><td class='text-center' width='20%'>".number_format($ligne["tot_ttc"], 2, ',', ' ')."</td><td class='text-center' width='20%'>".$ligne["programme"]." ".$ligne["anneeprog_annee"]."</td></tr>";
		}
		$disabled = "";
	}else{
		echo "<tr><td colspan='4' align='center'>Aucune prestation</td></tr>";
		$disabled = "disabled";
	}
	echo "</tbody>";
	echo "</table>";
}
function lst_typeos(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT lex_libelle, lex_id FROM lexique WHERE lex_codelexique='TYPE_OS' ORDER BY lex_libelle ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir un type</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["lex_id"].'">'.$ligne2["lex_libelle"].'</option>';
		}
	}
}
function lst_collectivite(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT coll_nom, coll_codeinsee, coll_type FROM collectivites WHERE coll_type in ('COMMUNE','EPCI') ORDER BY coll_type,coll_nom ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une collectivit�</option>';
	if ($row2!=0)
	{
		$c=0;
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			if ($c==0){echo '<option class="lstcomoptionfilter" disabled><b>-------- '.$ligne2["coll_type"].' --------</b></option>';$type_coll=$ligne2["coll_type"];}
			if ($type_coll!=$ligne2["coll_type"]){echo '<option class="lstcomoptionfilter" disabled><b>-------- '.$ligne2["coll_type"].' --------</b></option>';$type_coll=$ligne2["coll_type"];}
			echo '<option value="'.$ligne2["coll_codeinsee"].'">'.$ligne2["coll_nom"].'</option>';
			$c++;
		}
	}
}
function lst_entreprise(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT ent_nom, ent_id FROM entreprises ORDER BY ent_nom ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une entrepise</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["ent_id"].'">'.$ligne2["ent_nom"].'</option>';
		}
	}
}
function lst_categorie(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT cat_libelle, cat_id FROM categorie ORDER BY cat_libelle ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une cat�gorie</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["cat_id"].'">'.$ligne2["cat_libelle"].'</option>';
		}
	}
}
function lst_nature(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT nat_libelle, nat_id FROM nature ORDER BY nat_libelle ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une nature</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["nat_id"].'">'.$ligne2["nat_libelle"].'</option>';
		}
	}
}

function lst_operation($page,$loginca,$chp,$val,$criteres,$tabcrit){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	
	/* Recherche des droits de l'utilisateur */
	$retour_droit=mysql_query('SELECT user.user_droit FROM user where user.user_login="'.$loginca.'"');
	$donnees_droit=mysql_fetch_assoc($retour_droit);
	$droit=$donnees_droit['user_droit'];

	$messagesParPage=10;
	if ($criteres!=""){
		//$retour_total=mysql_query('SELECT COUNT(*) AS total FROM operations where '.$chp.' like "%'.$val.'%"');
		$retour_total=mysql_query('SELECT COUNT(*) AS total FROM operations where '.stripslashes(utf8_decode($criteres)));
		$donnees_total=mysql_fetch_assoc($retour_total);
	}else{
		if ($droit!="USER"){
			$retour_total=mysql_query('SELECT COUNT(*) AS total FROM operations');
			$donnees_total=mysql_fetch_assoc($retour_total);
		}else{
			$retour_total=mysql_query('SELECT COUNT(*) AS total FROM operations where operations.ope_chargeaffaire="'.$loginca.'"');
			$donnees_total=mysql_fetch_assoc($retour_total);
		}
	}
	$total=$donnees_total['total'];
	$nombreDePages=ceil($total/$messagesParPage);
	if(isset($page))
	{
		$pageActuelle=intval($page);

		if($pageActuelle>$nombreDePages)
		{
			$pageActuelle=$nombreDePages;
		}
	}else{
		$pageActuelle=1;
	}

	$premiereEntree=($pageActuelle-1)*$messagesParPage;

	if ($criteres!=""){
		$delimiter = ",";
		$tab_crit = explode($delimiter,$tabcrit);
		for ($i=0;$i<count($tab_crit);$i++){
			$tabtemp = explode(":",$tab_crit[$i]);
			switch($tabtemp[0]){
				case "ope_numero":
					$numero = $tabtemp[1];
					break;
				case "ope_codeinsee":
					$codeinsee = $tabtemp[1];
					break;
				case "ope_categorie":
					$categorie = utf8_decode($tabtemp[1]);
					break;
			}
		}
		//$req2="SELECT operations.ope_numero, operations.ope_libelle, operations.ope_categorie, operations.ope_id, collectivites.coll_nom, user.user_initial FROM operations LEFT JOIN collectivites ON collectivites.coll_codeinsee=operations.ope_codeinsee LEFT JOIN user ON user.user_login=operations.ope_chargeaffaire WHERE ".$chp." LIKE '%".$val."%' ORDER BY operations.ope_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
		$req2="SELECT operations.ope_numero, operations.ope_libelle, operations.ope_categorie, operations.ope_id, collectivites.coll_nom, user.user_initial FROM operations LEFT JOIN collectivites ON collectivites.coll_codeinsee=operations.ope_codeinsee LEFT JOIN user ON user.user_login=operations.ope_chargeaffaire WHERE ".stripslashes(utf8_decode($criteres))." ORDER BY operations.ope_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
	}else{
		if ($droit!="USER"){		
			$req2="SELECT operations.ope_numero, operations.ope_libelle, operations.ope_categorie, operations.ope_id, collectivites.coll_nom, user.user_initial FROM operations LEFT JOIN collectivites ON collectivites.coll_codeinsee=operations.ope_codeinsee LEFT JOIN user ON user.user_login=operations.ope_chargeaffaire ORDER BY operations.ope_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
		}else{
			$req2="SELECT operations.ope_numero, operations.ope_libelle, operations.ope_categorie, operations.ope_id, collectivites.coll_nom, user.user_initial, operations.ope_delegation FROM operations LEFT JOIN collectivites ON collectivites.coll_codeinsee=operations.ope_codeinsee LEFT JOIN user ON user.user_login=operations.ope_chargeaffaire WHERE operations.ope_chargeaffaire='".$loginca."' OR operations.ope_delegation='".$loginca."' ORDER BY operations.ope_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
		}
	}
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	$compteope = 1;
	if ($pageActuelle>1){
		echo "<div><div data-tooltip='Pr�c�dent' class='forward btnpager'><i class='fa fa-caret-left' aria-hidden='true'></i></div>";
	}
	echo "<div class='boxpager'>".$pageActuelle." / ".$nombreDePages."</div>";
	if ($pageActuelle<$nombreDePages){
		echo "<div data-tooltip='Suivant' class='next btnpager'><i class='fa fa-caret-right' aria-hidden='true'></i></div></div>";
	}
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	if ($droit!="USER"){
		echo "<thead><tr><th width='10%'></th><th class='text-center' width='5%'>CA</th><th class='text-center' width='10%'>Num�ro</th><th class='text-center' width='10%'>Cat�gorie</th><th class='text-center'>Libell�</th><th class='text-center' width='10%'>Commune</th><th class='text-center' width='40%'>Avancement</th></tr></thead>";
		echo "<tbody><tr><td></td><td></td><td><input data-chp='ope_numero' class='form-control input-sm text-center chpope_filter' value='".$numero."'></td>";
		echo "<td><select data-chp='ope_categorie' class='form-control input-sm chpope_filter'><option value=''>-- Cat�gories --</option>";
		$reqcat="SELECT cat_libelle, cat_id FROM categorie";
		$resultcat=mysql_query($reqcat,$link);
		while ($lignecat=mysql_fetch_assoc($resultcat))
		{
			if ($categorie==$lignecat["cat_libelle"]){$selected="selected";}else{$selected="";}
			echo "<option value='".$lignecat["cat_libelle"]."' ".$selected.">".$lignecat["cat_libelle"]."</option>";
		}
		echo "</select></td>";
		echo "<td></td>";
		echo "<td><select data-chp='ope_codeinsee' class='form-control input-sm chpope_filter'><option value=''>-- Collectivit�s --</option>";
		$reqcat="SELECT coll_nom, coll_codeinsee FROM collectivites WHERE coll_type='COMMUNE'";
		$resultcat=mysql_query($reqcat,$link);
		while ($lignecat=mysql_fetch_assoc($resultcat))
		{
			if ($codeinsee==$lignecat["coll_codeinsee"]){$selected="selected";}else{$selected="";}
			echo "<option value='".$lignecat["coll_codeinsee"]."' ".$selected.">".$lignecat["coll_nom"]."</option>";
		}
		echo "</select></td><td></td>";
		echo "</tr></tbody>";
	}else{
		$reqdeleg="SELECT * FROM operations WHERE operations.ope_delegation='".$loginca."'";
		$resultdeleg=mysql_query($reqdeleg,$link);
		$rowdeleg=mysql_num_rows($resultdeleg);
		if ($rowdeleg!=0){
			echo "<thead><tr><th width='10%'></th><th class='text-center' width='5%'><i class='fas fa-handshake' title='D�l�gation'></i></th><th class='text-center' width='10%'>Num�ro</th><th class='text-center' width='10%'>Cat�gorie</th><th class='text-center'>Libell�</th><th class='text-center' width='10%'>Commune</th><th class='text-center' width='40%'>Avancement</th></tr></thead>";
			echo "<tbody><tr><td></td><td></td><td><input data-chp='ope_numero' class='form-control input-sm text-center chpope_filter' value='".$numero."'></td>";
			echo "<td><select data-chp='ope_categorie' class='form-control input-sm chpope_filter'><option value=''>-- Cat�gories --</option>";
			$reqcat="SELECT cat_libelle, cat_id FROM categorie";
			$resultcat=mysql_query($reqcat,$link);
			while ($lignecat=mysql_fetch_assoc($resultcat))
			{
				if ($categorie==$lignecat["cat_libelle"]){$selected="selected";}else{$selected="";}
				echo "<option value='".$lignecat["cat_libelle"]."' ".$selected.">".$lignecat["cat_libelle"]."</option>";
			}
			echo "</select></td>";
			echo "<td></td>";
			echo "<td><select data-chp='ope_codeinsee' class='form-control input-sm chpope_filter'><option value=''>-- Collectivit�s --</option>";
			$reqcat="SELECT coll_nom, coll_codeinsee FROM collectivites WHERE coll_type='COMMUNE'";
			$resultcat=mysql_query($reqcat,$link);
			while ($lignecat=mysql_fetch_assoc($resultcat))
			{
				if ($codeinsee==$lignecat["coll_codeinsee"]){$selected="selected";}else{$selected="";}
				echo "<option value='".$lignecat["coll_codeinsee"]."' ".$selected.">".$lignecat["coll_nom"]."</option>";
			}
			echo "</select></td><td></td>";
			echo "</tr></tbody>";
		}else{
			echo "<thead><tr><th width='10%'></th><th class='text-center' width='10%'>Num�ro</th><th class='text-center' width='10%'>Cat�gorie</th><th class='text-center'>Libell�</th><th class='text-center' width='10%'>Commune</th><th class='text-center' width='40%'>Avancement</th></tr></thead>";
			//echo "<tbody><tr><td></td><td><input class='form-control input-sm'></td><td></td><td><select class='form-control input-sm'></select></td><td></td></tr></tbody>";
			echo "<tbody><tr><td></td><td><input data-chp='ope_numero' class='form-control input-sm text-center chpope_filter' value='".$numero."'></td>";
			echo "<td><select data-chp='ope_categorie' class='form-control input-sm chpope_filter'><option value=''>-- Cat�gories --</option>";
			$reqcat="SELECT cat_libelle, cat_id FROM categorie";
			$resultcat=mysql_query($reqcat,$link);
			while ($lignecat=mysql_fetch_assoc($resultcat))
			{
				if ($categorie==$lignecat["cat_libelle"]){$selected="selected";}else{$selected="";}
				echo "<option value='".$lignecat["cat_libelle"]."' ".$selected.">".$lignecat["cat_libelle"]."</option>";
			}
			echo "</select></td>";
			echo "<td></td>";
			echo "<td><select data-chp='ope_codeinsee' class='form-control input-sm chpope_filter'><option value=''>-- Collectivit�s --</option>";
			$reqcat="SELECT coll_nom, coll_codeinsee FROM collectivites WHERE coll_type='COMMUNE'";
			$resultcat=mysql_query($reqcat,$link);
			while ($lignecat=mysql_fetch_assoc($resultcat))
			{
				if ($codeinsee==$lignecat["coll_codeinsee"]){$selected="selected";}else{$selected="";}
				echo "<option value='".$lignecat["coll_codeinsee"]."' ".$selected.">".$lignecat["coll_nom"]."</option>";
			}
			echo "</select></td><td></td>";
			echo "</tr></tbody>";
		}
	}
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			/* Recherche Bon de commande rattach� pour activation des boutons */
			$reqchtbc="SELECT * FROM demandebc_chantiers JOIN chantiers ON chantiers.cht_id=demandebc_chantiers.id_cht WHERE chantiers.cht_idope=".$ligne2["ope_id"];
			$resultchtbc=mysql_query($reqchtbc,$link);
			$rowchtbc=mysql_num_rows($resultchtbc);
			if ($rowchtbc!=0){$buttons = "disabled";}else{$buttons = "";}
			echo "<tr><td align='center' width='10%'><button data-tooltip='Visualiser' data-id='".$ligne2["ope_id"]."' class='btn btn-sm btn-default visuope'><i class='fa fa-eye'></i></button> <button data-tooltip='Supprimer' data-id='".$ligne2["ope_id"]."' class='btn btn-sm btn-default delope' ".$buttons."><i class='fa fa-trash'></i></button></td>";
			if ($droit!="USER"){
				echo "<td align='center' width='5%'><i class='fa fa-user'></i><br>".$ligne2["user_initial"]."</td>";
			}
			if ($rowdeleg!=0){ 
				if ($ligne2["ope_delegation"]!=""){
					echo "<td align='center' width='5%'><i class='fa fa-user'></i><br>".$ligne2["user_initial"]."</td>";
				}else{
					echo "<td align='center' width='5%'></td>";
				}
			}
			echo "<td align='center' width='10%'>".$ligne2["ope_numero"]."</td><td align='center' width='10%'>".$ligne2["ope_categorie"]."</td><td data-position='bottom' data-tooltip='".$ligne2["ope_libelle"]."'>".substr($ligne2["ope_libelle"], 0, 49)."...</td><td width='10%'>".$ligne2["coll_nom"]."</td><td width='40%'>";

					$tab_jalon = array(4,7,13,25,27,55);
					$c = 0;
					$tab_jalon_success = array();
					foreach ($tab_jalon as $val){
						$reqjal="SELECT opejalon_datejalon FROM ope_jalon WHERE opejalon_idjalon=".$val." AND opejalon_idope=".$ligne2["ope_id"];
						$resultjal=mysql_query($reqjal,$link);
						$rowjal=mysql_num_rows($resultjal);
						$date_jal="";
						if ($rowjal!=0)
						{
							while ($lignejal=mysql_fetch_assoc($resultjal))
							{
								if ($lignejal["opejalon_datejalon"]=='0000-00-00'){$tab_jalon_success[$c]='disabledstep bg-default';}else{$tab_jalon_success[$c]='visitedstep bg-success';}
								$c++;
							}

						}
					}

					echo '<div class="col-sm-12">';
					echo '<div class="step '.$tab_jalon_success[0].' text-center"><font size="1">DT</font></div>';
					echo '<div class="step '.$tab_jalon_success[1].' text-center"><font size="1">BCE</font></div>';
					echo '<div class="step '.$tab_jalon_success[2].' text-center"><font size="1">PPP</font></div>';
					echo '<div class="step '.$tab_jalon_success[3].' text-center"><font size="1">AUT-2</font></div>';
					echo '<div class="step '.$tab_jalon_success[4].' text-center"><font size="1">DEVIS</font></div>';
					echo '<div class="step '.$tab_jalon_success[5].' text-center"><font size="1">BCT</font></div>';
					echo '<div class="step '.$tab_jalon_success[5].' text-center"><font size="1">RCPT</font></div>';
					echo '<div class="step '.$tab_jalon_success[5].' text-center"><font size="1">GPA</font></div>';
					echo '</div>';


					echo "</td></tr>";
		}
	}else{
		echo "<tr><td colspan='7' align='center'><b>Aucune op�ration</b></td></tr>";
	}
	echo "</tbody>";
	echo "</table>";
}

function detail_operation($id){
	require("./compte.php");
	require("./print.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT ope_numero, ope_libelle, ope_categorie, ope_id FROM operations WHERE ope_id=".$id;
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{

			$reqcat="SELECT cat_libelle, cat_codenumero, cat_AUT FROM categorie WHERE cat_libelle='".$ligne2["ope_categorie"]."'";
			$resultcat=mysql_query($reqcat,$link);
			while ($lignecat=mysql_fetch_assoc($resultcat))
			{
				$aut_cat = $lignecat["cat_AUT"];
				$code_cat = $lignecat["cat_codenumero"];
			}
			$panel=1;
			echo "<div class='col-md-12'>";
			echo "<div class='portlet'><div class='portlet-title'><i class='fa fa-bolt' aria-hidden='true'></i> <b>OPERATION</b> ".$ligne2["ope_categorie"]." - ".$ligne2["ope_numero"]." - ".$ligne2["ope_libelle"]."</div><div class='portlet-content' id='detoperation'>";
			echo "<div class='row'>";

			echo '<div class="col-sm-12">';
			echo "<button id='back_lstoperations' class='btn btn-default btn-sm' data-idope='".$ligne2["ope_id"]."'><i class='fa fa-reply'></i> Op�rations</button>";
			echo '</div><br><br>';

			echo '<div class="col-sm-12">';
			echo '<div class="panel-group" id="accordion">';
			/* G�n�ralit�s */
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h2 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-file" aria-hidden="true"></i> G�n�ralit�s</a>';
			echo '</h2>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detailgene_operation">';
				detailgene_operation($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Jalonnements */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-calendar-alt" aria-hidden="true"></i> Jalonnements</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detjalon">';
				detail_jalon($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Prestations */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			$rowscht = nbrchantier_operation($id);
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-copy" aria-hidden="true"></i> Prestations <span class="badge">'.$rowscht.'</span></a>';
			echo '</h4>';
			echo "";
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body">';
				lstchantier_operation($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Programmation */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="far fa-chart-bar"></i> Programmation (APS)</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detprogrammation">';
				detail_programmation($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Coordination */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="far fa-handshake" aria-hidden="true"></i> Coordination / Crit�re</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detcoordination">';
				detail_coordination($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			if($aut_cat==1){
			/* Autorisation */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-unlock" aria-hidden="true"></i> Autorisation</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detautorisation">';
				detail_autorisation($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			}
			if($code_cat=="RAC"){
			/* PCT */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-folder" aria-hidden="true"></i> PCT</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detpct">';
				detail_pctoperation($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* PCT */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-users" aria-hidden="true"></i> B�n�ficiaires</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detbeneficiaire">';
				detail_opebeneficiaire($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			}
			/* FACE */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-edit" aria-hidden="true"></i> FACE</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body">';

			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* VRG */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-edit" aria-hidden="true"></i> VRG</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body">';

			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Bon de commande */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Commandes</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detbcoperation">';
				detailbc_operation($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Etats */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="far fa-file-alt" aria-hidden="true"></i> Etats</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detdocoperation">';
				lstreportope($id);
				lst_print_contextuel("Travaux/OPERATION",$id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Documents */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fas fa-archive" aria-hidden="true"></i> Documents</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detgedoperation">';
				//lstreportope($id);
				//lst_print_contextuel("Travaux/OPERATION",$id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Historique */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-history" aria-hidden="true"></i> Historique</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="lsthistory">';
				lst_history($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			
			echo '</div>';
			echo '</div>';
		}
	}
}

function lstreportope($id){
	echo '<div class="col-sm-8" style="margin-bottom:20px;">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-file"></i> Fichiers</div>';
	echo '<div class="panel-body" id="detailope_document">';
		echo "<table class='table table-responsive table-bordered table-striped'>";
		echo "<thead><tr><th></th><th>Nom du fichier</th><th>Action(s)</th></tr></thead>";
		echo "<tbody>";
		$root = scandir("../report/OPE/");
		foreach($root as $value)
		{
			if($value === '.' || $value === '..') {continue;}
			echo '<tr><td width="5%" class="text-center"><i class="fa fa-file"></i></td><td>'.$value.'</td><td width="10%" class="text-center"><button class="btn btn-sm btn-default visurpt" data-type="OPE" data-file="'.$value.'" data-id="'.$id.'"><i class="fa fa-eye"></i></button></td></tr>';
		}
		echo "</tbody>";
		echo "</table>";
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-4">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-upload"></i> Nouveaux documents</div>';
	echo '<div class="panel-body">';

	echo '</div>';
	echo '</div>';
	echo '</div>';
}

function lst_history($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqhist="SELECT hist_dateevent, hist_libelleevent, hist_event, user_nom, user_prenom FROM historique LEFT JOIN user ON historique.hist_user=user.user_login WHERE hist_idocc=".$idope." AND hist_natureevent='TVX' ORDER BY hist_dateevent DESC";
	$resulthist=mysql_query($reqhist,$link);
	$rowhist=mysql_num_rows($resulthist);
	if ($rowhist!=0)
	{
		echo "<div class='col-sm-12'>";
		while ($lignehist=mysql_fetch_assoc($resulthist))
		{
			switch($lignehist["hist_event"]){
				case "create":
					$icon = "magic";
					break;
				case "delete":
					$icon = "trash";
					break;
			}
			//echo "<div class='row'><i class='fa fa-".$icon."'></i> ".$lignehist["hist_libelleevent"]." le ".strftime("%d/%m/%Y %T",strtotime($lignehist["hist_dateevent"]))." par ".$lignehist["hist_user"]."</div>";
			echo '<div class="media"><div class="media-left"><a href="#"><img src="../images/cogs_hist.png" class="media-object img-rounded" alt="Sample Image"></a></div>';
			echo '<div class="media-body"><h5 class="media-heading"><b>'.$lignehist["user_nom"].' '.$lignehist["user_prenom"].' </b><small><i>Effectu� le '.strftime("%d/%m/%Y %T",strtotime($lignehist["hist_dateevent"])).'</i></small></h5>';
			echo '<p><i class="fa fa-'.$icon.'"></i> '.$lignehist["hist_libelleevent"].'</p>';
			echo '</div></div>';
		}
		echo "</div>";
	}
}

function detailgene_operation($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM operations WHERE ope_id=".$idope;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$datecreate = strftime("%d/%m/%Y",strtotime($ligne["ope_datecreate"]));
		$datedemande = strftime("%d/%m/%Y",strtotime($ligne["ope_datedemande"]));
		$entite = $ligne["ope_entite"];
		$charge_affaire = $ligne["ope_chargeaffaire"];
		$epci = $ligne["ope_codeepci"];
		$insee = $ligne["ope_codeinsee"];
		$insee2 = $ligne["ope_codeinsee2"];
		$numero = $ligne["ope_numero"];
		$libelle = $ligne["ope_libelle"];
		$lieudit = $ligne["ope_lieudit"];
		$categorie = $ligne["ope_categorie"];
		$moa = $ligne["ope_moa"];
		$moe = $ligne["ope_moe"];
		$nature = $ligne["ope_nature"];
		$refconcess = $ligne["ope_refconcess"];
		$delegation = $ligne["ope_delegation"];
	}
	if ($charge_affaire==$_COOKIE["login"] || $delegation==$_COOKIE["login"]){$disabled="";}else{$disabled="disabled";}
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">G�n�ralit�s</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_datecreate">Date cr�ation:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="ope_datecreate" name="ope_datecreate" value="'.$datecreate.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_datedemande">Date demande:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_datedemande" id="ope_datedemande" name="ope_datedemande" value="'.$datedemande.'" '.$disabled.'>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_ca">Charg� d\'affaire:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_chargeaffaire" id="ope_ca" name="ope_ca" disabled>';
		echo '<option value="">S�lectionnez un charg� d\'affaire</option>';
		$req="SELECT user_login,user_nom,user_prenom FROM user";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["user_login"]==$charge_affaire){$selected = "selected";}else{$selected = "";}
			echo '<option value="'.$ligne["user_login"].'" '.$selected.'>'.$ligne["user_nom"].' '.$ligne["user_prenom"].'</option>';
		}
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_moa">MOA:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="ope_moa" name="ope_moa" value="'.$moa.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_moe">MOE:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="ope_moe" name="ope_moe" value="'.$moe.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_categorie">Cat�gorie:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="ope_categorie" name="ope_categorie" value="'.$categorie.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_nature">Nature:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_nature" id="ope_nature" name="ope_nature" '.$disabled.'>';
		echo '<option value="0">Choisir une nature de travaux</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='NAT_OPE'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_libelle"]==$nature){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_libelle"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_refsehv">R�f�rence SEHV:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="ope_refsehv" name="ope_refsehv" value="'.$numero.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_refconcess">R�f�rence Concessionnaire:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_refconcess" id="ope_refconcess" name="ope_refconcess" value="'.$refconcess.'" '.$disabled.'>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_libelle">Libell�:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_libelle" id="ope_libelle" name="ope_libelle" value="'.$libelle.'" '.$disabled.'>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_lieudit">Lieu-dit:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_lieudit" id="ope_lieudit" name="ope_lieudit" value="'.$lieudit.'" '.$disabled.'>';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-map-marker-alt"></i> Localisation</div>';
	echo '<div class="panel-body" id="detailope_localisation">';
		detailope_localisation($idope,$insee,$insee2,$entite,$epci,$disabled);
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function detailope_localisation($idope,$insee,$insee2,$entite,$epci,$disabled){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="ope_entite">Origine demande:</label>';
	echo '<div class="col-sm-7">';
	echo '<select class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_entite" id="ope_entite" name="ope_entite" '.$disabled.'>';
	echo '<option value="-1">S�lectionnez une entit�</option>';
	if ($entite=="EPCI"){echo '<option value="EPCI" selected>EPCI</option>';}else{echo '<option value="EPCI">EPCI</option>';}
	if ($entite=="COMMUNE"){echo '<option value="COMMUNE" selected>Commune</option>';}else{echo '<option value="COMMUNE">Commune</option>';}
	echo '</select>';
	echo '</div>';
	echo '</div>';
	switch ($entite){
		case "COMMUNE":
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="ope_commune">Commune:</label>';
			echo '<div class="col-sm-7">';
			echo '<select class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_codeinsee" id="ope_commune" name="ope_commune" '.$disabled.'>';
			echo '<option value="-1">S�lectionnez une commune</option>';
			if ($epci!=0){
				$req="SELECT * FROM collectivites WHERE coll_idpere=".$epci;
			}else{
				$req="SELECT * FROM collectivites WHERE coll_type='COMMUNE'";
			}
			$result=mysql_query($req,$link);
			$code_epci="";
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["coll_codeinsee"]==$insee){$selected="selected";$code_epci=$ligne["coll_idpere"];}else{$selected="";}
				echo '<option value="'.$ligne["coll_codeinsee"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="ope_epci">EPCI:</label>';
			echo '<div class="col-sm-7">';
			if ($disabled==""){if ($entite!="EPCI"){$disabled="disabled";}else{$disabled="";}}
			echo '<select class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_codeepci" id="ope_epci" name="ope_epci" '.$disabled.'>';
			echo '<option value="-1">S�lectionnez un EPCI</option>';
			$req="SELECT * FROM collectivites WHERE coll_type='EPCI'";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($epci!=0){
					if ($ligne["coll_id"]==$epci){$selected="selected";}else{$selected="";}
				}else{
					if ($ligne["coll_id"]==$code_epci){$selected="selected";}else{$selected="";}
				}
				echo '<option value="'.$ligne["coll_id"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
		break;
		case "EPCI":
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="ope_epci">EPCI:</label>';
			echo '<div class="col-sm-7">';
			if ($disabled==""){if ($entite!="EPCI"){$disabled="disabled";}else{$disabled="";}}
			echo '<select class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_codeepci" id="ope_epci" name="ope_epci" '.$disabled.'>';
			echo '<option value="-1">S�lectionnez un EPCI</option>';
			$req="SELECT * FROM collectivites WHERE coll_type='EPCI'";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($epci!=0){
					if ($ligne["coll_id"]==$epci){$selected="selected";}else{$selected="";}
				}else{
					if ($ligne["coll_id"]==$code_epci){$selected="selected";}else{$selected="";}
				}
				echo '<option value="'.$ligne["coll_id"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="ope_commune">Commune:</label>';
			echo '<div class="col-sm-7">';
			echo '<select class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_codeinsee" id="ope_commune" name="ope_commune" '.$disabled.'>';
			echo '<option value="-1">S�lectionnez une commune</option>';
			if ($epci!=0){
				$req="SELECT * FROM collectivites WHERE coll_idpere=".$epci;
			}else{
				$req="SELECT * FROM collectivites WHERE coll_type='COMMUNE'";
			}
			$result=mysql_query($req,$link);
			$code_epci="";
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["coll_codeinsee"]==$insee){$selected="selected";$code_epci=$ligne["coll_idpere"];}else{$selected="";}
				echo '<option value="'.$ligne["coll_codeinsee"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
		break;
	}
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="ope_commune2">Commune 2:</label>';
	echo '<div class="col-sm-7">';
	echo '<select class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_codeinsee2" id="ope_commune2" name="ope_commune2" '.$disabled.'>';
	echo '<option value="-1">S�lectionnez une commune</option>';
	$req="SELECT * FROM collectivites WHERE coll_type='COMMUNE'";
	$result=mysql_query($req,$link);
	$code_epci="";
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["coll_codeinsee"]==$insee2){$selected="selected";$code_epci=$ligne["coll_idpere"];}else{$selected="";}
		echo '<option value="'.$ligne["coll_codeinsee"].'" '.$selected.'>'.$ligne["coll_nom"].'</option>';
	}
	echo '</select>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="ope_ste">STE:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="ope_ste" name="ope_ste" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="ope_insee">Code insee:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="ope_insee" name="ope_insee" value="'.$insee.'" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="ope_cp">Code postal:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="ope_cp" name="ope_cp" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="ope_statcoll">Statut collectivit� (U/R):</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="ope_statcoll" name="ope_statcoll" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="ope_eligface">Eligibilit� FACE:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="ope_eligface" name="ope_eligface" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="ope_adhecl">Adh�rente ECL:</label>';
	echo '<div class="col-sm-2">';
	echo '<input type="text" class="form-control input-sm" id="ope_adhecl" name="ope_adhecl" disabled>';
	echo '</div>';
	echo '<label class="control-label col-sm-3" for="ope_adhesp">Adh�rente ESP:</label>';
	echo '<div class="col-sm-2">';
	echo '<input type="text" class="form-control input-sm" id="ope_adhesp" name="ope_adhesp" disabled>';
	echo '</div>';
	echo '</div>';
	echo '</form>';
}

function detailbc_operation($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	/* Affichage Demande de BC en cours */
	echo '<div class="col-sm-12">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">Bons de commande rattach�s � l\'op�ration</div>';
	echo '<div class="panel-body">';
	$req="SELECT DISTINCT(demande_bc.id_bc) FROM demande_bc JOIN demandebc_chantiers ON demandebc_chantiers.id_demandebc=demande_bc.id JOIN chantiers ON chantiers.cht_id=demandebc_chantiers.id_cht WHERE chantiers.cht_idope=".$idope;
	$result=mysql_query($req,$link);
	$row=mysql_num_rows($result);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='5%'></th><th width='10%'>num�ro</th><th width='8%'>Date</th><th>Prestations rattach�es</th><th width='8%'>Date Fin BC</th><th width='12%'>Montant<br>engag�</th><th width='12%'>Montant<br>pay�</th><th>Avancement<br>paiement</th></tr></thead>";
	echo "<tbody>";
	if ($row!=0)
	{
		while ($ligne=mysql_fetch_assoc($result))
		{
			$reqbc="SELECT boncommande.bc_id,boncommande.bc_numero,boncommande.bc_datebc,boncommande.bc_datefinbc,boncommande.bc_montantengmodifht,boncommande.bc_montantengmodifttc FROM boncommande WHERE boncommande.bc_id=".$ligne["id_bc"];
			$resultbc=mysql_query($reqbc,$link);
			while ($lignebc=mysql_fetch_assoc($resultbc))
			{
				if ($lignebc["bc_datefinbc"]=="0000-00-00"){$datefinbc="";}else{$datefinbc=strftime("%d/%m/%Y",strtotime($lignebc["bc_datefinbc"]));}
				echo "<tr><td align='center' width='5%'><button data-tooltip='Nouvel OS' class='btn btn-default btn-sm add_demandeos' data-idbc='".$lignebc["bc_id"]."'><span class='fa-layers fa-fw fa-lg'><i class='fa fa-edit'></i><i class='fas fa-plus-circle fa-inverse' data-fa-transform='shrink-2 up-3 left-8' style='color:green'></i></span></button></td><td align='center' width='10%'>".$lignebc["bc_numero"]."</td><td align='center' width='8%'>".strftime("%d/%m/%Y",strtotime($lignebc["bc_datebc"]))."</td><td></td><td align='center' width='8%'>".$datefinbc."</td><td align='right' width='12%'>".number_format($lignebc["bc_montantengmodifht"], 2, ',', ' ')." �</td><td align='right' width='12%'>".number_format($lignebc["bc_montantengmodifttc"], 2, ',', ' ')." �</td>";
				echo "<td><div class='progress'><div class='progress-bar progress-bar-success' role='progressbar' style='width: 70%'>70%</div></div></td></tr>";
			}
		}
	}else{
		echo "<tr><td colspan='8' align='center'><b>Aucun bon de commande</b></td></tr>";
	}
	echo "</tbody>";
	echo "</table>";	
	echo '</div>';
	echo '</div>';
	echo '</div>';
}

function add_demandebc($numconvep,$dateconvep,$ident,$delai,$tabcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$numero_demande=0000000000;
	$tabdate=explode("/", $dateconvep);
	$datefinal = $tabdate[2]."-".$tabdate[1]."-".$tabdate[0];
	$req2="SELECT numero FROM demande_bc";
	$result2=mysql_query($req2,$link);
	$row2 = mysql_num_rows($result2);
	if ($row2!=0){
		while ($lignedem=mysql_fetch_assoc($result2))
		{
			$tabnumdem = explode("-",$lignedem["numero"]);
		}
		if ($tabnumdem[0]!=date("y")){$numero_demande=date('y')."-DEMBC-";}else{$numero_demande=$tabnumdem[0]."-DEMBC-";}
		$numero_demande.=$tabnumdem[2]+1;
	}else{
		$numero_demande=date('y')."-DEMBC-1";
	}
	$req2="INSERT INTO demande_bc (date_create,id_statut,id_ent,delai,numero,num_convep,date_convep,login) VALUES (NOW(),'1','".$ident."','".$delai."','".$numero_demande."','".$numconvep."','".$datefinal."','".$_COOKIE["login"]."')";
	$result2=mysql_query($req2,$link);
	$id_demandebc = mysql_insert_id($link);
	$delimiter = ",";
	$tab_cht = explode($delimiter,$tabcht);
	for ($i=0;$i<count($tab_cht);$i++){
		$req="INSERT INTO demandebc_chantiers (id_demandebc,id_cht) VALUES ('".$id_demandebc."','".$tab_cht[$i]."')";
		$result=mysql_query($req,$link);
	}
	//$req2="INSERT INTO historique (hist_idocc,hist_dateevent,hist_libelleevent,hist_user,hist_event,hist_natureevent) VALUES (NOW(),'Cr�ation demande bon de commande','".$_COOKIE["login"]."','create','TVX')";
	//$result2=mysql_query($req2,$link);
	//envoimail();
}
function add_demandeos($idope,$idbc,$montantttc,$typeos,$ident,$delai,$tabcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$numero_demande=0000000000;
	$tabdate=explode("/", $dateconvep);
	$datefinal = $tabdate[2]."-".$tabdate[1]."-".$tabdate[0];
	$req2="SELECT numero FROM demande_os";
	$result2=mysql_query($req2,$link);
	$row2 = mysql_num_rows($result2);
	if ($row2!=0){
		while ($lignedem=mysql_fetch_assoc($result2))
		{
			$tabnumdem = explode("-",$lignedem["numero"]);
		}
		if ($tabnumdem[0]!=date("y")){$numero_demande=date('y')."-DEMOS-";}else{$numero_demande=$tabnumdem[0]."-DEMOS-";}
		$numero_demande.=$tabnumdem[2]+1;
	}else{
		$numero_demande=date('y')."-DEMOS-1";
	}
	$req2="INSERT INTO demande_os (date_create,id_bc,id_statut,id_ent,delai,numero,montant_ttc,type,login) VALUES (NOW(),'".$idbc."','1','".$ident."','".$delai."','".$numero_demande."','".$montantttc."','".$typeos."','".$_COOKIE["login"]."')";
	$result2=mysql_query($req2,$link);
	$id_demandeos = mysql_insert_id($link);
	$delimiter = ",";
	$tab_cht = explode($delimiter,$tabcht);
	for ($i=0;$i<count($tab_cht);$i++){
		$req="INSERT INTO demandeos_chantiers (id_demandeos,id_cht) VALUES ('".$id_demandeos."','".$tab_cht[$i]."')";
		$result=mysql_query($req,$link);
	}
	$req2="INSERT INTO historique (hist_idocc,hist_dateevent,hist_libelleevent,hist_user,hist_event,hist_natureevent) VALUES (NOW(),'Cr�ation demande os','".$_COOKIE["login"]."','create','TVX')";
	$result2=mysql_query($req2,$link);
	$req2="INSERT INTO tasks (date_task,texte,login,statut,traite) VALUES (NOW(),'Une demande d\'OS num�ro ".$numero_demande." est en attente','".$_COOKIE["login"]."','success','0')";
	$result2=mysql_query($req2,$link);
	$id_task = mysql_insert_id($link);
	$req2="UPDATE demande_os SET id_task=".$id_task." WHERE id=".$id_demandeos;
	$result2=mysql_query($req2,$link);	
	//envoimail();
}

function detail_jalon($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<div class="col-sm-12" style="margin-bottom:10px;">';
	echo '<button class="btn btn-default btn-sm" onClick="javascript:window.open(\'mailto:?subject=&body=Coucou\', \'Mail\');event.preventDefault()"><i class="fa fa-envelope"></i></button> ';
	echo '<button data-tooltip="Rendez-vous Zimbra sur 60 jours" class="btn btn-default btn-sm zimbracalendar"><i class="far fa-calendar-alt"></i> Zimbra</button>';
	echo '</div>';
	/* Affichage Jalon */
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '	<div class="panel-heading">Conception</div>';
	echo '	<div class="panel-body">';
	$req="SELECT jal_libelle,jal_id,jal_valid FROM jalon WHERE jal_nature='C' ORDER BY jal_ordre ASC";
	$result=mysql_query($req,$link);
	$row=mysql_num_rows($result);
	if ($row!=0)
	{
		while ($ligne=mysql_fetch_assoc($result))
		{
			/* Test validit� */
			$reqjal="SELECT opejalon_datejalon FROM ope_jalon WHERE opejalon_idope=".$id." AND opejalon_idjalon=".$ligne["jal_valid"];
			$resultjal=mysql_query($reqjal,$link);
			$rowjal=mysql_num_rows($resultjal);
			$disabled='';
			if ($rowjal!=0)
			{
				while ($lignejal=mysql_fetch_assoc($resultjal))
				{
					if ($lignejal["opejalon_datejalon"]=='0000-00-00' && $ligne["jal_valid"]!=0){$disabled='disabled';}else{$disabled="";}
				}

			}
			/* R�cup�ration date de jalon */
			$reqjal="SELECT opejalon_datejalon FROM ope_jalon WHERE opejalon_idope=".$id." AND opejalon_idjalon=".$ligne["jal_id"];
			$resultjal=mysql_query($reqjal,$link);
			$rowjal=mysql_num_rows($resultjal);
			$date_jal="";
			if ($rowjal!=0)
			{
				while ($lignejal=mysql_fetch_assoc($resultjal))
				{
					if ($lignejal["opejalon_datejalon"]=='0000-00-00'){$date_jal='';$btnuploaddisabled="disabled";}else{$date_jal=strftime("%d/%m/%Y",strtotime($lignejal["opejalon_datejalon"]));$btnuploaddisabled="";}
				}
				if ($ligne["jal_libelle"]=="D�p�t DT"){$upload_dt="<div class='col-sm-2'><input id='dtconcept' type='file' style='display:none;'><button data-tooltip='Import emprise DT' data-idope='".$id."' class='btn btn-default btn-sm upload_xmldt' ".$btnuploaddisabled."><i class='fa fa-upload'></i></button></div>";}else{$upload_dt="";}
				echo "<div class='row'><div class='col-sm-6'><label>".$ligne["jal_libelle"]."</label></div><div class='col-sm-4'><input data-idope='".$id."' data-idjal='".$ligne["jal_id"]."' class='form-control input-sm chpjal' ".$disabled." value='".$date_jal."'></div>".$upload_dt."</div>";//<div class='col-sm-1'><i class='fa fa-check text-success'></i></div>
			}
		}

	}
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '	<div class="panel-heading">R�alisation</div>';
	echo '	<div class="panel-body">';
	$req="SELECT jal_libelle,jal_id,jal_valid FROM jalon WHERE jal_nature='R' ORDER BY jal_ordre ASC";
	$result=mysql_query($req,$link);
	$row=mysql_num_rows($result);
	if ($row!=0)
	{
		while ($ligne=mysql_fetch_assoc($result))
		{
			/* Test validit� */
			$reqjal="SELECT opejalon_datejalon FROM ope_jalon WHERE opejalon_idope=".$id." AND opejalon_idjalon=".$ligne["jal_valid"];
			$resultjal=mysql_query($reqjal,$link);
			$rowjal=mysql_num_rows($resultjal);
			$disabled='';
			if ($rowjal!=0)
			{
				while ($lignejal=mysql_fetch_assoc($resultjal))
				{
					if ($lignejal["opejalon_datejalon"]=='0000-00-00' && $ligne["jal_valid"]!=0){$disabled='disabled';}else{$disabled="";}
				}

			}
			/* R�cup�ration date de jalon */
			$reqjal="SELECT opejalon_datejalon FROM ope_jalon WHERE opejalon_idope=".$id." AND opejalon_idjalon=".$ligne["jal_id"];
			$resultjal=mysql_query($reqjal,$link);
			$rowjal=mysql_num_rows($resultjal);
			$date_jal="";
			if ($rowjal!=0)
			{
				while ($lignejal=mysql_fetch_assoc($resultjal))
				{
					if ($lignejal["opejalon_datejalon"]=='0000-00-00'){$date_jal='';}else{$date_jal=strftime("%d/%m/%Y",strtotime($lignejal["opejalon_datejalon"]));}
				}

				echo "<div class='row'><div class='col-sm-6'><label>".$ligne["jal_libelle"]."</label></div><div class='col-sm-4'><input data-idope='".$id."' data-idjal='".$ligne["jal_id"]."' class='form-control input-sm chpjal' ".$disabled." value='".$date_jal."'></div></div>";//<div class='col-sm-1'><i class='fa fa-check text-success'></i></div>
			}
		}

	}
	echo '</div>';
	echo '</div>';
	echo '</div>';
}

function updatedatejalon_operation($idope,$idjal,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$duree_auto = 21;
	$duree_gpa = 365;
	$tabdate=explode("/", $val);
	$datefinal = $tabdate[2]."-".$tabdate[1]."-".$tabdate[0];
	$req2="UPDATE ope_jalon SET opejalon_datejalon='".$datefinal."' WHERE opejalon_idope=".$idope." AND opejalon_idjalon=".$idjal;
	$result2=mysql_query($req2,$link);
	if ($idjal==24){
		$datefinauto = date("Y-m-d",strtotime($datefinal." +21 day"));
		$req2="UPDATE ope_jalon SET opejalon_datejalon='".$datefinauto."' WHERE opejalon_idope=".$idope." AND opejalon_idjalon=25";
		$result2=mysql_query($req2,$link);
	}
}
function check_destinataireauto($idauto,$iddest,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	if ($val==1){
		$req2="INSERT INTO dest_auto (id_auto,id_dest) VALUES (".$idauto.",".$iddest.")";
		$result2=mysql_query($req2,$link);
	}else{
		$req2="DELETE FROM dest_auto WHERE id_auto=".$idauto." AND id_dest=".$iddest;
		$result2=mysql_query($req2,$link);
	}
}
function update_destinataireauto($idauto,$iddest,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	if ($chp=="date_reponse"){
		$tabdate=explode("/", $val);
		$val = $tabdate[2]."-".$tabdate[1]."-".$tabdate[0];
	}
	$req2="UPDATE dest_auto SET ".$chp."='".$val."' WHERE id_auto=".$idauto." AND id_dest=".$iddest;
	$result2=mysql_query($req2,$link);
}
function detail_programmation($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<div class="col-sm-12" style="margin-bottom:20px;">';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="far fa-clock"></i> Programmation</div>';
	echo '<div class="panel-body">';
		$reqproj="SELECT operations.ope_anneeprog,operations.ope_trimprog,operations.ope_identprog,tva.tva_taux,operations.ope_longdeposefs1,operations.ope_longdeposefs2,operations.ope_longdeposetorsade,operations.ope_deltau,operations.ope_idtype FROM operations LEFT JOIN tva ON tva.tva_id=operations.ope_idtva WHERE operations.ope_id=".$idope;
		$resultproj=mysql_query($reqproj,$link);
		while ($ligneproj=mysql_fetch_assoc($resultproj))
		{
			$annee_prog = $ligneproj["ope_anneeprog"];
			$trim_prog = $ligneproj["ope_trimprog"];
			$ident_prog = $ligneproj["ope_identprog"];
			$tva = $ligneproj["tva_taux"];
			$longdeposefs1 = $ligneproj["ope_longdeposefs1"];
			$longdeposefs2 = $ligneproj["ope_longdeposefs2"];
			$longdeposetorsade = $ligneproj["ope_longdeposetorsade"];
			$deltau = $ligneproj["ope_deltau"];
			$id_type = $ligneproj["ope_idtype"];
		}
		echo "<label class='col-sm-7'>Ann�e programmation:</label><div class='col-sm-4'><input id='ope_anneeprog' data-idope='".$idope."' data-chp='ope_anneeprog' class='form-control input-sm text-center modifchpoperation' value='".$annee_prog."'></div>";
		echo "<div class='col-sm-12'>";
		for ($i=1;$i<5;$i++){
			if ($i==$trim_prog){$checked='checked';}else{$checked='';}
			echo "<label class='radio col-sm-12'><input data-idope='".$idope."' data-chp='ope_trimprog' type='radio' name='optradio' class='modifchpoperation' value='".$i."' ".$checked.">Trimestre ".$i."</label>";
		}
		echo "</div>";
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-wrench"></i> Donn�es Techniques</div>';
	echo '<div class="panel-body">';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="ope_identprog">Entreprise: </label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_identprog" id="ope_identprog" name="ope_identprog">';
			echo '<option value="0">Choisir une entreprise</option>';
			$req="SELECT entreprises.ent_id, entreprises.ent_nom FROM entreprises";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["ent_id"]==$ident_prog){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["ent_id"].'" '.$selected.'>'.$row["ent_nom"].'</option>';
			}
			echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="ope_longdeposefs1">Long. d�pose FS1:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_longdeposefs1" id="ope_longdeposefs1" name="ope_longdeposefs1" value="'.$longdeposefs1.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="ope_longdeposefs2">Long. d�pose FS2:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_longdeposefs2" id="ope_longdeposefs2" name="ope_longdeposefs2" value="'.$longdeposefs2.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="ope_longdeposetorsade">Long. d�pose Torsad�:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_longdeposetorsade" id="ope_longdeposetorsade" name="ope_longdeposetorsade" value="'.$longdeposetorsade.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="ope_deltau">&#916 U/U (%):</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_deltau" id="ope_deltau" name="ope_deltau" value="'.$deltau.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="ope_idtype">Type: </label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_idtype" id="ope_idtype" name="ope_idtype">';
			echo '<option value="0">Choisir un type</option>';
			$req="SELECT lexique.lex_id, lexique.lex_libelle FROM lexique WHERE lexique.lex_codelexique='TYPE_PROJET'";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["lex_id"]==$id_type){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["lex_id"].'" '.$selected.'>'.$row["lex_libelle"].'</option>';
			}
			echo '</select>';
		echo '</div>';
		echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '</div>';
	/* APS */
	echo '<div class="col-sm-12">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="far fa-money-bill-alt" aria-hidden="true"></i> APS</div>';
	echo '<div class="panel-body">';
		$montant_ope = 0;
		echo '<table class="table table-sm table-bordered table-responsive table-striped table-hover">';
		echo '<thead><tr><th>Libell�</th><th>Prog.</th><th>Qte/mL</th><th>P.U.</th><th>Montants HT</th><th>Montants TTC</th></tr></thead>';
		echo '<tbody>';
		$reqaps="SELECT * FROM ope_aps WHERE ope_aps.opeaps_idope=".$idope;
		$resultaps=mysql_query($reqaps,$link);
		while ($ligneaps=mysql_fetch_assoc($resultaps))
		{
			echo '<tr><td>Etude</td>';
			echo '<td width="25%"><select class="form-control input-sm update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_idprogETU" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["opeaps_idprogETU"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_qteETU" value="'.$ligneaps["opeaps_qteETU"].'"></td><td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_puETU" value="'.$ligneaps["opeaps_puETU"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantHTETU" value="'.$ligneaps["opeaps_MontantHTETU"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantTTCETU" value="'.$ligneaps["opeaps_MontantTTCETU"].'"></td></tr>';
			echo '<tr><td>Electricit� BT 1</td>';
			echo '<td width="25%"><select class="form-control input-sm update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_idprogBT" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["projaps_idprogBT"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_qteBT" value="'.$ligneaps["opeaps_qteBT"].'"></td><td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_puBT" value="'.$ligneaps["opeaps_puBT"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantHTBT" value="'.$ligneaps["opeaps_MontantHTBT"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantTTCBT" value="'.$ligneaps["opeaps_MontantTTCBT"].'"></td></tr>';
			echo '<tr><td>Electricit� BT 2</td>';
			echo '<td width="20%"><select class="form-control input-sm update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_idprogBT2" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["opeaps_idprogBT2"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_qteBT2" value="'.$ligneaps["opeaps_qteBT2"].'"></td><td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_puBT2" value="'.$ligneaps["opeaps_puBT2"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantHTBT2" value="'.$ligneaps["opeaps_MontantHTBT2"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantTTCBT2" value="'.$ligneaps["opeaps_MontantTTCBT2"].'"></td></tr>';
			echo '<tr><td>Electricit� HT</td>';
			echo '<td width="25%"><select class="form-control input-sm update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_idprogHT" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["opeaps_idprogHT"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_qteHTA" value="'.$ligneaps["opeaps_qteHTA"].'"></td><td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_puHTA" value="'.$ligneaps["opeaps_puHTA"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantHTHTA" value="'.$ligneaps["opeaps_MontantHTHTA"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantTTCHTA" value="'.$ligneaps["opeaps_MontantTTCHTA"].'"></td></tr>';
			echo '<tr><td>Poste</td>';
			echo '<td width="25%"><select class="form-control input-sm update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_idprogPoste" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["opeaps_idprogPoste"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_qtePoste" value="'.$ligneaps["opeaps_qtePoste"].'"></td><td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_puPoste" value="'.$ligneaps["opeaps_puPoste"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantHTPoste" value="'.$ligneaps["opeaps_MontantHTPoste"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantTTCPoste" value="'.$ligneaps["opeaps_MontantTTCPoste"].'"></td></tr>';
			echo '<tr><td>Eclairage Public (Mat�riel)</td>';
			echo '<td width="25%"><select class="form-control input-sm update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_idprogMatECL" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["opeaps_idprogMatECL"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_qteMatECL" value="'.$ligneaps["opeaps_qteMatECL"].'"></td><td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_puMatECL" value="'.$ligneaps["opeaps_puMatECL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantHTMatECL" value="'.$ligneaps["opeaps_MontantHTMatECL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantTTCMatECL" value="'.$ligneaps["opeaps_MontantTTCMatECL"].'"></td></tr>';
			echo '<tr><td>Eclairage Public (R�seau)</td>';
			echo '<td width="25%"><select class="form-control input-sm update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_idprogResECL" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["opeaps_idprogResECL"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_qteResECL" value="'.$ligneaps["opeaps_qteResECL"].'"></td><td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_puResECL" value="'.$ligneaps["opeaps_puResECL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantHTResECL" value="'.$ligneaps["opeaps_MontantHTResECL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantTTCResECL" value="'.$ligneaps["opeaps_MontantTTCResECL"].'"></td></tr>';
			echo '<tr><td>T�l�communication</td>';
			echo '<td width="25%"><select class="form-control input-sm update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_idprogTEL" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["opeaps_idprogTEL"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_qteTEL" value="'.$ligneaps["opeaps_qteTEL"].'"></td><td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_puTEL" value="'.$ligneaps["opeaps_puTEL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantHTTEL" value="'.$ligneaps["opeaps_MontantHTTEL"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantTTCTEL" value="'.$ligneaps["opeaps_MontantTTCTEL"].'"></td></tr>';
			echo '<tr><td>Autre</td>';
			echo '<td width="25%"><select class="form-control input-sm update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_idprogAutre" >';
			echo '<option value="0">-- Programmes --</option>';
			$req="SELECT programme.prog_id, programme.prog_libelle FROM programme ";
			$result=mysql_query($req,$link);
			while ($row=mysql_fetch_assoc($result))
			{
				if ($row["prog_id"]==$ligneaps["opeaps_idprogAutre"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$row["prog_id"].'" '.$selected.'>'.$row["prog_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_qteAutre" value="'.$ligneaps["opeaps_qteAutre"].'"></td><td width="12%"><input class="form-control input-sm text-center update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_puAutre" value="'.$ligneaps["opeaps_puAutre"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantHTAutre" value="'.$ligneaps["opeaps_MontantHTAutre"].'"></td><td width="15%"><input class="form-control input-sm text-right update_opeaps" data-idope="'.$idope.'" data-chp="opeaps_MontantTTCAutre" value="'.$ligneaps["opeaps_MontantTTCAutre"].'"></td></tr>';
			echo '<tr><td colspan="4"><b>Total Op�ration</b></td><td class="text-right"><b>'.$ligneaps["opeaps_MontantHTOPE"].' �</b></td><td class="text-right"><b>'.$ligneaps["opeaps_MontantTTCOPE"].' �</b></td></tr>';
		}
		echo '</tbody>';
		echo '</table>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function update_apsope($idope,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE ope_aps SET ".$chp."='".$val."' WHERE opeaps_idope=".$idope;
	$result2=mysql_query($req2,$link);	
	$req="SELECT tva.tva_taux FROM operations JOIN tva ON tva.tva_id=operations.ope_idtva WHERE operations.ope_id=".$idope;
	$result=mysql_query($req,$link);
	$ligne=mysql_fetch_assoc($result);
	$tva = $ligne["tva_taux"];
	$reqaps="SELECT * FROM ope_aps WHERE ope_aps.opeaps_idope=".$idope;
	$resultaps=mysql_query($reqaps,$link);
	while ($ligneaps=mysql_fetch_assoc($resultaps))
	{
		$montant_ope=0;
		$tab = array("ETU","BT","BT2","HTA","Poste","MatECL","ResECL","TEL","Autre");
		for ($i=0;$i<count($tab);$i++){
			$chpqte="opeaps_qte".$tab[$i];
			$chppu="opeaps_pu".$tab[$i];
			$chpht="opeaps_MontantHT".$tab[$i];
			$chpttc="opeaps_MontantTTC".$tab[$i];
			if ($ligneaps[$chpqte]!=0 || $ligneaps[$chppu]!=0){$ht = $ligneaps[$chpqte]*$ligneaps[$chppu];$req2="UPDATE ope_aps SET ".$chpht."='".$ht."' WHERE opeaps_idope=".$idope;$result2=mysql_query($req2,$link);}else{$ht=$ligneaps[$chpht];}
			$ttc=$ht+($ht*$tva/100);
			$req2="UPDATE ope_aps SET ".$chpttc."='".$ttc."' WHERE opeaps_idope=".$idope;$result2=mysql_query($req2,$link);
			$montant_ope = $montant_ope + $ligneaps[$chpht];
		}
	}
	$montant_tva = $montant_ope * $tva/100;
	$montant_ttc = $montant_ope + $montant_tva;
	$req2="UPDATE ope_aps SET opeaps_MontantHTOPE='".$montant_ope."',opeaps_MontantTTCOPE='".$montant_ttc."' WHERE opeaps_idope=".$idope;
	$result2=mysql_query($req2,$link);	
}
function detail_coordination($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">';
	echo '<i class="far fa-handshake" aria-hidden="true"></i> Coordination';
	echo '</div>';
	echo '<div class="panel-body">';
		$reqcoord="SELECT coordination.coord_id, coordination.coord_libelle, ope_coord.opecoord_valeur FROM coordination JOIN ope_coord ON ope_coord.opecoord_idcoord=coordination.coord_id WHERE ope_coord.opecoord_idope=".$idope;
		$resultcoord=mysql_query($reqcoord,$link);
		echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
		echo "<thead><tr><th class='text-center'><i class='far fa-handshake' aria-hidden='true'></i></th><th>Libell�</th></tr></thead>";
		echo "<tbody>";
		while ($lignecoord=mysql_fetch_assoc($resultcoord))
		{
			if ($lignecoord["opecoord_valeur"]==1){$checked="checked";}else{$checked="";}
			echo '<tr><td align="center" width="8%"><input type="checkbox" class="chpcoord" data-idcoord="'.$lignecoord["coord_id"].'" data-idope="'.$idope.'" '.$checked.'></td><td>'.$lignecoord["coord_libelle"].'</td></tr>';
		}
		echo "</tbody>";
		echo "</table>";
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">';
	echo '<i class="fa fa-tags" aria-hidden="true"></i> Crit�res';
	echo '</div>';
	echo '<div class="panel-body">';
		$reqcoord="SELECT critere.critere_id, critere.critere_libelle, ope_critere.opecritere_valeur FROM critere JOIN ope_critere ON ope_critere.opecritere_idcritere=critere.critere_id WHERE ope_critere.opecritere_idope=".$idope;
		$resultcoord=mysql_query($reqcoord,$link);
		echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
		echo "<thead><tr><th class='text-center'><i class='fa fa-tags' aria-hidden='true'></i></th><th>Libell�</th></tr></thead>";
		echo "<tbody>";
		while ($lignecoord=mysql_fetch_assoc($resultcoord))
		{
			if ($lignecoord["opecritere_valeur"]==1){$checked="checked";}else{$checked="";}
			echo '<tr><td align="center" width="8%"><input type="checkbox" class="chpcritere" data-idcritere="'.$lignecoord["critere_id"].'" data-idope="'.$idope.'" '.$checked.'></td><td>'.$lignecoord["critere_libelle"].'</td></tr>';
		}
		echo "</tbody>";
		echo "</table>";
		$reqope="SELECT operations.ope_nbrbranchcrit,operations.ope_nbresupportdepose FROM operations WHERE operations.ope_id=".$idope;
		$resultope=mysql_query($reqope,$link);
		while ($ligneope=mysql_fetch_assoc($resultope))
		{
			$nbr_branch_crit = $ligneope["ope_nbrbranchcrit"];
			$nbr_support_depose = $ligneope["ope_nbresupportdepose"];
		}
		echo "<div class='form-group row'><label class='col-sm-7'>Nombre de branchements:</label><div class='col-sm-5'><input id='nbr_branch_crit' data-idope='".$idope."' data-chp='ope_nbrbranchcrit' class='form-control input-sm modifchpoperation' value='".$nbr_branch_crit."'></div></div>";
		echo "<div class='form-group row'><label class='col-sm-7'>Nombre de support d�pos�:</label><div class='col-sm-5'><input id='nbr_branch_crit' data-idope='".$idope."' data-chp='ope_nbresupportdepose' class='form-control input-sm modifchpoperation' value='".$nbr_support_depose."'></div></div>";
	echo '</div>';
	echo '</div>';
	echo '</div>';
}

function updatecoord_operation($idope,$idcoord,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE ope_coord SET opecoord_valeur='".$val."' WHERE opecoord_idope=".$idope." AND opecoord_idcoord=".$idcoord;
	$result2=mysql_query($req2,$link);
}
function updatecritere_operation($idope,$idcritere,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE ope_critere SET opecritere_valeur='".$val."' WHERE opecritere_idope=".$idope." AND opecritere_idcritere=".$idcritere;
	$result2=mysql_query($req2,$link);
}

function detail_autorisation($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqaut="SELECT auto_id,auto_type,auto_numero FROM autorisation WHERE autorisation.auto_idope=".$idope;
	$resultaut=mysql_query($reqaut,$link);
	while ($ligneaut=mysql_fetch_assoc($resultaut))
	{
		$id_auto = $ligneaut["auto_id"];
		$type_auto = $ligneaut["auto_type"];
		$num_auto = $ligneaut["auto_numero"];
	}
	echo '<div class="col-sm-12">';
	echo "<label class='col-sm-2'>Type:</label><div class='col-sm-4'>";
	echo "<select class='form-control input-sm'><option></option>";
	$reqlexique="SELECT lex_id,lex_libelle FROM lexique WHERE lex_codelexique='TYPE_AUTO'";
	$resultlexique=mysql_query($reqlexique,$link);
	while ($datalexique=mysql_fetch_assoc($resultlexique))
	{
		if($type_auto==$datalexique["lex_id"]){$selected="selected";}else{$selected="";}
		echo "<option value='".$datalexique["lex_id"]."' ".$selected.">".$datalexique["lex_libelle"]."</option>";
	}
	echo "</select>";
	echo "</div>";
	echo "<div class='col-sm-4'><input class='form-control input-sm' value='".$num_auto."' disabled></div>";
	echo '</div><br><br><br>';

	echo '<div class="col-sm-12" style="margin-bottom:20px;">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"> Valorisation</div>';
	echo '<div class="panel-body">';
		echo '<div class="col-sm-12">';
		echo '<div class="panel panel-default">';
		echo '<div class="panel-heading">Articles</div>';
		echo '<div class="panel-body" id="lstarticlevalorisationauto">';
			lst_valorisationauto($idope);
		echo '</div>';
		echo '</div>';
		echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '<div class="col-sm-12" style="margin-bottom:20px;">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">';
	echo '<i class="fa fa-envelope" aria-hidden="true"></i> Destinataires';
	echo '</div>';
	echo '<div class="panel-body">';
	$reqdest="SELECT id, service FROM destinataires ORDER BY service ASC";
	$resultdest=mysql_query($reqdest,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center'></th><th>Service</th><th>Date r�ponse</th><th>R�ponse</th></tr></thead>";
	echo "<tbody>";
	while ($lignedest=mysql_fetch_assoc($resultdest))
	{
		$reqdestauto="SELECT * FROM dest_auto WHERE id_auto=".$id_auto." AND id_dest=".$lignedest["id"];
		$resultdestauto=mysql_query($reqdestauto,$link);
		$rowdestauto = mysql_num_rows($resultdestauto);
		$arraydestauto = mysql_fetch_assoc($resultdestauto);
		if($rowdestauto!=0){$checked="checked";}else{$checked="";}
		$date_reponse='';
		if ($arraydestauto["date_reponse"]=='0000-00-00'){$date_reponse='';}else{$date_reponse=strftime("%d/%m/%Y",strtotime($arraydestauto["date_reponse"]));}
		echo "<tr><td class='text-center' width='5%'><input data-idauto='".$id_auto."' data-iddest='".$lignedest["id"]."' type='checkbox' class='chkdestauto' ".$checked."></td><td width='20%'>".$lignedest["service"]."</td><td width='15%'><input class='form-control input-sm text-center chpmodifdestauto datedestauto' data-idauto='".$id_auto."' data-iddest='".$lignedest["id"]."' data-chp='date_reponse' value='".$date_reponse."'></td><td><input class='form-control input-sm chpmodifdestauto' data-idauto='".$id_auto."' data-iddest='".$lignedest["id"]."' data-chp='reponse' value='".$arraydestauto["reponse"]."'></td></tr>";
	}
	echo "</tbody>";
	echo "</table>";
	$reqdestauto="SELECT * FROM dest_auto WHERE id_auto=".$id_auto;
	$resultdestauto=mysql_query($reqdestauto,$link);
	$rowdestauto = mysql_num_rows($resultdestauto);
	if($rowdestauto!=0){$disabled="";}else{$disabled="disabled";}
	echo "<div class='col-md-12'><button class='btn btn-default btn-sm' ".$disabled."><i class='fa fa-envelope'></i> Envoyer</button></div>";
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function add_valauto($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="INSERT INTO val_auto (id_ope) VALUES (".$idope.")";
	$result=mysql_query($req,$link);
}
function del_valauto($idvalauto){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="DELETE FROM val_auto WHERE val_auto.id=".$idvalauto;
	$result=mysql_query($req,$link);
}
function lst_valorisationauto($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqvalauto="SELECT * FROM val_auto WHERE id_ope=".$idope;
	$resultvalauto=mysql_query($reqvalauto,$link);
	$rowvalauto=mysql_num_rows($resultvalauto);
	echo '<table class="table table-bordered table-responsive">';
	echo '<thead><tr><th width="10%" class="text-center"><button id="addvalauto" data-idope="'.$idope.'" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"></i></button></th><th>Articles</th><th>Bordereau</th><th>Nature</th><th>Quantit�/ml</th></tr></thead>';
	echo '<tbody>';
	if ($rowvalauto!=0){
		while ($datavalauto=mysql_fetch_assoc($resultvalauto))
		{
			echo '<tr><td class="text-center" width="10%"><button data-id="'.$datavalauto["id"].'" data-idope="'.$idope.'" class="btn btn-default btn-sm del_valauto"><i class="fa fa-trash"></i></button></td>';
			echo '<td>';
			echo "<select class='form-control input-sm update_valauto' data-id='".$datavalauto["id"]."' data-idope='".$idope."' data-chp='article'><option value=''>Choisir un article</option>";
			$reqlexique="SELECT lex_id,lex_libelle FROM lexique WHERE lex_codelexique='ART_AUTO'";
			$resultlexique=mysql_query($reqlexique,$link);
			while ($datalexique=mysql_fetch_assoc($resultlexique))
			{
				if($datavalauto["article"]==$datalexique["lex_libelle"]){$selected="selected";}else{$selected="";}
				echo "<option value='".$datalexique["lex_libelle"]."' ".$selected.">".$datalexique["lex_libelle"]."</option>";
			}
			echo "</select>";
			echo '</td>';
			echo '<td>';
			echo "<select class='form-control input-sm update_valauto' data-id='".$datavalauto["id"]."' data-idope='".$idope."' data-chp='bordereau'><option></option>";
			$reqlexique="SELECT lex_id,lex_libelle FROM lexique WHERE lex_codelexique='BORD_AUTO'";
			$resultlexique=mysql_query($reqlexique,$link);
			while ($datalexique=mysql_fetch_assoc($resultlexique))
			{
				if($datavalauto["bordereau"]==$datalexique["lex_libelle"]){$selected="selected";}else{$selected="";}
				echo "<option value='".$datalexique["lex_libelle"]."' ".$selected.">".$datalexique["lex_libelle"]."</option>";
			}
			echo "</select>";
			echo '</td>';
			echo '<td><input class="form-control input-sm text-center update_valauto" data-id="'.$datavalauto["id"].'" data-idope="'.$idope.'" data-chp="nature" value="'.$datavalauto["nature"].'"></td>';
			echo '<td><input class="form-control input-sm text-center update_valauto" data-id="'.$datavalauto["id"].'" data-idope="'.$idope.'" data-chp="qte" value="'.$datavalauto["qte"].'"></td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td colspan="5" class="text-center"><b>Aucun article</b></td></tr>';
	}
	echo '</tbody>';
	echo '</table>';
}
function update_valauto($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE val_auto SET ".$chp."='".$val."' WHERE id=".$id;
	$result2=mysql_query($req2,$link);	
}
function lstchantier_operation($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqcht="SELECT chantiers.cht_numero, chantiers.cht_libelle, chantiers.cht_categorie, chantiers.cht_id, chantiers.cht_nature, programme.prog_libelle as programme, annee_prog.anneeprog_annee, chantiers.cht_ident, chantiers.cht_idope, boncommande.bc_datefinbc FROM chantiers LEFT JOIN annee_prog ON chantiers.cht_idanneeprog=annee_prog.anneeprog_id LEFT JOIN programme ON programme.prog_id=annee_prog.anneeprog_idprog LEFT JOIN bc_cht ON bc_cht.bccht_idcht=chantiers.cht_id LEFT JOIN boncommande ON boncommande.bc_id=bc_cht.bccht_idbc WHERE chantiers.cht_idope=".$id." ORDER BY chantiers.cht_nature, chantiers.cht_datecreate ASC";
	$resultcht=mysql_query($reqcht,$link);
	$rowcht=mysql_num_rows($resultcht);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center'><button id='add_chantier' data-idope='".$id."' class='btn btn-sm btn-default' data-tooltip='Nouvelle prestation'><i class='fa fa-plus-circle'></i></button></th><th>Num�ro</th><th>Nature</th><th>Date Fin B.C.</th><th>Financement</th><th>Entreprise</th><th>APD (� TTC)</th></tr></thead>";
	echo "<tbody>";
	$tot_apd = 0;
	if ($rowcht!=0)
	{
		while ($lignecht=mysql_fetch_assoc($resultcht))
		{
			/* Recherche de la valeur apd */
			$apd = 0.00;$ent_cht="";
			$reqapd="SELECT tot_TTC FROM apd WHERE id_cht=".$lignecht["cht_id"];
			$resultapd=mysql_query($reqapd,$link);
			$rowapd=mysql_num_rows($resultapd);
			if ($rowapd!=0){
				while ($ligneapd=mysql_fetch_assoc($resultapd))
				{
					$apd = $ligneapd["tot_TTC"];
				}
			}else{$apd=0.00;}
			$tot_apd = $tot_apd + $apd;

			/* Recherche de l'entreprise titulaire */
			$reqent="SELECT ent_nom FROM entreprises WHERE ent_id=".$lignecht["cht_ident"];
			$resultent=mysql_query($reqent,$link);
			$rowent=mysql_num_rows($resultent);
			if ($rowent!=0){
				while ($ligneent=mysql_fetch_assoc($resultent))
				{
					$ent_cht = $ligneent["ent_nom"];
				}
			}
			/* Recherche Bon de commande rattach� pour activation des boutons */
			$reqchtbc="SELECT * FROM demandebc_chantiers WHERE id_cht=".$lignecht["cht_id"];
			$resultchtbc=mysql_query($reqchtbc,$link);
			$rowchtbc=mysql_num_rows($resultchtbc);
			if ($rowchtbc!=0){$buttons = "disabled";}else{$buttons = "";}
			if ($lignecht["bc_datefinbc"]){if ($lignecht["bc_datefinbc"]!="0000-00-00"){$datefinbc=strftime("%d/%m/%Y",strtotime($lignecht["bc_datefinbc"]));}else{$datefinbc="";}}else{$datefinbc="";}
			echo '<tr><td align="center" width="10%"><button data-id="'.$lignecht["cht_id"].'" class="btn btn-sm btn-default visucht" data-tooltip="Visualiser"><i class="fa fa-eye"></i></button> <button data-id="'.$lignecht["cht_id"].'" data-idope="'.$lignecht["cht_idope"].'" class="btn btn-sm btn-default delcht" data-tooltip="Supprimer" '.$buttons.'><i class="fa fa-trash"></i></button></td><td align="center" width="12%">'.$lignecht["cht_numero"].'</td><td width="10%">'.$lignecht["cht_nature"].'</td><td width="10%" class="text-center">'.$datefinbc.'</td><td width="15%" class="text-center">'.$lignecht["programme"].' '.$lignecht["anneeprog_annee"].'</td><td>'.$ent_cht.'</td><td width="10%" class="text-right">'.number_format($apd, 2, ',', ' ').' �</td></tr>';
		}
		echo '<tr><td class="text-right" colspan="6"><b>Total</b></td><td class="text-right"><b>'.number_format($tot_apd, 2, ',', ' ').' �</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function nbrchantier_operation($id){
	require("./compte.php");
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqcht="SELECT * FROM chantiers WHERE cht_idope=".$id;
	$resultcht=mysql_query($reqcht,$link);
	$rowcht=mysql_num_rows($resultcht);
	return $rowcht;
}
function del_chantier($idcht){
	require("./compte.php");
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql = "SELECT id FROM apd WHERE id_cht=".$idcht;
	$resul=mysql_query($sql,$link);
	$data=mysql_fetch_assoc($resul);
	$idapd = $data["id"];
	/* Suppression des valorisations apd */
	$sql = "DELETE FROM val_apd WHERE id_apd=".$idapd;
	$resul=mysql_query($sql,$link);
	/* Suppression de l'apd */
	$sql = "DELETE FROM apd WHERE id=".$idapd;
	$resul=mysql_query($sql,$link);
	/* Suppression du sp�cifique */
	/* Recherche de la nature */
	$req2="SELECT nature.nat_codenumero,nature.nat_libelle,chantiers.cht_idope FROM nature JOIN chantiers ON chantiers.cht_nature=nature.nat_libelle WHERE chantiers.cht_id=".$idcht;
	$result2=mysql_query($req2,$link);
	$lignenat=mysql_fetch_assoc($result2);
	$code_nat = $lignenat["nat_codenumero"];
	$libelle_nat = $lignenat["nat_libelle"];
	$idope = $lignenat["cht_idope"];
	$sql = "SHOW TABLES FROM $baseSYGALE";
	$result = mysql_query($sql);
	while ($row = mysql_fetch_row($result)) {
		if ($row[0]==strtolower($code_nat)){
			require("./".strtolower($code_nat).".php");
			del_specif($idcht);
			break;
		}
	}
	/* Suppression de la prestation */
	$sql = "DELETE FROM chantiers WHERE cht_id=".$idcht;
	$resul=mysql_query($sql,$link);
	/* Ajout de l'historique */
	$req2="INSERT INTO historique (hist_idocc,hist_dateevent,hist_libelleevent,hist_user,hist_event,hist_natureevent) VALUES ('".$idope."',NOW(),'Suppression prestation ".$libelle_nat."','".$_COOKIE["login"]."','delete','TVX')";
	$result2=mysql_query($req2,$link);
}
function del_operation($idope){
	require("./compte.php");
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql = "SELECT id FROM autorisation WHERE id_ope=".$idope;
	$resul=mysql_query($sql,$link);
	$data=mysql_fetch_assoc($resul);
	$idauto = $data["id"];
	/* Suppression des valorisations autorisation */
	$sql = "DELETE FROM val_auto_auto WHERE id_auto=".$idauto;
	$resul=mysql_query($sql,$link);
	/* Suppression des destinataires autorisation */
	$sql = "DELETE FROM dest_auto WHERE id_auto=".$idauto;
	$resul=mysql_query($sql,$link);
	/* Suppression des autorisations */
	$sql = "DELETE FROM autorisation WHERE auto_idope=".$idope;
	$resul=mysql_query($sql,$link);
	/* Suppression des historiques */
	$sql = "DELETE FROM historique WHERE hist_idocc=".$idope;
	$resul=mysql_query($sql,$link);
	/* Suppression des coordinations */
	$sql = "DELETE FROM ope_coord WHERE opecoord_idope=".$idope;
	$resul=mysql_query($sql,$link);
	/* Suppression des crit�res */
	$sql = "DELETE FROM ope_critere WHERE opecritere_idope=".$idope;
	$resul=mysql_query($sql,$link);
	/* Suppression des jalonnements */
	$sql = "DELETE FROM ope_jalon WHERE opejalon_idope=".$idope;
	$resul=mysql_query($sql,$link);

	/* Suppression des prestations rattach�es */
	$sqlcht = "SELECT cht_id FROM chantiers WHERE cht_idope=".$idope;
	$resulcht=mysql_query($sqlcht,$link);
	while ($rowcht = mysql_fetch_assoc($resulcht)) {
		del_chantier($rowcht["cht_id"]);
	}
	/* Suppression de l'op�ration */
	$sqlope = "DELETE FROM operations WHERE ope_id=".$idope;
	$resulope=mysql_query($sqlope,$link);
}
function add_operation($idcat){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT cat_libelle, cat_codenumero, cat_AUT FROM categorie WHERE cat_id=".$idcat;
	$result2=mysql_query($req2,$link);
	while ($lignecat=mysql_fetch_assoc($result2))
	{
		$lib_cat = $lignecat["cat_libelle"];
		$code_cat = $lignecat["cat_codenumero"];
		$aut_cat = $lignecat["cat_AUT"];
	}
	$req2="SELECT ope_numero FROM operations WHERE ope_categorie='".$lib_cat."' ORDER BY ope_numero DESC LIMIT 1";
	$result2=mysql_query($req2,$link);
	while ($ligneope=mysql_fetch_assoc($result2))
	{
		$tabnumope = explode("-",$ligneope["ope_numero"]);
	}
	$newref = date("y")."-".$code_cat."-";
	if ($tabnumope[0]==date("y")){
		$compteur = $tabnumope[2]+1;
	}else{
		$compteur = 1;
	}
	$compt = strlen($compteur);
	$diff = 3 - $compt;
	for ($i=0;$i<$diff;$i++){
		$newref .= "0";
	}
	$newref .= $compteur;
	/* Insert l'op�ration */
	$req2="INSERT INTO operations (ope_numero,ope_datecreate,ope_categorie,ope_chargeaffaire) VALUES ('".$newref."',NOW(),'".$lib_cat."','".$_COOKIE["login"]."')";
	$result2=mysql_query($req2,$link);
	$idope = mysql_insert_id($link);
	/* Ajout APS */
	$req2="INSERT INTO ope_aps (opeaps_idope) VALUES ('".$idope."')";
	$result2=mysql_query($req2,$link);
	$req2="INSERT INTO historique (hist_idocc,hist_dateevent,hist_libelleevent,hist_user,hist_event,hist_natureevent) VALUES ('".$idope."',NOW(),'Cr�ation op�ration','".$_COOKIE["login"]."','create','TVX')";
	$result2=mysql_query($req2,$link);
	/* Insert les jalons */
	$reqjal="SELECT jal_id FROM jalon WHERE jal_activite in ('','ETU','CTO','".$code_cat."') ORDER BY jal_ordre ASC";
	$resultjal=mysql_query($reqjal,$link);
	while ($lignejal=mysql_fetch_assoc($resultjal))
	{
		$req2="INSERT INTO ope_jalon (opejalon_idope,opejalon_idjalon) VALUES ('".$idope."','".$lignejal["jal_id"]."')";
		$result2=mysql_query($req2,$link);
	}
	/* Cr�ation des coordinations */
	$reqcoord="SELECT coord_id, coord_libelle FROM coordination";
	$resultcoord=mysql_query($reqcoord,$link);
	while ($lignecoord=mysql_fetch_assoc($resultcoord))
	{
		$req2="INSERT INTO ope_coord (opecoord_idope,opecoord_idcoord,opecoord_valeur) VALUES ('".$idope."','".$lignecoord["coord_id"]."','0')";
		$result2=mysql_query($req2,$link);
	}
	/* Cr�ation des crit�res */
	$reqcrit="SELECT critere_id, critere_libelle FROM critere";
	$resultcrit=mysql_query($reqcrit,$link);
	while ($lignecrit=mysql_fetch_assoc($resultcrit))
	{
		$req2="INSERT INTO ope_critere (opecritere_idope,opecritere_idcritere,opecritere_valeur) VALUES ('".$idope."','".$lignecrit["critere_id"]."','0')";
		$result2=mysql_query($req2,$link);
	}
	if ($aut_cat==1){
		/* Cr�ation de l'autorisation */
		$num_aut = $newref."-AUT-1";
		$req2="INSERT INTO autorisation (auto_idope,auto_type,auto_numero) VALUES ('".$idope."','74','".$num_aut."')";
		$result2=mysql_query($req2,$link);
	}
	/* Cr�ation des prestations obligatoires */
	$reqnat="SELECT nat_libelle,nat_codenumero FROM nature WHERE nat_oblig='1'";
	$resultnat=mysql_query($reqnat,$link);
	while ($lignenat=mysql_fetch_assoc($resultnat))
	{
		$newref_cht = $newref."-".$lignenat["nat_codenumero"]."-1";
		$req2="INSERT INTO chantiers (cht_idope,cht_numero,cht_datecreate,cht_nature,cht_categorie) VALUES ('".$idope."','".$newref_cht."',NOW(),'".$lignenat["nat_libelle"]."','".$lib_cat."')";
		$result2=mysql_query($req2,$link);
		$idcht = mysql_insert_id($link);
		$req2="INSERT INTO apd (id_cht) VALUES ('".$idcht."')";
		$result2=mysql_query($req2,$link);
		$sql = "SHOW TABLES FROM $baseSYGALE";
		$result = mysql_query($sql);
		while ($row = mysql_fetch_row($result)) {
			if ($row[0]==strtolower($lignenat["nat_codenumero"])){
				$req2="INSERT INTO ".$row[0]." (id_cht) VALUES ('".$idcht."')";
				$result2=mysql_query($req2,$link);
				break;
			}
		}
		$req2="INSERT INTO historique (hist_idocc,hist_dateevent,hist_libelleevent,hist_user,hist_event,hist_natureevent) VALUES ('".$idope."',NOW(),'Cr�ation prestation ".$lignenat["nat_libelle"]."','".$_COOKIE["login"]."','create','TVX')";
		$result2=mysql_query($req2,$link);
	}
}
function add_chantier($idope,$idnat){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT nat_libelle, nat_codenumero FROM nature WHERE nat_id=".$idnat;
	$result2=mysql_query($req2,$link);
	while ($lignenat=mysql_fetch_assoc($result2))
	{
		$lib_nat = $lignenat["nat_libelle"];
		$code_nat = $lignenat["nat_codenumero"];
	}
	$req2="SELECT ope_numero,ope_categorie FROM operations WHERE ope_id=".$idope;
	$result2=mysql_query($req2,$link);
	while ($ligneope=mysql_fetch_assoc($result2))
	{
		$tabnumope = explode("-",$ligneope["ope_numero"]);
		$categorie = $ligneope["ope_categorie"];
	}
	$req2="SELECT cht_numero,cht_categorie FROM chantiers WHERE cht_nature='".$lib_nat."' AND cht_idope=".$idope." ORDER BY cht_datecreate DESC LIMIT 1";
	$result2=mysql_query($req2,$link);
	$rowcht = mysql_num_rows($result2);
	if ($rowcht!=0){
		while ($lignecht=mysql_fetch_assoc($result2))
		{
			$tabnumcht = explode("-",$lignecht["cht_numero"]);
			$categorie = $lignecht["cht_categorie"];
		}
		$newref = $tabnumcht[0]."-".$tabnumcht[1]."-".$tabnumcht[2]."-".$tabnumcht[3]."-";
		$compteur = $tabnumcht[4]+1;
	}else{
		$newref = $tabnumope[0]."-".$tabnumope[1]."-".$tabnumope[2]."-".$code_nat."-";
		$compteur = 1;
	}
/*	$compt = strlen($compteur);
	$diff = 1 - $compt;
	for ($i=0;$i<$diff;$i++){
		$newref .= "0";
	}
*/	$newref .= $compteur;
	/* Ajout de la prestation */
	$req2="INSERT INTO chantiers (cht_idope,cht_numero,cht_datecreate,cht_nature,cht_categorie) VALUES ('".$idope."','".$newref."',NOW(),'".$lib_nat."','".$categorie."')";
	$result2=mysql_query($req2,$link);
	$idcht = mysql_insert_id($link);
	/* Ajout de la table sp�cifique correspondante � la nature */
	$sql = "SHOW TABLES FROM $baseSYGALE";
	$result = mysql_query($sql);
	while ($row = mysql_fetch_row($result)) {
		if ($row[0]==strtolower($code_nat)){
			$req2="INSERT INTO ".$row[0]." (".$row[0]."_idcht) VALUES ('".$idcht."')";
			$result2=mysql_query($req2,$link);
			break;
		}
	}
	/* Ajout de l'apd */
	$req2="INSERT INTO apd (id_cht) VALUES ('".$idcht."')";
	$result2=mysql_query($req2,$link);
	$req2="INSERT INTO historique (hist_idocc,hist_dateevent,hist_libelleevent,hist_user,hist_event,hist_natureevent) VALUES ('".$idope."',NOW(),'Cr�ation prestation ".$lib_nat."','".$_COOKIE["login"]."','create','TVX')";
	$result2=mysql_query($req2,$link);
}

function envoimail(){
	// Envoi du mail � l'entreprise
	require('../PHPMailer/class.phpmailer.php');
	$mail = new PHPMailer();
	$mail->Host = "cli-mail.devopsys.com";
	$mail->SMTPAuth   = false;
	$mail->Port = 25; // Par d�faut
	 
	// Exp�diteur
	$mail->SetFrom('sehv@sehv.fr', 'SEHV');
	$mail->Sender = 'sehv@sehv.fr';

	$ldaphost = "192.168.8.122";
	$ldapport = 389;
	$ldapuser = "administrateur@sehv87.fr";
	$ldappass = "147258";
	$login = $_COOKIE["login"];
	// Connexion LDAP
	$ldapconn = ldap_connect($ldaphost) or die("Impossible de se connecter au serveur LDAP $ldaphost");

	if($ldapconn) {
		$ldapbind = ldap_bind($ldapconn, $ldapuser, $ldappass);
		$ldaptree = "OU=Agents SEHV,DC=sehv87,DC=fr";
		$result = ldap_search($ldapconn,$ldaptree, "(samaccountname=".$login.")") or die ("Error in search query: ".ldap_error($ldapconn));

		$data = ldap_get_entries($ldapconn, $result);
	        for ($i=0; $i<$data["count"]; $i++) {
	            //echo "dn is: ". $data[$i]["dn"] ."<br />";
	            if(isset($data[$i]["mail"][0])) {
	                 $mailagent = $data[$i]["mail"][0];
	            }
	        }
	}
	ldap_close($ldapconn);

	// Re�evoir une confirmation de lecture
	$mail->AddReplyTo($mailagent, 'SEHV');
	$mail->addCustomHeader("X-Confirm-Reading-To: ".$mailagent);
	$mail->addCustomHeader("Disposition-notification-to: ".$mailagent);

	// Destinataire
	$mail->AddAddress('resptechnique@sehv.fr', 'PEYRICHON Alain');

	// Objet
	$mail->Subject = 'Demande BC ';
	 
	// Votre message
	$mail->MsgHTML('Test demande BC');

	// Ajouter une pi�ce jointe
	//$mail->AddAttachment(dirname(dirname(__DIR__)).'/PdfDepannage/'.$filename.'.pdf');

	if(!$mail->Send()) {
		echo 'Erreur : ' . $mail->ErrorInfo;
	} else {
		echo 'Message envoy� !';
	}
}

function detail_chantier($id){
	require("./compte.php");
	require("./print.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT cht_numero, cht_libelle, cht_categorie, cht_id, cht_idope FROM chantiers WHERE cht_id=".$id;
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{

			$panel=1;
			echo "<div class='col-md-12'>";
			echo "<div class='portlet'><div class='portlet-title'><i class='fa fa-bolt' aria-hidden='true'></i> <b>PRESTATION</b> ".$ligne2["cht_categorie"]." - ".$ligne2["cht_numero"]." - ".$ligne2["cht_libelle"]."</div><div class='portlet-content' id='detchantier'>";
			echo "<div class='row'>";

			echo '<div class="col-sm-12">';
			echo "<button id='back_operation' class='btn btn-default btn-sm' data-idope='".$ligne2["cht_idope"]."'><i class='fa fa-reply'></i> Op�ration</button>";
			echo '</div><br><br>';

			echo '<div class="col-sm-12">';
			echo '<div class="panel-group" id="accordion">';
			/* G�n�ralit�s */
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h2 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-file" aria-hidden="true"></i> G�n�ralit�s</a>';
			echo '</h2>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detailgene_chantier">';
				detailgene_chantier($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* APD */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h2 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-money-bill-alt" aria-hidden="true"></i> APD</a>';
			echo '</h2>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body">';
				detailapd_chantier($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Documents */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="far fa-file-alt" aria-hidden="true"></i> Documents</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detdocoperation">';
				lst_print_contextuel("Travaux/CHT",$id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			$nat = explode("-",$ligne2["cht_numero"]);
			$sql = "SHOW TABLES FROM $baseSYGALE";
			$result = mysql_query($sql);
			while ($row = mysql_fetch_row($result)) {
   				if ($row[0]==strtolower($nat[3])){
   					require("./".strtolower($nat[3]).".php");
					/* Sp�cifique */
					$panel++;
					echo '<div class="panel panel-default">';
					echo '<div class="panel-heading">';
					echo '<h2 class="panel-title">';
					echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-cog" aria-hidden="true"></i> Sp�cificit�</a>';
					echo '</h2>';
					echo '</div>';
					echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
					echo '<div class="panel-body" id="detspecif">';
						detail_specif($id,strtolower($nat[3]));
					echo '</div>';
					echo '</div>';
					echo '</div>';
					exit();
   				}
			}
			echo '</div>';
			echo '</div>';
		}
	}
}
function detailgene_chantier($idcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM chantiers WHERE cht_id=".$idcht;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$datecreate = $ligne["cht_datecreate"];
		$numero = $ligne["cht_numero"];
		$libelle = $ligne["cht_libelle"];
		$categorie = $ligne["cht_categorie"];
		$id_ent = $ligne["cht_ident"];
		$id_anneeprog = $ligne["cht_idanneeprog"];
		$monttitulairecht = $ligne["cht_monttitulaire"];
	}
	$numbc="";
	$req="SELECT boncommande.bc_numero FROM bc_cht JOIN boncommande ON boncommande.bc_id=bc_cht.id_bc WHERE bc_cht.id_cht=".$idcht;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$numbc = $ligne["bc_numero"];
	}
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-2" for="cht_num">Num�ro:</label>';
	echo '<div class="col-sm-6">';
	echo '<input class="form-control input-sm" id="cht_num" name="cht_num" value="'.$numero.'" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-2" for="cht_numbc">Num�ro BC:</label>';
	echo '<div class="col-sm-6">';
	echo '<input class="form-control input-sm" id="cht_numbc" name="cht_numbc" value="'.$numbc.'" disabled>';
	echo '</div>';
	echo '</div>';
	echo '</form>';

	/* Entreprises de la prestation */
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">Entreprises</div>';
	echo '<div class="panel-body">';
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-2" for="lsttitulairecht">Titulaire:</label>';
	echo '<div class="col-sm-4">';
	echo '<select class="form-control input-sm update_chantier" data-chp="cht_ident" id="lsttitulairecht" name="lsttitulairecht">';
	echo '<option value="-1">S�lectionnez une entreprise</option>';
	$req="SELECT ent_id,ent_nom FROM entreprises";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["ent_id"]==$id_ent){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
	}
	echo '</select>';
	echo '</div>';
	echo '<label class="control-label col-sm-2" for="monttitulairecht">Montant:</label>';
	echo '<div class="col-sm-3">';
	echo '<input class="form-control input-sm text-right update_chantier" data-chp="cht_monttitulaire" id="monttitulairecht" name="monttitulairecht" value="'.$monttitulairecht.'">';
	echo '</div>';
	echo '<label class="control-label col-sm-1"> �</label>';
	echo '</div>';
	echo '</form>';

	/* Entreprises sous-traitante de la prestation */
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">Sous-traitants</div>';
	echo '<div class="panel-body" id="detailsstraitantcht">';
		lst_sstraitantcht($idcht);
	echo '</div>';
	echo '</div>';	
	echo '</div>';
	echo '</div>';	
	/* Financement de la prestation */
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">Financement</div>';
	echo '<div class="panel-body" id="detailfinancecht">';
		$req="SELECT annee_prog.anneeprog_idprog FROM annee_prog JOIN programme ON programme.prog_id=annee_prog.anneeprog_idprog WHERE annee_prog.anneeprog_id=".$id_anneeprog;
		$result=mysql_query($req,$link);
		$ligne=mysql_fetch_assoc($result);
		$id_prog = $ligne["anneeprog_idprog"];
		finance_cht($id_anneeprog,$id_prog);
	echo '</div>';
	echo '</div>';	
}
function finance_cht($id_anneeprog,$id_prog){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-2" for="lstprogcht">Programme:</label>';
	echo '<div class="col-sm-4">';
	echo '<select class="form-control input-sm update_chantier" data-chp="programme" id="lstprogcht" name="lstprogcht">';
	echo '<option value="-1">S�lectionnez un programme</option>';
	$req="SELECT prog_id,prog_libelle FROM programme";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["prog_id"]==$id_prog){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$ligne["prog_id"].'" '.$selected.'>'.$ligne["prog_libelle"].'</option>';
	}
	echo '</select>';
	echo '</div>';
	echo '<label class="control-label col-sm-2" for="lstannprogcht">Ann�e:</label>';
	echo '<div class="col-sm-4">';
	lst_anneeprog($id_anneeprog,$id_prog);
	echo '</div>';
	echo '</div>';
	echo '</form>';
}
function lst_anneeprog($id_anneeprog,$id_prog){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<select class="form-control input-sm update_chantier" data-chp="id_anneeprog" id="lstannprogcht" name="lstannprogcht">';
	echo '<option value="-1">S�lectionnez une ann�e</option>';
	$req="SELECT annee_prog.anneeprog_id,annee_prog.anneeprog_annee FROM annee_prog WHERE annee_prog.anneeprog_idprog=".$id_prog;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["anneeprog_id"]==$id_anneeprog){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$ligne["anneeprog_id"].'" '.$selected.'>'.$ligne["anneeprog_annee"].'</option>';
	}
	echo '</select>';
}
function update_chantier($idcht,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	if ($chp=="programme"){
		/* R�initialisation de l'ann�e programme pour le programme */
		$chp="id_anneeprog";
		$val=0;		
	}
	$req2="UPDATE chantiers SET ".$chp."='".$val."' WHERE cht_id=".$idcht;
	$result2=mysql_query($req2,$link);	
}
function add_sstraitantcht($idcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO soustraitant_cht (id_cht) VALUES (".$idcht.")";
	$result2=mysql_query($req2,$link);	
}
function del_sstraitantcht($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM soustraitant_cht WHERE id=".$id;
	$result2=mysql_query($req2,$link);	
}
function update_sstraitantcht($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE soustraitant_cht SET ".$chp."='".$val."' WHERE id=".$id;
	$result2=mysql_query($req2,$link);	
}
function lst_sstraitantcht($idcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button id='add_sstraitantcht' data-idcht='".$idcht."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th>Entreprises</th><th>Montant</th></tr></thead>";
	echo "<tbody>";
	$reqcht="SELECT soustraitant_cht.id,soustraitant_cht.montant,soustraitant_cht.id_ent FROM soustraitant_cht WHERE id_cht=".$idcht;
	$resultcht=mysql_query($reqcht,$link);
	$rowcht=mysql_num_rows($resultcht);
	if ($rowcht!=0)
	{
		while ($data=mysql_fetch_assoc($resultcht))
		{
			echo '<tr><td align="center" width="8%"><button data-id="'.$data["id"].'" class="btn btn-sm btn-default del_sstraitantcht"><i class="fa fa-trash"></i></button></td><td align="center">';
			echo '<select class="form-control input-sm update_sstraitantcht" data-chp="id_ent" data-id="'.$data["id"].'" id="lstsstraitantcht" name="lstsstraitantcht">';
			echo '<option value="-1">S�lectionnez une entreprise</option>';
			$req="SELECT ent_id,ent_nom FROM entreprises";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["ent_id"]==$data["id_ent"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
			}
			echo '</select></td>';			
			echo '<td width="10%" class="text-center"><input data-chp="montant" data-id="'.$data["id"].'" class="form-control input-sm text-right update_sstraitantcht" value="'.$data["montant"].'"></td></tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="3"><b>Aucun sous-traitant</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function detailapd_chantier($idcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT apd.id FROM apd WHERE apd.id_cht=".$idcht;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$idapd = $ligne["id"];
	}
	$req="SELECT nature.nat_codenumero FROM chantiers JOIN nature ON chantiers.cht_nature=nature.nat_libelle WHERE chantiers.cht_id=".$idcht;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$nat = $ligne["nat_codenumero"];
	}
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-2" for="lstbordapd">Bordereaux:</label>';
	echo '<div class="col-sm-6">';
	echo '<select class="form-control input-sm" id="lstbordapd" name="lstbordapd" data-idapd="'.$idapd.'">';
	echo '<option value="-1">S�lectionnez un bordereau</option>';
	$req="SELECT id,libelle,date_cloture FROM bordereau WHERE type='FACE' AND nature LIKE '%".$nat."%'";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["date_cloture"]=="0000-00-00"){$title = $ligne["libelle"];}else{$title = $ligne["libelle"]." [CLOTURE]";}
		echo '<option value="'.$ligne["id"].'" data-idapd="'.$idapd.'">'.$title.'</option>';
	}
	echo '</select>';
	echo '</div>';
	echo '</div>';
	echo '</form>';
	/* Liste des articles du bordereau selectionn� */
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">Articles</div>';
	echo '<div class="panel-body" id="lstarticlesapd" style="max-height: 200px;overflow-y: scroll;">';
		lst_article_bord("-1",$idapd);
	echo '</div>';
	echo '</div>';
	/* Liste des articles valoris�s */
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">Valorisation</div>';
	echo '<div class="panel-body" id="lstvalapd">';
		lst_val_apd($idapd);
	echo '</div>';
	echo '</div>';

}
function lst_article_bord($idbord,$idapd){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM articles WHERE articles.id_bord=".$idbord;
	$result=mysql_query($req,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='5%'></th><th>Libell�</th><th width='10%'>P.U.</th></tr></thead>";
	echo "<tbody>";
	$rowart = mysql_num_rows($result);
	if ($rowart!=0){
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo '<tr><td width="5%" class="text-center"><button data-idarticle="'.$ligne["id"].'" data-idapd="'.$idapd.'" class="btn btn-sm btn-default add_valapd"><i class="fa fa-plus-circle"></i></button></td><td>'.$ligne["libelle"].'</td><td width="10%" class="text-center">'.$ligne["pu"].'</td></tr>';
		}
	}else{
		echo '<tr><td class="text-center" colspan="3"><b>Aucun article</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function add_valapd($idapd,$idart){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="INSERT INTO val_apd (id_apd,id_art) VALUES (".$idapd.",".$idart.")";
	$result=mysql_query($req,$link);
}
function lst_val_apd($idapd){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT val_apd.id,articles.libelle,articles.pu,val_apd.qte,val_apd.tot,val_apd.longueur,val_apd.puissance,val_apd.DMAR,val_apd.DIDCAC,val_apd.RHTA FROM val_apd JOIN articles ON articles.id=val_apd.id_art WHERE val_apd.id_apd=".$idapd;
	$result=mysql_query($req,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='5%'></th><th>Libell�</th><th>Qte</th><th width='10%'>P.U.</th><th>Montant</th><th>Longueur</th><th>Puissance</th><th>DMAR</th><th>DIDCAC</th><th>RHTA</th></tr></thead>";
	echo "<tbody>";
	$rowvalapd = mysql_num_rows($result);
	if ($rowvalapd!=0){
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo '<tr><td width="5%" class="text-center"><button data-idvalapd="'.$ligne["id"].'" data-idapd="'.$idapd.'" class="btn btn-sm btn-default del_valapd"><i class="fa fa-trash"></i></button></td><td>'.$ligne["libelle"].'</td><td width="10%"><input data-chp="qte" data-idvalapd="'.$ligne["id"].'" data-idapd="'.$idapd.'" class="form-control input-sm text-center update_valapd" value="'.$ligne["qte"].'"></td><td width="10%" class="text-center">'.number_format($ligne["pu"], 2, ',', ' ').'</td><td width="10%"><input data-chp="tot" data-idvalapd="'.$ligne["id"].'" data-idapd="'.$idapd.'" class="form-control input-sm text-right update_valapd" value="'.$ligne["tot"].'"></td><td width="10%"><input data-chp="longueur" data-idvalapd="'.$ligne["id"].'" data-idapd="'.$idapd.'" class="form-control input-sm text-center update_valapd" value="'.$ligne["longueur"].'"></td><td width="10%"><input data-chp="puissance" data-idvalapd="'.$ligne["id"].'" data-idapd="'.$idapd.'" class="form-control input-sm text-center update_valapd" value="'.$ligne["puissance"].'"></td><td width="10%"><input data-chp="DMAR" data-idvalapd="'.$ligne["id"].'" data-idapd="'.$idapd.'" class="form-control input-sm text-center update_valapd" value="'.$ligne["DMAR"].'"></td><td width="10%"><input data-chp="DIDCAC" data-idvalapd="'.$ligne["id"].'" data-idapd="'.$idapd.'" class="form-control input-sm text-center update_valapd" value="'.$ligne["DIDCAC"].'"></td><td width="10%"><input data-chp="RHTA" data-idvalapd="'.$ligne["id"].'" data-idapd="'.$idapd.'" class="form-control input-sm text-center update_valapd" value="'.$ligne["RHTA"].'"></td></tr>';
		}
	}else{
		echo '<tr><td class="text-center" colspan="10"><b>Aucune valorisation</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
	$req="SELECT * FROM apd WHERE apd.id=".$idapd;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result)){
		$estimation = $ligne["estimation_HT"];
		$tauxactu = $ligne["tx_actu"];
		$totactu = $ligne["m_actu"];
		$tauxsav = $ligne["tx_sav"];
		$totsav = $ligne["m_sav"];
		$tauxdivers = $ligne["tx_divers"];
		$totdivers = $ligne["m_divers"];
		$totht = $ligne["tot_ht"];
		$tva = $ligne["tva"];
		$totttc = $ligne["tot_ttc"];
	}

	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="pwd">Estimation TVX HT:</label>';
	echo '<div class="col-sm-4">';
	echo '<input class="form-control input-sm text-right" value="'.$estimation.'" disabled>';
	echo '</div>';
	echo '</div>';
	/* Actualisation */
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-2" for="pwd">Actualisation (%):</label>';
	echo '<div class="col-sm-2">';
	echo '<input data-chp="tx_actu" data-type="actu" data-idapd="'.$idapd.'" class="form-control input-sm text-center update_tauxapd" value="'.$tauxactu.'">';
	echo '</div>';
	echo '<div class="col-sm-4">';
	echo '<input class="form-control input-sm text-right" value="'.$totactu.'" disabled>';
	echo '</div>';
	echo '</div>';
	/* SAV */
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-2" for="pwd">SAV (%):</label>';
	echo '<div class="col-sm-2">';
	echo '<input data-chp="tx_sav" data-type="sav" data-idapd="'.$idapd.'" class="form-control input-sm text-center update_tauxapd" value="'.$tauxsav.'">';
	echo '</div>';
	echo '<div class="col-sm-4">';
	echo '<input class="form-control input-sm text-right" value="'.$totsav.'" disabled>';
	echo '</div>';
	echo '</div>';
	/* Divers */
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-2" for="pwd">Divers (%):</label>';
	echo '<div class="col-sm-2">';
	echo '<input data-chp="tx_divers" data-type="divers" data-idapd="'.$idapd.'" class="form-control input-sm text-center update_tauxapd" value="'.$tauxdivers.'">';
	echo '</div>';
	echo '<div class="col-sm-4">';
	echo '<input class="form-control input-sm text-right" value="'.$totdivers.'" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="pwd">Total HT:</label>';
	echo '<div class="col-sm-4">';
	echo '<input class="form-control input-sm text-right" value="'.$totht.'" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="pwd">TVA:</label>';
	echo '<div class="col-sm-4">';
	echo '<input class="form-control input-sm text-right" value="'.$tva.'" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="pwd">Total TTC:</label>';
	echo '<div class="col-sm-4">';
	echo '<input class="form-control input-sm text-right" value="'.$totttc.'" disabled>';
	echo '</div>';
	echo '</div>';

	echo '</form>';

}
function del_valapd($idvalapd){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT tva.tva_taux FROM tva ORDER BY tva_datetva DESC LIMIT 1";
	$result=mysql_query($req,$link);	
	while ($ligne=mysql_fetch_assoc($result)){
		$taux_tva = $ligne["tva_taux"];
	}
	$req="SELECT apd.id,apd.m_actu,apd.m_sav,apd.m_divers FROM val_apd JOIN apd ON apd.id=val_apd.id_apd WHERE val_apd.id=".$idvalapd;
	$result=mysql_query($req,$link);	
	while ($ligne=mysql_fetch_assoc($result)){
		$id_apd = $ligne["id"];
		$m_actu = $ligne["m_actu"];
		$m_sav = $ligne["m_sav"];
		$m_divers = $ligne["m_divers"];
	}
	$req="DELETE FROM val_apd WHERE val_apd.id=".$idvalapd;
	$result=mysql_query($req,$link);
	$req="SELECT val_apd.tot FROM val_apd WHERE val_apd.id_apd=".$id_apd;
	$result=mysql_query($req,$link);	
	while ($ligne=mysql_fetch_assoc($result)){
		$estimation = $estimation + $ligne["tot"];
	}
	$val = $estimation;
	$totHT = $estimation + $m_actu + $m_sav + $m_divers;
	$tva = $totHT * $taux_tva / 100;
	$totTTC = $totHT + $tva;
	$req2="UPDATE apd SET estimation_HT='".$val."',tva='".$tva."',tot_HT='".$totHT."',tot_TTC='".$totTTC."' WHERE id=".$id_apd;
	$result2=mysql_query($req2,$link);	
}
function update_valapd($idvalapd,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE val_apd SET ".$chp."='".$val."' WHERE id=".$idvalapd;
	$result2=mysql_query($req2,$link);	
	if ($chp=="tot"){
		$req="SELECT tva.tva_taux FROM tva ORDER BY tva_datetva DESC LIMIT 1";
		$result=mysql_query($req,$link);	
		while ($ligne=mysql_fetch_assoc($result)){
			$taux_tva = $ligne["tva_taux"];
		}
		$req="SELECT apd.id,apd.m_actu,apd.m_sav,apd.m_divers FROM val_apd JOIN apd ON apd.id=val_apd.id_apd WHERE val_apd.id=".$idvalapd;
		$result=mysql_query($req,$link);	
		while ($ligne=mysql_fetch_assoc($result)){
			$id_apd = $ligne["id"];
			$m_actu = $ligne["m_actu"];
			$m_sav = $ligne["m_sav"];
			$m_divers = $ligne["m_divers"];
		}
		$req="SELECT val_apd.tot FROM val_apd WHERE val_apd.id_apd=".$id_apd;
		$result=mysql_query($req,$link);	
		while ($ligne=mysql_fetch_assoc($result)){
			$estimation = $estimation + $ligne["tot"];
		}
		$val = $estimation;
		$totHT = $estimation + $m_actu + $m_sav + $m_divers;
		$tva = $totHT * $taux_tva / 100;
		$totTTC = $totHT + $tva;
		$req2="UPDATE apd SET estimation_HT='".$val."',tva='".$tva."',tot_HT='".$totHT."',tot_TTC='".$totTTC."' WHERE id=".$id_apd;
		$result2=mysql_query($req2,$link);	
	}
}
function update_tauxapd($idapd,$chp,$val,$type){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT apd.estimation_HT,apd.m_actu,apd.m_sav,apd.m_divers FROM apd WHERE apd.id=".$idapd;
	$result=mysql_query($req,$link);	
	while ($ligne=mysql_fetch_assoc($result)){
		$estimation = $ligne["estimation_HT"];
		$m_actu = $ligne["m_actu"];
		$m_sav = $ligne["m_sav"];
		$m_divers = $ligne["m_divers"];
	}
	switch($type){
		case "actu":
			$montant = ($estimation * (float)($val)) / 100;
			break;
		case "sav":
			$montant = (($estimation+$m_actu) * (float)($val)) / 100;
			break;
		case "divers":
			$montant = (($estimation+$m_actu+$m_divers) * (float)($val)) / 100;
			break;
	}
	$req2="UPDATE apd SET ".$chp."='".$val."',m_".$type."='".$montant."' WHERE id=".$idapd;
	$result2=mysql_query($req2,$link);	
	$req="SELECT tva.tva_taux FROM tva ORDER BY tva_datetva DESC LIMIT 1";
	$result=mysql_query($req,$link);	
	while ($ligne=mysql_fetch_assoc($result)){
		$taux_tva = $ligne["tva_taux"];
	}
	$req="SELECT apd.id,apd.m_actu,apd.m_sav,apd.m_divers FROM apd WHERE apd.id=".$idapd;
	$result=mysql_query($req,$link);	
	while ($ligne=mysql_fetch_assoc($result)){
		$id_apd = $ligne["id"];
		$m_actu = $ligne["m_actu"];
		$m_sav = $ligne["m_sav"];
		$m_divers = $ligne["m_divers"];
	}
	$totHT = $estimation + $m_actu + $m_sav + $m_divers;
	$tva = $totHT * $taux_tva / 100;
	$totTTC = $totHT + $tva;
	$req2="UPDATE apd SET tva='".$tva."',tot_HT='".$totHT."',tot_TTC='".$totTTC."' WHERE id=".$id_apd;
	$result2=mysql_query($req2,$link);	
}

/* PCT */
function lst_bordpct($page,$loginca,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	
	/* Recherche des droits de l'utilisateur */
	$retour_droit=mysql_query('SELECT user.user_droit FROM user where user.user_login="'.$loginca.'"');
	$donnees_droit=mysql_fetch_assoc($retour_droit);
	$droit=$donnees_droit['user_droit'];

	$messagesParPage=10;
	if ($chp!=""){
		$retour_total=mysql_query('SELECT COUNT(*) AS total FROM pct_bord where '.$chp.' like "%'.$val.'%"');
		$donnees_total=mysql_fetch_assoc($retour_total);
	}else{
		if ($droit!="USER"){
			$retour_total=mysql_query('SELECT COUNT(*) AS total FROM pct_bord');
			$donnees_total=mysql_fetch_assoc($retour_total);
		}else{
			$retour_total=mysql_query('SELECT COUNT(*) AS total FROM pct_bord');
			$donnees_total=mysql_fetch_assoc($retour_total);
		}
	}
	$total=$donnees_total['total'];
	$nombreDePages=ceil($total/$messagesParPage);
	if(isset($page))
	{
		$pageActuelle=intval($page);

		if($pageActuelle>$nombreDePages)
		{
			$pageActuelle=$nombreDePages;
		}
	}else{
		$pageActuelle=1;
	}

	$premiereEntree=($pageActuelle-1)*$messagesParPage;

	if ($chp!=""){
		$req2="SELECT pct_bord.pct_numero, pct_bord.pct_libelle, pct_bord.pct_montant, pct_bord.pct_dateenvoi, pct_bord.pct_id FROM pct_bord WHERE ".$chp." LIKE '%".$val."%' ORDER BY pct_bord.pct_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
	}else{
		if ($droit!="USER"){		
			$req2="SELECT pct_bord.pct_numero, pct_bord.pct_libelle, pct_bord.pct_montant, pct_bord.pct_dateenvoi,pct_bord.pct_id FROM pct_bord ORDER BY pct_bord.pct_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
		}else{
			$req2="SELECT pct_bord.pct_numero, pct_bord.pct_libelle, pct_bord.pct_montant, pct_bord.pct_dateenvoi,pct_bord.pct_id FROM pct_bord ORDER BY pct_bord.pct_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
		}
	}
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	$compteope = 1;
	if ($pageActuelle>1){
		echo "<div><div class='forwardpct btnpager'><i class='fa fa-caret-left' aria-hidden='true'></i></div>";
	}
	echo "<div class='boxpager'>".$pageActuelle." / ".$nombreDePages."</div>";
	if ($pageActuelle<$nombreDePages){
		echo "<div class='nextpct btnpager'><i class='fa fa-caret-right' aria-hidden='true'></i></div></div>";
	}
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	if ($droit!="USER"){
		echo "<thead><tr><th width='10%'></th><th class='text-center'>CA</th><th class='text-center' width='10%'>Num�ro</th><th class='text-center'>Libell�</th><th class='text-center' width='10%'>Montant</th><th class='text-center' width='10%'>Date d'envoi</th></tr></thead>";
	}else{
		$reqdeleg="SELECT * FROM operations WHERE operations.ope_delegation='".$loginca."'";
		$resultdeleg=mysql_query($reqdeleg,$link);
		$rowdeleg=mysql_num_rows($resultdeleg);
		if ($rowdeleg!=0){
			echo "<thead><tr><th width='10%'></th><th class='text-center' width='5%'><i class='fas fa-handshake' title='D�l�gation'></i></th><th class='text-center' width='10%'>Num�ro</th><th class='text-center'>Libell�</th><th class='text-center' width='10%'>Montant</th><th class='text-center' width='10%'>Date d'envoi</th></tr></thead>";
		}else{
			echo "<thead><tr><th width='10%'></th><th class='text-center' width='10%'>Num�ro</th><th class='text-center'>Libell�</th><th class='text-center' width='10%'>Montant</th><th class='text-center' width='10%'>Date d'envoi</th></tr></thead>";
		}
	}
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			/* Recherche Bon de commande rattach� pour activation des boutons */
			$reqchtbc="SELECT * FROM demandebc_chantiers JOIN chantiers ON chantiers.cht_id=demandebc_chantiers.id_cht WHERE chantiers.cht_idope=".$ligne2["id"];
			$resultchtbc=mysql_query($reqchtbc,$link);
			$rowchtbc=mysql_num_rows($resultchtbc);
			if ($rowchtbc!=0){$buttons = "disabled";}else{$buttons = "";}
			if ($ligne2["pct_dateenvoi"]!="0000-00-00 00:00:00"){$date_envoi=strftime("%d/%m/%Y",strtotime($ligne2["pct_dateenvoi"]));}else{$date_envoi="";}
			echo "<tr><td align='center' width='10%'><button data-tooltip='Visualiser' data-id='".$ligne2["pct_id"]."' class='btn btn-sm btn-default visubordpct'><i class='fa fa-eye'></i></button> <button data-tooltip='Supprimer' data-id='".$ligne2["pct_id"]."' class='btn btn-sm btn-default delbordpct' ".$buttons."><i class='fa fa-trash'></i></button></td>";
			if ($droit!="USER"){
				echo "<td align='center' width='5%'><i class='fa fa-user'></i><br>".$ligne2["user_initial"]."</td>";
			}
			if ($rowdeleg!=0){ 
				if ($ligne2["delegation"]!=""){
					echo "<td align='center' width='5%'><i class='fa fa-user'></i><br>".$ligne2["user_initial"]."</td>";
				}else{
					echo "<td align='center' width='5%'></td>";
				}
			}
			echo "<td align='center' width='10%'>".$ligne2["pct_numero"]."</td><td>".$ligne2["pct_libelle"]."</td><td width='10%' class='text-right'>".number_format($ligne2["pct_montant"], 2, ',', ' ')." �</td><td width='10%' class='text-center'>".$date_envoi."</td>";
			echo "</tr>";
		}
	}else{
		echo "<tr><td colspan='6' align='center'><b>Aucun bordereau PCT</b></td></tr>";
	}
	echo "</tbody>";
	echo "</table>";
}
function detail_bordpct($id){
	require("./compte.php");
	require("./print.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT pct_numero, pct_libelle, pct_id FROM pct_bord WHERE pct_id=".$id;
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{

			$panel=1;
			echo "<div class='col-md-12'>";
			echo "<div class='portlet'><div class='portlet-title'><i class='fas fa-folder-open' aria-hidden='true'></i> ".$ligne2["pct_numero"]." - ".$ligne2["pct_libelle"]."</div><div class='portlet-content' id='detbordpct'>";
			echo "<div class='row'>";

			echo '<div class="col-sm-12">';
			echo "<button id='back_lstbordpct' class='btn btn-default btn-sm' data-idope='".$ligne2["pct_id"]."'><i class='fa fa-reply'></i> Bordereaux</button>";
			echo '</div><br><br>';

			echo '<div class="col-sm-12">';
			echo '<div class="panel-group" id="accordion">';
			/* G�n�ralit�s */
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h2 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-file" aria-hidden="true"></i> G�n�ralit�s</a>';
			echo '</h2>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detailgene_operation">';
				detailgene_bordpct($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Op�rations rattach�es */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-copy" aria-hidden="true"></i> Op�rations rattach�es</a>';
			echo '</h4>';
			echo "";
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body">';
				lstoperation_bordpct($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			
			echo '</div>';
			echo '</div>';
		}
	}
}
function detailgene_bordpct($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM pct_bord WHERE pct_id=".$id;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$datecreate = strftime("%d/%m/%Y",strtotime($ligne["pct_datecreate"]));
		$numero = $ligne["pct_numero"];
		$libelle = $ligne["pct_libelle"];
	}
	echo '<div class="col-sm-12">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">G�n�ralit�s</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_datecreate">Date cr�ation:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="ope_datecreate" name="ope_datecreate" value="'.$datecreate.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_refsehv">R�f�rence SEHV:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="ope_refsehv" name="ope_refsehv" value="'.$numero.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="ope_libelle">Libell�:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="libelle" id="ope_libelle" name="ope_libelle" value="'.$libelle.'" '.$disabled.'>';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function lstoperation_bordpct($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT DISTINCT operations.ope_id,operations.ope_numero,operations.ope_libelle,collectivites.coll_nom as nomcom FROM operations JOIN chantiers ON chantiers.cht_idope=operations.ope_id LEFT JOIN collectivites ON collectivites.coll_codeinsee=operations.ope_codeinsee LEFT JOIN annee_prog ON annee_prog.anneeprog_id=chantiers.cht_idanneeprog LEFT JOIN programme ON programme.prog_id=annee_prog.anneeprog_idprog WHERE programme.prog_libelle='HP' AND operations.ope_categorie='Raccordement' ORDER BY operations.ope_datecreate DESC";
	$result=mysql_query($req,$link);
	echo '<table class="table table-responsive table-bordered table-striped table-hover">';
	echo '<thead><tr><th width="10%"></th><th width="10%">Num�ro</th><th>Commune</th><th>Libell�</th></tr></thead>';
	echo '<tbody>';
	while ($ligne=mysql_fetch_assoc($result))
	{
		//$datecreate = strftime("%d/%m/%Y",strtotime($ligne["date_create"]));
		echo '<tr><td width="10%" class="text-center"><button class="btn btn-default btn-sm" data-tooltip="Fiche PCT"><i class="fa fa-file"></i></button></td><td width="10%" class="text-center">'.$ligne["ope_numero"].'</td><td width="20%" class="text-center">'.$ligne["nomcom"].'</td><td>'.$ligne["ope_libelle"].'</td></tr>';
	}
	echo '</tbody>';
	echo '</table>';
	echo '<div><button class="btn btn-default btn-sm" id="btn_generepctbord"><i class="fa fa-cogs"></i> G�n�rer</button></div>';
}
function detail_pctoperation($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT ope_numAU,ope_longueuraerien,ope_longueursout FROM operations WHERE ope_id=".$idope;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$num_AU = $ligne["ope_numAU"];
		$longueur_aerien = $ligne["ope_longueuraerien"];
		$longueur_sout = $ligne["ope_longueursout"];
	}
	$req="SELECT SUM(apd.estimation_HT) as sum_apd FROM operations JOIN chantiers ON chantiers.cht_idope=operations.ope_id LEFT JOIN apd ON apd.id_cht=chantiers.cht_id LEFT JOIN annee_prog ON annee_prog.anneeprog_id=chantiers.cht_idanneeprog LEFT JOIN programme ON programme.prog_id=annee_prog.anneeprog_idprog WHERE programme.prog_libelle='HP' AND operations.ope_id=".$idope;
	$result=mysql_query($req,$link);
	$ligne=mysql_fetch_assoc($result);
	$cout_reel_provisoire = $ligne["sum_apd"];
	echo '<div class="col-sm-12">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">G�n�ralit�s</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="pct_numau">Num�ro AU:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_numAU" id="pct_numau" name="pct_numau" value="'.$num_AU.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="pct_longueuraerien">Longueur A�rien:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_longueuraerien" id="pct_longueuraerien" name="pct_longueuraerien" value="'.$longueur_aerien.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="pct_longueursout">Longueur Souterrain:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpoperation" data-idope="'.$idope.'" data-chp="ope_longueursout" id="pct_longueursout" name="pct_longueursout" value="'.$longueur_sout.'" '.$disabled.'>';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	$PctTauxMOO=0;$D=0;$Pc=0;$Pd=0;$PctTauxRef=0;
	$param = array("PctTauxMOO","D","Pc","Pd","PctTauxRef","PUExtAeri","PUExtSout");
	for ($i=0;$i<count($param);$i++){
		$req="SELECT * FROM parametres WHERE libelle='".$param[$i]."' AND section='PCT' ORDER BY date_valeur DESC";
		$result=mysql_query($req,$link);
		$ligne=mysql_fetch_assoc($result);
		switch ($ligne['libelle']){
			case "PctTauxMOO":
				$PctTauxMOO = $ligne["valeur"];
				break;
			case "D":
				$D = $ligne["valeur"];
				break;
			case "Pc":
				$Pc = $ligne["valeur"];
				break;
			case "Pd":
				$Pd = $ligne["valeur"];
				break;
			case "PctTauxRef":
				$PctTauxRef = $ligne["valeur"];
				break;
			case "PUExtAeri":
				$PUExtAeri = $ligne["valeur"];
				break;
			case "PUExtSout":
				$PUExtSout = $ligne["valeur"];
				break;
		}
	}
	/* Calculs */
	$cout_reel_estime = ($longueur_aerien * $PUExtAeri)+($longueur_sout * $PUExtSout);
	$Mont_MOO = $cout_reel_estime * $PctTauxMOO /100;
	$cout_racc = $cout_reel_estime + $Mont_MOO;
	$pct = $cout_racc * $PctTauxRef /100;
	$terme_ajustement = (0.74*(1+$Pc/$Pd)*(0.005*$D+0.125)-($PctTauxRef/100));
	$complement_pct = number_format($cout_racc * $terme_ajustement, 2, '.', '');
	$pct_demandee = $pct + $complement_pct;
	/* Intitul�s */
	echo '<div class="columns intitule">';
	echo '<ul class="price">';
	echo '<li class="header">PCT</li>';
	echo '<li>Co�t r�el expos� H.T.</li>';
	echo '<li>Taux Ma�trise d\'oeuvre et ouvrage</li>';
	echo '<li>Montant Ma�trise d\'oeuvre et ouvrage</li>';
	echo '<li>Co�t de raccordement</li>';
	echo '<li>Taux de r�faction tarifaire</li>';
	echo '<li>Part couverte par le tarif</li>';
	echo '<li>D</li>';
	echo '<li>Pd</li>';
	echo '<li>Pc</li>';
	echo '<li>Terme d\'ajustement</li>';
	echo '<li>Compl�ment de PCT</li>';
	echo '<li class="grey">PCT demand�e</li>';
	echo '</ul>';
	echo '</div>';
	/* Estim�e */
	echo '<div class="columns">';
	echo '<ul class="price">';
	echo '<li class="header">Estim�e</li>';
	echo '<li><span class="themepct">Co�t r�el expos� H.T.</span>'.$cout_reel_estime.'</li>';
	echo '<li><span class="themepct">Taux Ma�trise d\'oeuvre et ouvrage</span>'.$PctTauxMOO.'</li>';
	echo '<li><span class="themepct">Montant Ma�trise d\'oeuvre et ouvrage</span>'.$Mont_MOO.'</li>';
	echo '<li><span class="themepct">Co�t de raccordement</span>'.$cout_racc.'</li>';
	echo '<li><span class="themepct">Taux de r�faction tarifaire</span>'.$PctTauxRef.'</li>';
	echo '<li><span class="themepct">Part couverte par le tarif</span>'.$pct.'</li>';
	echo '<li><span class="themepct">D</span>'.$D.'</li>';
	echo '<li><span class="themepct">Pd</span>'.$Pd.'</li>';
	echo '<li><span class="themepct">Pc</span>'.$Pc.'</li>';
	echo '<li><span class="themepct">Terme d\'ajustement</span>'.$terme_ajustement.'</li>';
	echo '<li><span class="themepct">Compl�ment de PCT</span>'.$complement_pct.'</li>';
	echo '<li class="grey"><span class="themepct">PCT demand�e</span>'.$pct_demandee.'<button class="btn btn-default btn-sm pull-right"><i class="fa fa-calculator"></i></button></li>';
	echo '</ul>';
	echo '</div>';
	/* Provisoire */
	echo '<div class="columns">';
	echo '<ul class="price">';
	echo '<li class="header">Provisoire</li>';
	echo '<li><span class="themepct">Co�t r�el expos� H.T.</span>'.$cout_reel_provisoire.'</li>';
	echo '<li><span class="themepct">Taux Ma�trise d\'oeuvre et ouvrage</span>'.$PctTauxMOO.'</li>';
	echo '<li><span class="themepct">Montant Ma�trise d\'oeuvre et ouvrage</span>0</li>';
	echo '<li><span class="themepct">Co�t de raccordement</span>0</li>';
	echo '<li><span class="themepct">Taux de r�faction tarifaire</span>'.$PctTauxRef.'</li>';
	echo '<li><span class="themepct">Part couverte par le tarif</span>0</li>';
	echo '<li><span class="themepct">D</span>'.$D.'</li>';
	echo '<li><span class="themepct">Pd</span>'.$Pd.'</li>';
	echo '<li><span class="themepct">Pc</span>'.$Pc.'</li>';
	echo '<li><span class="themepct">Terme d\'ajustement</span>0</li>';
	echo '<li><span class="themepct">Compl�ment de PCT</span>0</li>';
	echo '<li class="grey"><span class="themepct">PCT demand�e</span>0<button class="btn btn-default btn-sm pull-right"><i class="fa fa-calculator"></i></button></li>';
	echo '</ul>';
	echo '</div>';
	/* D�finitive */
	echo '<div class="columns">';
	echo '<ul class="price">';
	echo '<li class="header">D�finitive</li>';
	echo '<li><span class="themepct">Co�t r�el expos� H.T.</span>0</li>';
	echo '<li><span class="themepct">Taux Ma�trise d\'oeuvre et ouvrage</span>'.$PctTauxMOO.'</li>';
	echo '<li><span class="themepct">Montant Ma�trise d\'oeuvre et ouvrage</span>0</li>';
	echo '<li><span class="themepct">Co�t de raccordement</span>0</li>';
	echo '<li><span class="themepct">Taux de r�faction tarifaire</span>'.$PctTauxRef.'</li>';
	echo '<li><span class="themepct">Part couverte par le tarif</span>0</li>';
	echo '<li><span class="themepct">D</span>'.$D.'</li>';
	echo '<li><span class="themepct">Pd</span>'.$Pd.'</li>';
	echo '<li><span class="themepct">Pc</span>'.$Pc.'</li>';
	echo '<li><span class="themepct">Terme d\'ajustement</span>0</li>';
	echo '<li><span class="themepct">Compl�ment de PCT</span>0</li>';
	echo '<li class="grey"><span class="themepct">PCT demand�e</span>0<button class="btn btn-default btn-sm pull-right"><i class="fa fa-calculator"></i></button></li>';
	echo '</ul>';
	echo '</div>';
}
/* Beneficiaires */
function detail_opebeneficiaire($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th width='10%' class='text-center'><button data-tooltip='Ajouter un b�n�ficiaire' id='add_opebeneficiaire' data-idope='".$idope."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th class='text-center' width='15%'>Nom</th><th width='15%' class='text-center'>Adresse</th><th class='text-center' width='5%'>Code postal</th><th class='text-center' width='15%'>Ville</th><th class='text-center' width='15%'>Mail</th><th class='text-center' width='5%'>Longueur</th><th class='text-center' width='15%'>R�gime alimentation</th></tr></thead>";
	echo "<tbody>";
	$req="SELECT * FROM ope_beneficiaire WHERE opebenef_idope=".$idope;
	$result=mysql_query($req,$link);
	$row = mysql_num_rows($result);
	$totlot=0;
	if ($row!=0)
	{
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo "<tr><td align='center' width='10%'><button data-tooltip='Supprimer' data-id='".$ligne["opebenef_id"]."' data-idope='".$idope."' class='btn btn-sm btn-default delopebeneficiaire'><i class='fa fa-trash'></i></button></td><td class='text-center' width='15%'><input type='text' class='form-control input-sm modifchpopebeneficiaire' data-id='".$ligne["opebenef_id"]."' data-idope='".$idope."' data-chp='opebenef_nom' value='".$ligne["opebenef_nom"]."'></td><td class='text-center' width='15%'><input type='text' class='form-control input-sm modifchpopebeneficiaire' data-id='".$ligne["opebenef_id"]."' data-idope='".$idope."' data-chp='opebenef_adresse' value='".$ligne["opebenef_adresse"]."'></td><td class='text-center' width='5%'><input type='text' class='form-control input-sm text-center modifchpopebeneficiaire' data-id='".$ligne["opebenef_id"]."' data-idope='".$idope."' data-chp='opebenef_codepostal' value='".$ligne["opebenef_codepostal"]."'></td><td class='text-center' width='15%'><input type='text' class='form-control input-sm text-center modifchpopebeneficiaire' data-id='".$ligne["opebenef_id"]."' data-idope='".$idope."' data-chp='opebenef_ville' value='".$ligne["opebenef_ville"]."'></td><td class='text-center' width='15%'><input type='text' class='form-control input-sm text-center modifchpopebeneficiaire' data-id='".$ligne["opebenef_id"]."' data-idope='".$idope."' data-chp='opebenef_mail' value='".$ligne["opebenef_mail"]."'></td><td class='text-center' width='5%'><input type='text' class='form-control input-sm text-center modifchpopebeneficiaire' data-id='".$ligne["opebenef_id"]."' data-idope='".$idope."' data-chp='opebenef_longueur' value='".$ligne["opebenef_longueur"]."'></td>";
			echo '<td class="text-center" width="15%"><select class="form-control input-sm modifchpopebeneficiaire" data-id="'.$ligne["opebenef_id"].'" data-idope="'.$idope.'" data-chp="opebenef_regimealim">';
			echo '<option value="0">Choisir un type</option>';
			$req2="SELECT * FROM lexique WHERE lex_codelexique='REG_ALIM' AND lex_typelexique!='COM'";
			$result2=mysql_query($req2,$link);
			while ($ligne2=mysql_fetch_assoc($result2))
			{
				if ($ligne2["lex_libelle"]==$ligne["opebenef_regimealim"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$ligne2["lex_libelle"].'" '.$selected.'>'.$ligne2["lex_libelle"].'</option>';
			}		
			echo '</select></td>';
			echo "</tr>";
		}
	}else{
		echo "<tr><td colspan='8' class='text-center'><b>Aucun b�n�ficiaire</b></td></tr>";
	}
	echo "</tbody>";
	echo "</table>";
}
function add_opebeneficiaire($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql="INSERT INTO ope_beneficiaire (opebenef_idope) VALUES(".$idope.")";	
	$result=mysql_query($sql,$link);
}
function del_opebeneficiaire($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql="DELETE FROM ope_beneficiaire WHERE opebenef_id=".$id;	
	$result=mysql_query($sql,$link);
}
function update_opebeneficiaire($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE ope_beneficiaire SET ".$chp."='".utf8_decode($val)."' WHERE opebenef_id=".$id;
	$result2=mysql_query($req2,$link);	
}
/* DT */
function create_emprisedt($idope,$file_emprise){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT ope_codeinsee,ope_numero FROM operations WHERE ope_id=".$idope;
	$result=mysql_query($req,$link);
	$ligne=mysql_fetch_assoc($result);
	$numope = $ligne["ope_numero"];
	$code_insee = $ligne["ope_codeinsee"];
	$uploads_dir="../dt";
    
    if (file_exists("$uploads_dir/$code_insee")==false){mkdir("$uploads_dir/$code_insee", 0755);}
    $filename_user = "$uploads_dir/$code_insee/$numope.txt";
    $filename = "$uploads_dir/$code_insee/$numope.xml";
    touch($filename);
    chmod($filename_user, 0777);
    chmod($filename, 0777);
	
	$file_emprise = str_replace("\n"," ",$file_emprise); 
	$file_emprise = str_replace("\r"," ",$file_emprise);
	$file_emprise = str_replace("  ", " ", $file_emprise);
	$traitement = explode(";", $file_emprise);
	$emprise = "";
	for ($i=0;$i<count($traitement);$i++){
		echo $traitement[$i];
		$toto = str_replace("(", "", $traitement[$i]);
		$toto = str_replace(")", "", $toto);
		$totobis = explode(" ", $toto);
		if ($emprise!=""){$emprise .= ",".$totobis[1]." ".$totobis[0];}else{$emprise .= $totobis[1]." ".$totobis[0];}
	}

    $content = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><dt><emprise>POLYGON(('.$emprise.'))</emprise></dt>';
    file_put_contents($filename, $content);
    unlink($filename_user);
}