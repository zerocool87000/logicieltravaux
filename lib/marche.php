<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(isset($_POST['action']) && !empty($_POST['action'])) {
	$action = $_POST['action'];
	switch($action) {
		case 'lstdemandebc' : lst_demandebc();break;
		case 'lstdemandeos' : lst_demandeos();break;
		case 'lstnaturepaiement' : lst_naturepaiement();break;
		case 'lsttypeos' : lst_typeos();break;
		case 'lsttypetranche' : lst_typetranche();break;
		case 'convertdemandebc' : convert_demandebc($_POST["iddembc"],$_POST["numbc"],$_POST["nummarche"],$_POST["numlot"]);break;
		case 'convertdemandeos' : convert_demandeos($_POST["iddemos"]);break;
		case 'lstboncommande' : lst_boncommande($_POST["page"],$_POST["login"],$_POST["chp"],$_POST["val"]);break;
		case 'lstmarche' : lst_marche($_POST["page"],$_POST["login"],$_POST["chp"],$_POST["val"]);break;
		case 'lstnouvbcmarche' : lstnouvbc_marche();break;
		case 'addboncommande' : add_boncommande($_POST["numbc"],$_POST["nummarche"],$_POST["numlot"]);break;
		case 'addmarche' : add_marche($_POST["nummarche"]);break;
		case 'detailboncommande' : detail_boncommande($_POST["idbc"]);break;
		case 'detailgeneboncommande' : detailgene_boncommande($_POST["idbc"]);break;
		case 'updateboncommande' : update_boncommande($_POST["idbc"],$_POST["chp"],$_POST["val"]);break;
		case 'addchtbc' : add_chtbc($_POST["idbc"],$_POST["numero"]);break;
		case 'updatechtbc' : update_chtbc($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		case 'financecht' : finance_cht($_POST["idanneeprog"],$_POST["idprog"],$_POST["idbc"],$_POST["idbccht"]);break;
		case 'lstchtbc' : lst_chtbc($_POST["idbc"]);break;
		case 'delchtbc' : del_chtbc($_POST["id"]);break;
		case 'addpaiebc' : add_paiebc($_POST["idbc"],$_POST["natpaie"]);break;
		case 'lstpaiebc' : lst_paiebc($_POST["idbc"]);break;
		case 'delpaiebc' : del_paiebc($_POST["id"]);break;
		case 'addosbc' : add_osbc($_POST["idbc"],$_POST["typeos"]);break;
		case 'lstosbc' : lst_osbc($_POST["idbc"]);break;
		case 'delosbc' : del_osbc($_POST["id"]);break;
		case 'updatemarche' : update_marche($_POST["idmarche"],$_POST["chp"],$_POST["val"]);break;
		case 'updatedatemarche' : updatedate_marche($_POST["idmarche"],$_POST["chp"],$_POST["val"]);break;
		case 'detailmarche' : detail_marche($_POST["idmarche"]);break;
		case 'detailgenemarche' : detailgene_marche($_POST["idmarche"]);break;
		case 'detailjalonmarche' : detailjalon_marche($_POST["idmarche"]);break;
		case 'addentmarche' : add_entmarche($_POST["idmarche"]);break;
		case 'lstentmarche' : lst_entmarche($_POST["idmarche"]);break;
		case 'updateentmarche' : update_entmarche($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		case 'delentmarche' : del_entmarche($_POST["id"]);break;
		case 'addindicemarche' : add_indicemarche($_POST["idmarche"]);break;
		case 'lstindicemarche' : lst_indicemarche($_POST["idmarche"]);break;
		case 'delindicemarche' : del_indicemarche($_POST["id"]);break;
		case 'addformulemarche' : add_formulemarche($_POST["idmarche"]);break;
		case 'lstformulemarche' : lst_formulemarche($_POST["idmarche"]);break;
		case 'delformulemarche' : del_formulemarche($_POST["id"]);break;
		case 'updateindicemarche' : update_indicemarche($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		case 'updateformulemarche' : update_formulemarche($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		case 'lstlotmarche' : lstlot_marche($_POST["idmarche"]);break;
		case 'detaillot' : detail_lot($_POST["idlot"]);break;
		case 'detailgenelot' : detailgene_lot($_POST["idlot"]);break;
		case 'detailjalonlot' : detailjalon_lot($_POST["idlot"]);break;
		case 'updatelot' : update_lot($_POST["idlot"],$_POST["chp"],$_POST["val"]);break;
		case 'updatedatelot' : updatedate_lot($_POST["idlot"],$_POST["chp"],$_POST["val"]);break;
		case 'addlot' : add_lot($_POST["idmarche"],$_POST["numero"],$_POST["montantmini"],$_POST["montantmaxi"]);break;
		case 'addentlot' : add_entlot($_POST["idlot"]);break;
		case 'lstentlot' : lst_entlot($_POST["idlot"]);break;
		case 'updateentlot' : update_entlot($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		case 'delentlot' : del_entlot($_POST["id"]);break;
		case 'dellot' : del_lot($_POST["idlot"]);break;
		case 'lstlot' : lst_lot($_POST["nummarche"]);break;
		case 'addindicelot' : add_indicelot($_POST["idlot"]);break;
		case 'lstindicelot' : lst_indicelot($_POST["idlot"]);break;
		case 'delindicelot' : del_indicelot($_POST["id"]);break;
		case 'addformulelot' : add_formulelot($_POST["idlot"]);break;
		case 'lstformulelot' : lst_formulelot($_POST["idlot"]);break;
		case 'delformulelot' : del_formulelot($_POST["id"]);break;
		case 'updateindicelot' : update_indicelot($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		case 'updateformulelot' : update_formulelot($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		case 'addtranche' : add_tranche($_POST["idmarche"],$_POST["type"]);break;
		case 'deltranche' : del_tranche($_POST["idtranche"]);break;
		case 'detailpaiement' : detail_paiement($_POST["idpaie"]);break;
		case 'detailgenepaiement' : detailgene_paiement($_POST["idpaie"]);break;
		case 'detailjalonpaiement' : detailjalon_paiement($_POST["idpaie"]);break;
		case 'updatepaiement' : update_paiement($_POST["idpaie"],$_POST["chp"],$_POST["val"]);break;
		case 'apdventchtpaiement' : apdventcht_paiement($_POST["idpaie"],$_POST["idcht"]);break;
		case 'tabventchtpaiement' : tabventcht_paiement($_POST["idpaie"]);break;
		case 'lstarticlebord' : lst_article_bord($_POST["idbord"],$_POST["idventpaie"]);break;
		case 'lstvalventpaie' : lst_val_ventpaie($_POST["idventpaie"]);break;
		case 'addvalventpaie' : add_valventpaie($_POST["idventpaie"],$_POST["idart"]);break;
		case 'delvalventpaie' : del_valventpaie($_POST["idvalventpaie"]);break;
		case 'updatevalventpaie' : update_valventpaie($_POST["idvalventpaie"],$_POST["chp"],$_POST["val"]);break;
		case 'detailos' : detail_os($_POST["idos"]);break;
		case 'detailgeneos' : detailgene_os($_POST["idos"]);break;
		case 'updateos' : update_os($_POST["idos"],$_POST["chp"],$_POST["val"]);break;
		case 'blah' : blah();break;
		// ...etc...
	}
}

function lst_demandebc(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT demande_bc.numero, demande_bc.date_create, statut_demandebc.libelle, demande_bc.id, demande_bc.login FROM demande_bc JOIN statut_demandebc ON statut_demandebc.id=demande_bc.id_statut WHERE id_statut=1 ORDER BY demande_bc.date_create ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center'></th><th width='10%'>Num�ro</th><th>Date</th><th>Charg� d'affaire</th><th>Num�ro BC</th><th>Lot</th><th></th></tr></thead>";
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo "<tr class='showtrcht'><td align='center' width='5%'><i class='fa fa-envelope'></i></td><td align='center' width='10%'>".$ligne2["numero"]."</td><td align='center' width='8%'>".strftime("%d/%m/%Y",strtotime($ligne2["date_create"]))."</td><td align='center' width='10%'>".$ligne2["login"]."</td><td align='center' width='15%'><input class='form-control input-sm text-center numbc".$ligne2["id"]."'></td><td align='center' width='20%'>";
			echo "<select data-iddembc='".$ligne2["id"]."' class='form-control input-sm lstmarche nummarche".$ligne2["id"]."'><option value='0'>Choisir march�</option>";
			$reqlot="SELECT * FROM marche";
			$resultlot=mysql_query($reqlot,$link);
			while ($lignelot=mysql_fetch_assoc($resultlot))
			{
				echo '<option value="'.$lignelot["mar_id"].'">'.$lignelot["mar_numero"].'</option>';
			}
			echo "</select>";
			echo "<select class='form-control input-sm numlot".$ligne2["id"]."'><option value='0'>Choisir lot</option>";
			echo "</select>";
			echo "</td><td align='center' width='10%'><button data-iddembc='".$ligne2["id"]."' class='btn btn-sm btn-default convertdembc'><i class='fa fa-cog' title='Convertir'></i></button> <button class='btn btn-sm btn-default showcht'><i class='fas fa-search-plus' title='D�tail'></i></button></td></tr>";
			$reqcht="SELECT chantiers.cht_numero FROM chantiers JOIN demandebc_chantiers ON chantiers.cht_id=demandebc_chantiers.id_cht WHERE demandebc_chantiers.id_demandebc=".$ligne2["id"];
			$resultcht=mysql_query($reqcht,$link);
			echo '<tr><td colspan="3">Prestations associ�es</td><td colspan="4">';
			$c=1;
			while ($lignecht=mysql_fetch_assoc($resultcht))
			{
				if ($c!=1){echo "<br>";}
				echo "<i class='fa fa-bolt'></i> ".$lignecht["cht_numero"];
				$c++;
			}
			echo '</td></tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="7">Aucune demande</td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function lst_demandeos(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT demande_os.numero, demande_os.date_create, statut_demandebc.libelle, demande_os.id, demande_os.login,boncommande.bc_numero as numbc,lot.lot_numero as numlot,lexique.lex_libelle as typeos FROM demande_os JOIN statut_demandebc ON statut_demandebc.id=demande_os.id_statut JOIN boncommande ON boncommande.bc_id=demande_os.id_bc JOIN lot ON boncommande.bc_idlot=lot.lot_id JOIN lexique ON demande_os.type=lexique.lex_id WHERE id_statut=1 ORDER BY demande_os.date_create ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center'></th><th>Num�ro</th><th>Date</th><th>Charg� d'affaire</th><th>Num�ro Lot / Num�ro BC</th><th>Type</th><th></th></tr></thead>";
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo "<tr class='showcht' style='cursor:zoom-in;'><td align='center' width='5%'><i class='fa fa-envelope'></i></td><td width='15%'>".$ligne2["numero"]."</td><td width='8%'>".strftime("%d/%m/%Y",strtotime($ligne2["date_create"]))."</td><td width='10%'>".$ligne2["login"]."</td><td align='center' width='15%'>".$ligne2["numlot"]." / ".$ligne2["numbc"]."</td><td align='center' width='15%'>".$ligne2["typeos"]."</td><td align='center' width='8%'><button data-iddemos='".$ligne2["id"]."' class='btn btn-sm btn-default convertdemos'><i class='fa fa-cog' title='Convertir'></i></button></td></tr>";
			$reqcht="SELECT chantiers.cht_numero FROM chantiers JOIN demandeos_chantiers ON chantiers.cht_id=demandeos_chantiers.id_cht WHERE demandeos_chantiers.id_demandeos=".$ligne2["id"];
			$resultcht=mysql_query($reqcht,$link);
			echo '<tr><td colspan="3">Prestations associ�es</td><td colspan="4">';
			$c=1;
			while ($lignecht=mysql_fetch_assoc($resultcht))
			{
				if ($c!=1){echo "<br>";}
				echo "<i class='fa fa-bolt'></i> ".$lignecht["cht_numero"];
				$c++;
			}
			echo '</td></tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="7">Aucune demande</td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function lst_typeos(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT lex_libelle, lex_id FROM lexique WHERE lex_codelexique='TYPE_OS' ORDER BY lex_libelle ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir un type</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["lex_id"].'">'.$ligne2["lex_libelle"].'</option>';
		}
	}
}
function lst_typetranche(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT lex_libelle, lex_id FROM lexique WHERE lex_codelexique='TYPE_TRANCHE' ORDER BY lex_libelle ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir un type</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["lex_id"].'">'.$ligne2["lex_libelle"].'</option>';
		}
	}
}
function lst_naturepaiement(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT lex_libelle, lex_id FROM lexique WHERE lex_codelexique='NAT_PAIE' ORDER BY lex_libelle ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une nature</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["lex_id"].'">'.$ligne2["lex_libelle"].'</option>';
		}
	}
}
function lst_marche($page,$loginca,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	
	$messagesParPage=5;
	if ($chp!=""){
		$retour_total=mysql_query('SELECT COUNT(*) AS total FROM marche where '.$chp.' like "%'.$val.'%"');
		$donnees_total=mysql_fetch_assoc($retour_total);
	}else{
		$retour_total=mysql_query('SELECT COUNT(*) AS total FROM marche');
		$donnees_total=mysql_fetch_assoc($retour_total);
	}
	$total=$donnees_total['total'];
	$nombreDePages=ceil($total/$messagesParPage);
	if(isset($page))
	{
		$pageActuelle=intval($page);

		if($pageActuelle>$nombreDePages)
		{
			$pageActuelle=$nombreDePages;
		}
	}else{
		$pageActuelle=1;
	}

	$premiereEntree=($pageActuelle-1)*$messagesParPage;

	if ($chp!=""){
		$req2="SELECT marche.mar_dateeffet, marche.mar_numero, marche.mar_id, marche.mar_objet, lexique.lex_libelle FROM marche LEFT JOIN lexique ON lexique.lex_id=marche.mar_id_type WHERE ".$chp." LIKE '%".$val."%' ORDER BY marche.type, marche.date_effet DESC limit ".$premiereEntree.",".$messagesParPage." ";
	}else{
		$req2="SELECT marche.mar_dateeffet, marche.mar_numero, marche.mar_id, marche.mar_objet, lexique.lex_libelle FROM marche LEFT JOIN lexique ON lexique.lex_id=marche.mar_idtype ORDER BY marche.mar_idtype, marche.mar_dateeffet DESC limit ".$premiereEntree.",".$messagesParPage." ";
	}
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	$compteope = 1;
	if ($pageActuelle>1){
		echo "<div><div data-tooltip='Pr�cedent' class='forward btnpager'><i class='fa fa-caret-left' aria-hidden='true'></i></div>";
	}
	echo "<div class='boxpager'>".$pageActuelle." / ".$nombreDePages."</div>";
	if ($pageActuelle<$nombreDePages){
		echo "<div data-tooltip='Suivant' class='next btnpager'><i class='fa fa-caret-right' aria-hidden='true'></i></div></div>";
	}
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th width='10%'></th><th width='8%'>Date</th><th width='8%'>Num�ro</th><th width='20%'>Type</th><th>Objet</th></tr></thead>";
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			/* Recherche Bon de commande rattach� pour activation des boutons */
			$reqbcmarche="SELECT * FROM boncommande JOIN lot ON lot.id=boncommande.bc_idlot WHERE lot.id_marche=".$ligne2["mar_id"];
			$resultbcmarche=mysql_query($reqbcmarche,$link);
			$rowbcmarche=mysql_num_rows($resultbcmarche);
			if ($rowbcmarche!=0){$buttons = "disabled";}else{$buttons = "";}
			echo "<tr><td align='center' width='8%'><button data-tooltip='Visualiser' data-idmarche='".$ligne2["mar_id"]."' class='btn btn-sm btn-default visumarche'><i class='fa fa-eye'></i></button> <button data-tooltip='Supprimer' data-idbc='".$ligne2["mar_id"]."' class='btn btn-sm btn-default delmarche' ".$buttons."><i class='fa fa-trash'></i></button></td><td align='center' width='8%'>".strftime("%d/%m/%Y",strtotime($ligne2["mar_dateeffet"]))."</td><td align='center' width='8%'>".$ligne2["mar_numero"]."</td><td>".$ligne2["lex_libelle"]."</td><td>".$ligne2["mar_objet"]."</td></tr>";
		}
	}else{
		echo "<tr><td colspan='4' align='center'>Aucun march�</td></tr>";
	}
	echo "</tbody>";
	echo "</table>";
}
function lst_boncommande($page,$loginca,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	
	$messagesParPage=10;
	if ($chp!=""){
		$retour_total=mysql_query('SELECT COUNT(*) AS total FROM boncommande where '.$chp.' like "%'.$val.'%"');
		$donnees_total=mysql_fetch_assoc($retour_total);
	}else{
		$retour_total=mysql_query('SELECT COUNT(*) AS total FROM boncommande');
		$donnees_total=mysql_fetch_assoc($retour_total);
	}
	$total=$donnees_total['total'];
	$nombreDePages=ceil($total/$messagesParPage);
	if(isset($page))
	{
		$pageActuelle=intval($page);

		if($pageActuelle>$nombreDePages)
		{
			$pageActuelle=$nombreDePages;
		}
	}else{
		$pageActuelle=1;
	}

	$premiereEntree=($pageActuelle-1)*$messagesParPage;

	if ($chp!=""){
		$req2="SELECT boncommande.bc_datebc, boncommande.bc_numero, boncommande.bc_id, marche.mar_id as idmarche, marche.mar_numero as nummarche, lot.lot_id as idlot, lot.lot_numero as numlot, lexique.lex_libelle FROM boncommande LEFT JOIN lot ON lot.lot_id=boncommande.bc_idlot LEFT JOIN marche ON marche.mar_id=boncommande.bc_idmarche LEFT JOIN lexique ON lexique.lex_id=marche.mar_idtype WHERE ".$chp." LIKE '%".$val."%' ORDER BY boncommande.bc_datebc DESC limit ".$premiereEntree.",".$messagesParPage." ";
	}else{
		$req2="SELECT boncommande.bc_datebc, boncommande.bc_numero, boncommande.bc_id, marche.mar_id as idmarche, marche.mar_numero as nummarche, lot.lot_id as idlot, lot.lot_numero as numlot, lexique.lex_libelle FROM boncommande LEFT JOIN lot ON lot.lot_id=boncommande.bc_idlot LEFT JOIN marche ON marche.mar_id=boncommande.bc_idmarche LEFT JOIN lexique ON lexique.lex_id=marche.mar_idtype ORDER BY boncommande.bc_datebc DESC limit ".$premiereEntree.",".$messagesParPage." ";
	}
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	$compteope = 1;
	if ($pageActuelle>1){
		echo "<div><div data-tooltip='Pr�c�dent' class='forward btnpager'><i class='fa fa-caret-left' aria-hidden='true'></i></div>";
	}
	echo "<div class='boxpager'>".$pageActuelle." / ".$nombreDePages."</div>";
	if ($pageActuelle<$nombreDePages){
		echo "<div data-tooltip='Suivant' class='next btnpager'><i class='fa fa-caret-right' aria-hidden='true'></i></div></div>";
	}
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th width='10%'></th><th width='8%'>Date</th><th width='8%'>Num�ro</th><th>March�</th><th>Lot</th></tr></thead>";
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo "<tr><td align='center' width='8%'><button data-tooltip='Visualiser' data-idbc='".$ligne2["bc_id"]."' class='btn btn-sm btn-default visubc'><i class='fa fa-eye'></i></button> <button data-tooltip='Supprimer' data-idbc='".$ligne2["id"]."' class='btn btn-sm btn-default delbc'><i class='fa fa-trash'></i></button></td><td align='center' width='8%'>".strftime("%d/%m/%Y",strtotime($ligne2["bc_datebc"]))."</td><td align='center' width='8%'>".$ligne2["bc_numero"]."</td><td><a data-tooltip='Visualiser' href='#' data-idmarche='".$ligne2["idmarche"]."' class='visumarche'>".$ligne2["lex_libelle"]." ".$ligne2["nummarche"]."</a></td><td><a data-tooltip='Visualiser' href='#' data-idlot='".$ligne2["idlot"]."' class='visulot'>".$ligne2["numlot"]."</a></td></tr>";
		}
	}else{
		echo "<tr><td colspan='5' align='center'><b>Aucun bon de commande</b></td></tr>";
	}
	echo "</tbody>";
	echo "</table>";
}
function add_marche($numero){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO marche (mar_numero,mar_datecreate) VALUES ('".$numero."',NOW())";
	$result2=mysql_query($req2,$link);
}
function add_boncommande($numero,$nummarche,$numlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT tva_id FROM tva ORDER BY tva_datetva DESC LIMIT 1";
	$result2=mysql_query($req2,$link);
	$ligne2=mysql_fetch_assoc($result2);
	$id_tva = $ligne2["tva_id"];

	$req2="INSERT INTO boncommande (bc_numero,bc_datebc,bc_idmarche,bc_idlot,bc_idtva) VALUES ('".$numero."',NOW(),'".$nummarche."','".$numlot."','".$id_tva."')";
	$result2=mysql_query($req2,$link);
	$idbc = mysql_insert_id($link);
}
function convert_demandebc($iddembc,$numero,$nummarche,$numlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT * FROM demande_bc WHERE id=".$iddembc;
	$result2=mysql_query($req2,$link);
	$ligne2=mysql_fetch_assoc($result2);
	$identdemandebc = $ligne2["id_ent"];
	/* Calcul du montant de l'APD */
	$req2="SELECT demandebc_chantiers.id_cht,apd.estimation_HT,apd.tot_ht,apd.tot_ttc FROM demandebc_chantiers JOIN apd ON apd.id_cht=demandebc_chantiers.id_cht WHERE demandebc_chantiers.id_demandebc=".$iddembc;
	$result2=mysql_query($req2,$link);
	$mont_devis = 0;$mont_ht = 0;$mont_ttc = 0;
	while ($ligne2=mysql_fetch_assoc($result2))
	{
		$mont_devis = $mont_devis + $ligne2["estimation_HT"];
		$mont_ht = $mont_ht + $ligne2["tot_ht"];
		$mont_ttc = $mont_ttc + $ligne2["tot_ttc"];
	}
	/* Cr�ation du bon de commande */
	$req2="INSERT INTO boncommande (bc_numero,bc_datebc,bc_idmarche,bc_idlot,bc_ident,bc_montantdevisht,bc_montantenginiht,bc_montantenginittc) VALUES ('".$numero."',NOW(),'".$nummarche."','".$numlot."','".$identdemandebc."',".$mont_devis.",".$mont_ht.",".$mont_ttc.")";
	$result2=mysql_query($req2,$link);
	$idbc = mysql_insert_id($link);
	/* Cr�er les liaisons entre bon de commande et prestations */
	$req2="SELECT demandebc_chantiers.id_cht,chantiers.cht_ident, annee_prog.anneeprog_id,apd.estimation_HT,apd.tot_ht,apd.tot_ttc FROM demandebc_chantiers JOIN chantiers ON chantiers.cht_id=demandebc_chantiers.id_cht LEFT JOIN annee_prog ON chantiers.cht_idanneeprog=annee_prog.anneeprog_id JOIN programme ON programme.prog_id=annee_prog.anneeprog_idprog LEFT JOIN apd ON apd.id_cht=demandebc_chantiers.id_cht WHERE demandebc_chantiers.id_demandebc=".$iddembc;
	$result2=mysql_query($req2,$link);
	while ($ligne2=mysql_fetch_assoc($result2))
	{
		$reqcht="INSERT INTO bc_cht (bccht_idbc,bccht_idcht,bccht_ident,bccht_idanneeprog,bccht_montantdevisht,bccht_montantenginiht,bccht_montantenginittc) VALUES (".$idbc.",".$ligne2["id_cht"].",".$ligne2["cht_ident"].",'".$ligne2["anneeprog_id"]."','".$ligne2["estimation_HT"]."','".$ligne2["tot_ht"]."','".$ligne2["tot_ttc"]."')";
		$resultcht=mysql_query($reqcht,$link);
	}
	$req2="UPDATE demande_bc SET id_bc=".$idbc." ,id_statut=2 WHERE id=".$iddembc;
	$result2=mysql_query($req2,$link);
	$req2="SELECT * FROM demande_bc WHERE id=".$iddembc;
	$result2=mysql_query($req2,$link);
	while ($ligne2=mysql_fetch_assoc($result2))
	{
		$numdemandebc = $ligne2["numero"];
		$logindemandebc = $ligne2["login"];
	}
	$req2="INSERT INTO notifications (notif_statut,notif_texte,notif_datenotif,notif_login) VALUES ('success','Bon de commande concernant la demande ".$numdemandebc." a �t� cr��.',NOW(),'".$logindemandebc."')";
	$result2=mysql_query($req2,$link);
	require('../PHPMailer/class.phpmailer.php');
	$mail = new PHPMailer();
	$mail->Host = "cli-mail.devopsys.com";
	$mail->SMTPAuth   = false;
	$mail->Port = 25; // Par d�faut
	 
	// Exp�diteur
	$mail->SetFrom('sehv@sehv.fr', 'SEHV');
	$mail->Sender = 'sehv@sehv.fr';

	$req2="SELECT * FROM user WHERE user_login='".$logindemandebc."'";
	$result2=mysql_query($req2,$link);
	while ($ligne2=mysql_fetch_assoc($result2))
	{
		$mailagent = $ligne2["user_mail"];
		$identite = $ligne2["user_nom"].' '.$ligne2["user_prenom"];;
	}
	// Re�evoir une confirmation de lecture
	$mail->addCustomHeader("X-Confirm-Reading-To: ".$mailagent);
	$mail->addCustomHeader("Disposition-notification-to: ".$mailagent);

	// Destinataire
	$mail->AddAddress($mailagent, $identite);

	// Objet
	$mail->Subject = 'Outil m�tier';
	 
	// Votre message
	$mail->MsgHTML('Bon de commande concernant la demande '.$numdemandebc.' � �t� cr��.');

	// Ajouter une pi�ce jointe
	//$mail->AddAttachment(dirname(dirname(__DIR__)).'/PdfDepannage/'.$filename.'.pdf');

	if(!$mail->Send()) {
		echo 'Erreur : ' . $mail->ErrorInfo;
	} else {
		echo 'Message envoy� !';
	}
}
function convert_demandeos($iddemos){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT * FROM demande_os WHERE id=".$iddemos;
	$result2=mysql_query($req2,$link);
	$ligne2=mysql_fetch_assoc($result2);
	$idbcdemandeos = $ligne2["id_bc"];
	$identdemandeos = $ligne2["id_ent"];
	$typedemandeos = $ligne2["type"];
	$delaidemandeos = $ligne2["delai"];
	$prolongdemandeos = $ligne2["prolong"];
	$montantdemandeos = $ligne2["montant_ttc"];
	$idtaskdemandeos = $ligne2["id_task"];
	/* Cr�ation de l'OS */
	$req2="INSERT INTO os (date_os,id_bc,id_ent,type,montant_ttc) VALUES (NOW(),'".$idbcdemandeos."','".$identdemandebc."',".$typedemandeos.",".$montantdemandeos.")";
	$result2=mysql_query($req2,$link);
	$idos = mysql_insert_id($link);
	/* Cr�er les liaisons entre os et prestations */
	$req2="SELECT demandeos_chantiers.id_cht,chantiers.cht_ident,chantiers.programme,chantiers.annee_prog FROM demandeos_chantiers JOIN chantiers ON chantiers.cht_id=demandeos_chantiers.id_cht WHERE demandeos_chantiers.id_demandeos=".$iddemos;
	$result2=mysql_query($req2,$link);
	while ($ligne2=mysql_fetch_assoc($result2))
	{
		$reqcht="INSERT INTO os_cht (id_os,id_cht) VALUES (".$idos.",".$ligne2["id_cht"].")";
		$resultcht=mysql_query($reqcht,$link);
		if ($typedemandeos==8){
			/* Liaison des BC et prestations suppl�mentaires */
			$reqchtbc="INSERT INTO bc_cht (bccht_idbc,bccht_idcht,bccht_ident,bccht_programme,bccht_anneeprog) VALUES (".$idbcdemandeos.",".$ligne2["id_cht"].",".$ligne2["cht_ident"].",'".$ligne2["programme"]."','".$ligne2["annee_prog"]."')";
			$resultchtbc=mysql_query($reqchtbc,$link);
			/* Liaison des paiements et prestations suppl�mentaires */
			$req3="SELECT paiement.id FROM paiement WHERE paiement.id_bc=".$idbcdemandeos;
			$result3=mysql_query($req3,$link);
			while ($ligne3=mysql_fetch_assoc($result3))
			{
				$reqchtpaie="INSERT INTO vent_paie (id_paie,id_cht) VALUES (".$ligne3["id"].",".$ligne2["id_cht"]."')";
				$resultchtpaie=mysql_query($reqchtpaie,$link);
			}
		}
	}
	switch ($typedemandeos){
		case 8: 
			/* Prestations suppl�mentaires */

			/* Calcul du montant de l'APD BC*/
			$req2="SELECT bc_cht.bccht_idcht,apd.tot_ttc FROM bc_cht JOIN apd ON apd.id_cht=bc_cht.bccht_idcht WHERE bc_cht.bccht_idbc=".$idbcdemandeos;
			$result2=mysql_query($req2,$link);
			$mont_apd_bc = 0;
			while ($ligne2=mysql_fetch_assoc($result2))
			{
				$mont_apd_bc = $mont_apd_bc + $ligne2["tot_ttc"];
			}
			$req2="UPDATE boncommande SET bc_montantapd=".$mont_apd_bc." WHERE bc_id=".$idbcdemandeos;
			$result2=mysql_query($req2,$link);
			break;
	}
	$req2="UPDATE demande_os SET id_os=".$idos." ,id_statut=2 WHERE id=".$iddemos;
	$result2=mysql_query($req2,$link);
	$req2="SELECT * FROM demande_os WHERE id=".$iddemos;
	$result2=mysql_query($req2,$link);
	while ($ligne2=mysql_fetch_assoc($result2))
	{
		$numdemandeos = $ligne2["numero"];
		$logindemandeos = $ligne2["login"];
	}
	$req2="UPDATE tasks SET traite=1 WHERE id=".$idtaskdemandeos;
	$result2=mysql_query($req2,$link);
	$req2="INSERT INTO notifications (notif_statut,notif_texte,notif_datenotif,notif_login) VALUES ('success','OS concernant la demande ".$numdemandeos." a �t� cr��.',NOW(),'".$logindemandeos."')";
	$result2=mysql_query($req2,$link);
	require('../PHPMailer/class.phpmailer.php');
	$mail = new PHPMailer();
	$mail->Host = "cli-mail.devopsys.com";
	$mail->SMTPAuth   = false;
	$mail->Port = 25; // Par d�faut
	 
	// Exp�diteur
	$mail->SetFrom('sehv@sehv.fr', 'SEHV');
	$mail->Sender = 'sehv@sehv.fr';

	$req2="SELECT * FROM user WHERE user_login='".$logindemandebc."'";
	$result2=mysql_query($req2,$link);
	while ($ligne2=mysql_fetch_assoc($result2))
	{
		$mailagent = $ligne2["user_mail"];
		$identite = $ligne2["user_nom"].' '.$ligne2["user_prenom"];;
	}
	// Re�evoir une confirmation de lecture
	$mail->addCustomHeader("X-Confirm-Reading-To: ".$mailagent);
	$mail->addCustomHeader("Disposition-notification-to: ".$mailagent);

	// Destinataire
	$mail->AddAddress($mailagent, $identite);

	// Objet
	$mail->Subject = 'Outil m�tier';
	 
	// Votre message
	$mail->MsgHTML('Bon de commande concernant la demande '.$numdemandebc.' � �t� cr��.');

	// Ajouter une pi�ce jointe
	//$mail->AddAttachment(dirname(dirname(__DIR__)).'/PdfDepannage/'.$filename.'.pdf');

	if(!$mail->Send()) {
		echo 'Erreur : ' . $mail->ErrorInfo;
	} else {
		echo 'Message envoy� !';
	}
}
function update_marche($idmarche,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE marche SET ".$chp."='".$val."' WHERE mar_id=".$idmarche;
	$result2=mysql_query($req2,$link);	
}
function updatedate_marche($idmarche,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$tabdate=explode("/", $val);
	$datefinal = $tabdate[2]."-".$tabdate[1]."-".$tabdate[0];
	$req2="UPDATE marche SET ".$chp."='".$datefinal."' WHERE mar_id=".$idmarche;
	$result2=mysql_query($req2,$link);	
}
function detail_marche($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT marche.mar_numero, marche.mar_id, lexique.lex_libelle FROM marche LEFT JOIN lexique ON lexique.lex_id=marche.mar_idtype WHERE marche.mar_id=".$id;
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			$panel=1;
			echo "<div class='col-md-12'>";
			echo "<div class='portlet'><div class='portlet-title'><i class='fa fa-university' aria-hidden='true'></i> <b>March�</b> ".$ligne2["lex_libelle"]." - ".$ligne2["mar_numero"]."</div><div class='portlet-content' id='detmarche'>";
			echo "<div class='row'>";

			echo '<div class="col-sm-12">';
			echo "<button id='back_accueil' class='btn btn-default btn-sm' data-idope='".$ligne2["mar_id"]."'><i class='fa fa-reply'></i> Accueil</button>";
			echo '</div><br><br>';

			echo '<div class="col-sm-12">';
			echo '<div class="panel-group" id="accordion">';
			/* G�n�ralit�s */
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h2 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-file" aria-hidden="true"></i> G�n�ralit�s</a>';
			echo '</h2>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detailgene_marche">';
				detailgene_marche($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Jalonnements */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-calendar-alt" aria-hidden="true"></i> Jalonnements</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detjalon">';
				detailjalon_marche($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Entreprises */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-users" aria-hidden="true"></i> Entreprises</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detentmarche">';
				lst_entmarche($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Formule d'actualisation */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-calculator" aria-hidden="true"></i> Formule d\'actualisation</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detformactumarche">';
				echo '<div class="col-sm-12" style="margin-bottom:10px;">';
				echo '<div class="panel panel-default">';
				echo '<div class="panel-heading"> P�riodicit�</div>';
				echo '<div class="panel-body" id="detpropactumarche">';
					prop_actumarche($id);
				echo '</div>';
				echo '</div>';
				echo '</div>';
				echo '<div class="col-sm-12" style="margin-bottom:10px;">';
				echo '<div class="panel panel-default">';
				echo '<div class="panel-heading"><i class="fas fa-subscript"></i> Indices (I0)</div>';
				echo '<div class="panel-body" id="detindicemarche">';
					lst_indicemarche($id);
				echo '</div>';
				echo '</div>';
				echo '</div>';
				echo '<div class="col-sm-12" style="margin-bottom:10px;">';
				echo '<div class="panel panel-default">';
				echo '<div class="panel-heading"><font size="3"><b>&#931;</b></font> Formules</div>';
				echo '<div class="panel-body" id="detformulemarche">';
					lst_formulemarche($id);
				echo '</div>';
				echo '</div>';
				echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Lots associ�s */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			$rowslot = nbrlot_marche($id);
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-file" aria-hidden="true"></i> Lots associ�s <span class="badge">'.$rowslot.'</span></a>';
			echo '</h4>';
			echo "";
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detlotmarche">';
				lstlot_marche($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Tranches associ�es */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fas fa-bars" aria-hidden="true"></i> Tranches associ�es</a>';
			echo '</h4>';
			echo "";
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="dettranchemarche">';
				lsttranche_marche($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';

			echo '</div>';
			echo '</div>';
		}
	}
}
function detailgene_marche($idmarche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM marche WHERE mar_id=".$idmarche;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		//$datecreate = strftime("%d/%m/%Y",strtotime($ligne["date_create"]));
		//$datedemande = strftime("%d/%m/%Y",strtotime($ligne["date_demande"]));
		$numero = $ligne["mar_numero"];
		$id_type = $ligne["mar_idtype"];
		$id_passation = $ligne["mar_idpassation"];
		$id_execution = $ligne["mar_idexecution"];
		$objet = $ligne["mar_objet"];
		$nomgrpt = $ligne["mar_nomgrpt"];
		$idtypegrpt = $ligne["mar_idtypegrpt"];
		$delaimandat = $ligne["mar_delaimandat"];
		$delaipaie = $ligne["mar_delaipaie"];
		$delaigarantie = $ligne["mar_delaigarantie"];
		$tauxretenugarantie = $ligne["mar_tauxretenugarantie"];
		$tauxavancegarantie = $ligne["mar_tauxavancegarantie"];
		$montantHTmini = $ligne["mar_montantHTmini"];
		$montantTTCmini = $ligne["mar_montantTTCmini"];
		$qtemini = $ligne["mar_qtemini"];
		$montantHTmaxi = $ligne["mar_montantHTmaxi"];
		$montantTTCmaxi = $ligne["mar_montantTTCmaxi"];
		$qtemaxi = $ligne["mar_qtemaxi"];
		$montantHTprev = $ligne["mar_montantHTprev"];
		$montantTTCprev = $ligne["mar_montantTTCprev"];
		$qteprev = $ligne["mar_qteprev"];
	}

	echo '<div class="col-sm-8">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">G�n�ralit�s</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_numero">Num�ro:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_numero" id="marche_numero" name="marche_numero" value="'.$numero.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_type">Type:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_idtype" id="marche_type" name="marche_type">';
		echo '<option value="0">Choisir un type de march�</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='TYPE_MAR'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$id_type){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_passation">Proc�dure de passation:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_idpassation" id="marche_passation" name="marche_passation">';
		echo '<option value="0">Choisir une proc�dure de passation</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='PASSATION_MAR'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$id_passation){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_execution">Ex�cution de march�:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_idexecution" id="marche_execution" name="marche_execution">';
		echo '<option value="0">Choisir une ex�cution de march�</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='EXEC_MAR'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$id_execution){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_objet">Objet:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_objet" id="marche_objet" name="marche_objet" value="'.$objet.'" >';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-4">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-clipboard-check"></i> Garantie</div>';
	echo '<div class="panel-body" id="detailgenegarantie_marche">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="marche_delaigarantie">D�lai garantie:</label>';
		echo '<div class="col-sm-6">';
		echo '<input type="text" class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_delaigarantie" id="marche_delaigarantie" name="marche_delaigarantie" value="'.$delaigarantie.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="marche_tauxretenugarantie">Taux de la retenue:</label>';
		echo '<div class="col-sm-6">';
		echo '<input type="text" class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_tauxretenugarantie" id="marche_tauxretenugarantie" name="marche_tauxretenugarantie" value="'.$tauxretenugarantie.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="marche_tauxavancegarantie">Taux avance:</label>';
		echo '<div class="col-sm-6">';
		echo '<input type="text" class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_tauxavancegarantie" id="marche_tauxavancegarantie" name="marche_tauxavancegarantie" value="'.$tauxavancegarantie.'" >';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-12" style="margin-top:10px;">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-user-friends"></i> Groupement</div>';
	echo '<div class="panel-body" id="detailgenegroupe_marche">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="marche_nomgrpt">Nom groupement:</label>';
		echo '<div class="col-sm-6">';
		echo '<input type="text" class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_nomgrpt" id="marche_nomgrpt" name="marche_nomgrpt" value="'.$nomgrpt.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="marche_typegrpt">Type de groupement:</label>';
		echo '<div class="col-sm-6">';
		echo '<select class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_idtypegrpt" id="marche_typegrpt" name="marche_typegrpt">';
		echo '<option value="0">Choisir un type de groupement</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='GRPT_TYPE'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$idtypegrpt){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-12" style="margin-top:10px;">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-sliders-h"></i> Seuils</div>';
	echo '<div class="panel-body" id="detailgenemontant_marche">';
		echo '<table class="table table-sm table-responsive table-bordered table-striped table-hover">';
		echo '<thead><tr><th class="text-center">Type</th><th class="text-center">� H.T.</th><th class="text-center">� T.T.C.</th><th class="text-center">Qte</th></tr></thead>';
		echo '<tbody>';
		echo '<tr><td>Minimum</td><td><input class="form-control input-sm text-right modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_montantHTmini" id="marche_montHTmini" name="marche_montHTmini" value="'.$montantHTmini.'"></td><td><input class="form-control input-sm text-right text-right modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_montantTTCmini" id="marche_montTTCmini" name="marche_montTTCmini" value="'.$montantTTCmini.'"></td><td><input class="form-control input-sm text-right text-right modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_qtemini" id="marche_qtemini" name="marche_qtemini" value="'.$qtemini.'"></td></tr>';
		echo '<tr><td>Maximum</td><td><input class="form-control input-sm text-right modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_montantHTmaxi" id="marche_montHTmaxi" name="marche_montHTmaxi" value="'.$montantHTmaxi.'"></td><td><input class="form-control input-sm text-right modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_montantTTCmaxi" id="marche_montTTCmaxi" name="marche_montTTCmaxi" value="'.$montantTTCmaxi.'"></td><td><input class="form-control input-sm text-right modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_qtemaxi" id="marche_qtemaxi" name="marche_qtemaxi" value="'.$qtemaxi.'"></td></tr>';
		echo '<tr><td>Pr�visionnel</td><td><input class="form-control input-sm text-right modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_montantHTprev" id="marche_montHTprev" name="marche_montHTprev" value="'.$montantHTprev.'"></td><td><input class="form-control input-sm text-right modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_montantTTCprev" id="marche_montTTCprev" name="marche_montTTCprev" value="'.$montantTTCprev.'"></td><td><input class="form-control input-sm text-right modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_qteprev" id="marche_qteprev" name="marche_qteprev" value="'.$qteprev.'"></td></tr>';
		echo '</tbody>';
		echo '</table>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function detailjalon_marche($idmarche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM marche WHERE mar_id=".$idmarche;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$delaimandat = $ligne["mar_delaimandat"];
		$delaipaie = $ligne["mar_delaipaie"];
		$datenotification = strftime("%d/%m/%Y",strtotime($ligne["mar_datenotification"]));
		$dateeffet = strftime("%d/%m/%Y",strtotime($ligne["mar_dateeffet"]));
		$dateeffetperiod = $ligne["mar_dateeffet"];
		$datefininitiale = strftime("%d/%m/%Y",strtotime($ligne["mar_datefininitiale"]));
		$datecloture = strftime("%d/%m/%Y",strtotime($ligne["mar_datecloture"]));
		$nbreconduction = $ligne["mar_nbreconduction"];
		$typeconduction = $ligne["mar_typereconduction"];
		$dureeconduction = $ligne["mar_dureereconduction"];
	}

	echo '<div class="col-sm-4">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-calendar-alt"></i> D�lais</div>';
	echo '<div class="panel-body" id="detailgenedelai_marche">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_delaimandat">D�lai mandat (j):</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_delaimandat" id="marche_delaimandat" name="marche_delaimandat" value="'.$delaimandat.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_delaipaie">D�lai paiement (j):</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_delaipaie" id="marche_delaipaie" name="marche_delaipaie" value="'.$delaipaie.'" >';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-calendar-alt"></i> Dates</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_type">Notification:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpdatemarche" data-idmarche="'.$idmarche.'" data-chp="mar_datenotification" id="marche_numero" name="marche_numero" value="'.$datenotification.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_objet">Effet:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpdatemarche" data-idmarche="'.$idmarche.'" data-chp="mar_dateeffet" id="marche_objet" name="marche_objet" value="'.$dateeffet.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_objet">Fin initiale:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpdatemarche" data-idmarche="'.$idmarche.'" data-chp="mar_datefininitiale" id="marche_objet" name="marche_objet" value="'.$datefininitiale.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_objet">Cloture:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpdatemarche" data-idmarche="'.$idmarche.'" data-chp="mar_datecloture" id="marche_objet" name="marche_objet" value="'.$datecloture.'" >';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-8">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-sync-alt"></i> Reconduction</div>';
	echo '<div class="panel-body" id="detailgenedelai_marche">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_nbreconduc">Nombre:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_nbreconduction" id="marche_nbreconduc" name="marche_nbreconduc" value="'.$nbreconduction.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_typeconduc">Type:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_typereconduction" id="marche_typeconduc" name="marche_typeconduc">';
		echo '<option value="0">Choisir un type de reconduction</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='TYPE_RECMAR'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$typeconduction){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_dureeconduc">Dur�e (mois):</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_dureereconduction" id="marche_dureeconduc" name="marche_dureeconduc" value="'.$dureeconduction.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<div class="col-sm-12" id="panelreconduc">';
			panel_reconduct($dateeffet,$dateeffetperiod,$dureeconduction,$nbreconduction);
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function panel_reconduct($dateeffet,$dateeffetperiod,$dureeconduction,$nbreconduction){
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo '<tbody>';
	$datefinini = date("d/m/Y",strtotime($dateeffetperiod." +".$dureeconduction." month"));
	echo '<tr><td><b>Initiale</b></td><td>'.$dateeffet.' - '.$datefinini.'</td></tr>';
	$datefinini2 = date("Y-m-d",strtotime($dateeffetperiod." +".$dureeconduction." month"));
	$temp1 = $datefinini2;
	for ($i=0;$i<$nbreconduction;$i++){
		$temp = date("Y-m-d",strtotime($temp1." +1 day"));
		$temp1 = date("Y-m-d",strtotime($temp." +".$dureeconduction." month"));
		$period = $i+1;
		echo '<tr><td><b>Reconduction n�'.$period.'</b></td><td>'.date("d/m/Y",strtotime($temp)).' - '.date("d/m/Y",strtotime($temp1)).'</td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function add_entmarche($idmarche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO marche_ent (marent_idmarche) VALUES (".$idmarche.")";
	$result2=mysql_query($req2,$link);
}
function update_entmarche($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE marche_ent SET ".$chp."='".$val."' WHERE marent_id=".$id;
	$result2=mysql_query($req2,$link);	
}
function del_entmarche($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM marche_ent WHERE marent_id=".$id;
	$result2=mysql_query($req2,$link);
}
function lst_entmarche($idmarche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button data-tooltip='Attacher une entreprise' id='add_entmarche' data-idmarche='".$idmarche."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th>Entreprises</th><th>R�le</th><th width='5%'>Grpt</th><th class='text-right' width='10%'>Montant mini H.T.</th><th class='text-right' width='10%'>Montant maxi H.T.</th></tr></thead>";
	echo "<tbody>";
	$reqentlot="SELECT marche_ent.marent_id,marche_ent.marent_montantmini,marche_ent.marent_montantmaxi,marche_ent.marent_ident,marche_ent.marent_roleent,marche_ent.marent_grpt FROM marche_ent WHERE marent_idmarche=".$idmarche;
	$resultentlot=mysql_query($reqentlot,$link);
	$rowentlot=mysql_num_rows($resultentlot);
	if ($rowentlot!=0)
	{
		while ($data=mysql_fetch_assoc($resultentlot))
		{
			if ($data["marent_grpt"]==0){$checkgrpt="";}else{$checkgrpt="checked";}
			echo '<tr><td align="center" width="8%"><button data-tooltip="D�tacher" data-id="'.$data["marent_id"].'" data-idmarche="'.$idmarche.'" class="btn btn-sm btn-default del_entmarche"><i class="fa fa-trash"></i></button></td><td align="center">';
			echo '<select class="form-control input-sm modifchpentmarche" data-chp="marent_ident" data-id="'.$data["marent_id"].'" data-idmarche="'.$idmarche.'">';
			echo '<option value="0">S�lectionnez une entreprise</option>';
			$req="SELECT ent_id,ent_nom FROM entreprises";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["ent_id"]==$data["marent_ident"]){$selected = "selected";}else{$selected = "";}
				echo '<option data-entity="ent" value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
			}
			echo '</select></td>';
			echo '<td><select class="form-control input-sm modifchpentmarche" data-chp="marent_roleent" data-id="'.$data["marent_id"].'" data-idmarche="'.$idmarche.'">';
			echo '<option value="0">Choisir un r�le</option>';
			$req="SELECT * FROM lexique WHERE lex_codelexique='ROLE_ENT'";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["lex_id"]==$data["marent_roleent"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="5%" class="text-center"><input type="checkbox" data-chp="marent_grpt" data-id="'.$data["marent_id"].'" data-idmarche="'.$idmarche.'" class="modifchpentmarche" '.$checkgrpt.'></td>';
			echo '<td width="10%" class="text-center"><input data-chp="marent_montantmini" data-id="'.$data["marent_id"].'" data-idmarche="'.$idmarche.'" class="form-control input-sm text-right modifchpentmarche" value="'.$data["marent_montantmini"].'"></td>';
			echo '<td width="10%" class="text-center"><input data-chp="marent_montantmaxi" data-id="'.$data["marent_id"].'" data-idmarche="'.$idmarche.'" class="form-control input-sm text-right modifchpentmarche" value="'.$data["marent_montantmaxi"].'"></td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="5"><b>Aucune entreprise</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function prop_actumarche($idmarche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM marche WHERE mar_id=".$idmarche;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$datemiseajour = strftime("%d/%m/%Y",strtotime($ligne["mar_datemiseajour"]));
		$idperiodeactu = $ligne["mar_idperiodeactu"];
		$idappliqactu = $ligne["mar_idappliqactu"];
	}

	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-5" for="marche_datemaj">Date de d�but de l\'actualisation:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm modifchpdatemarche" data-idmarche="'.$idmarche.'" data-chp="mar_datemiseajour" id="marche_datemaj" name="marche_datemaj" value="'.$datemiseajour.'">';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-5" for="marche_periodmajactu">P�riodicit� de mise � jour:</label>';
	echo '<div class="col-sm-7">';
	echo '<select class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_idperiodeactu" id="marche_periodmajactu" name="marche_periodmajactu">';
	echo '<option value="0">Choisir une p�riodicit�</option>';
	$req="SELECT * FROM lexique WHERE lex_codelexique='PERIODACTU_MAR'";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["lex_id"]==$idperiodeactu){$selected="selected";}else{$selected="";}
		echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
	}		
	echo '</select>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-5" for="marche_appliqactu">Application de l\'actualisation:</label>';
	echo '<div class="col-sm-7">';
	echo '<select class="form-control input-sm modifchpmarche" data-idmarche="'.$idmarche.'" data-chp="mar_idappliqactu" id="marche_appliqactu" name="marche_appliqactu">';
	echo '<option value="0">Choisir une m�thode</option>';
	$req="SELECT * FROM lexique WHERE lex_codelexique='APPLIQACTU_MAR'";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["lex_id"]==$idappliqactu){$selected="selected";}else{$selected="";}
		echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
	}		
	echo '</select>';
	echo '</div>';
	echo '</div>';
	echo '</form>';
}
function add_indicemarche($idmarche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO marche_indice (marind_idmarche) VALUES (".$idmarche.")";
	$result2=mysql_query($req2,$link);
}
function update_indicemarche($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE marche_indice SET ".$chp."='".mysql_real_escape_string($val)."' WHERE marind_id=".$id;
	$result2=mysql_query($req2,$link);	
	$reqentlot="SELECT marche_indice.marind_type,marche_indice.marind_dateindice FROM marche_indice WHERE marind_id=".$id;
	$resultentlot=mysql_query($reqentlot,$link);
	$rowentlot=mysql_num_rows($resultentlot);
	if ($rowentlot!=0)
	{
		while ($data=mysql_fetch_assoc($resultentlot))
		{
			if ($data["marind_type"]!="" && $data["marind_dateindice"]!=""){
				$reqentind="SELECT valeur_indice FROM indice_valeur WHERE type_indice='".$data["marind_type"]."' AND date_indice='".$data["marind_dateindice"]."'";
				$reqentind=mysql_query($reqentind,$link);
				$ind=mysql_fetch_assoc($reqentind);
				$req2="UPDATE marche_indice SET marind_valeur='".$ind["valeur_indice"]."' WHERE marind_id=".$id;
				$result2=mysql_query($req2,$link);
			}
		}
	}
}
function del_indicemarche($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM marche_indice WHERE marind_id=".$id;
	$result2=mysql_query($req2,$link);
}
function lst_indicemarche($idmarche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button data-tooltip='Nouvel indice' id='add_indicemarche' data-idmarche='".$idmarche."' data-idcht='".$idcht."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th class='text-center'>Type</th><th class='text-center'>Date</th><th class='text-center' width='10%'>Valeur</th></tr></thead>";
	echo "<tbody>";
	$reqentlot="SELECT marche_indice.marind_id,marche_indice.marind_type,marche_indice.marind_dateindice,marche_indice.marind_valeur FROM marche_indice WHERE marind_idmarche=".$idmarche;
	$resultentlot=mysql_query($reqentlot,$link);
	$rowentlot=mysql_num_rows($resultentlot);
	if ($rowentlot!=0)
	{
		while ($data=mysql_fetch_assoc($resultentlot))
		{
			echo '<tr><td align="center" width="8%"><button data-tooltip="Supprimer" data-id="'.$data["marind_id"].'" data-idmarche="'.$idmarche.'" class="btn btn-sm btn-default del_indicemarche"><i class="fa fa-trash"></i></button></td>';
			echo '<td><select class="form-control input-sm modifchpindicemarche" data-chp="marind_type" data-id="'.$data["marind_id"].'" data-idmarche="'.$idmarche.'">';
			echo '<option value="0">S�lectionnez un type</option>';
			$req="SELECT libelle FROM indice_type";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["libelle"]==$data["marind_type"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$ligne["libelle"].'" '.$selected.'>'.$ligne["libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td><select class="form-control input-sm modifchpindicemarche" data-chp="marind_dateindice" data-id="'.$data["marind_id"].'" data-idmarche="'.$idmarche.'">';
			echo '<option value="0">Ann�e/Mois</option>';
			$req="SELECT date_indice FROM indice_valeur WHERE type_indice='".$data["marind_type"]."'";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				$anneecode = substr($ligne["date_indice"],0,4);
				$moiscode = substr($ligne["date_indice"],-2);
				if ($ligne["date_indice"]==$data["marind_dateindice"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$ligne["date_indice"].'" '.$selected.'>'.$anneecode.'/'.$moiscode.'</option>';
			}
			echo '</select></td>';
			echo '<td width="15%" class="text-center">'.$data["marind_valeur"].'</td>';
			//echo '<td class="text-center"><input class="form-control input-sm modifchpindicelot" data-chp="formule_actu" data-id="'.$data["id"].'" data-idlot="'.$idlot.'" value="'.$data["formule_actu"].'"></td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="5"><b>Aucun indice</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function add_formulemarche($idmarche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO marche_formule (marform_idmarche) VALUES (".$idmarche.")";
	$result2=mysql_query($req2,$link);
}
function update_formulemarche($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE marche_formule SET ".$chp."='".utf8_decode($val)."' WHERE marform_id=".$id;
	$result2=mysql_query($req2,$link);	
}
function del_formulemarche($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM marche_formule WHERE marform_id=".$id;
	$result2=mysql_query($req2,$link);
}
function lst_formulemarche($idmarche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button data-tooltip='Nouvelle formule' id='add_formulemarche' data-idmarche='".$idmarche."' data-idcht='".$idcht."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th class='text-center'>Libell�</th><th class='text-center'>Formule</th></tr></thead>";
	echo "<tbody>";
	$reqentlot="SELECT marche_formule.marform_id,marche_formule.marform_libelle,marche_formule.marform_formuleactu FROM marche_formule WHERE marform_idmarche=".$idmarche;
	$resultentlot=mysql_query($reqentlot,$link);
	$rowentlot=mysql_num_rows($resultentlot);
	if ($rowentlot!=0)
	{
		while ($data=mysql_fetch_assoc($resultentlot))
		{
			echo '<tr><td align="center" width="8%"><button data-tooltip="Supprimer" data-id="'.$data["marform_id"].'" data-idmarche="'.$idmarche.'" class="btn btn-sm btn-default del_formulemarche"><i class="fa fa-trash"></i></button></td>';
			echo '<td class="text-center"><input class="form-control input-sm modifchpformulemarche" data-chp="marform_libelle" data-id="'.$data["marform_id"].'" data-idmarche="'.$idmarche.'" value="'.$data["marform_libelle"].'"></td>';
			echo '<td class="text-center"><input class="form-control input-sm modifchpformulemarche" data-chp="marform_formuleactu" data-id="'.$data["marform_id"].'" data-idmarche="'.$idmarche.'" value="'.$data["marform_formuleactu"].'"></td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="3"><b>Aucune formule</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function lstlot_marche($idmarche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqlot="SELECT lot.lot_numero, lot.lot_objet, lot.lot_montantHTmini, lot.lot_montantHTmaxi, lot.lot_id FROM lot WHERE lot.lot_idmarche=".$idmarche;
	$resultlot=mysql_query($reqlot,$link);
	$rowlot=mysql_num_rows($resultlot);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='10%'><button data-tooltip='Nouveau lot' id='add_lot' data-idmarche='".$idmarche."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th width='15%'>Num�ro</th><th>Objet</th><th width='10%'>Montant mini (� HT)</th><th width='10%'>Montant Maxi (� HT)</th></tr></thead>";
	echo "<tbody>";
	$tot_apd = 0;
	if ($rowlot!=0)
	{
		while ($lignelot=mysql_fetch_assoc($resultlot))
		{
			/* Recherche de l'entreprise titulaire 
			$reqent="SELECT ent_nom FROM entreprises WHERE ent_id=".$lignecht["id_ent"];
			$resultent=mysql_query($reqent,$link);
			$rowent=mysql_num_rows($resultent);
			if ($rowent!=0){
				while ($ligneent=mysql_fetch_assoc($resultent))
				{
					$ent_cht = $ligneent["ent_nom"];
				}
			}*/
			echo '<tr><td align="center" width="10%"><button data-tooltip="Visualiser" data-id="'.$lignelot["lot_id"].'" class="btn btn-sm btn-default visulot"><i class="fa fa-eye"></i></button> <button data-tooltip="Supprimer" data-id="'.$lignelot["lot_id"].'" class="btn btn-sm btn-default dellot"><i class="fa fa-trash"></i></button></td><td align="center" width="15%">'.$lignelot["lot_numero"].'</td><td>'.$lignelot["lot_objet"].'</td><td width="10%" class="text-right">'.number_format($lignelot["lot_montantHTmini"], 2, ',', ' ').' �</td><td width="10%" class="text-right">'.number_format($lignelot["lot_montantHTmaxi"], 2, ',', ' ').' �</td></tr>';
		}
	}else{
		echo '<tr><td colspan="5" align="center"><b>Aucun lot</b></td></tr>'; 
	}
	echo "</tbody>";
	echo "</table>";
}
function nbrlot_marche($idmarche){
	require("./compte.php");
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqlot="SELECT * FROM lot WHERE lot.lot_idmarche=".$idmarche;
	$resultlot=mysql_query($reqlot,$link);
	$rowlot=mysql_num_rows($resultlot);
	return $rowlot;
}
function detail_lot($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT lot.lot_numero, lot.lot_id, marche.mar_id as idmarche FROM lot JOIN marche ON marche.mar_id=lot.lot_idmarche WHERE lot.lot_id=".$id;
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			$panel=1;
			echo "<div class='col-md-12'>";
			echo "<div class='portlet'><div class='portlet-title'><i class='fa fa-university' aria-hidden='true'></i> <b>Lot</b> ".$ligne2["lot_numero"]."</div><div class='portlet-content' id='detlot'>";
			echo "<div class='row'>";

			echo '<div class="col-sm-12">';
			echo "<button id='back_marche' class='btn btn-default btn-sm' data-idmarche='".$ligne2["idmarche"]."'><i class='fa fa-reply'></i> March�</button>";
			echo '</div><br><br>';

			echo '<div class="col-sm-12">';
			echo '<div class="panel-group" id="accordion">';
			/* G�n�ralit�s */
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h2 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-file" aria-hidden="true"></i> G�n�ralit�s</a>';
			echo '</h2>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detailgene_lot">';
				detailgene_lot($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Jalonnements */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-calendar-alt" aria-hidden="true"></i> Jalonnements</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detjalon_lot">';
				detailjalon_lot($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Entreprises */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-users" aria-hidden="true"></i> Entreprises</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detentlot">';
				lst_entlot($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Formule d'actualisation */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-calculator" aria-hidden="true"></i> Formule d\'actualisation</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detformactulot">';
				echo '<div class="col-sm-12" style="margin-bottom:10px;">';
				echo '<div class="panel panel-default">';
				echo '<div class="panel-heading"> P�riodicit�</div>';
				echo '<div class="panel-body" id="detpropactumarche">';
					prop_actulot($id);
				echo '</div>';
				echo '</div>';
				echo '</div>';
				echo '<div class="col-sm-12" style="margin-bottom:10px;">';
				echo '<div class="panel panel-default">';
				echo '<div class="panel-heading"><i class="fas fa-subscript"></i> Indices (I0)</div>';
				echo '<div class="panel-body" id="detindicelot">';
					lst_indicelot($id);
				echo '</div>';
				echo '</div>';
				echo '</div>';
				echo '<div class="col-sm-12" style="margin-bottom:10px;">';
				echo '<div class="panel panel-default">';
				echo '<div class="panel-heading"><font size="3"><b>&#931;</b></font> Formules</div>';
				echo '<div class="panel-body" id="detformulelot">';
					lst_formulelot($id);
				echo '</div>';
				echo '</div>';
				echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '</div>';

			echo '</div>';
			echo '</div>';
		}
	}
}
function detailgene_lot($idlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM lot WHERE lot_id=".$idlot;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		//$datecreate = strftime("%d/%m/%Y",strtotime($ligne["date_create"]));
		//$datedemande = strftime("%d/%m/%Y",strtotime($ligne["date_demande"]));
		$numero = $ligne["lot_numero"];
		$objet = $ligne["lot_objet"];
		$id_type = $ligne["lot_idtype"];
		$id_execution = $ligne["lot_idexecution"];
		$nomgrpt = $ligne["lot_nomgrpt"];
		$idtypegrpt = $ligne["lot_idtypegrpt"];
		$delaimandat = $ligne["lot_delaimandat"];
		$delaipaie = $ligne["lot_delaipaie"];
		$delaigarantie = $ligne["lot_delaigarantie"];
		$tauxretenugarantie = $ligne["lot_tauxretenugarantie"];
		$tauxavancegarantie = $ligne["lot_tauxavancegarantie"];
		$montantHTmini = $ligne["lot_montantHTmini"];
		$montantTTCmini = $ligne["lot_montantTTCmini"];
		$qtemini = $ligne["lot_qtemini"];
		$montantHTmaxi = $ligne["lot_montantHTmaxi"];
		$montantTTCmaxi = $ligne["lot_montantTTCmaxi"];
		$qtemaxi = $ligne["lot_qtemaxi"];
		$montantHTprev = $ligne["lot_montantHTprev"];
		$montantTTCprev = $ligne["lot_montantTTCprev"];
		$qteprev = $ligne["lot_qteprev"];
	}

	echo '<div class="col-sm-8">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">G�n�ralit�s</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="lot_numero">Num�ro:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_numero" id="lot_numero" name="lot_numero" value="'.$numero.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="lot_type">Type:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_idtype" id="lot_type" name="lot_type">';
		echo '<option value="0">Choisir un type de lot</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='TYPE_MAR'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$id_type){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="lot_execution">Ex�cution du lot:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_idexecution" id="lot_execution" name="lot_execution">';
		echo '<option value="0">Choisir une ex�cution de lot</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='EXEC_MAR'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$id_execution){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="lot_objet">Objet:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_objet" id="lot_objet" name="lot_objet" value="'.$objet.'" >';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-4">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-clipboard-check"></i> Garantie</div>';
	echo '<div class="panel-body" id="detailgenegarantie_lot">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="lot_delaigarantie">D�lai garantie:</label>';
		echo '<div class="col-sm-6">';
		echo '<input type="text" class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_delaigarantie" id="lot_delaigarantie" name="lot_delaigarantie" value="'.$delaigarantie.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="lot_tauxretenugarantie">Taux de la retenue:</label>';
		echo '<div class="col-sm-6">';
		echo '<input type="text" class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_tauxretenugarantie" id="lot_tauxretenugarantie" name="lot_tauxretenugarantie" value="'.$tauxretenugarantie.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="lot_tauxavancegarantie">Taux avance:</label>';
		echo '<div class="col-sm-6">';
		echo '<input type="text" class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_tauxavancegarantie" id="lot_tauxavancegarantie" name="lot_tauxavancegarantie" value="'.$tauxavancegarantie.'" >';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-12" style="margin-top:10px;">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-user-friends"></i> Groupement</div>';
	echo '<div class="panel-body" id="detailgenegroupe_lot">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="lot_nomgrpt">Nom groupement:</label>';
		echo '<div class="col-sm-6">';
		echo '<input type="text" class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_nomgrpt" id="lot_nomgrpt" name="lot_nomgrpt" value="'.$nomgrpt.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="lot_typegrpt">Type de groupement:</label>';
		echo '<div class="col-sm-6">';
		echo '<select class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_idtypegrpt" id="lot_typegrpt" name="lot_typegrpt">';
		echo '<option value="0">Choisir un type de groupement</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='GRPT_TYPE'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$idtypegrpt){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-12" style="margin-top:10px;">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-sliders-h"></i> Seuils</div>';
	echo '<div class="panel-body" id="detailgenemontant_lot">';
		echo '<table class="table table-sm table-responsive table-bordered table-striped table-hover">';
		echo '<thead><tr><th class="text-center">Type</th><th class="text-center">� H.T.</th><th class="text-center">� T.T.C.</th><th class="text-center">Qte</th></tr></thead>';
		echo '<tbody>';
		echo '<tr><td>Minimum</td><td><input class="form-control input-sm text-right modifchplot" data-idlot="'.$idlot.'" data-chp="mar_montantHTmini" id="marche_montHTmini" name="marche_montHTmini" value="'.$montantHTmini.'"></td><td><input class="form-control input-sm text-right text-right modifchplot" data-idlot="'.$idlot.'" data-chp="mar_montantTTCmini" id="marche_montTTCmini" name="marche_montTTCmini" value="'.$montantTTCmini.'"></td><td><input class="form-control input-sm text-right text-right modifchplot" data-idlot="'.$idlot.'" data-chp="mar_qtemini" id="marche_qtemini" name="marche_qtemini" value="'.$qtemini.'"></td></tr>';
		echo '<tr><td>Maximum</td><td><input class="form-control input-sm text-right modifchplot" data-idlot="'.$idlot.'" data-chp="mar_montantHTmaxi" id="marche_montHTmaxi" name="marche_montHTmaxi" value="'.$montantHTmaxi.'"></td><td><input class="form-control input-sm text-right modifchplot" data-idlot="'.$idlot.'" data-chp="mar_montantTTCmaxi" id="marche_montTTCmaxi" name="marche_montTTCmaxi" value="'.$montantTTCmaxi.'"></td><td><input class="form-control input-sm text-right modifchplot" data-idlot="'.$idlot.'" data-chp="mar_qtemaxi" id="marche_qtemaxi" name="marche_qtemaxi" value="'.$qtemaxi.'"></td></tr>';
		echo '<tr><td>Pr�visionnel</td><td><input class="form-control input-sm text-right modifchplot" data-idlot="'.$idmarche.'" data-chp="mar_montantHTprev" id="marche_montHTprev" name="marche_montHTprev" value="'.$montantHTprev.'"></td><td><input class="form-control input-sm text-right modifchplot" data-idlot="'.$idlot.'" data-chp="mar_montantTTCprev" id="marche_montTTCprev" name="marche_montTTCprev" value="'.$montantTTCprev.'"></td><td><input class="form-control input-sm text-right modifchplot" data-idlot="'.$idlot.'" data-chp="mar_qteprev" id="marche_qteprev" name="marche_qteprev" value="'.$qteprev.'"></td></tr>';
		echo '</tbody>';
		echo '</table>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function detailjalon_lot($idlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM lot WHERE lot_id=".$idlot;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$delaimandat = $ligne["lot_delaimandat"];
		$delaipaie = $ligne["lot_delaipaie"];
		$datenotification = strftime("%d/%m/%Y",strtotime($ligne["lot_datenotification"]));
		$dateeffet = strftime("%d/%m/%Y",strtotime($ligne["lot_dateeffet"]));
		$dateeffetperiod = $ligne["lot_dateeffet"];
		$datefininitiale = strftime("%d/%m/%Y",strtotime($ligne["lot_datefininitiale"]));
		$datecloture = strftime("%d/%m/%Y",strtotime($ligne["lot_datecloture"]));
		$nbreconduction = $ligne["lot_nbreconduction"];
		$typeconduction = $ligne["lot_typereconduction"];
		$dureeconduction = $ligne["lot_dureereconduction"];
	}

	echo '<div class="col-sm-4">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-calendar-alt"></i> D�lais</div>';
	echo '<div class="panel-body" id="detaildelai_lot">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_delaimandat">D�lai mandat (j):</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_delaimandat" id="marche_delaimandat" name="marche_delaimandat" value="'.$delaimandat.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_delaipaie">D�lai paiement (j):</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_delaipaie" id="marche_delaipaie" name="marche_delaipaie" value="'.$delaipaie.'" >';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-calendar-alt"></i> Dates</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_type">Notification:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpdatelot" data-idlot="'.$idlot.'" data-chp="lot_datenotification" id="marche_numero" name="marche_numero" value="'.$datenotification.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_objet">Effet:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpdatelot" data-idlot="'.$idlot.'" data-chp="lot_dateeffet" id="marche_objet" name="marche_objet" value="'.$dateeffet.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_objet">Fin initiale:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpdatelot" data-idlot="'.$idlot.'" data-chp="lot_datefininitiale" id="marche_objet" name="marche_objet" value="'.$datefininitiale.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_objet">Cloture:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpdatelot" data-idlot="'.$idlot.'" data-chp="lot_datecloture" id="marche_objet" name="marche_objet" value="'.$datecloture.'" >';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-8">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-sync-alt"></i> Reconduction</div>';
	echo '<div class="panel-body" id="detailreconduc_lot">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_nbreconduc">Nombre:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_nbreconduction" id="marche_nbreconduc" name="marche_nbreconduc" value="'.$nbreconduction.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_typeconduc">Type:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_typereconduction" id="marche_typeconduc" name="marche_typeconduc">';
		echo '<option value="0">Choisir un type de reconduction</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='TYPE_RECMAR'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$typeconduction){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="marche_dureeconduc">Dur�e (mois):</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_dureereconduction" id="marche_dureeconduc" name="marche_dureeconduc" value="'.$dureeconduction.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<div class="col-sm-12" id="panelreconduc">';
			panel_reconduct($dateeffet,$dateeffetperiod,$dureeconduction,$nbreconduction);
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function add_entlot($idlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO lot_ent (lotent_idlot) VALUES (".$idlot.")";
	$result2=mysql_query($req2,$link);
}
function update_entlot($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE lot_ent SET ".$chp."='".$val."' WHERE lotent_id=".$id;
	$result2=mysql_query($req2,$link);	
}
function del_entlot($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM lot_ent WHERE lotent_id=".$id;
	$result2=mysql_query($req2,$link);
}
function lst_entlot($idlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button id='add_entlot' data-idlot='".$idlot."' data-idcht='".$idcht."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th>Entreprises</th><th>R�le</th><th class='text-right' width='10%'>Montant mini</th><th class='text-right' width='10%'>Montant maxi</th></tr></thead>";
	echo "<tbody>";
	$reqentlot="SELECT lot_ent.lotent_id,lot_ent.lotent_montantmini,lot_ent.lotent_montantmaxi,lot_ent.lotent_ident,lot_ent.lotent_roleent FROM lot_ent WHERE lotent_idlot=".$idlot;
	$resultentlot=mysql_query($reqentlot,$link);
	$rowentlot=mysql_num_rows($resultentlot);
	if ($rowentlot!=0)
	{
		while ($data=mysql_fetch_assoc($resultentlot))
		{
			echo '<tr><td align="center" width="8%"><button data-id="'.$data["lotent_id"].'" data-idlot="'.$idlot.'" class="btn btn-sm btn-default del_entlot"><i class="fa fa-trash"></i></button></td><td align="center">';
			echo '<select class="form-control input-sm modifchpentlot" data-chp="lotent_ident" data-id="'.$data["lotent_id"].'" data-idlot="'.$idlot.'">';
			echo '<option value="0">S�lectionnez une entreprise</option>';
			$req="SELECT ent_id,ent_nom FROM entreprises";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["ent_id"]==$data["lotent_ident"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
			}
			echo '</select></td>';
			echo '<td><select class="form-control input-sm modifchpentlot" data-chp="role_ent" data-id="'.$data["id"].'" data-idlot="'.$idlot.'">';
			echo '<option value="0">Choisir un r�le</option>';
			$req="SELECT * FROM lexique WHERE lex_codelexique='ROLE_ENT'";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["lex_id"]==$data["lotent_roleent"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td width="10%" class="text-center"><input data-chp="lotent_montantmini" data-id="'.$data["lotent_id"].'" data-idlot="'.$idlot.'" class="form-control input-sm text-right modifchpentlot" value="'.$data["lotent_montantmini"].'"></td>';
			echo '<td width="10%" class="text-center"><input data-chp="lotent_montantmaxi" data-id="'.$data["lotent_id"].'" data-idlot="'.$idlot.'" class="form-control input-sm text-right modifchpentlot" value="'.$data["lotent_montantmaxi"].'"></td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="5"><b>Aucune entreprise</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function prop_actulot($idlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM lot WHERE lot_id=".$idlot;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$datemiseajour = strftime("%d/%m/%Y",strtotime($ligne["lot_datemiseajour"]));
		$idperiodeactu = $ligne["lot_idperiodeactu"];
		$idappliqactu = $ligne["lot_idappliqactu"];
	}

	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-5" for="lot_datemaj">Date de d�but de l\'actualisation:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm modifchpdatelot" data-idlot="'.$idlot.'" data-chp="lot_datemiseajour" id="lot_datemaj" name="lot_datemaj" value="'.$datemiseajour.'">';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-5" for="lot_periodmajactu">P�riodicit� de mise � jour:</label>';
	echo '<div class="col-sm-7">';
	echo '<select class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_idperiodeactu" id="lot_periodmajactu" name="lot_periodmajactu">';
	echo '<option value="0">Choisir une p�riodicit�</option>';
	$req="SELECT * FROM lexique WHERE lex_codelexique='PERIODACTU_MAR'";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["lex_id"]==$idperiodeactu){$selected="selected";}else{$selected="";}
		echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
	}		
	echo '</select>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-5" for="lot_appliqactu">Application de l\'actualisation:</label>';
	echo '<div class="col-sm-7">';
	echo '<select class="form-control input-sm modifchplot" data-idlot="'.$idlot.'" data-chp="lot_idappliqactu" id="lot_appliqactu" name="lot_appliqactu">';
	echo '<option value="0">Choisir une m�thode</option>';
	$req="SELECT * FROM lexique WHERE lex_codelexique='APPLIQACTU_MAR'";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["lex_id"]==$idappliqactu){$selected="selected";}else{$selected="";}
		echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
	}		
	echo '</select>';
	echo '</div>';
	echo '</div>';
	echo '</form>';
}
function update_lot($idlot,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE lot SET ".$chp."='".$val."' WHERE lot_id=".$idlot;
	$result2=mysql_query($req2,$link);	
}
function updatedate_lot($idlot,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$tabdate=explode("/", $val);
	$datefinal = $tabdate[2]."-".$tabdate[1]."-".$tabdate[0];
	$req2="UPDATE lot SET ".$chp."='".$datefinal."' WHERE lot_id=".$idlot;
	$result2=mysql_query($req2,$link);	
}
function add_lot($idmarche,$numero,$montantmini,$montantmaxi){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO lot (lot_idmarche,lot_numero,lot_montantHTmini,lot_montantHTmaxi) VALUES (".$idmarche.",'".$numero."','".$montantmini."','".$montantmaxi."')";
	$result2=mysql_query($req2,$link);
}
function del_lot($idlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM lot WHERE lot_id=".$idlot;
	$result2=mysql_query($req2,$link);
}
function lst_lot($id_marche){
	require("./compte.php");
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqlot="SELECT * FROM lot WHERE lot_idmarche=".$id_marche;
	$resultlot=mysql_query($reqlot,$link);
	$rowlot=mysql_num_rows($resultlot);
	echo '<option value="0">Choisir un lot</option>';
	if ($rowlot!=0)
	{
		while ($lignelot=mysql_fetch_assoc($resultlot))
		{
			echo '<option value="'.$lignelot["lot_id"].'">'.$lignelot["lot_numero"].'</option>';
		}
	}
}
/* Tranches */
function add_tranche($idmarche,$type){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO tranches (tranche_idmarche,tranche_idtype) VALUES (".$idmarche.",'".$type."')";
	$result2=mysql_query($req2,$link);
}
function del_tranche($idtranche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM tranches WHERE tranche_id=".$idtranche;
	$result2=mysql_query($req2,$link);
}
function lsttranche_marche($idmarche){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqlot="SELECT tranche_id,lexique.lex_libelle FROM tranches JOIN lexique ON lexique.lex_id=tranches.tranche_idtype WHERE tranches.tranche_idmarche=".$idmarche;
	$resultlot=mysql_query($reqlot,$link);
	$rowlot=mysql_num_rows($resultlot);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='10%'><button data-tooltip='Nouvelle tranche' id='add_tranche' data-idmarche='".$idmarche."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th width='15%'>Num�ro</th><th>Type</th><th width='10%'>Montant mini (� HT)</th><th width='10%'>Montant Maxi (� HT)</th></tr></thead>";
	echo "<tbody>";
	$tot_apd = 0;
	if ($rowlot!=0)
	{
		while ($lignelot=mysql_fetch_assoc($resultlot))
		{
			/* Recherche de l'entreprise titulaire 
			$reqent="SELECT ent_nom FROM entreprises WHERE ent_id=".$lignecht["id_ent"];
			$resultent=mysql_query($reqent,$link);
			$rowent=mysql_num_rows($resultent);
			if ($rowent!=0){
				while ($ligneent=mysql_fetch_assoc($resultent))
				{
					$ent_cht = $ligneent["ent_nom"];
				}
			}*/
			echo '<tr><td align="center" width="10%"><button data-tooltip="Visualiser" data-id="'.$lignelot["tranche_id"].'" class="btn btn-sm btn-default visutranche"><i class="fa fa-eye"></i></button> <button data-tooltip="Supprimer" data-id="'.$lignelot["tranche_id"].'" class="btn btn-sm btn-default deltranche"><i class="fa fa-trash"></i></button></td><td align="center" width="15%"></td><td>'.$lignelot["lex_libelle"].'</td><td width="10%" class="text-right"> �</td><td width="10%" class="text-right"> �</td></tr>';
		}
	}else{
		echo '<tr><td colspan="5" align="center"><b>Aucune tranche</b></td></tr>'; 
	}
	echo "</tbody>";
	echo "</table>";
}
function lstnouvbc_marche(){
	require("./compte.php");
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqlot="SELECT * FROM marche";
	$resultlot=mysql_query($reqlot,$link);
	$rowlot=mysql_num_rows($resultlot);
	echo '<option value="0">Choisir un march�</option>';
	if ($rowlot!=0)
	{
		while ($lignelot=mysql_fetch_assoc($resultlot))
		{
			echo '<option value="'.$lignelot["mar_id"].'">'.$lignelot["mar_numero"].'</option>';
		}
	}
}
function add_indicelot($idlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO lot_indice (lotind_idlot) VALUES (".$idlot.")";
	$result2=mysql_query($req2,$link);
}
function update_indicelot($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE lot_indice SET ".$chp."='".mysql_real_escape_string($val)."' WHERE lotind_id=".$id;
	$result2=mysql_query($req2,$link);	
	$reqentlot="SELECT lot_indice.lotind_type,lot_indice.lotind_dateindice FROM lot_indice WHERE lotind_id=".$id;
	$resultentlot=mysql_query($reqentlot,$link);
	$rowentlot=mysql_num_rows($resultentlot);
	if ($rowentlot!=0)
	{
		while ($data=mysql_fetch_assoc($resultentlot))
		{
			if ($data["lotind_type"]!="" && $data["lotind_dateindice"]!=""){
				$reqentind="SELECT valeur_indice FROM indice_valeur WHERE type_indice='".$data["lotind_type"]."' AND date_indice='".$data["lotind_dateindice"]."'";
				$reqentind=mysql_query($reqentind,$link);
				$ind=mysql_fetch_assoc($reqentind);
				$req2="UPDATE lot_indice SET lotind_valeur='".$ind["valeur_indice"]."' WHERE lotind_id=".$id;
				$result2=mysql_query($req2,$link);
			}
		}
	}
}
function del_indicelot($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM lot_indice WHERE lotind_id=".$id;
	$result2=mysql_query($req2,$link);
}
function lst_indicelot($idlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button id='add_indicelot' data-idlot='".$idlot."' data-idcht='".$idcht."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th class='text-center'>Type</th><th class='text-center'>Date</th><th class='text-center' width='10%'>Valeur</th></tr></thead>";
	echo "<tbody>";
	$reqentlot="SELECT lot_indice.lotind_id,lot_indice.lotind_type,lot_indice.lotind_dateindice,lot_indice.lotind_valeur FROM lot_indice WHERE lotind_idlot=".$idlot;
	$resultentlot=mysql_query($reqentlot,$link);
	$rowentlot=mysql_num_rows($resultentlot);
	if ($rowentlot!=0)
	{
		while ($data=mysql_fetch_assoc($resultentlot))
		{
			echo '<tr><td align="center" width="8%"><button data-id="'.$data["lotind_id"].'" data-idlot="'.$idlot.'" class="btn btn-sm btn-default del_indicelot"><i class="fa fa-trash"></i></button></td>';
			echo '<td><select class="form-control input-sm modifchpindicelot" data-chp="lotind_type" data-id="'.$data["lotind_id"].'" data-idlot="'.$idlot.'">';
			echo '<option value="0">S�lectionnez un type</option>';
			$req="SELECT libelle FROM indice_type";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["libelle"]==$data["lotind_type"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$ligne["libelle"].'" '.$selected.'>'.$ligne["libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td><select class="form-control input-sm modifchpindicelot" data-chp="lotind_dateindice" data-id="'.$data["lotind_id"].'" data-idlot="'.$idlot.'">';
			echo '<option value="0">Ann�e/Mois</option>';
			$req="SELECT date_indice FROM indice_valeur WHERE type_indice='".$data["lotind_type"]."'";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				$anneecode = substr($ligne["date_indice"],0,4);
				$moiscode = substr($ligne["date_indice"],-2);
				if ($ligne["date_indice"]==$data["lotind_dateindice"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$ligne["date_indice"].'" '.$selected.'>'.$anneecode.'/'.$moiscode.'</option>';
			}
			echo '</select></td>';
			echo '<td width="15%" class="text-center">'.$data["lotind_valeur"].'</td>';
			//echo '<td class="text-center"><input class="form-control input-sm modifchpindicelot" data-chp="formule_actu" data-id="'.$data["id"].'" data-idlot="'.$idlot.'" value="'.$data["formule_actu"].'"></td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="5"><b>Aucun indice</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function add_formulelot($idlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO lot_formule (lotform_idlot) VALUES (".$idlot.")";
	$result2=mysql_query($req2,$link);
}
function update_formulelot($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE lot_formule SET ".$chp."='".utf8_decode($val)."' WHERE lotform_id=".$id;
	$result2=mysql_query($req2,$link);	
}
function del_formulelot($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM lot_formule WHERE lotform_id=".$id;
	$result2=mysql_query($req2,$link);
}
function lst_formulelot($idlot){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button id='add_formulelot' data-idlot='".$idlot."' data-idcht='".$idcht."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th class='text-center'>Libell�</th><th class='text-center'>Formule</th></tr></thead>";
	echo "<tbody>";
	$reqentlot="SELECT lot_formule.lotform_id,lot_formule.lotform_libelle,lot_formule.lotform_formuleactu FROM lot_formule WHERE lotform_idlot=".$idlot;
	$resultentlot=mysql_query($reqentlot,$link);
	$rowentlot=mysql_num_rows($resultentlot);
	if ($rowentlot!=0)
	{
		while ($data=mysql_fetch_assoc($resultentlot))
		{
			echo '<tr><td align="center" width="8%"><button data-id="'.$data["lotform_id"].'" data-idlot="'.$idlot.'" class="btn btn-sm btn-default del_formulelot"><i class="fa fa-trash"></i></button></td>';
			echo '<td class="text-center"><input class="form-control input-sm modifchpformulelot" data-chp="lotform_libelle" data-id="'.$data["lotform_id"].'" data-idlot="'.$idlot.'" value="'.$data["lotform_libelle"].'"></td>';
			echo '<td class="text-center"><input class="form-control input-sm modifchpformulelot" data-chp="lotform_formuleactu" data-id="'.$data["lotform_id"].'" data-idlot="'.$idlot.'" value="'.$data["lotform_formuleactu"].'"></td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="3"><b>Aucune formule</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
/* Bon de commande */
function detail_boncommande($idbc){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT boncommande.bc_numero, boncommande.bc_id FROM boncommande WHERE boncommande.bc_id=".$idbc;
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			$panel=1;
			echo "<div class='col-md-12'>";
			echo "<div class='portlet'><div class='portlet-title'><i class='fa fa-shopping-basket' aria-hidden='true'></i> <b>Bon de commande</b> ".$ligne2["bc_numero"]."</div><div class='portlet-content' id='detboncommande'>";
			echo "<div class='row'>";

			echo '<div class="col-sm-12">';
			echo "<button id='back_accueil' class='btn btn-default btn-sm' data-idope='".$ligne2["bc_id"]."'><i class='fa fa-reply'></i> Accueil</button>";
			echo '</div><br><br>';

			echo '<div class="col-sm-12">';
			echo '<div class="panel-group" id="accordion">';
			/* G�n�ralit�s */
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h2 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-file" aria-hidden="true"></i> G�n�ralit�s</a>';
			echo '</h2>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detailgene_boncommande">';
				detailgene_boncommande($idbc);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Prestations associ�es */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-bolt" aria-hidden="true"></i> Prestations associ�es</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detchtbc">';
				lst_chtbc($idbc);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Paiements associ�s */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="far fa-money-bill-alt" aria-hidden="true"></i> Paiements associ�s</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detpaiebc">';
				lst_paiebc($idbc);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* OS/OE associ�s */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-edit" aria-hidden="true"></i> OS/OE associ�s</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detosbc">';
				lst_osbc($idbc);
			echo '</div>';
			echo '</div>';
			echo '</div>';

			echo '</div>';
			echo '</div>';
		}
	}
}
function detailgene_boncommande($idbc){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM boncommande WHERE bc_id=".$idbc;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		//$datedemande = strftime("%d/%m/%Y",strtotime($ligne["date_demande"]));
		$numero = $ligne["bc_numero"];
		$type = $ligne["bc_type"];
		$id_ent = $ligne["bc_ident"];
		$id_tva = $ligne["bc_idtva"];
		$date_bc = strftime("%d/%m/%Y",strtotime($ligne["bc_datebc"]));
		$delai = $ligne["bc_delai"];
		$typedelai = $ligne["bc_typedelai"];
		$date_finbc = strftime("%d/%m/%Y",strtotime($ligne["bc_datefinbc"]));
		$bc_montantdevisht = $ligne["bc_montantdevisht"];
		$bc_montantenginiht = $ligne["bc_montantenginiht"];
		$bc_montantenginittc = $ligne["bc_montantenginittc"];
		$bc_montantengmodifht = $ligne["bc_montantengmodifht"];
		$bc_montantengmodifttc = $ligne["bc_montantengmodifttc"];
		$bc_cumulpaieht = $ligne["bc_cumulpaieht"];
		$bc_cumulpaiettc = $ligne["bc_cumulpaiettc"];
	}

	echo '<div class="col-sm-12">';
	echo '<div class="panel panel-default" style="margin-bottom:20px;">';
	echo '<div class="panel-heading">G�n�ralit�s</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="boncommande_numero">Num�ro:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpboncommande" data-idbc="'.$idbc.'" data-chp="bc_numero" id="boncommande_numero" name="boncommande_numero" value="'.$numero.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="boncommande_type">Type:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpboncommande" data-idbc="'.$idbc.'" data-chp="bc_type" id="boncommande_type" name="boncommande_type">';
		echo '<option value="0">Choisir un type de bon de commande</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='TYPE_BC'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$type){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="boncommande_ent">Entreprise:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpboncommande" data-idbc="'.$idbc.'" data-chp="bc_ident" id="boncommande_ent" name="boncommande_ent">';
		echo '<option value="0">S�lectionnez une entreprise</option>';
		$req="SELECT ent_id,ent_nom FROM entreprises";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["ent_id"]==$id_ent){$selected = "selected";}else{$selected = "";}
			echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
		}
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="boncommande_datebc">Date:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm chpdatebc modifchpboncommande" data-idbc="'.$idbc.'" data-chp="bc_datebc" id="boncommande_datebc" name="boncommande_datebc" value="'.$date_bc.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="boncommande_typedelai">Type de d�lai:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpboncommande" data-idbc="'.$idbc.'" data-chp="bc_typedelai" id="boncommande_typedelai" name="boncommande_typedelai">';
		echo '<option value="0">Choisir un type de d�lai</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='TYPEDELAI_BC'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$typedelai){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="boncommande_delai">D�lai:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpboncommande" data-idbc="'.$idbc.'" data-chp="bc_delai" id="boncommande_delai" name="boncommande_delai" value="'.$delai.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="boncommande_datefinbc">Date fin:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm chpdatebc modifchpboncommande" data-idbc="'.$idbc.'" data-chp="bc_datefinbc" id="boncommande_datefinbc" name="boncommande_datefinbc" value="'.$date_finbc.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-12">';
	echo '<div class="panel panel-default" style="margin-bottom:20px;">';
	echo '<div class="panel-heading"><i class="far fa-money-bill-alt"></i> Montants</div>';
	echo '<div class="panel-body" id="detailgenemont_boncommande">';
		echo '<table class="table table-sm table-responsive table-bordered table-striped table-hover">';
		echo '<thead><tr><th class="text-center" width="10%">Prestations</th><th class="text-center">Devis HT</th><th class="text-center">Engag� initial HT</th><th class="text-center">Engag� initial TTC</th><th class="text-center">Engag� modif HT</th><th class="text-center">Engag� modif TTC</th><th class="text-center">Cumul des paiements HT</th><th class="text-center">Cumul des paiements TTC</th></tr></thead>';
		echo '<tbody>';
		$req="SELECT * FROM bc_cht LEFT JOIN chantiers ON chantiers.cht_id=bc_cht.bccht_idcht WHERE bccht_idbc=".$idbc;
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo '<tr><td width="30%">'.$ligne["cht_numero"].'</td><td width="10%" class="text-right">'.$ligne["bccht_montantdevisht"].'</td><td width="10%" class="text-right">'.$ligne["bccht_montantenginiht"].'</td><td width="10%" class="text-right">'.$ligne["bccht_montantenginittc"].'</td><td width="10%" class="text-right">'.$ligne["bccht_montantengmodifht"].'</td><td width="10%" class="text-right">'.$ligne["bccht_montantengmodifttc"].'</td><td width="10%" class="text-right">'.$ligne["bccht_cumulpaieht"].'</td><td width="10%" class="text-right">'.$ligne["bccht_cumulpaiettc"].'</td></tr>';
		}
		echo '<tr><td width="10%"><b>Total</b></td><td width="10%" class="text-right"><b>'.$bc_montantdevisht.'</b></td><td width="10%" class="text-right"><b>'.$bc_montantenginiht.'</b></td><td width="10%" class="text-right"><b>'.$bc_montantenginittc.'</b></td><td width="10%" class="text-right"><b>'.$bc_montantengmodifht.'</b></td><td width="10%" class="text-right"><b>'.$bc_montantengmodifttc.'</b></td><td width="10%" class="text-right"><b>'.$bc_cumulpaieht.'</b></td><td width="10%" class="text-right"><b>'.$bc_cumulpaiettc.'</b></td></tr>';
		echo '</tbody>';
		echo '</table>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function update_boncommande($idbc,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	switch ($chp){
		case "bc_datebc":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
	}
	$req2="UPDATE boncommande SET ".$chp."='".$val."' WHERE bc_id=".$idbc;
	$result2=mysql_query($req2,$link);
	/* Calcul date fin de bon de commande */
	$sql = "SELECT boncommande.bc_delai,boncommande.bc_datebc,lexique.lex_typelexique FROM boncommande LEFT JOIN lexique ON lexique.lex_id=boncommande.bc_typedelai WHERE bc_id=".$idbc;
	$result=mysql_query($sql,$link);
	$ligne=mysql_fetch_assoc($result);
	$datebc = $ligne["bc_datebc"];
	$delai = $ligne["bc_delai"];
	$typedelai = $ligne["lex_typelexique"];
	$datefinbc = date("Y-m-d",strtotime($datebc." +".$delai." ".$typedelai));
	$req2="UPDATE boncommande SET bc_datefinbc='".$datefinbc."' WHERE bc_id=".$idbc;
	$result2=mysql_query($req2,$link);
}
function add_chtbc($idbc,$numero){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT cht_id,cht_ident,cht_programme,cht_anneeprog FROM chantiers WHERE cht_numero LIKE '%".$numero."'";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$idcht = $ligne["cht_id"];
		$ident = $ligne["cht_ident"];
		$programme = $ligne["cht_programme"];
		$anneeprog = $ligne["cht_anneeprog"];
	}
	$req2="INSERT INTO bc_cht (id_bc,id_cht,id_ent,programme,annee_prog) VALUES (".$idbc.",".$idcht.",".$ident.",'".$programme."','".$anneeprog."')";
	$result2=mysql_query($req2,$link);
}
function update_chtbc($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	if ($chp=="programme"){
		/* R�initialisation de l'ann�e programme pour le programme */
		$chp="bccht_idanneeprog";
		$val=0;		
	}
	$req2="UPDATE bc_cht SET ".$chp."='".$val."' WHERE bccht_id=".$id;
	$result2=mysql_query($req2,$link);	
	$req3="SELECT bc_cht.bccht_typeindice,bc_cht.bccht_dateindice FROM bc_cht WHERE bccht_id=".$id;
	$result3=mysql_query($req3,$link);
	$row3=mysql_num_rows($result3);
	if ($row3!=0)
	{
		while ($data=mysql_fetch_assoc($result3))
		{
			if ($data["bccht_typeindice"]!="" && $data["bccht_dateindice"]!=""){
				$reqentind="SELECT valeur_indice FROM indice_valeur WHERE type_indice='".$data["bccht_typeindice"]."' AND date_indice='".$data["bccht_dateindice"]."'";
				$reqentind=mysql_query($reqentind,$link);
				$ind=mysql_fetch_assoc($reqentind);
				$req2="UPDATE bc_cht SET bccht_valindice='".$ind["valeur_indice"]."' WHERE bccht_id=".$id;
				$result2=mysql_query($req2,$link);
			}
		}
	}
	$req="SELECT bccht_idcht,bccht_ident,bccht_programme,bccht_anneeprog FROM bc_cht WHERE bccht_id=".$id;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$idcht = $ligne["bccht_idcht"];
		$ident = $ligne["bccht_ident"];
		$programme = $ligne["bccht_programme"];
		$anneeprog = $ligne["bccht_anneeprog"];
	}
	$req2="UPDATE chantiers SET cht_ident=".$ident.", cht_programme='".$programme."',cht_anneeprog='".$anneeprog."' WHERE cht_id=".$idcht;
	$result2=mysql_query($req2,$link);	
}
function del_chtbc($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM bc_cht WHERE bccht_id=".$id;
	$result2=mysql_query($req2,$link);
}
function lst_chtbc($idbc){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button id='add_chtbc' data-idbc='".$idbc."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th align='center' width='10%'>Op�ration</th><th align='center' width='15%'>Prestation</th><th class='text-center' width='30%'>Programme</th><th class='text-center'>Entreprise</th><th class='text-center'>Type indice</th><th class='text-center'>Date indice</th><th class='text-center'>Valeur</th></tr></thead>";
	echo "<tbody>";
	$reqentlot="SELECT bc_cht.bccht_id, bc_cht.bccht_idanneeprog, bc_cht.bccht_ident, bc_cht.bccht_typeindice, bc_cht.bccht_dateindice, bc_cht.bccht_valindice, chantiers.cht_numero as numcht, operations.ope_numero as numope FROM bc_cht JOIN chantiers ON chantiers.cht_id=bc_cht.bccht_idcht JOIN operations ON operations.ope_id=chantiers.cht_idope WHERE bccht_idbc=".$idbc;
	$resultentlot=mysql_query($reqentlot,$link);
	$rowentlot=mysql_num_rows($resultentlot);
	if ($rowentlot!=0)
	{
		while ($data=mysql_fetch_assoc($resultentlot))
		{
			echo '<tr><td align="center" width="8%"><button data-id="'.$data["bccht_id"].'" data-idbc="'.$idbc.'" class="btn btn-sm btn-default del_chtbc"><i class="fa fa-trash"></i></button></td>';
			echo '<td width="10%" class="text-center">'.$data["numope"].'</td>';
			echo '<td width="15%" class="text-center">'.$data["numcht"].'</td>';
			echo '<td><div class="col-md-12" id="detailfinancebccht">';

			$req="SELECT annee_prog.anneeprog_idprog FROM annee_prog JOIN programme ON programme.prog_id=annee_prog.anneeprog_idprog WHERE annee_prog.anneeprog_id=".$data["bccht_idanneeprog"];
			$result=mysql_query($req,$link);
			$ligne=mysql_fetch_assoc($result);
			$id_prog = $ligne["anneeprog_idprog"];
			finance_cht($data["bccht_idanneeprog"],$id_prog,$idbc,$data["bccht_id"]);

			echo '</div></td>';
			echo '<td><select class="form-control input-sm modifchpchtbc" data-idbc="'.$idbc.'" data-chp="bccht_ident" data-id="'.$data["bccht_id"].'">';
			echo '<option value="0">S�lectionnez une entreprise</option>';
			$req="SELECT ent_id,ent_nom FROM entreprises";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["ent_id"]==$data["bccht_ident"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
			}
			echo '</select></td>';
			echo '<td><select class="form-control input-sm modifchpchtbc" data-chp="bccht_typeindice" data-id="'.$data["bccht_id"].'" data-idbc="'.$idbc.'">';
			echo '<option value="0">S�lectionnez un type</option>';
			$req="SELECT libelle FROM indice_type";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["libelle"]==$data["bccht_typeindice"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$ligne["libelle"].'" '.$selected.'>'.$ligne["libelle"].'</option>';
			}
			echo '</select></td>';
			echo '<td><select class="form-control input-sm modifchpchtbc" data-chp="bccht_dateindice" data-id="'.$data["bccht_id"].'" data-idbc="'.$idbc.'">';
			echo '<option value="0">Ann�e/Mois</option>';
			$req="SELECT date_indice FROM indice_valeur WHERE type_indice='".$data["bccht_typeindice"]."'";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				$anneecode = substr($ligne["date_indice"],0,4);
				$moiscode = substr($ligne["date_indice"],-2);
				if ($ligne["date_indice"]==$data["bccht_dateindice"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$ligne["date_indice"].'" '.$selected.'>'.$anneecode.'/'.$moiscode.'</option>';
			}
			echo '</select></td>';
			echo '<td width="5%" class="text-center">'.$data["bccht_valindice"].'</td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="9"><b>Aucune prestation</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function finance_cht($id_anneeprog,$id_prog,$idbc,$idbccht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<div class="col-md-6"><select class="form-control input-sm col-md-6 modifchpchtbc" data-chp="programme" id="lstprogbccht" name="lstprogbccht" data-id="'.$idbccht.'" data-idbc="'.$idbc.'">';
	echo '<option value="-1">S�lectionnez un programme</option>';
	$req="SELECT prog_id,prog_libelle FROM programme";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["prog_id"]==$id_prog){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$ligne["prog_id"].'" '.$selected.'>'.$ligne["prog_libelle"].'</option>';
	}
	echo '</select></div>';
	lst_anneeprog($id_anneeprog,$id_prog,$idbc,$idbccht);
}
function lst_anneeprog($id_anneeprog,$id_prog,$idbc,$idbccht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<div class="col-md-6"><select class="form-control input-sm modifchpchtbc" data-id="'.$idbccht.'" data-idbc="'.$idbc.'" data-chp="bccht_idanneeprog" id="lstannprogbccht" name="lstannprogbccht">';
	echo '<option value="-1">S�lectionnez une ann�e</option>';
	$req="SELECT annee_prog.anneeprog_id,annee_prog.anneeprog_annee FROM annee_prog WHERE annee_prog.anneeprog_idprog=".$id_prog;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["anneeprog_id"]==$id_anneeprog){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$ligne["anneeprog_id"].'" '.$selected.'>'.$ligne["anneeprog_annee"].'</option>';
	}
	echo '</select></div>';
}
function add_paiebc($idbc,$natpaie){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO paiement (id_bc,date_paie,nature_paie) VALUES (".$idbc.",NOW(),".$natpaie.")";
	$result2=mysql_query($req2,$link);
	$idpaie = mysql_insert_id($link);
	$req="SELECT bc_cht.bccht_idcht FROM bc_cht WHERE bccht_idbc=".$idbc;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$req2="INSERT INTO vent_paie (id_paie,id_cht) VALUES (".$idpaie.",".$ligne["bccht_idcht"].")";
		$result2=mysql_query($req2,$link);
	}
}
function del_paiebc($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	/* Suppression des ventilations */
	$req="SELECT vent_paie.id FROM vent_paie WHERE id_paie=".$id;
	$result=mysql_query($req,$link);
	//$idventpaie = $ligne=mysql_fetch_assoc($result);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$req2="DELETE FROM val_ventpaie WHERE id_ventpaie=".$ligne["id"];
		$result2=mysql_query($req2,$link);
	}
	$req2="DELETE FROM vent_paie WHERE id_paie=".$id;
	$result2=mysql_query($req2,$link);
	/* Suppression du paiement*/
	$req2="DELETE FROM paiement WHERE id=".$id;
	$result2=mysql_query($req2,$link);
}
function lst_paiebc($idbc){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='10%'><button id='add_paiebc' data-idbc='".$idbc."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th align='center' width='8%'>Num�ro</th><th align='center' width='8%'>Date</th><th align='center' width='46%'>Nature</th><th align='center' width='15%'>Montant Travaux (TTC)</th><th align='center' width='15%'>Montant � payer</th></tr></thead>";
	echo "<tbody>";
	$reqpaiebc="SELECT paiement.id,paiement.numero,paiement.date_paie,lexique.lex_libelle,paiement.cumul_ttc FROM paiement JOIN lexique ON lexique.lex_id=paiement.nature_paie WHERE id_bc=".$idbc." ORDER BY paiement.date_paie ASC";
	$resultpaiebc=mysql_query($reqpaiebc,$link);
	$rowpaiebc=mysql_num_rows($resultpaiebc);
	if ($rowpaiebc!=0)
	{
		while ($data=mysql_fetch_assoc($resultpaiebc))
		{
			/* Recherche paiement ult�rieur pour activation des boutons */
			$reqbcpaie="SELECT * FROM paiement WHERE paiement.date_paie>DATE('".$data["date_paie"]."') AND paiement.id_bc=".$idbc;
			$resultbcpaie=mysql_query($reqbcpaie,$link);
			$rowbcpaie=mysql_num_rows($resultbcpaie);
			if ($rowbcpaie!=0){$buttons = "disabled";}else{$buttons = "";}
			echo '<tr><td align="center" width="10%"><button data-id="'.$data["id"].'" data-idbc="'.$idbc.'" class="btn btn-sm btn-default visu_paiebc"><i class="fa fa-eye"></i></button> <button data-id="'.$data["id"].'" data-idbc="'.$idbc.'" class="btn btn-sm btn-default del_paiebc" '.$buttons.'><i class="fa fa-trash"></i></button></td>';
			echo '<td width="8%" class="text-center">'.$data["numero"].'</td>';
			echo '<td width="8%" class="text-center">'.strftime("%d/%m/%Y",strtotime($data["date_paie"])).'</td>';
			echo '<td width="46%" class="text-center">'.$data["lex_libelle"].'</td>';
			echo '<td width="15%" class="text-right">'.$data["cumul_ttc"].'</td>';
			echo '<td width="15%" class="text-right">'.$data["cumul_ttc"].'</td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="6"><b>Aucun paiement</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function add_osbc($idbc,$typeos){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO os (id_bc,date_os,date_finos,type) VALUES (".$idbc.",NOW(),NOW(),".$typeos.")";
	$result2=mysql_query($req2,$link);
	$idos = mysql_insert_id($link);
	$req="SELECT bc_cht.bccht_idcht FROM bc_cht WHERE bccht_idbc=".$idbc;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$req2="INSERT INTO os_cht (id_os,id_cht) VALUES (".$idos.",".$ligne["bccht_idcht"].")";
		$result2=mysql_query($req2,$link);
	}
}
function del_osbc($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM os_cht WHERE id_os=".$id;
	$result2=mysql_query($req2,$link);
	/* Suppression du paiement*/
	$req2="DELETE FROM os WHERE id=".$id;
	$result2=mysql_query($req2,$link);
}
function lst_osbc($idbc){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='10%'><button id='add_osbc' data-idbc='".$idbc."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th align='center' width='10%'>Num�ro</th><th align='center' width='8%'>Date</th><th align='center' width='46%'>Type</th><th align='center' width='8%'>D�lai (sem.)</th><th align='center' width='8%'>Prolongation (sem.)</th><th align='center' width='15%'>Montant (TTC)</th></tr></thead>";
	echo "<tbody>";
	$reqosbc="SELECT os.id,os.numero,os.date_os,lexique.lex_libelle,os.delai,os.prolong,os.montant_ttc FROM os JOIN lexique ON lexique.lex_id=os.type WHERE os.id_bc=".$idbc." ORDER BY os.date_os ASC";
	$resultosbc=mysql_query($reqosbc,$link);
	$rowosbc=mysql_num_rows($resultosbc);
	if ($rowosbc!=0)
	{
		while ($data=mysql_fetch_assoc($resultosbc))
		{
			echo '<tr><td align="center" width="10%"><button data-id="'.$data["id"].'" data-idbc="'.$idbc.'" class="btn btn-sm btn-default visu_osbc"><i class="fa fa-eye"></i></button> <button data-id="'.$data["id"].'" data-idbc="'.$idbc.'" class="btn btn-sm btn-default del_osbc"><i class="fa fa-trash"></i></button></td>';
			echo '<td width="10%" class="text-center">'.$data["numero"].'</td>';
			echo '<td width="8%" class="text-center">'.strftime("%d/%m/%Y",strtotime($data["date_os"])).'</td>';
			echo '<td class="text-center">'.$data["lex_libelle"].'</td>';
			echo '<td width="8%" class="text-center">'.$data["delai"].'</td>';
			echo '<td width="8%" class="text-center">'.$data["prolong"].'</td>';
			echo '<td width="15%" class="text-right">'.$data["montant_ttc"].'</td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="7"><b>Aucun ordre de service</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function detail_os($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT os.numero, os.id, boncommande.bc_id as idbc FROM os JOIN boncommande ON boncommande.bc_id=os.id_bc WHERE os.id=".$id;
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			$panel=1;
			echo "<div class='col-md-12'>";
			echo "<div class='portlet'><div class='portlet-title'><i class='fa fa-edit' aria-hidden='true'></i> OS n� ".$ligne2["numero"]."</div><div class='portlet-content' id='detos'>";
			echo "<div class='row'>";

			echo '<div class="col-sm-12">';
			echo "<button id='back_bc' class='btn btn-default btn-sm' data-idbc='".$ligne2["idbc"]."'><i class='fa fa-reply'></i> Bon de commande</button>";
			echo '</div><br><br>';

			echo '<div class="col-sm-12">';
			echo '<div class="panel-group" id="accordion">';
			/* G�n�ralit�s */
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h2 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-file" aria-hidden="true"></i> G�n�ralit�s</a>';
			echo '</h2>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse in">';
			echo '<div class="panel-body" id="detailgene_os">';
				detailgene_os($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Prestations associ�es */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-bolt" aria-hidden="true"></i> Prestations associ�es</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="lst_chtos">';
				lst_chtos($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';

			echo '</div>';
			echo '</div>';
		}
	}
}
function detailgene_os($idos){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM os WHERE id=".$idos;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		//$datedemande = strftime("%d/%m/%Y",strtotime($ligne["date_demande"]));
		$numero = $ligne["numero"];
		$type = $ligne["type"];
		$id_ent = $ligne["id_ent"];
		$id_tva = $ligne["id_tva"];
		$delai = $ligne["delai"];
		$prolong = $ligne["prolong"];
		$date_os = strftime("%d/%m/%Y",strtotime($ligne["date_os"]));
		$date_finos = strftime("%d/%m/%Y",strtotime($ligne["date_finos"]));
		$montant_ht = $ligne["montant_ht"];
		$montant_ttc = $ligne["montant_ttc"];
	}

	echo '<div class="col-sm-8">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">G�n�ralit�s</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="os_numero">Num�ro OS:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpos" data-idos="'.$idos.'" data-chp="numero" id="os_numero" name="os_numero" value="'.$numero.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="os_type">Type:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpos" data-idos="'.$idos.'" data-chp="type" id="os_type" name="os_type">';
		echo '<option value="0">Choisir un type ordre de service</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='TYPE_OS'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$type){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="os_ent">Entreprise:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpos" data-idos="'.$idos.'" data-chp="id_ent" id="os_ent" name="os_ent">';
		echo '<option value="0">S�lectionnez une entreprise</option>';
		$req="SELECT ent_id,ent_nom FROM entreprises";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["ent_id"]==$id_ent){$selected = "selected";}else{$selected = "";}
			echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
		}
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="os_date">Date OS:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm chpdateos modifchpos" data-idos="'.$idos.'" data-chp="date_os" id="os_date" name="os_date" value="'.$date_os.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="os_delai">D�lai (sem.):</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpos" data-idos="'.$idos.'" data-chp="delai" id="os_delai" name="os_delai" value="'.$delai.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="os_prolong">Prolongation (sem.):</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpos" data-idos="'.$idos.'" data-chp="prolong" id="os_prolong" name="os_prolong" value="'.$prolong.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="os_datefinos">Date effet:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm chpdateos modifchpos" data-idos="'.$idos.'" data-chp="date_finos" id="os_datefinos" name="os_datefinos" value="'.$date_finos.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '<div class="col-sm-4">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="far fa-money-bill-alt" aria-hidden="true"></i> Montants</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="montant_ht">Montant HT:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpos text-right" data-idos="'.$idos.'" data-chp="montant_ht" id="montant_ht" name="montant_ht" value="'.$montant_ht.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="os_tva">TVA:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm text-right modifchpos" data-idos="'.$idos.'" data-chp="id_tva" id="os_tva" name="os_tva">';
		echo '<option value="0">Choisir un taux de tva</option>';
		$req="SELECT tva_id,tva_taux FROM tva";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["tva_id"]==$id_tva){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["tva_id"].'" '.$selected.'>'.$ligne["tva_taux"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="montant_ttc">Montant TTC:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpos text-right" data-idos="'.$idos.'" data-chp="montant_ttc" id="montant_ttc" name="montant_ttc" value="'.$montant_ttc.'">';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function lst_chtos($idos){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button id='add_chtbc' data-idbc='".$idbc."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th align='center' width='15%'>Op�ration</th><th align='center' width='15%'>Prestation</th><th class='text-center' width='10%'>Prog.</th><th class='text-center' width='10%'>Ann�e prog.</th><th class='text-center'>Entreprise</th></tr></thead>";
	echo "<tbody>";
	$reqoscht="SELECT os_cht.id, chantiers.cht_numero as numcht, operations.ope_numero as numope FROM os_cht JOIN chantiers ON chantiers.cht_id=os_cht.id_cht JOIN operations ON chantiers.cht_idope=operations.ope_id WHERE id_os=".$idos;
	$resultoscht=mysql_query($reqoscht,$link);
	$rowoscht=mysql_num_rows($resultoscht);
	if ($rowoscht!=0)
	{
		while ($data=mysql_fetch_assoc($resultoscht))
		{
			echo '<tr><td align="center" width="8%"><button data-id="'.$data["id"].'" data-idos="'.$idos.'" class="btn btn-sm btn-default del_chtos"><i class="fa fa-trash"></i></button></td>';
			echo '<td width="8%" class="text-center">'.$data["numope"].'</td>';
			echo '<td width="8%" class="text-center">'.$data["numcht"].'</td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="3"><b>Aucune prestation</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function update_os($idos,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	switch ($chp){
		case "date_os":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "date_finos":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "id_tva":
			$sql = "SELECT tva.tva_taux FROM tva WHERE tva.tva_id=".$val;
			$result=mysql_query($sql,$link);
			$ligne=mysql_fetch_assoc($result);
			$taux = $ligne["tva_taux"];
			$sql = "SELECT os.montant_ttc FROM os WHERE os.id=".$idos;
			$result=mysql_query($sql,$link);
			$ligne=mysql_fetch_assoc($result);
			$montant_ttc = $ligne["montant_ttc"];
			$ht = $montant_ttc - ($montant_ttc*($taux/100));
			$req2="UPDATE os SET montant_ht='".$ht."' WHERE id=".$idos;
			$result2=mysql_query($req2,$link);
			break;
		case "montant_ttc":
			$sql = "SELECT tva.tva_taux FROM tva join os on tva.tva_id=os.id_tva WHERE os.id=".$idos;
			$result=mysql_query($sql,$link);
			$ligne=mysql_fetch_assoc($result);
			$taux = $ligne["tva_taux"];
			$ht = $val - ($val*($taux/100));
			$req2="UPDATE os SET montant_ht='".$ht."' WHERE id=".$idos;
			$result2=mysql_query($req2,$link);
			break;
	}
	$req2="UPDATE os SET ".$chp."='".$val."' WHERE id=".$idos;
	$result2=mysql_query($req2,$link);	
}
function detail_paiement($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT paiement.numero, paiement.id, boncommande.bc_id as idbc FROM paiement JOIN boncommande ON boncommande.bc_id=paiement.id_bc WHERE paiement.id=".$id;
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			$panel=1;
			echo "<div class='col-md-12'>";
			echo "<div class='portlet'><div class='portlet-title'><i class='far fa-money-bill-alt' aria-hidden='true'></i> Paiement n� ".$ligne2["numero"]."</div><div class='portlet-content' id='detpaiement'>";
			echo "<div class='row'>";

			echo '<div class="col-sm-12">';
			echo "<button id='back_bc' class='btn btn-default btn-sm' data-idbc='".$ligne2["idbc"]."'><i class='fa fa-reply'></i> Bon de commande</button>";
			echo '</div><br><br>';

			echo '<div class="col-sm-12">';
			echo '<div class="panel-group" id="accordion">';
			/* G�n�ralit�s */
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h2 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-file" aria-hidden="true"></i> G�n�ralit�s</a>';
			echo '</h2>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse in">';
			echo '<div class="panel-body" id="detailgene_paiement">';
				detailgene_paiement($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Jalonnements */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-calendar-alt" aria-hidden="true"></i> Jalonnements</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detailjalon_paiement">';
				detailjalon_paiement($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Ventilation */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-inbox" aria-hidden="true"></i> Ventilation</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detailvent_paiement">';
				detailvent_paiement($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';

			echo '</div>';
			echo '</div>';
		}
	}
}
function detailgene_paiement($idpaie){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM paiement WHERE id=".$idpaie;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		//$datedemande = strftime("%d/%m/%Y",strtotime($ligne["date_demande"]));
		$numero = $ligne["numero"];
		$num_certif = $ligne["num_certif"];
		$num_mandat = $ligne["num_mandat"];
		$nature = $ligne["nature_paie"];
		$id_tva = $ligne["id_tva"];
		$id_ent = $ligne["id_ent"];
		$date_paie = strftime("%d/%m/%Y",strtotime($ligne["date_paie"]));
		$date_certif = strftime("%d/%m/%Y",strtotime($ligne["date_certif"]));
		$date_mandat = strftime("%d/%m/%Y",strtotime($ligne["date_mandat"]));
	}

	echo '<div class="col-sm-8">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">G�n�ralit�s</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="paiement_numero">Num�ro facture:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="numero" id="paiement_numero" name="paiement_numero" value="'.$numero.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="paiement_nature">Nature:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="nature_paie" id="paiement_nature" name="paiement_nature">';
		echo '<option value="0">Choisir un type de bon de commande</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='NAT_PAIE'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_id"]==$nature){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_id"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="paiement_tva">TVA:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm text-right modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="id_tva" id="paiement_tva" name="paiement_tva">';
		echo '<option value="0">Choisir un taux de tva</option>';
		$req="SELECT tva_id,tva_taux FROM tva";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["tva_id"]==$id_tva){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["tva_id"].'" '.$selected.'>'.$ligne["tva_taux"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="paiement_ent">Entreprise:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="id_ent" id="paiement_ent" name="paiement_ent">';
		echo '<option value="0">S�lectionnez une entreprise</option>';
		$req="SELECT ent_id,ent_nom FROM entreprises";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["ent_id"]==$id_ent){$selected = "selected";}else{$selected = "";}
			echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
		}
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="paiement_numcertif">Num�ro certificat:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="num_certif" id="paiement_numcertif" name="paiement_numcertif" value="'.$num_certif.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="paiement_nummandat">Num�ro mandat:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="num_mandat" id="paiement_nummandat" name="paiement_nummandat" value="'.$num_mandat.'">';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-4">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-calendar-alt"></i> Dates</div>';
	echo '<div class="panel-body" id="detailgenedelai_paiement">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="paiement_date">Date facture:</label>';
		echo '<div class="col-sm-6">';
		echo '<input type="text" class="form-control input-sm text-right chpdatepaie modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_paie" id="paiement_date" name="paiement_date" value="'.$date_paie.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="paiement_datecertif">Date certificat:</label>';
		echo '<div class="col-sm-6">';
		echo '<input type="text" class="form-control input-sm text-right chpdatepaie modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_certif" id="paiement_datecertif" name="paiement_datecertif" value="'.$date_certif.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-6" for="paiement_datemandat">Date mandat:</label>';
		echo '<div class="col-sm-6">';
		echo '<input type="text" class="form-control input-sm text-right chpdatepaie modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_mandat" id="paiement_datemandat" name="paiement_datemandat" value="'.$date_mandat.'">';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function detailjalon_paiement($idpaie){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM paiement WHERE id=".$idpaie;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$date_presfactsstraitant = strftime("%d/%m/%Y",strtotime($ligne["date_presfactsstraitant"]));
		$date_limitacceptfacttitulaire = strftime("%d/%m/%Y",strtotime($ligne["date_limitacceptfacttitulaire"]));
		$date_arrivefacture = strftime("%d/%m/%Y",strtotime($ligne["date_arrivefacture"]));
		$date_limitemandat = strftime("%d/%m/%Y",strtotime($ligne["date_limitemandat"]));
		$date_limitepaiement = strftime("%d/%m/%Y",strtotime($ligne["date_limitepaiement"]));
	}

	//echo '<div class="col-sm-12">';
	//echo '<div class="panel panel-default">';
	//echo '<div class="panel-heading"><i class="fa fa-calendar-alt"></i> Dates</div>';
	//echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-8" for="paiement_datepresfactsstrait">Pr�sentation facture sous-traitant:</label>';
		echo '<div class="col-sm-4">';
		echo '<input type="text" class="form-control input-sm chpdatepaie modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_presfactsstraitant" id="paiement_datepresfactsstrait" name="paiement_datepresfactsstrait" value="'.$date_presfactsstraitant.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-8" for="paiement_datelimacceptfacttit">Limite acceptation facture par titulaire:</label>';
		echo '<div class="col-sm-4">';
		echo '<input type="text" class="form-control input-sm chpdatepaie modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_limitacceptfacttitulaire" id="paiement_datelimacceptfacttit" name="paiement_datelimacceptfacttit" value="'.$date_limitacceptfacttitulaire.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-8" for="paiement_datearrivefact">Arriv� facture:</label>';
		echo '<div class="col-sm-4">';
		echo '<input type="text" class="form-control input-sm chpdatepaie modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_arrivefacture" id="paiement_datearrivefact" name="paiement_datearrivefact" value="'.$date_arrivefacture.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-8" for="paiement_datelimmandat">Limite mandatement:</label>';
		echo '<div class="col-sm-4">';
		echo '<input type="text" class="form-control input-sm chpdatepaie modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_limitemandat" id="paiement_datelimmandat" name="paiement_datelimmandat" value="'.$date_limitemandat.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-8" for="paiement_datelimpaie">Limite paiement:</label>';
		echo '<div class="col-sm-4">';
		echo '<input type="text" class="form-control input-sm chpdatepaie modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_limitepaiement" id="paiement_datelimpaie" name="paiement_datelimpaie" value="'.$date_limitepaiement.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	//echo '</div>';
	//echo '</div>';
	//echo '</div>';
}
function detailvent_paiement($idpaie){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<div class="row">';
	echo '<div class="col-sm-12">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-bolt"></i> Prestations</div>';
	echo '<div class="panel-body" id="tabventcht_paiement">';
		tabventcht_paiement($idpaie);
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '<div class="row">';
	echo '<div class="col-sm-12" id="detailventcht_paiement">';

	echo '</div>';
	echo '</div>';
}
function tabventcht_paiement($idpaie){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'></th><th align='center'>Num�ro</th><th align='center'>Travaux (HT)</th><th align='center'>Montant Actu</th><th align='center'>Autre (HT)</th><th align='center'>P�nalit�s</th><th align='center'>Total HT</th><th align='center'>TVA</th></tr></thead>";
	echo "<tbody>";
	$reqpaiecht="SELECT vent_paie.id,chantiers.cht_numero,vent_paie.montant_trx_ht,vent_paie.montant_actu,vent_paie.montant_autre_ht,vent_paie.montant_penalite,vent_paie.montant_tot_ht,vent_paie.montant_tva,chantiers.cht_id as idcht FROM vent_paie JOIN chantiers ON chantiers.cht_id=vent_paie.id_cht WHERE vent_paie.id_paie=".$idpaie;
	$resultpaiecht=mysql_query($reqpaiecht,$link);
	$tot_trx_ht=0;$tot_actu=0;$tot_autre_ht=0;$tot_penalite=0;$tot_tot_ht=0;$tot_tva=0;
	while ($data=mysql_fetch_assoc($resultpaiecht))
	{
		echo '<tr><td align="center" width="8%"><button data-id="'.$data["id"].'" data-idpaie="'.$idpaie.'" data-idcht="'.$data["idcht"].'" class="btn btn-sm btn-default visu_paieventcht"><i class="fa fa-edit"></i></button></td>';
		echo '<td class="text-center">'.$data["cht_numero"].'</td>';
		echo '<td class="text-right" width="12%">'.number_format($data["montant_trx_ht"], 2, ',', ' ').'</td>';
		echo '<td class="text-right" width="12%">'.number_format($data["montant_actu"], 2, ',', ' ').'</td>';
		echo '<td class="text-right" width="12%">'.number_format($data["montant_autre_ht"], 2, ',', ' ').'</td>';
		echo '<td class="text-right" width="12%">'.number_format($data["montant_penalite"], 2, ',', ' ').'</td>';
		echo '<td class="text-right" width="12%">'.number_format($data["montant_tot_ht"], 2, ',', ' ').'</td>';
		echo '<td class="text-right" width="12%">'.number_format($data["montant_tva"], 2, ',', ' ').'</td>';
		echo '</tr>';
		$tot_trx_ht = $tot_trx_ht + $data["montant_trx_ht"];
		$tot_actu = $tot_actu + $data["montant_actu"];
		$tot_autre_ht = $tot_autre_ht + $data["montant_autre_ht"];
		$tot_penalite = $tot_penalite + $data["montant_penalite"];
		$tot_tot_ht = $tot_tot_ht + $data["montant_tot_ht"];
		$tot_tva = $tot_tva + $data["montant_tva"];
	}
	echo '<tr><td align="center" width="8%"></td>';
	echo '<td class="text-center"><b>TOTAL</b></td>';
	echo '<td class="text-right" width="12%"><b>'.number_format($tot_trx_ht, 2, ',', ' ').'</b></td>';
	echo '<td class="text-right" width="12%"><b>'.number_format($tot_actu, 2, ',', ' ').'</b></td>';
	echo '<td class="text-right" width="12%"><b>'.number_format($tot_autre_ht, 2, ',', ' ').'</b></td>';
	echo '<td class="text-right" width="12%"><b>'.number_format($tot_penalite, 2, ',', ' ').'</b></td>';
	echo '<td class="text-right" width="12%"><b>'.number_format($tot_tot_ht, 2, ',', ' ').'</b></td>';
	echo '<td class="text-right" width="12%"><b>'.number_format($tot_tva, 2, ',', ' ').'</b></td>';
	echo '</tr>';
	echo "</tbody>";
	echo "</table>";
}
function apdventcht_paiement($idpaie,$idcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqcht = "SELECT chantiers.cht_numero,nature.nat_codenumero FROM chantiers JOIN nature ON nature.nat_libelle=chantiers.cht_nature WHERE chantiers.cht_id=".$idcht;
	$resultcht=mysql_query($reqcht,$link);
	while ($data=mysql_fetch_assoc($resultcht))
	{
		$numero = $data["cht_numero"];
		$nature = $data["nat_codenumero"];
	}
	$reqventpaie = "SELECT * FROM vent_paie WHERE id_paie=".$idpaie." AND id_cht=".$idcht;
	$resultventpaie=mysql_query($reqventpaie,$link);
	while ($data=mysql_fetch_assoc($resultventpaie))
	{
		$idventpaie = $data["id"];
		$montant_trx_ht = $data["montant_trx_ht"];
		$montant_actu = $data["montant_actu"];
		$montant_autre_ht = $data["montant_autre_ht"];
		$montant_penalite = $data["montant_penalite"];
		$montant_tot_ht = $data["montant_tot_ht"];
		$montant_tva = $data["montant_tva"];
		$montant_tot_ttc = $data["montant_tot_ttc"];
	}
	echo "<br>";
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-file-alt"></i> D�tail prestation <b>'.$numero.'</b></div>';
	echo '<div class="panel-body">';
		echo '<div class="col-sm-12" style="margin-bottom:20px;" id="detailapdventcht_paiement">';
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading"><a data-toggle="collapse" href="#collapseBord"><i class="fa fa-plus-circle"></i> Ajouter un article</a></div>';
			echo '<div class="panel-body collapse" id="collapseBord">';
				echo '<form>';
				echo '<div class="form-group row">';
				echo '<label class="control-label col-sm-2" for="lstbordapd"><i class="fa fa-book"></i> Bordereaux:</label>';
				echo '<div class="col-sm-6">';
				echo '<select class="form-control input-sm" id="lstbordapd" name="lstbordapd" data-idventpaie="'.$idventpaie.'">';
				echo '<option value="-1">S�lectionnez un bordereau</option>';
				$req="SELECT id,libelle,date_cloture FROM bordereau WHERE type='".$nature."'";
				$result=mysql_query($req,$link);
				while ($ligne=mysql_fetch_assoc($result))
				{
					if ($ligne["date_cloture"]=="0000-00-00"){$title = $ligne["libelle"];}else{$title = $ligne["libelle"]." [CLOTURE]";}
					echo '<option value="'.$ligne["id"].'">'.$title.'</option>';
				}
				echo '</select>';
				echo '</div>';
				echo '</div>';
				echo '</form>';
				/* Liste des articles du bordereau selectionn� */
				echo '<div class="panel panel-default">';
				echo '<div class="panel-heading">Articles</div>';
				echo '<div class="panel-body" id="lstarticlesapd" style="max-height: 200px;overflow-y: scroll;">';
					lst_article_bord("-1",$idventpaie);
				echo '</div>';
				echo '</div>';
			echo '</div>';
			echo '</div>';
		echo '</div>';
		echo '<div class="col-sm-12" id="detailgeneventcht_paiement">';
			/* Liste des articles valoris�s */
			echo '<div class="panel panel-default" style="margin-bottom:20px;" >';
			echo '<div class="panel-heading">Valorisation</div>';
			echo '<div class="panel-body" id="lstvalventpaie">';
				lst_val_ventpaie($idventpaie);
			echo '</div>';
			echo '</div>';
			echo '<form>';
			echo '<div class="col-md-4">';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-6" for="ventpaie_trxht">Travaux HT:</label>';
			echo '<div class="col-sm-6">';
			echo '<input type="text" class="form-control input-sm text-right modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_presfactsstraitant" id="ventpaie_trxht" name="ventpaie_trxht" value="'.$montant_trx_ht.'">';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-6" for="ventpaie_montactu">Actualisation:</label>';
			echo '<div class="col-sm-6">';
			echo '<input type="text" class="form-control input-sm text-right modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_presfactsstraitant" id="ventpaie_montactu" name="ventpaie_montactu" value="'.$montant_actu.'" disabled>';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-6" for="ventpaie_montautre">Autre HT:</label>';
			echo '<div class="col-sm-6">';
			echo '<input type="text" class="form-control input-sm text-right modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_presfactsstraitant" id="ventpaie_montautre" name="ventpaie_montautre" value="'.$montant_autre_ht.'">';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-6" for="ventpaie_montpenalite">P�nalit�s:</label>';
			echo '<div class="col-sm-6">';
			echo '<input type="text" class="form-control input-sm text-right modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_presfactsstraitant" id="ventpaie_montpenalite" name="ventpaie_montpenalite" value="'.$montant_penalite.'">';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-6" for="ventpaie_monttotht">Total HT:</label>';
			echo '<div class="col-sm-6">';
			echo '<input type="text" class="form-control input-sm text-right modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_presfactsstraitant" id="ventpaie_monttotht" name="ventpaie_monttotht" value="'.$montant_tot_ht.'" disabled>';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-6" for="ventpaie_monttva">TVA:</label>';
			echo '<div class="col-sm-6">';
			echo '<input type="text" class="form-control input-sm text-right modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_presfactsstraitant" id="ventpaie_monttva" name="ventpaie_monttva" value="'.$montant_tva.'" disabled>';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-6" for="ventpaie_monttotttc">Total TTC:</label>';
			echo '<div class="col-sm-6">';
			echo '<input type="text" class="form-control input-sm text-right modifchppaiement" data-idpaie="'.$idpaie.'" data-chp="date_presfactsstraitant" id="ventpaie_monttotttc" name="ventpaie_monttotttc" value="'.$montant_tot_ttc.'" disabled>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '</form>';
			/* Sous traitance */
			echo '<div class="col-sm-8" id="detailapdventchtsstraitance_paiement">';
			echo '<div class="panel panel-default" style="margin-bottom:20px;" >';
			echo '<div class="panel-heading">Sous-traitance</div>';
			echo '<div class="panel-body" id="lstvalventpaiesstraitance">';
				//lst_val_ventpaie($idventpaie);
			echo '</div>';
			echo '</div>';
			echo '</div>';

		echo '</div>';

	echo '</div>';
	echo '</div>';	
}
function lst_article_bord($idbord,$idventpaie){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM articles WHERE articles.id_bord=".$idbord;
	$result=mysql_query($req,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='5%'></th><th>Libell�</th><th width='10%'>P.U.</th></tr></thead>";
	echo "<tbody>";
	$rowart = mysql_num_rows($result);
	if ($rowart!=0){
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo '<tr><td width="5%" class="text-center"><button data-idarticle="'.$ligne["id"].'" data-idventpaie="'.$idventpaie.'" class="btn btn-sm btn-default add_valventpaie"><i class="fa fa-plus-circle"></i></button></td><td>'.$ligne["libelle"].'</td><td width="10%" class="text-center">'.$ligne["pu"].'</td></tr>';
		}
	}else{
		echo '<tr><td class="text-center" colspan="3"><b>Aucun article</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function add_valventpaie($idventpaie,$idart){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="INSERT INTO val_ventpaie (id_ventpaie,id_art) VALUES (".$idventpaie.",".$idart.")";
	$result=mysql_query($req,$link);
}
function lst_val_ventpaie($idventpaie){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT val_ventpaie.id,articles.libelle,articles.pu,val_ventpaie.qte,val_ventpaie.tot,val_ventpaie.longueur,val_ventpaie.puissance,val_ventpaie.DMAR,val_ventpaie.DIDCAC,val_ventpaie.RHTA FROM val_ventpaie JOIN articles ON articles.id=val_ventpaie.id_art WHERE val_ventpaie.id_ventpaie=".$idventpaie;
	$result=mysql_query($req,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='5%'></th><th>Libell�</th><th>Qte</th><th width='10%'>P.U.</th><th>Montant</th><th>Longueur</th><th>Puissance</th><th>DMAR</th><th>DIDCAC</th><th>RHTA</th></tr></thead>";
	echo "<tbody>";
	$rowvalventpaie = mysql_num_rows($result);
	if ($rowvalventpaie!=0){
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo '<tr><td width="5%" class="text-center"><button data-idvalventpaie="'.$ligne["id"].'" data-idventpaie="'.$idventpaie.'" class="btn btn-sm btn-default del_valventpaie"><i class="fa fa-trash"></i></button></td><td>'.$ligne["libelle"].'</td><td width="10%"><input data-chp="qte" data-idvalventpaie="'.$ligne["id"].'" data-idventpaie="'.$idventpaie.'" class="form-control input-sm text-center modifchpvalventpaie" value="'.$ligne["qte"].'"></td><td width="10%" class="text-center">'.number_format($ligne["pu"], 2, ',', ' ').'</td><td width="12%"><input data-chp="tot" data-idvalventpaie="'.$ligne["id"].'" data-idventpaie="'.$idventpaie.'" class="form-control input-sm text-right modifchpvalventpaie" value="'.$ligne["tot"].'"></td><td width="10%"><input data-chp="longueur" data-idvalventpaie="'.$ligne["id"].'" data-idventpaie="'.$idventpaie.'" class="form-control input-sm text-center modifchpvalventpaie" value="'.$ligne["longueur"].'"></td><td width="10%"><input data-chp="puissance" data-idvalventpaie="'.$ligne["id"].'" data-idventpaie="'.$idventpaie.'" class="form-control input-sm text-center modifchpvalventpaie" value="'.$ligne["puissance"].'"></td><td width="10%"><input data-chp="DMAR" data-idvalventpaie="'.$ligne["id"].'" data-idventpaie="'.$idventpaie.'" class="form-control input-sm text-center modifchpvalventpaie" value="'.$ligne["DMAR"].'"></td><td width="10%"><input data-chp="DIDCAC" data-idvalventpaie="'.$ligne["id"].'" data-idventpaie="'.$idventpaie.'" class="form-control input-sm text-center modifchpvalventpaie" value="'.$ligne["DIDCAC"].'"></td><td width="10%"><input data-chp="RHTA" data-idvalventpaie="'.$ligne["id"].'" data-idventpaie="'.$idventpaie.'" class="form-control input-sm text-center modifchpvalventpaie" value="'.$ligne["RHTA"].'"></td></tr>';
		}
	}else{
		echo '<tr><td class="text-center" colspan="10"><b>Aucune valorisation</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function del_valventpaie($idvalventpaie){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT tva.tva_taux FROM tva ORDER BY tva_datetva DESC LIMIT 1";
	$result=mysql_query($req,$link);	
	while ($ligne=mysql_fetch_assoc($result)){
		$taux_tva = $ligne["tva_taux"];
	}
	$req="SELECT vent_paie.id FROM val_ventpaie JOIN vent_paie ON vent_paie.id=val_ventpaie.id_ventpaie WHERE val_ventpaie.id=".$idvalventpaie;
	$result=mysql_query($req,$link);	
	while ($ligne=mysql_fetch_assoc($result)){
		$id_ventpaie = $ligne["id"];
	}
	$req="DELETE FROM val_ventpaie WHERE val_ventpaie.id=".$idvalventpaie;
	$result=mysql_query($req,$link);
	$req="SELECT val_ventpaie.tot FROM val_apd WHERE val_apd.id_apd=".$id_ventpaie;
	$result=mysql_query($req,$link);	
	while ($ligne=mysql_fetch_assoc($result)){
		$estimation = $estimation + $ligne["tot"];
	}
	$val = $estimation;
/*	$totHT = $estimation; //+ $m_actu + $m_sav + $m_divers
	$tva = $totHT * $taux_tva / 100;
	$totTTC = $totHT + $tva;
*/	$req2="UPDATE vent_paie SET montant_trx_ht='".$val."' WHERE id=".$id_ventpaie;
	$result2=mysql_query($req2,$link);	
}
function update_valventpaie($idvalventpaie,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE val_ventpaie SET ".$chp."='".$val."' WHERE id=".$idvalventpaie;
	$result2=mysql_query($req2,$link);	
	if ($chp=="tot"){
		$req="SELECT tva.tva_taux FROM tva ORDER BY tva_datetva DESC LIMIT 1";
		$result=mysql_query($req,$link);	
		while ($ligne=mysql_fetch_assoc($result)){
			$taux_tva = $ligne["tva_taux"];
		}
		$req="SELECT vent_paie.id,vent_paie.id_paie,vent_paie.id_cht FROM val_ventpaie JOIN vent_paie ON vent_paie.id=val_ventpaie.id_ventpaie WHERE val_ventpaie.id=".$idvalventpaie;
		$result=mysql_query($req,$link);	
		while ($ligne=mysql_fetch_assoc($result)){
			$id_ventpaie = $ligne["id"];
			$id_paie = $ligne["id_paie"];
			$id_cht = $ligne["id_cht"];
		}
		$req="SELECT val_ventpaie.tot FROM val_ventpaie WHERE val_ventpaie.id_ventpaie=".$id_ventpaie;
		$result=mysql_query($req,$link);	
		while ($ligne=mysql_fetch_assoc($result)){
			$estimation = $estimation + $ligne["tot"];
		}
		$val = $estimation;
/*		$totHT = $estimation + $m_actu + $m_sav + $m_divers;
		$tva = $totHT * $taux_tva / 100;
		$totTTC = $totHT + $tva;
*/		$req2="UPDATE vent_paie SET montant_trx_ht='".$val."' WHERE id=".$id_ventpaie;
		$result2=mysql_query($req2,$link);
		$tab=array();
		$tab[0]=$id_paie;
		$tab[1]=$id_cht;
		echo json_encode($tab);
	}
}
function update_paiement($idpaie,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	switch ($chp){
		case "numero":
			$req2="UPDATE paiement SET num_certif='".$val."' WHERE id=".$idpaie;
			$result2=mysql_query($req2,$link);	
			break;
		case "date_paie":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "date_certif":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "date_mandat":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "date_presfactsstraitant":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "date_arrivefacture":
			$req="SELECT marche.mar_delaimandat,marche.mar_delaipaie FROM paiement JOIN boncommande ON boncommande.bc_id=paiement.id_bc JOIN lot ON lot.id=boncommande.bc_idlot JOIN marche ON marche.mar_id=lot.id_marche WHERE paiement.id=".$idpaie;
			$result=mysql_query($req,$link);
			$ligne=mysql_fetch_assoc($result);
			$delaimandat = $ligne["mar_delaimandat"];
			$delaipaie = $ligne["mar_delaipaie"];
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			$datelimitmandat = date("Y-m-d",strtotime($val." +".$delaimandat." day"));
			$req2="UPDATE paiement SET date_limitemandat='".$datelimitmandat."' WHERE id=".$idpaie;
			$result2=mysql_query($req2,$link);
			$datelimitpaiement = date("Y-m-d",strtotime($val." +".$delaipaie." day"));
			$req2="UPDATE paiement SET date_limitepaiement='".$datelimitpaiement."' WHERE id=".$idpaie;
			$result2=mysql_query($req2,$link);	
			break;
	}
	$req2="UPDATE paiement SET ".$chp."='".$val."' WHERE id=".$idpaie;
	$result2=mysql_query($req2,$link);	
}