<?php

/* Tables concern�es
tel
tel_demandeur
tel_chambre
tel_bpu //Pas supprimer dans del_specif
*/

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(isset($_POST['action']) && !empty($_POST['action'])) {
	$action = $_POST['action'];
	switch($action) {
		case 'detailspecif' : detail_specif($_POST["idcht"],$_POST["nat"]);break;
		case 'updatespecif' : update_specif($_POST["id"],$_POST["chp"],$_POST["val"],$_POST["nat"],$_POST["table"]);break;
		case 'addspecif' : add_specif($_POST["idcht"],$_POST["table"]);break;
		case 'delinspecif' : del_inspecif($_POST["id"],$_POST["table"]);break;
		case 'blah' : blah();break;
		// ...etc...
	}
}

function detail_specif($idcht,$nat){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<div class='col-md-6'>";
	/* Gestionnaire de voirie */
	$req="SELECT * FROM tel WHERE tel_idcht=".$idcht;
	$result=mysql_query($req,$link);
	$row = mysql_num_rows($result);
	while ($data=mysql_fetch_assoc($result))
	{
		$id = $data["tel_id"];
		$rodp_rn = $data["tel_rodprn"];
		$rodp_rd = $data["tel_rodprd"];
		$rodp_vc = $data["tel_rodpvc"];
		$rodp_total = $data["tel_rodptotal"];
		$ru_rn = $data["tel_rurn"];
		$ru_rd = $data["tel_rurd"];
		$ru_vc = $data["tel_ruvc"];
		$ru_total = $data["tel_rutotal"];
	}
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-road"></i> Gestionnaires de voiries</div>';
	echo '<div class="panel-body" id="detail_specif_room">';
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th rowspan='2'>Gestionnaires de voiries</th><th colspan='2' class='text-center'>Longueurs tubes en ml</th></tr></thead>";
	echo "<thead><tr><th></th><th>RODP</th><th>RU</th></tr></thead>";
	echo "<tbody>";
	echo '<tr><td class="text-center">RN</td><td><input class="form-control input-sm text-center update_specif" data-id="'.$id.'" data-chp="tel_rodprn" data-idcht="'.$idcht.'" data-table="tel" data-type="" data-nat="'.$nat.'" id="tel_rodp_rn" name="tel_rodp_rn" value="'.$rodp_rn.'"></td><td><input class="form-control input-sm text-center update_specif" data-id="'.$id.'" data-chp="tel_rurn" data-idcht="'.$idcht.'" data-table="tel" data-type="" data-nat="'.$nat.'" id="tel_ru_rn" name="tel_ru_rn" value="'.$ru_rn.'"></td></tr>';
	echo '<tr><td class="text-center">RD</td><td><input class="form-control input-sm text-center update_specif" data-id="'.$id.'" data-chp="tel_rodprd" data-idcht="'.$idcht.'" data-table="tel" data-type="" data-nat="'.$nat.'" id="tel_rodp_rd" name="tel_rodp_rd" value="'.$rodp_rd.'"></td><td><input class="form-control input-sm text-center update_specif" data-id="'.$id.'" data-chp="tel_rurd" data-idcht="'.$idcht.'" data-table="tel" data-type="" data-nat="'.$nat.'" id="tel_ru_rd" name="tel_ru_rd" value="'.$ru_rd.'"></td></tr>';
	echo '<tr><td class="text-center">VC</td><td><input class="form-control input-sm text-center update_specif" data-id="'.$id.'" data-chp="tel_rodpvc" data-idcht="'.$idcht.'" data-table="tel" data-type="" data-nat="'.$nat.'" id="tel_rodp_vc" name="tel_rodp_vc" value="'.$rodp_vc.'"></td><td><input class="form-control input-sm text-center update_specif" data-id="'.$id.'" data-chp="tel_ruvc" data-idcht="'.$idcht.'" data-table="tel" data-type="" data-nat="'.$nat.'" id="tel_ru_vc" name="tel_ru_vc" value="'.$ru_vc.'"></td></tr>';
	echo '<tr><td class="text-center"><b>Total</b></td><td class="text-center"><b>'.$rodp_total.'</b></td><td class="text-center"><b>'.$ru_total.'</b></td></tr>';
	echo "</tbody>";
	echo "</table>";
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo "<div class='col-md-6'>";
	/* Chambre de tirage */
	$req="SELECT * FROM tel_chambre WHERE telchbre_idcht=".$idcht;
	$result=mysql_query($req,$link);
	$row = mysql_num_rows($result);
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-phone-square"></i> Chambres de tirage</div>';
	echo '<div class="panel-body" id="detail_specif_room">';
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button id='add_specif' data-idcht='".$idcht."' data-table='tel_chambre' data-nat='".$nat."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th align='center'>Type</th><th align='center' width='20%'>Quantit�</th></tr></thead>";
	echo "<tbody>";
	if ($row!=0){
		while ($data=mysql_fetch_assoc($result))
		{
			echo '<tr><td align="center" width="8%"><button data-id="'.$data["telchbre_id"].'" data-idcht="'.$idcht.'" data-nat="'.$nat.'" data-table="tel_chambre" class="btn btn-sm btn-default del_specif"><i class="fa fa-trash"></i></button></td>';
			echo '<td>';
			echo '<select class="form-control input-sm text-right update_specif" data-id="'.$data["telchbre_id"].'" data-chp="telchbre_type" data-idcht="'.$idcht.'" data-table="tel_chambre" data-type="" data-nat="'.$nat.'" id="tel_montant" name="tel_montant">';
			echo '<option value="0">Choisir un type de chambre</option>';
			$reqtype="SELECT lex_id,lex_libelle FROM lexique WHERE lex_codelexique='TYPE_TELCHAMBRE'";
			$resulttype=mysql_query($reqtype,$link);
			while ($lignetype=mysql_fetch_assoc($resulttype))
			{
				if ($lignetype["lex_libelle"]==$data["telchbre_type"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$lignetype["lex_libelle"].'" '.$selected.'>'.$lignetype["lex_libelle"].'</option>';
			}
			echo '</select>';
			echo '</td>';
			echo '<td class="text-center" width="20%"><input class="form-control input-sm text-center update_specif" data-id="'.$data["telchbre_id"].'" data-chp="telchbre_qte" data-idcht="'.$idcht.'" data-table="tel_chambre" data-type="" data-nat="'.$nat.'" id="tel_chambreqte" name="tel_chambreqte" value="'.$data["telchbre_qte"].'"></td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="9"><b>Aucune chambre de tirage</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo "<div class='col-md-12'>";
	/* Demandeurs */
	$req="SELECT * FROM tel_demandeur WHERE teldem_idcht=".$idcht;
	$result=mysql_query($req,$link);
	$row = mysql_num_rows($result);
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-users"></i> Demandeurs</div>';
	echo '<div class="panel-body" id="detail_specif_dem">';
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button id='add_specif' data-idcht='".$idcht."' data-table='tel_demandeur' data-nat='".$nat."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th align='center' width='5%'>Genre</th><th align='center'>Nom</th><th align='center'>Pr�nom</th><th align='center'>Adresse</th><th align='center' width='5%'>CP</th><th align='center'>Ville</th><th align='center'>Mail</th><th align='center' width='15%'>Participation (�)</th></tr></thead>";
	echo "<tbody>";
	if ($row!=0){
		while ($data=mysql_fetch_assoc($result))
		{
			if ($data["teldem_mail"]==""){$disabled = "disabled";}else{$disabled = "";}
			echo '<tr><td align="center" width="8%"><button data-id="'.$data["teldem_id"].'" data-idcht="'.$idcht.'" class="btn btn-sm btn-default"><i class="fa fa-file-pdf"></i></button> <button data-id="'.$data["teldem_id"].'" data-idcht="'.$idcht.'" class="btn btn-sm btn-default" '.$disabled.'><i class="fa fa-envelope"></i></button> <button data-id="'.$data["teldem_id"].'" data-idcht="'.$idcht.'" data-nat="'.$nat.'" data-table="tel_demandeur" class="btn btn-sm btn-default del_specif"><i class="fa fa-trash"></i></button></td>';
			echo '<td class="text-center" width="5%"><input class="form-control input-sm update_specif" data-id="'.$data["teldem_id"].'" data-chp="teldem_genre" data-idcht="'.$idcht.'" data-table="tel_demandeur" data-type="" data-nat="'.$nat.'" id="tel_genre" name="tel_genre" value="'.$data["teldem_genre"].'" maxlength="3"></td>';
			echo '<td class="text-center"><input class="form-control input-sm update_specif" data-id="'.$data["teldem_id"].'" data-chp="teldem_nom" data-idcht="'.$idcht.'" data-table="tel_demandeur" data-type="" data-nat="'.$nat.'" id="tel_nom" name="tel_nom" value="'.$data["teldem_nom"].'"></td>';
			echo '<td class="text-center"><input class="form-control input-sm update_specif" data-id="'.$data["teldem_id"].'" data-chp="teldem_prenom" data-idcht="'.$idcht.'" data-table="tel_demandeur" data-type="" data-nat="'.$nat.'" id="tel_prenom" name="tel_prenom" value="'.$data["teldem_prenom"].'"></td>';
			echo '<td class="text-center"><input class="form-control input-sm update_specif" data-id="'.$data["teldem_id"].'" data-chp="teldem_adresse" data-idcht="'.$idcht.'" data-table="tel_demandeur" data-type="" data-nat="'.$nat.'" id="tel_adresse" name="tel_adresse" value="'.$data["teldem_adresse"].'"></td>';
			echo '<td class="text-center" width="5%"><input class="form-control input-sm update_specif" data-id="'.$data["teldem_id"].'" data-chp="teldem_cp" data-idcht="'.$idcht.'" data-table="tel_demandeur" data-type="" data-nat="'.$nat.'" id="tel_cp" name="tel_cp" value="'.$data["teldem_cp"].'"></td>';
			echo '<td class="text-center"><input class="form-control input-sm update_specif" data-id="'.$data["teldem_id"].'" data-chp="teldem_ville" data-idcht="'.$idcht.'" data-table="tel_demandeur" data-type="" data-nat="'.$nat.'" id="tel_ville" name="tel_ville" value="'.$data["teldem_ville"].'"></td>';
			echo '<td class="text-center"><input class="form-control input-sm update_specif" data-id="'.$data["teldem_id"].'" data-chp="teldem_mail" data-idcht="'.$idcht.'" data-table="tel_demandeur" data-type="" data-nat="'.$nat.'" id="tel_mail" name="tel_mail" value="'.$data["teldem_mail"].'"></td>';
			echo '<td width="15%">';
			echo '<select class="form-control input-sm text-right update_specif" data-id="'.$data["teldem_id"].'" data-chp="teldem_montantpart" data-idcht="'.$idcht.'" data-table="tel_demandeur" data-type="" data-nat="'.$nat.'" id="tel_montant" name="tel_montant">';
			echo '<option value="0">Choisir un forfait</option>';
			$reqbpu="SELECT telbpu_id,telbpu_libelle,telbpu_forfait FROM tel_bpu";
			$resultbpu=mysql_query($reqbpu,$link);
			while ($lignebpu=mysql_fetch_assoc($resultbpu))
			{
				if ($lignebpu["telbpu_forfait"]==$data["teldem_montantpart"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$lignebpu["telbpu_forfait"].'" '.$selected.'>'.$lignebpu["telbpu_forfait"].' � ('.$lignebpu["telbpu_libelle"].')</option>';
			}
			echo '</select>';
			echo '</td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="9"><b>Aucun demandeur</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function update_specif($id,$chp,$val,$nat,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE ".$table." SET ".$chp."='".$val."' WHERE ".$table."_id=".$id;
	$result2=mysql_query($req2,$link);
	if ($table=="tel"){
		$req="SELECT * FROM tel WHERE tel_id=".$id;
		$result=mysql_query($req,$link);
		while ($data=mysql_fetch_assoc($result))
		{
			$rodp_rn = $data["tel_rodprn"];
			$rodp_rd = $data["tel_rodprd"];
			$rodp_vc = $data["tel_rodpvc"];
			$ru_rn = $data["tel_rurn"];
			$ru_rd = $data["tel_rurd"];
			$ru_vc = $data["tel_ruvc"];
		}
		$rodp_total = $rodp_rn + $rodp_rd + $rodp_vc;
		$ru_total = $ru_rn + $ru_rd + $ru_vc;
		$req2="UPDATE tel SET tel_rodptotal = '".$rodp_total."', tel_rutotal = '".$ru_total."' WHERE tel_id=".$id;
		$result2=mysql_query($req2,$link);
	}
}
function del_specif($idcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM tel_chambre WHERE telchbre_idcht=".$idcht;
	$result2=mysql_query($req2,$link);	
	$req2="DELETE FROM tel_demandeur WHERE teldem_idcht=".$idcht;
	$result2=mysql_query($req2,$link);	
	$req2="DELETE FROM tel WHERE tel_idcht=".$idcht;
	$result2=mysql_query($req2,$link);	
}
function del_inspecif($id,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM ".$table." WHERE ".$table."_id=".$id;
	$result2=mysql_query($req2,$link);	
}
function add_specif($idcht,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO ".$table." (".$table."_idcht) VALUES (".$idcht.")";
	$result2=mysql_query($req2,$link);	
}