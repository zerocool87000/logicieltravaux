<?php
//error_reporting(E_ALL);
//ini_set('display_errors','on');
require("./compte.php");
setlocale (LC_TIME, 'fr_FR.utf8','fra');
date_default_timezone_set('Europe/Paris');
$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
mysql_select_db($baseSYGALE,$link);

$outputFileName = '../data/'.date('Y-m').'.txt';

$req2="SELECT boncommande.id,boncommande.montant_ttc,boncommande.numero FROM boncommande WHERE export_sedit=0";
$result2=mysql_query($req2,$link);
$row2=mysql_num_rows($result2);
$file = "/##/PARAM/01/AA/2018/3/O/N/N";
$file .= "/##/\n";
if ($row2!=0)
{
	$i=1;
	while ($data=mysql_fetch_assoc($result2))
	{
		$file .= "/01/17E000005\n"; /* N° engagement */
		$file .= "/02/D\n"; /* D - Dépense  R - Recette */
		$file .= "/03/009019\n"; /* Code tiers (entreprise) */
		$file .= "/04/BC_".$data["numero"]."\n"; /* Libellé de l'engagement */
		$file .= "/05/01\n"; /* Code calendrier 01 - Dépense  02 - Recette */
		$file .= "/--/\n"; /* A chaque nouvelle ligne */
		$file .= "/51/".$i."\n"; /* N° ligne de l'engagement de 001 à 999 ligne par prestation dans le BC */
		$file .= "/54/011211\n"; /* Code Imputation étendue */
		$file .= "/57/AF \n"; /* Libellé de la ligne */
		$file .= "/66/".number_format($data["montant_ttc"], 2, ',', '')."\n"; /* Montant TTC en euros */
		$file .= "/69/EXT\n"; /* Nature de prestation */
		$file .= "/##/\n";
		$i++;
		$req="UPDATE boncommande SET export_sedit=1,date_export=NOW() WHERE id=".$data["id"];
		$result=mysql_query($req,$link);	
	}
}

touch($outputFileName);
file_put_contents($outputFileName, $file);
echo utf8_decode("Génération effectuée !!!");
?>