<?php

/* Tables concern�es
sps
*/

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(isset($_POST['action']) && !empty($_POST['action'])) {
	$action = $_POST['action'];
	switch($action) {
		case 'detailspecif' : detail_specif($_POST["idcht"],$_POST["nat"]);break;
		case 'updatespecif' : update_specif($_POST["id"],$_POST["chp"],$_POST["val"],$_POST["nat"],$_POST["table"]);break;
		case 'addspecif' : add_specif($_POST["idcht"],$_POST["table"]);break;
		case 'delinspecif' : del_inspecif($_POST["id"],$_POST["table"]);break;
		case 'blah' : blah();break;
		// ...etc...
	}
}

function detail_specif($idcht,$nat){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	/* R�cup�ration du num�ro d'op�ration */
	$req="SELECT operations.ope_numero FROM operations JOIN chantiers ON chantiers.cht_idope=operations.ope_id WHERE chantiers.cht_id=".$idcht;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$numope = $ligne["ope_numero"];
	}
	$nouv = 0;
	/* Fiche */
	$req="SELECT * FROM sps WHERE sps_idcht=".$idcht;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$id = $ligne["sps_id"];
		$id_ent = $ligne["sps_ident"];
		if ($ligne["sps_sehvsps"]==1){$checked_sehv="checked";}else{$checked_sehv="";}
		$niveau_sps = $ligne["sps_niveausps"];
		$phase_sps = $ligne["sps_phasesps"];
		for ($i=0;$i<2;$i++){
			$chk[$i]=="";
			$active[$i]=="";
			switch ($phase_sps){
				case "conception":
					$chk[0]="checked";
					$active[0]="active";
					break;
				case "realisation":
					$chk[1]="checked";
					$active[1]="active";
					break;
			}
		}
		$numero_pgc = $ligne["sps_numeropgc"];
		$numero_rjo = $ligne["sps_numerorjo"];
		$numero_diu = $ligne["sps_numerodiu"];
		if ($ligne["sps_protocole"]==1){$checked_protocole="checked";}else{$checked_protocole="";}
		$nature_protocole = $ligne["sps_natureprotocole"];
		for ($i=0;$i<2;$i++){
			$chk_protocole[$i]=="";
			$active_protocole[$i]=="";
			switch ($nature_protocole){
				case "particulier":
					$chk_protocole[0]="checked";
					$active_protocole[0]="active";
					break;
				case "global":
					$chk_protocole[1]="checked";
					$active_protocole[1]="active";
					break;
			}
		}
		$numero_protocole = $ligne["sps_numeroprotocole"];
		$ee1 = $ligne["sps_ee1"];
		$ee2 = $ligne["sps_ee2"];
		$ee3 = $ligne["sps_ee3"];
		$ee4 = $ligne["sps_ee4"];
		$ee5 = $ligne["sps_ee5"];
		/* Pr�paration de la coordination si num�ro vide ou si premi�re ouverture */
		if ($phase_sps==""){$phase_sps="conception";$nouv=1;}
		if ($numero_pgc==""){$numero_pgc=$numope."-PGC-1";$nouv=1;}
		if ($numero_rjo==""){$numero_rjo=$numope."-RJO-1";$nouv=1;}
		if ($numero_diu==""){$numero_diu=$numope."-DIU-1";$nouv=1;}
	}
	if ($nouv==1){
		$req2="UPDATE sps SET sps_phasesps='".$phase_sps."',sps_numeropgc='".$numero_pgc."',sps_numerorjo='".$numero_rjo."',sps_numerodiu='".$numero_diu."' WHERE sps_id=".$id;
		$result2=mysql_query($req2,$link);
	}

	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"> CSPS</div>';
	echo '<div class="panel-body" id="detail_specif_csps">';
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<div class="col-sm-12">';
	echo '<div class="btn-group update_specif" data-toggle="buttons">';
	echo '<label class="btn btn-default '.$active[0].'">';
	echo '<input type="radio" name="options" data-id="'.$id.'" data-chp="sps_phasesps" data-type="" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-nat="'.$nat.'" value="conception" autocomplete="off" '.$chk[0].'> Conception';
	echo '</label>';
	echo '<label class="btn btn-default '.$active[1].'">';
	echo '<input type="radio" name="options" data-id="'.$id.'" data-chp="sps_phasesps" data-type="" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-nat="'.$nat.'" value="realisation" autocomplete="off" '.$chk[1].'> R�alisation';
	echo '</label>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="sps_sehv">SEHV:</label>';
	echo '<div class="col-sm-4">';
	echo '<input type="checkbox" class="update_specif" data-id="'.$id.'" data-chp="sps_sehvsps" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="chk" data-nat="'.$nat.'" id="sps_sehv" name="sps_sehv" '.$checked_sehv.'>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="sps_niveau">Niveaux:</label>';
	echo '<div class="col-sm-8">';
	echo '<select class="form-control input-sm text-right update_specif" data-id="'.$id.'" data-chp="sps_niveausps" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="" data-nat="'.$nat.'" id="sps_niveau" name="sps_niveau" >';
	echo '<option value="">S�lectionnez un niveau</option>';
	$arr = array("1","2","3");
	foreach ($arr as &$value) {
		if ($value==$niveau_sps){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$value.'" '.$selected.'>'.$value.'</option>';
	}
	echo '</select>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="sps_numpgc">Num�ro PGC:</label>';
	echo '<div class="col-sm-8">';
	echo '<input class="form-control input-sm update_specif" data-id="'.$id.'" data-chp="sps_numeropgc" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="" data-nat="'.$nat.'" id="sps_numpgc" name="sps_numpgc" value="'.$numero_pgc.'">';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="sps_numrjo">Num�ro RJO:</label>';
	echo '<div class="col-sm-8">';
	echo '<input class="form-control input-sm update_specif" data-id="'.$id.'" data-chp="sps_numerorjo" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="" data-nat="'.$nat.'" id="sps_numrjo" name="sps_numrjo" value="'.$numero_rjo.'">';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="sps_numdiu">Num�ro DIUO:</label>';
	echo '<div class="col-sm-8">';
	echo '<input class="form-control input-sm update_specif" data-id="'.$id.'" data-chp="sps_numerodiu" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="" data-nat="'.$nat.'" id="sps_numdiu" name="sps_numdiu" value="'.$numero_diu.'">';
	echo '</div>';
	echo '</div>';
	echo '</form>';
	$req="SELECT * FROM sps_ent WHERE spsent_idcht=".$idcht;
	$result=mysql_query($req,$link);
	$row = mysql_num_rows($result);
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-users"></i> Liste des entreprises</div>';
	echo '<div class="panel-body" id="detail_specif_spsent">';
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button id='add_specif' data-idcht='".$idcht."' data-table='sps_ent' data-nat='".$nat."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th align='center'>Entreprises</th></tr></thead>";
	echo "<tbody>";
	if ($row!=0){
		while ($data=mysql_fetch_assoc($result))
		{
			echo '<tr><td align="center" width="8%"><button data-id="'.$data["spsent_id"].'" data-idcht="'.$idcht.'" data-nat="'.$nat.'" data-table="sps_ent" class="btn btn-sm btn-default del_specif"><i class="fa fa-trash"></i></button></td>';
			echo '<td>';
			echo '<select class="form-control input-sm text-right update_specif" data-id="'.$data["spsent_id"].'" data-chp="spsent_ident" data-idcht="'.$idcht.'" data-table="sps_ent" data-type="" data-nat="'.$nat.'">';
			echo '<option value="0">S�lectionnez une entreprise</option>';
			$reqent="SELECT ent_id,ent_nom FROM entreprises";
			$resultent=mysql_query($reqent,$link);
			while ($ligne=mysql_fetch_assoc($resultent))
			{
				if ($ligne["ent_id"]==$data["spsent_ident"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
			}
			echo '</select>';
			echo '</td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="2"><b>Aucune entreprise</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"> Protocole</div>';
	echo '<div class="panel-body" id="detail_specif_protocole">';
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="sps_protocole">Protocole:</label>';
	echo '<div class="col-sm-2">';
	echo '<input type="checkbox" class="update_specif" data-id="'.$id.'" data-chp="sps_protocole" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="chk" data-nat="'.$nat.'" id="sps_protocole" name="sps_protocole" '.$checked_protocole.'>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<div class="col-sm-12">';
	echo '<div class="btn-group update_specif" data-toggle="buttons">';
	echo '<label class="btn btn-default '.$active_protocole[0].'">';
	echo '<input type="radio" name="options" data-id="'.$id.'" data-chp="sps_natureprotocole" data-type="" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-nat="'.$nat.'" value="particulier" autocomplete="off" '.$chk_protocole[0].'> Particulier';
	echo '</label>';
	echo '<label class="btn btn-default '.$active_protocole[1].'">';
	echo '<input type="radio" name="options" data-id="'.$id.'" data-chp="sps_natureprotocole" data-type="" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-nat="'.$nat.'" value="global" autocomplete="off" '.$chk_protocole[1].'> Global';
	echo '</label>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="sps_numpp">Num�ro PP:</label>';
	echo '<div class="col-sm-4">';
	echo '<input class="form-control input-sm update_specif" data-id="'.$id.'" data-chp="sps_numeroprotocole" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="" data-nat="'.$nat.'" id="sps_numpp" name="sps_numpp" value="'.$numero_protocole.'">';
	echo '</div>';
	echo '</div>';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-users"></i> Entreprises ext�rieures</div>';
	echo '<div class="panel-body" id="detail_specif_spsent">';
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='10%'></th><th align='center'>Entreprises</th></tr></thead>";
	echo "<tbody>";
	echo '<tr><td width="15%"><i class="fas fa-user"></i> EE1</td>';
	echo '<td>';
	echo '<select class="form-control input-sm text-right update_specif" data-id="'.$id.'" data-chp="sps_ee1" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="" data-nat="'.$nat.'" id="sps_ee1" name="sps_ee1" >';
	echo '<option value="0">S�lectionnez une entreprise</option>';
	$req="SELECT ent_id,ent_nom FROM entreprises";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["ent_id"]==$ee1){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
	}
	echo '</select>';
	echo '</td></tr>';
	echo '<tr><td width="15%"><i class="fas fa-user"></i> EE2</td>';
	echo '<td>';
	echo '<select class="form-control input-sm text-right update_specif" data-id="'.$id.'" data-chp="sps_ee2" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="" data-nat="'.$nat.'" id="sps_ee2" name="sps_ee2" >';
	echo '<option value="0">S�lectionnez une entreprise</option>';
	$req="SELECT ent_id,ent_nom FROM entreprises";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["ent_id"]==$ee2){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
	}
	echo '</select>';
	echo '</td></tr>';
	echo '<tr><td width="15%"><i class="fas fa-user"></i> EE3</td>';
	echo '<td>';
	echo '<select class="form-control input-sm text-right update_specif" data-id="'.$id.'" data-chp="sps_ee3" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="" data-nat="'.$nat.'" id="sps_ee3" name="sps_ee3" >';
	echo '<option value="0">S�lectionnez une entreprise</option>';
	$req="SELECT ent_id,ent_nom FROM entreprises";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["ent_id"]==$ee3){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
	}
	echo '</select>';
	echo '</td></tr>';
	echo '<tr><td width="15%"><i class="fas fa-user"></i> EE4</td>';
	echo '<td>';
	echo '<select class="form-control input-sm text-right update_specif" data-id="'.$id.'" data-chp="sps_ee4" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="" data-nat="'.$nat.'" id="sps_ee4" name="sps_ee4" >';
	echo '<option value="0">S�lectionnez une entreprise</option>';
	$req="SELECT ent_id,ent_nom FROM entreprises";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["ent_id"]==$ee4){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
	}
	echo '</select>';
	echo '</td></tr>';
	echo '<tr><td width="15%"><i class="fas fa-user"></i> EE5</td>';
	echo '<td>';
	echo '<select class="form-control input-sm text-right update_specif" data-id="'.$id.'" data-chp="sps_ee5" data-idcht="'.$idcht.'" data-table="'.$nat.'" data-type="" data-nat="'.$nat.'" id="sps_ee5" name="sps_ee5" >';
	echo '<option value="0">S�lectionnez une entreprise</option>';
	$req="SELECT ent_id,ent_nom FROM entreprises";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["ent_id"]==$ee5){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$ligne["ent_id"].'" '.$selected.'>'.$ligne["ent_nom"].'</option>';
	}
	echo '</select>';
	echo '</td></tr>';
	echo "</tbody>";
	echo "</table>";
	echo '</div>';
	echo '</div>';

	echo '</div>';
	echo '</div>';
	echo '</div>';

}
function update_specif($id,$chp,$val,$nat,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE ".$table." SET ".$chp."='".$val."' WHERE ".$table."_id=".$id;
	$result2=mysql_query($req2,$link);
}
function del_specif($idcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM sps_ent WHERE spsent_idcht=".$idcht;
	$result2=mysql_query($req2,$link);	
	$req2="DELETE FROM sps WHERE sps_idcht=".$idcht;
	$result2=mysql_query($req2,$link);	
}
function del_inspecif($id,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM ".$table." WHERE ".$table."_id=".$id;
	$result2=mysql_query($req2,$link);	
}
function add_specif($idcht,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO ".$table." (".$table."_idcht) VALUES (".$idcht.")";
	$result2=mysql_query($req2,$link);	
}