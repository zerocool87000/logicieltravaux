<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(isset($_POST['action']) && !empty($_POST['action'])) {
	$action = $_POST['action'];
	switch($action) {
		case 'loadsettings' : load_settings();break;
		/* Profil user */
		case 'loadcompte' : load_compte();break;
		case 'updatecompte' : update_compte();break;
		/* D�l�gation */
		case 'loaddelegation' : load_delegation();break;
		case 'updatedelegation' : update_delegation($_POST["val"],$_POST["type"]);break;
		/* Collectivit�s */
		case 'loadcollectivite' : load_collectivite();break;
		case 'loadlstcollectivite' : loadlst_collectivite();break;
		case 'loadinfocollectivite' : loadinfo_collectivite($_POST["id"]);break;
		case 'addcollectivite' : add_collectivite();break;
		case 'delcollectivite' : del_collectivite($_POST["id"]);break;
		case 'updatecollectivite' : update_collectivite();break;
		/* Comptes */
		case 'loadcomptes' : load_comptes($_POST["id"]);break;
		case 'loadlstcomptes' : loadlst_comptes();break;
		case 'loadinfocomptes' : loadinfo_comptes($_POST["id"]);break;
		case 'addcomptes' : add_comptes();break;
		case 'delcomptes' : del_comptes($_POST["id"]);break;
		case 'updatecomptes' : update_comptes();break;
		/* PCT */
		case 'loadpct' : load_pct();break;
		case 'addpct' : add_pct();break;
		case 'delpct' : del_pct($_POST["id"]);break;
		case 'updatepct' : update_pct($_POST["id"],$_POST["chp"],$_POST["val"]);break;

		case 'blah' : blah();break;
		// ...etc...
	}
}

function load_settings(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM user WHERE user_login='".$_COOKIE["login"]."'";
	$result=mysql_query($req,$link);
	$ligne=mysql_fetch_assoc($result);
	$droit = $ligne["user_droit"];

	echo '<div class="well well-sm"><h3>PREFERENCES</h3></div>';
	$panel=1;
	echo '<div class="col-sm-12">';
	echo '<div class="panel-group" id="accordion">';
	/* Compte */
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">';
	echo '<h2 class="panel-title">';
	echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-user" aria-hidden="true"></i> Compte</a>';
	echo '</h2>';
	echo '</div>';
	echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
	echo '<div class="panel-body" id="detailcompte_settings">';
		load_compte();
	echo '</div>';
	echo '</div>';
	echo '</div>';
	if ($droit=="ADMIN"){
	/* Comptes */
	$panel++;
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">';
	echo '<h2 class="panel-title">';
	echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-users" aria-hidden="true"></i> Comptes</a>';
	echo '</h2>';
	echo '</div>';
	echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
	echo '<div class="panel-body" id="detailcomptes_settings">';
		load_comptes(0);
	echo '</div>';
	echo '</div>';
	echo '</div>';
	}
	/* D�l�gation */
	$panel++;
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">';
	echo '<h4 class="panel-title">';
	echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="far fa-handshake" aria-hidden="true"></i> D�l�gation</a>';
	echo '</h4>';
	echo '</div>';
	echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
	echo '<div class="panel-body" id="detaildelegation_settings">';
		load_delegation();
	echo '</div>';
	echo '</div>';
	echo '</div>';
	if ($droit!="USER"){
	/* Collectivit�s */
	$panel++;
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">';
	echo '<h4 class="panel-title">';
	echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fas fa-university" aria-hidden="true"></i> Collectivit�s</a>';
	echo '</h4>';
	echo '</div>';
	echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
	echo '<div class="panel-body" id="detailcollectivite_settings">';
		load_collectivite(0);
	echo '</div>';
	echo '</div>';
	echo '</div>';
	/* Tiers */
	$panel++;
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">';
	echo '<h4 class="panel-title">';
	echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fas fa-address-card" aria-hidden="true"></i> Tiers</a>';
	echo '</h4>';
	echo '</div>';
	echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
	echo '<div class="panel-body" id="detailtiers_settings">';
		//load_tiers();
	echo '</div>';
	echo '</div>';
	echo '</div>';
	}
	/* PCT */
	$panel++;
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">';
	echo '<h4 class="panel-title">';
	echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fas fa-folder-open" aria-hidden="true"></i> PCT</a>';
	echo '</h4>';
	echo '</div>';
	echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
	echo '<div class="panel-body" style="overflow-y: scroll;height:450px;" id="detailpct_settings">';
		load_pct();
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '</div>';
	echo '</div>';
}
/* Comptes */
function load_comptes($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<div col-md-12>';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><b>Liste des agents</b></div>';
	echo '<div class="panel-body" style="overflow-y: scroll;height:420px;" id="detaillstcomptes_settings">';
		loadlst_comptes();
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><b>Informations agent</b></div>';
	echo '<div class="panel-body" style="overflow-y: scroll;height:420px;" id="detailinfocomptes_settings">';
		loadinfo_comptes(0);
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '</div>';
}
function loadlst_comptes(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<table class="table table-responsive table-bordered table-striped table-hover">';
	echo '<thead><tr><th width="15%" class="text-center"><button data-tooltip="Nouvel utilisateur" id="btnadd_setusers" class="btn btn-default btn-sm"><i class="fas fa-plus-circle"></i></button> <button data-tooltip="Synchronisation LDAP" class="btn btn-default btn-sm btnsync_setusers"><i class="fas fa-sync"></i></button></th><th width="10%">Identifiants</th><th>Nom</th><th>Pr�nom</th></tr></thead>';
	echo '<tbody>';
	$req="SELECT * FROM user ORDER BY user_id DESC";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		echo '<tr><td width="15%" class="text-center"><button data-id="'.$ligne["user_id"].'" class="btn btn-default btn-sm visu_comptes"><i class="fa fa-eye"></i></button> <button data-id="'.$ligne["user_id"].'" class="btn btn-default btn-sm del_comptes"><i class="fa fa-trash"></i></button></td><td width="10%">'.$ligne["user_login"].'</td><td>'.$ligne["user_nom"].'</td><td>'.$ligne["user_prenom"].'</td></tr>';
	}
	echo '</tbody>';
	echo '</table>';
}
function loadinfo_comptes($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM user WHERE user_id=".$id;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$user_nom = $ligne["user_nom"];
		$user_prenom = $ligne["user_prenom"];
		$user_mail = $ligne["user_mail"];
		$user_initial = $ligne["user_initial"];
		$user_login = $ligne["user_login"];
		$user_droit = $ligne["user_droit"];
		if ($ligne["user_instructeur"]=="1"){$chk_inst="checked";}else{$chk_inst="";}
	}
	echo '<form id="formsetusers">';
	echo '<input type="hidden" id="action" name="action" value="updatecomptes">';
	echo '<input type="hidden" id="setcompte_id" name="setcompte_id" value="'.$id.'">';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcompte_nom">Nom: </label></div><div class="col-sm-11 col-md-8"><input id="setcompte_nom" name="setcompte_nom" class="form-control input-sm" value="'.$user_nom.'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcompte_prenom">Pr�nom: </label></div><div class="col-sm-11 col-md-8"><input id="setcompte_prenom" name="setcompte_prenom" class="form-control input-sm" value="'.$user_prenom.'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcompte_mail">Mail: </label></div><div class="col-sm-11 col-md-8"><input id="setcompte_mail" name="setcompte_mail" class="form-control input-sm" value="'.$user_mail.'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcompte_initial">Initiale: </label></div><div class="col-sm-11 col-md-4"><input id="setcompte_initial" name="setcompte_initial" class="form-control input-sm" value="'.$user_initial.'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcompte_login">Login: </label></div><div class="col-sm-11 col-md-8"><input id="setcompte_login" name="setcompte_login" class="form-control input-sm" value="'.$user_login.'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcompte_droit">Droit: </label></div><div class="col-sm-11 col-md-8"><select id="setcompte_droit" name="setcompte_droit" class="form-control input-sm"><option value=""></option>';
	$droituser=array("ADMIN","USER","SUPERV");
	$a = '0'; $b = count($droituser);
	while ($a < $b) { 
		if ($user_droit==$droituser[$a]){$selected="selected";}else{$selected="";}
		echo '<option value="'.$droituser[$a].'" '.$selected.'>'.$droituser[$a].'</option>';
		$a++;
	}
	echo '</select></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcompte_instructeur">Instructeur: </label></div><div class="col-sm-11 col-md-8"><input id="setcompte_instructeur" name="setcompte_instructeur" type="checkbox" '.$chk_inst.'></div></div>';
	echo '</form>';
	echo '<button data-tooltip="Modifier" id="update_comptes" class="btn btn-default btn-sm pull-right"><i class="fas fa-edit"></i> utilisateur</button>';
}
function del_comptes($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql = "DELETE FROM user WHERE user_id=".$id;
	$result=mysql_query($sql,$link);
}
function add_comptes(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql ="INSERT INTO user (user_nom) VALUES ('')";
	$result=mysql_query($sql,$link);
}
function update_comptes(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	if ($_POST["setcompte_instructeur"]){$setcompte_instructeur="1";}else{$setcompte_instructeur="0";}
	//$setcompte_password = md5($_POST["setcompte_password"]);
	$sql ="UPDATE user SET user_login='".$_POST["setcompte_login"]."',user_nom='".$_POST["setcompte_nom"]."',user_prenom='".$_POST["setcompte_prenom"]."',user_initial='".$_POST["setcompte_initial"]."',user_mail='".$_POST["setcompte_mail"]."',user_instructeur='".$setcompte_instructeur."',user_droit='".$_POST["setcompte_droit"]."' WHERE user_id=".$_POST["setcompte_id"];
	$result=mysql_query($sql,$link);
}
/* Profil */
function load_compte(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM user WHERE user_login='".$_COOKIE["login"]."'";
	$result=mysql_query($req,$link);
	$row=mysql_num_rows($result);
	$data=mysql_fetch_assoc($result);
	echo '<div class="col-md-12">';
	echo '<form id="formsetuser">';
	echo '<input type="hidden" id="action" name="action" value="updatecompte">';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-2"><label for="set_usernom">Nom: </label></div><div class="col-sm-11 col-md-10"><input id="set_usernom" name="set_usernom" class="form-control input-sm" value="'.$data["user_nom"].'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-2"><label for="set_userprenom">Pr�nom: </label></div><div class="col-sm-11 col-md-10"><input id="set_userprenom" name="set_userprenom" class="form-control input-sm" value="'.$data["user_prenom"].'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-2"><label for="set_userinitial">Initiales: </label></div><div class="col-sm-3 col-md-3"><input id="set_userinitial" name="set_userinitial" class="form-control input-sm" value="'.$data["user_initial"].'" maxlength="5"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-2"><label for="set_usermail">Mail: </label></div><div class="col-sm-11 col-md-10"><input id="set_usermail" name="set_usermail" class="form-control input-sm" value="'.$data["user_mail"].'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-2"><label>Droit: </label></div><div class="col-sm-11 col-md-10">'.$data["user_droit"].'</div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-2"><label for="set_userlogin">Login: </label></div><div class="col-sm-11 col-md-10"><input id="set_userlogin" name="set_userlogin" class="form-control input-sm" value="'.$data["user_login"].'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-2"><label for="set_userpwd">Nouveau mot de passe: </label></div><div class="col-sm-11 col-md-10"><input id="set_userpwd" name="set_userpwd" type="password" class="form-control input-sm"></div></div>';
	echo '</form>';
	echo '<div class="form-group row"><div class="col-md-12"><button id="update_usercompte" data-tooltip="Modifier" class="btn btn-default btn-sm pull-right"><i class="fas fa-edit"></i> Utilisateur</button></div></div>';
	echo '</div>';
}
function update_compte(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql ="UPDATE user SET user_login='".$_POST["set_userlogin"]."',user_nom='".$_POST["set_usernom"]."',user_prenom='".$_POST["set_userprenom"]."',user_initial='".$_POST["set_userinitial"]."',user_mail='".$_POST["set_usermail"]."' WHERE user_login='".$_COOKIE["login"]."'";
	$result=mysql_query($sql,$link);
	if ($_POST["set_userpwd"]!=""){
		$setcompte_password = md5($_POST["set_userpwd"]);
		$sql ="UPDATE user SET user_pwd='".$setcompte_password."' WHERE user_login='".$_COOKIE["login"]."'";
		$result=mysql_query($sql,$link);
	}
}
/* D�l�gation */
function load_delegation(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqdeleg="SELECT * FROM operations WHERE ope_chargeaffaire='".$_COOKIE["login"]."' AND ope_delegation<>''";
	$resultdeleg=mysql_query($reqdeleg,$link);
	$rowdeleg=mysql_num_rows($resultdeleg);
	$datadeleg=mysql_fetch_assoc($resultdeleg);
	$ca_deleg = $datadeleg["ope_delegation"];
	echo '<div class="col-md-12">';
	$disabledreprendre="";$disableddeleg="";
	if ($rowdeleg!=0){echo '<div class="alert alert-success text-center">Votre compte est actuellement d�l�gu�</div>';$disableddeleg="disabled";}else{$disabledreprendre="disabled";}
	echo '<form>';
	echo '<div class="form-group row"><div class="col-sm-2 col-md-3"><label>Charg� d\'affaire: </label></div><div class="col-sm-10 col-md-9">';
	echo '<select class="form-control input-sm" id="settings_lstca">';
	echo '<option value="">S�lectionnez un charg� d\'affaire</option>';
	$req="SELECT user_login,user_nom,user_prenom FROM user WHERE user_login<>'".$_COOKIE["login"]."'";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["user_login"]==$ca_deleg){$selected = "selected";}else{$selected = "";}
		echo '<option value="'.$ligne["user_login"].'" '.$selected.'>'.$ligne["user_nom"].' '.$ligne["user_prenom"].'</option>';
	}
	echo '</select>';
	echo '</div></div>';
	echo '</form>';
	echo '<div class="row"><div class="col-md-2"><button data-deleg="deleg" class="btn btn-default btnsettings_deleg" '.$disableddeleg.'>D�l�guer</button></div><div class="col-md-2"><button data-deleg="reprendre" class="btn btn-default btnsettings_deleg" '.$disabledreprendre.'>Reprendre la main</button></div></div>';
	echo '</div>';
}
function update_delegation($val,$deleg){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	if ($deleg=="deleg"){
		$req="UPDATE operations SET ope_delegation='".$val."' WHERE ope_chargeaffaire='".$_COOKIE["login"]."'";
		$result=mysql_query($req,$link);
	}else{
		$req="UPDATE operations SET ope_delegation='' WHERE ope_chargeaffaire='".$_COOKIE["login"]."'";
		$result=mysql_query($req,$link);
	}
}
/* Collectivit�s */
function load_collectivite(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<div col-md-12>';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><b>Liste des collectivit�s</b></div>';
	echo '<div class="panel-body" style="overflow-y: scroll;height:450px;" id="detaillstcollectivite_settings">';
		loadlst_collectivite();
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><b>Informations collectivit�</b></div>';
	echo '<div class="panel-body" style="overflow-y: scroll;height:450px;" id="detailinfocollectivite_settings">';
		loadinfo_collectivite(0);
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '</div>';
}
function loadlst_collectivite(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<table class="table table-responsive table-bordered table-striped table-hover">';
	echo '<thead><tr><th width="15%" class="text-center"><button data-tooltip="Nouvelle collectivit�" id="btnadd_collectivite" class="btn btn-default btn-sm"><i class="fas fa-plus-circle"></i></button></th><th class="text-center">Type</th><th width="10%" class="text-center">Code insee</th><th class="text-center">Libell�</th></tr></thead>';
	echo '<tbody>';
	$req="SELECT coll_id,coll_codeinsee,coll_nom,coll_type FROM collectivites ORDER BY coll_datecreate DESC";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		echo '<tr><td width="15%" class="text-center"><button data-tooltip="Visualiser" data-id="'.$ligne["coll_id"].'" class="btn btn-default btn-sm visu_collectivite"><i class="fa fa-eye"></i></button> <button data-tooltip="Supprimer" data-id="'.$ligne["coll_id"].'" class="btn btn-default btn-sm del_collectivite"><i class="fa fa-trash"></i></button></td><td>'.$ligne["coll_type"].'</td><td width="10%">'.$ligne["coll_codeinsee"].'</td><td>'.$ligne["coll_nom"].'</td></tr>';
	}
	echo '</tbody>';
	echo '</table>';
}
function loadinfo_collectivite($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	if ($id==0){
		$type="";
		$nom="";
	}else{
		$req="SELECT * FROM collectivites WHERE coll_id=".$id;
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			$coll_type=$ligne["coll_type"];
			$coll_nom=$ligne["coll_nom"];
			$coll_codeinsee=$ligne["coll_codeinsee"];
			$coll_codepostal=$ligne["coll_codepostal"];
			$coll_adresse=$ligne["coll_adresse"];
			if ($ligne["coll_ur"]=="1"){$chk_ur="checked";}else{$chk_ur="";}
			if ($ligne["coll_eligibleface"]=="1"){$chk_elig="checked";}else{$chk_elig="";}
			if ($ligne["coll_adhesp"]=="1"){$chk_adhesp="checked";}else{$chk_adhesp="";}
			if ($ligne["coll_adhecl"]=="1"){$chk_adhecl="checked";}else{$chk_adhecl="";}
		}
	}
	$typecoll=array("EPCI","COMMUNE");
	echo '<form id="formsetcoll">';
	echo '<input type="hidden" id="action" name="action" value="updatecollectivite">';
	echo '<input type="hidden" id="setcoll_id" name="setcoll_id" value="'.$id.'">';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcoll_type">Type: </label></div><div class="col-sm-11 col-md-8"><select id="setcoll_type" name="setcoll_type" class="form-control input-sm"><option value="">Choisir le type de collectivit�</option>';
	$a = '0'; $b = count($typecoll);
	while ($a < $b) { 
		if ($coll_type==$typecoll[$a]){$selected="selected";}else{$selected="";}
		echo '<option value="'.$typecoll[$a].'" '.$selected.'>'.$typecoll[$a].'</option>';
		$a++;
	}
	echo '</select></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcoll_nom">Nom: </label></div><div class="col-sm-11 col-md-8"><input id="setcoll_nom" name="setcoll_nom" class="form-control input-sm" value="'.$coll_nom.'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcoll_codeinsee">Code insee: </label></div><div class="col-sm-11 col-md-8"><input id="setcoll_codeinsee" name="setcoll_codeinsee" class="form-control input-sm" value="'.$coll_codeinsee.'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcoll_adresse">Adresse: </label></div><div class="col-sm-11 col-md-8"><input id="setcoll_adresse" name="setcoll_adresse" class="form-control input-sm" value="'.$coll_adresse.'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcoll_codepostal">Code postal: </label></div><div class="col-sm-11 col-md-8"><input id="setcoll_codepostal" name="setcoll_codepostal" class="form-control input-sm" value="'.$coll_codepostal.'"></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcoll_ur">U/R: </label></div><div class="col-sm-11 col-md-8"><input id="setcoll_ur" name="setcoll_ur" type="checkbox" '.$chk_ur.'></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcoll_eligibleface">Eligible FACE: </label></div><div class="col-sm-11 col-md-8"><input id="setcoll_eligibleface" name="setcoll_eligibleface" type="checkbox" '.$chk_elig.'></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcoll_adhesp">Adh�rent ESP: </label></div><div class="col-sm-11 col-md-8"><input id="setcoll_adhesp" name="setcoll_adhesp" type="checkbox" '.$chk_adhesp.'></div></div>';
	echo '<div class="form-group row"><div class="col-sm-1 col-md-4"><label for="setcoll_adhecl">Adh�rent ECL: </label></div><div class="col-sm-11 col-md-8"><input id="setcoll_adhecl" name="setcoll_adhecl" type="checkbox" '.$chk_adhecl.'></div></div>';
	echo '</form>';
	echo '<button data-tooltip="Modifier" class="btn btn-default btn-sm pull-right" id="update_collectivite"><i class="fas fa-edit"></i> Collectivit�</button>';
}
function add_collectivite(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql ="INSERT INTO collectivites (coll_datecreate) VALUES (NOW())";
	$result=mysql_query($sql,$link);
}
function del_collectivite($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql = "SELECT * FROM collectivites JOIN operations ON operations.ope_codeinsee=collectivites.coll_codeinsee WHERE collectivites.coll_id=".$id;
	$result=mysql_query($sql,$link);
	$nbope = mysql_num_rows($result);
	$sql = "SELECT * FROM collectivites JOIN instructions ON instructions.inst_codeinsee=collectivites.coll_codeinsee WHERE collectivites.coll_id=".$id;
	$result=mysql_query($sql,$link);
	$nbinst = mysql_num_rows($result);
	$sql = "SELECT * FROM collectivites JOIN projets ON projets.proj_codeinsee=collectivites.coll_codeinsee WHERE collectivites.coll_id=".$id;
	$result=mysql_query($sql,$link);
	$nbproj = mysql_num_rows($result);
	if ($nbope==0 && $nbinst==0 && $nbproj==0){
		$sql = "DELETE FROM collectivites WHERE coll_id=".$id;
		$result=mysql_query($sql,$link);
	}else{
		echo "erreur";
	}
}
function update_collectivite(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	if ($_POST["setcoll_ur"]){$setcoll_ur="1";}else{$setcoll_ur="0";}
	if ($_POST["setcoll_eligibleface"]){$setcoll_eligibleface="1";}else{$setcoll_eligibleface="0";}
	if ($_POST["setcoll_adhesp"]){$setcoll_adhesp="1";}else{$setcoll_adhesp="0";}
	if ($_POST["setcoll_adhecl"]){$setcoll_adhecl="1";}else{$setcoll_adhecl="0";}
	$sql ="UPDATE collectivites SET coll_type='".$_POST["setcoll_type"]."',coll_nom='".$_POST["setcoll_nom"]."',coll_codeinsee='".$_POST["setcoll_codeinsee"]."',coll_codepostal='".$_POST["setcoll_codepostal"]."',coll_adresse='".$_POST["setcoll_adresse"]."',coll_ur='".$setcoll_ur."',coll_eligibleface='".$setcoll_eligibleface."',coll_adhesp='".$setcoll_adhesp."',coll_adhecl='".$setcoll_adhecl."' WHERE coll_id=".$_POST["setcoll_id"];
	$result=mysql_query($sql,$link);
}

/* PCT */
function load_pct(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<table class="table table-responsive table-bordered table-striped table-hover">';
	echo '<thead><tr><th width="10%" class="text-center"><button id="btnadd_pct" class="btn btn-default btn-sm"><i class="fas fa-plus-circle"></i></button></th><th class="text-center">Libell�</th><th width="10%" class="text-center">Valeur</th><th width="10%" class="text-center">Date valeur</th></tr></thead>';
	echo '<tbody>';
	$req="SELECT * FROM parametres WHERE param_section='PCT' ORDER BY param_libelle,param_datevaleur DESC";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		echo '<tr><td width="10%" class="text-center"><button data-id="'.$ligne["param_id"].'" class="btn btn-default btn-sm del_pct"><i class="fa fa-trash"></i></button></td><td class="text-center"><input data-id="'.$ligne["param_id"].'" data-chp="param_libelle" class="form-control input-sm update_pct" value="'.$ligne["param_libelle"].'"></td><td class="text-center" width="10%"><input data-id="'.$ligne["param_id"].'" data-chp="param_valeur" class="form-control input-sm text-center update_pct" value="'.$ligne["param_valeur"].'"></td><td class="text-center" width="10%"><input data-id="'.$ligne["param_id"].'" data-chp="param_datevaleur" class="form-control input-sm text-center datepct update_pct" value="'.strftime("%d/%m/%Y",strtotime($ligne["param_datevaleur"])).'"></td></tr>';
	}	
	echo '</tbody>';
	echo '</table>';
}
function add_pct(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql ="INSERT INTO parametres (param_section) VALUES ('PCT')";
	$result=mysql_query($sql,$link);
}
function del_pct($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql = "DELETE FROM parametres WHERE param_id=".$id;
	$result=mysql_query($sql,$link);
}
function update_pct($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	switch ($chp){
		case "param_datevaleur":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
	}
	$req="UPDATE parametres SET ".$chp."='".$val."' WHERE param_id=".$id;
	$result=mysql_query($req,$link);
}