<?php
//error_reporting(E_ALL);
//ini_set('display_errors','on');

if(isset($_POST['action']) && !empty($_POST['action'])) {
  $action = $_POST['action'];
  switch($action) {
    case 'exportsedit' : export_sedit($_POST["bud"],$_POST["annee"],$_POST["avance"],$_POST["tab"]);break;
    case 'lsttabexportsedit' : lsttabexport_sedit();break;
    case 'lstbudgetsedit' : lstbudget_sedit();break;
    case 'lstavancesedit' : lstavance_sedit();break;
    case 'blah' : blah();break;
    // ...etc...
  }
}

function lstbudget_sedit(){
  require("./compte.php");
  setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
  $link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
  mysql_select_db($baseSYGALE,$link);
  $req2="SELECT lex_libelle, lex_id FROM lexique WHERE lex_codelexique='BUD_SEDIT' ORDER BY lex_libelle ASC";
  $result2=mysql_query($req2,$link);
  $row2=mysql_num_rows($result2);
  echo '<option value="0">Choisir un budget</option>';
  if ($row2!=0)
  {
    while ($ligne2=mysql_fetch_assoc($result2))
    {
      echo '<option value="'.$ligne2["lex_libelle"].'">'.$ligne2["lex_libelle"].'</option>';
    }
  }
}
function lstavance_sedit(){
  require("./compte.php");
  setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
  $link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
  mysql_select_db($baseSYGALE,$link);
  $req2="SELECT lex_libelle, lex_id FROM lexique WHERE lex_codelexique='AVANCE_SEDIT' ORDER BY lex_id ASC";
  $result2=mysql_query($req2,$link);
  $row2=mysql_num_rows($result2);
  echo '<option value="0">Choisir un avancement</option>';
  if ($row2!=0)
  {
    $i=1;
    while ($ligne2=mysql_fetch_assoc($result2))
    {
      echo '<option value="'.$i.'">'.$ligne2["lex_libelle"].'</option>';
      $i++;
    }
  }
}
function lsttabexport_sedit(){
  require("./compte.php");
  setlocale (LC_TIME, 'fr_FR.utf8','fra');
  date_default_timezone_set('Europe/Paris');
  $link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
  mysql_select_db($baseSYGALE,$link);

  echo '<table class="table table-responsive table-bordered table-hover table-striped table-sm">';
  echo '<thead><tr><th width="8%"></th><th>D�signation</th></tr></thead>';
  echo '<tbody>';
  $req2="SELECT boncommande.id,boncommande.montant_ttc,boncommande.numero FROM boncommande WHERE export_sedit=0";
  $result2=mysql_query($req2,$link);
  $row2=mysql_num_rows($result2);
  if ($row2!=0)
  {
    while ($data=mysql_fetch_assoc($result2))
    {
      echo '<tr><td class="text-center"><input class="chkbcsedit" type="checkbox" data-id="'.$data["id"].'"></td><td class="text-center">'.$data["numero"].'</td></tr>';
    }
  }else{
    echo '<tr><td colspan="2" class="text-center">Aucun mouvement</td></tr>';
  } 
  echo '</tbody>';
  echo '</table>';
}

function export_sedit($bud,$annee,$avance,$tab){
  require("./compte.php");
  setlocale (LC_TIME, 'fr_FR.utf8','fra');
  date_default_timezone_set('Europe/Paris');
  $link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
  mysql_select_db($baseSYGALE,$link);

  $orga = substr($bud, 0,2);
  $codebud = substr($bud, 2,2);
  $outputFileName = '../data/'.$annee."_".$bud."_".date('Ymd').'.txt';

  $req2="SELECT boncommande.id,boncommande.montant_ttc,boncommande.numero,entreprises.ent_codetiers FROM boncommande JOIN entreprises ON entreprises.ent_id=boncommande.id_ent WHERE boncommande.id IN (".$tab.")";
  $result2=mysql_query($req2,$link);
  $row2=mysql_num_rows($result2);
  $file = "/##/PARAM/".$orga."/".$codebud."/".$annee."/".$avance."/O/N/N\n";
  $file .= "/##/\n";
  $numengagement = 18050812;
  if ($row2!=0)
  {
    $i=1;
    while ($data=mysql_fetch_assoc($result2))
    {
      $numengagement++;
      $file .= "/01/".$numengagement."\n"; /* N� engagement */
      $file .= "/02/D\n"; /* D - D�pense  R - Recette */
      $file .= "/03/".$data["ent_codetiers"]."\n"; /* Code tiers (entreprise) */
      $file .= "/04/BC_".$data["numero"]."\n"; /* Libell� de l'engagement */
      $file .= "/05/01\n"; /* Code calendrier 01 - D�pense  02 - Recette */
      $file .= "/--/\n"; /* A chaque nouvelle ligne */
      $file .= "/51/".$i."\n"; /* N� ligne de l'engagement de 001 � 999 ligne par prestation dans le BC */
      $file .= "/54/011211\n"; /* Code Imputation �tendue */
      $file .= "/57/AF \n"; /* Libell� de la ligne */
      $file .= "/66/".number_format($data["montant_ttc"], 2, ',', '')."\n"; /* Montant TTC en euros */
      $file .= "/69/EXT\n"; /* Nature de prestation */
      $file .= "/##/\n";
      $i++;
      //$req="UPDATE boncommande SET export_sedit=1,date_export=NOW() WHERE id=".$data["id"];
      //$result=mysql_query($req,$link);  
    }
  }

  touch($outputFileName);
  file_put_contents($outputFileName, $file);
  echo utf8_decode("G�n�ration effectu�e !!!");
}
?>