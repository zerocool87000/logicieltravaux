<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(isset($_POST['action']) && !empty($_POST['action'])) {
	$action = $_POST['action'];
	switch($action) {
		case 'loadrequete' : load_requete();break;
		case 'executerequete' : execute_requete($_POST["tabfield"],$_POST["tabgroupe"],$_POST["tabtri"],$_POST["where"]);break;
		case 'executerequete2' : execute_requete2();break;
		case 'blah' : blah();break;
		// ...etc...
	}
}

function load_requete(){

	echo '<div class="row">';

	echo '<div class="col-sm-2">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-th-list"></i> Champs</div>';
	echo '<div class="panel-body" id="lstchampvue">';
		lst_field();
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '<div class="col-sm-10">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-cogs"></i> Param�trages</div>';
	echo '<div class="panel-body" id="detailgene_requete">';

	echo '<div class="col-sm-12">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-code"></i> Crit�res</div>';
	echo '<div class="panel-body" id="">';

	/* Filtre */
	echo '<div class="col-sm-4">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-filter"></i> Filtres</div>';
	echo '<div class="panel-body" id="">';
		echo '<div id="dropfieldfilter_requete" class="dropfieldrequete">D�poser vos champs ici</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	/* Tri */
	echo '<div class="col-sm-4">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-sort-alpha-down"></i> Tri</div>';
	echo '<div class="panel-body" id="">';
		echo '<div id="dropfieldtri_requete" class="dropfieldrequete">D�poser vos champs ici</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	/* Groupes */
	echo '<div class="col-sm-4">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-sitemap"></i> Groupes</div>';
	echo '<div class="panel-body" id="">';
		echo '<div id="dropfieldgroupe_requete" class="dropfieldrequete">D�poser vos champs ici</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';


	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '<div class="col-sm-12">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-file-code"></i> R�sultat <div class="pull-right"><button id="btnexportresultat" class="btn btn-default btn-sm" title="Export excel"><i class="far fa-file-excel"></i></button></div></div>';
	echo '<div class="panel-body" id="detailview_requete">';
		echo '<div class="well well-sm text-center"><b>Aucun r�sultat</b></div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';


	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '</div>';
}

function lst_field(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$result = mysql_query("SHOW COLUMNS FROM vue_travaux");
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_assoc($result)) {
			$type = explode("(", $row["Type"]);
			switch ($type[0]){
				case "int": $icon = "<span style='color:#FAA21B;font-size:12px;'><b>123</b></span>";
					break;
				case "varchar": $icon = "<i class='fas fa-font' style='color:#FAA21B;font-size:12px;'></i>";//"<span style='color:blue;font-size:10px;'>Abc</span>";
					break;
				case "date": $icon = "<i class='far fa-calendar-alt' style='color:#FAA21B;font-size:12px;'></i>";//"<span style='color:blue;font-size:10px;'>Abc</span>";
					break;
			}
			echo '<div draggable="true" class="dragfieldrequete" data-chp="'.$row["Field"].'"><input type="checkbox" class="chkfieldrequete" data-id="'.$row["Field"].'"> '.$icon." ".$row["Field"]."</div>";
		}
	}
}

function execute_requete($tabfield,$tabgroup,$tabtri,$where){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$delimiter = ",";
	$tab_field = explode($delimiter,$tabfield);

	//comptage du nombre de champ
	$tabchp = explode($delimiter,$tabfield);
	$counttabchpini=count($tabchp);
	$counttabchp=$counttabchpini;

	/* Cr�ation de la liste des champs */
	$tabfieldfin = $tabfield;

	/* Cr�ation des tris */
	$trifin="";
	if ($tabtri!=""){
		$delimiter = ",";
		$tab_tri = explode($delimiter,$tabtri);
		$counttabtri=count($tab_tri);
		if ($counttabtri>0){
			$trifin = " ORDER BY ".$tabtri;
		}
	}

	/* Cr�ation des groupes */
	$group="";
	if ($tabgroup!=""){
		$delimiter = ",";
		$tab_groupe = explode($delimiter,$tabgroup);
		$counttabgroup=count($tab_groupe);
		//$group = " ORDER BY ".$tabgroup;
		$tabfieldfin = $tabgroup.",".$tabfield;
		$tabchp = explode($delimiter,$tabfieldfin);
		$counttabchp=count($tabchp);
		if ($tabtri!=""){
			$trifin = " ORDER BY ".$tabgroup.",".$tabtri;
		}else{
			$trifin = " ORDER BY ".$tabgroup;
		}
	}

	if ($where!=""){$where=" WHERE ".stripslashes($where);}

	if ($tabfield!=""){
		$sql = "SELECT ".$tabfieldfin." FROM vue_travaux ".$where." ".$trifin;
		//echo $sql;
		$req = mysql_query($sql,$link);
		$rows = mysql_num_rows($req);
		if ($rows!=0){
			echo "<table class='table' id='tabresultatrequete'>";
			/* Nom des colonnes */
			echo "<thead><tr>";
			for ($i=0; $i < $counttabchpini; $i++) {
				echo "<th>".utf8_encode($tab_field[$i])."</th>";
			}
			echo "</tr></thead>";

			echo "<tbody>";
			$grouppre = array();
			$c=0;
			while($data3 = mysql_fetch_row($req))
			{
				if ($counttabgroup!=0)
				{
					if ($c==0){
						for ($i=0; $i < $counttabgroup; $i++) {
							$grouppre[]=utf8_encode($data3[$i]);
							echo "<tbody class='labels'>";
							echo "<tr style='background-color:rgba(250,162,27,0.5)!important;border:1px solid #FAA21B!important;'>";// 
							echo '<td colspan="'.$counttabchp.'"><i class="fa fa-caret-down"></i> ';
							//echo utf8_encode($data3[$i]);
							echo '<label for="'.utf8_encode($data3[$i]).'">'.utf8_encode($data3[$i]).'</label>';
							//echo '<input type="checkbox" name="'.utf8_encode($data3[$i]).'" id="'.utf8_encode($data3[$i]).'" data-toggle="toggle">';
							echo '</td>';
							echo "</tr>";
							echo "</tbody>";
						}
						$c++;
					}else{
						for ($i=0; $i < $counttabgroup; $i++) {
							if ($grouppre[$i]!=utf8_encode($data3[$i])){
								echo "<tbody class='labels'>";
								echo "<tr style='background-color:rgba(250,162,27,0.5)!important;border:1px solid #FAA21B!important;'>";// 
								echo '<td colspan="'.$counttabchp.'"><i class="fa fa-caret-down"></i> ';
								//echo utf8_encode($data3[$i]);
								echo '<label for="'.utf8_encode($data3[$i]).'">'.utf8_encode($data3[$i]).'</label>';
								//echo '<input type="checkbox" name="'.utf8_encode($data3[$i]).'" id="'.utf8_encode($data3[$i]).'" data-toggle="toggle">';
								echo '</td>';
								echo "</tr>";
								echo "</tbody>";
								$grouppre[$i]=utf8_encode($data3[$i]);
							}
						}					
					}
					echo "<tbody class='toto' style='background-color:#fff!important;border:1px solid #636466!important;'>";
					echo "<tr style='background-color:#fff!important;border:1px solid #636466!important;'>";// 
					if ($counttabgroup!=0){$deb=$counttabgroup;}else{$deb=0;}
					for ($i=$deb; $i < $counttabchp; $i++) {
						if (mysql_field_type($req,$i)=="date"){$cellule=strftime("%d/%m/%Y",strtotime($data3[$i]));}else{$cellule=utf8_encode($data3[$i]);}
						echo "<td>".$cellule."</td>";
					}	
					echo "</tr>";
					echo "</tbody>";
				}else{
					echo "<tr>";
					for ($i=0; $i < $counttabchp; $i++) {
						if (mysql_field_type($req,$i)=="date"){$cellule=strftime("%d/%m/%Y",strtotime($data3[$i]));}else{$cellule=utf8_encode($data3[$i]);}
						echo "<td>".$cellule."</td>";
					}	
					echo "</tr>";
				}
			}
			echo "</tbody>";
			echo "</table>";
		}else{
			echo '<div class="well well-sm text-center"><b>Aucun r�sultat</b></div>';		
		}
	}else{
		echo '<div class="well well-sm text-center"><b>Aucun r�sultat</b></div>';		
	}
}

function execute_requete2(){
	require("../compte.php");
	include("./fonctions.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	//comptage du nombre de champ
	$tabchp = explode(";",$_POST["b"]);
	$counttabchp=count($tabchp);
	//Cr�ation des conditions
	$tabcrit = array();
	$tabcrit = explode(";",$_POST["tbcrit"]);
	$counttabcrit=count($tabcrit);
	$crit="";
	if ($counttabcrit>0){
		for ($i=0; $i < $counttabcrit; $i++) {
			if (stripslashes($tabcrit[$i])!=''){
				if ($i>0){
					$crittemp = stripslashes($tabcrit[$i]);
					$crit .= " AND ".$crittemp;
				}else{
					$crit = " WHERE";
					$crittemp = stripslashes($tabcrit[$i]);
					$crit .= " ".$crittemp;
				}
			}
		}	
	}
	//Cr�ation des groupes
	$tabgroup = array();
	$tabgroup = explode(";",$_POST["tbgroup"]);
	$counttabgroup=count($tabgroup);
	$group="";
	if ($counttabgroup>0){
		for ($i=0; $i < $counttabgroup; $i++) {
			if (stripslashes($tabgroup[$i])!=''){
				if ($i>0){
					$grouptemp = stripslashes($tabgroup[$i]);
					$group .= ", ".$grouptemp;
				}else{
					$group = " ORDER BY";
					$grouptemp = stripslashes($tabgroup[$i]);
					$group .= " ".$grouptemp;
				}
			}
		}	
	}

	$sql = "SELECT ".$_POST["a"]." FROM esteleuser_chantier INNER JOIN esteleuser_collect ON COLIDX=CHTCOLIDX1 INNER JOIN esteleuser_operat ON OPEIDX=CHTOPEIDX ".$crit.$group." limit 2000";
	// echo $sql;
	$req = mysql_query($sql);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	while($data3 = mysql_fetch_row($req))
	{
		echo "<tr>";
		for ($i=0; $i < $counttabchp; $i++) {
			echo "<td>".utf8_encode($data3[$i])."</td>";
		}	
		echo "</tr>";
	}
	mysql_close();
	echo "</table>";
}
?>