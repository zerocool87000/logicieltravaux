<?php

/* Tables concern�es
pos
pos_article //Pas supprimer dans del_specif
pos_bord //Pas supprimer dans del_specif
*/

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(isset($_POST['action']) && !empty($_POST['action'])) {
	$action = $_POST['action'];
	switch($action) {
		case 'detailspecif' : detail_specif($_POST["idcht"],$_POST["nat"]);break;
		case 'updatespecif' : update_specif($_POST["id"],$_POST["chp"],$_POST["val"],$_POST["nat"],$_POST["table"]);break;
		case 'addspecif' : add_specif($_POST["idcht"],$_POST["idart"],$_POST["table"]);break;
		case 'delinspecif' : del_inspecif($_POST["id"],$_POST["table"]);break;
		case 'selectspecif' : select_specif($_POST["val"],$_POST["idcht"],$_POST["nat"],$_POST["table"]);break;
		case 'lstvalposte' : lst_val_poste($_POST["idcht"]);break;
		case 'blah' : blah();break;
		// ...etc...
	}
}

function detail_specif($idcht,$nat){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo "<div class='col-md-6'>";
	/* Gestionnaire de voirie */
	$req="SELECT * FROM pos WHERE pos_idcht=".$idcht;
	$result=mysql_query($req,$link);
	$row = mysql_num_rows($result);
	while ($data=mysql_fetch_assoc($result))
	{
		$id = $data["pos_id"];
		$type_transfo = $data["pos_typetransfo"];
		$nom_poste = $data["pos_nomposte"];
		$numero_poste = $data["pos_numeroposte"];
		$puissance = $data["pos_puissance"];
		$tension = $data["pos_tension"];
		$nature = $data["pos_nature"];
	}
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-bolt"></i> Poste �lectrique</div>';
	echo '<div class="panel-body" id="detail_specif_poste">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="pos_nomposte">Nom du poste:</label>';
		echo '<div class="col-sm-6">';
		echo '<input class="form-control input-sm update_specif" data-id="'.$id.'" data-idcht="'.$idcht.'" data-table="pos" data-type="" data-nat="'.$nat.'" data-chp="pos_nomposte" id="pos_nomposte" name="pos_nomposte" value="'.$nom_poste.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="pos_numposte">Num�ro du poste:</label>';
		echo '<div class="col-sm-6">';
		echo '<input class="form-control input-sm update_specif" data-id="'.$id.'" data-idcht="'.$idcht.'" data-table="pos" data-type="" data-nat="'.$nat.'" data-chp="pos_numeroposte" id="pos_numposte" name="pos_numposte" value="'.$numero_poste.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="pos_nature">Nature:</label>';
		echo '<div class="col-sm-6">';
		echo '<select class="form-control input-sm text-right update_specif" data-id="'.$id.'" data-chp="pos_nature" data-idcht="'.$idcht.'" data-table="pos" data-type="" data-nat="'.$nat.'" id="pos_nature" name="pos_nature">';
		echo '<option value="">Choisir une nature</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='NAT_POSTE'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_libelle"]==$nature){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_libelle"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="pos_typetransfo">Type transfo:</label>';
		echo '<div class="col-sm-6">';
		echo '<select class="form-control input-sm text-right update_specif" data-id="'.$id.'" data-chp="pos_typetransfo" data-idcht="'.$idcht.'" data-table="pos" data-type="" data-nat="'.$nat.'" id="pos_nature" name="pos_nature">';
		echo '<option value="">Choisir un type</option>';
		$req="SELECT * FROM lexique WHERE lex_codelexique='TYPE_TRANSFO'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["lex_libelle"]==$type_transfo){$selected="selected";}else{$selected="";}
			echo '<option value="'.$ligne["lex_libelle"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
		}		
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="pos_puissance">Puissance:</label>';
		echo '<div class="col-sm-6">';
		echo '<input class="form-control input-sm update_specif" data-id="'.$id.'" data-idcht="'.$idcht.'" data-table="pos" data-type="" data-nat="'.$nat.'" data-chp="pos_puissance" id="pos_puissance" name="pos_puissance" value="'.$puissance.'">';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-4" for="pos_tension">Tension:</label>';
		echo '<div class="col-sm-6">';
		echo '<input class="form-control input-sm update_specif" data-id="'.$id.'" data-idcht="'.$idcht.'" data-table="pos" data-type="" data-nat="'.$nat.'" data-chp="pos_tension" id="pos_tension" name="pos_tension" value="'.$tension.'">';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo "<div class='col-md-12'>";
	/* Demandeurs */
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-book"></i> Bordereau de prix</div>';
	echo '<div class="panel-body" id="detail_specif_dem">';
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-2" for="lstbordapd">Bordereaux:</label>';
	echo '<div class="col-sm-6">';
	echo '<select class="form-control input-sm select_specif" data-table="pos_bord" data-type="" data-nat="'.$nat.'" data-idcht="'.$idcht.'" data-div="lstarticlesposte" id="lstbordposte" name="lstbordposte" data-idapd="'.$idapd.'">';
	echo '<option value="-1">S�lectionnez un bordereau</option>';
	$req="SELECT posbord_id,posbord_libelle FROM pos_bord";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		//if ($ligne["date_cloture"]=="0000-00-00"){$title = $ligne["libelle"];}else{$title = $ligne["libelle"]." [CLOTURE]";}
		echo '<option value="'.$ligne["posbord_id"].'" data-idapd="'.$idapd.'">'.$ligne["posbord_libelle"].'</option>';
	}
	echo '</select>';
	echo '</div>';
	echo '</div>';
	echo '</form>';
	/* Liste des articles du bordereau selectionn� */
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">Articles</div>';
	echo '<div class="panel-body" id="lstarticlesposte" style="max-height: 200px;overflow-y: scroll;">';
		//lst_article_bord("-1",$idapd);
	echo '</div>';
	echo '</div>';
	/* Liste des articles valoris�s */
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">Valorisation</div>';
	echo '<div class="panel-body" id="lstvalposte">';
		lst_val_poste($idcht);
	echo '</div>';
	echo '</div>';

	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function update_specif($id,$chp,$val,$nat,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE ".$table." SET ".$chp."='".$val."' WHERE id=".$id;
	$result2=mysql_query($req2,$link);
	switch ($chp){
		case "qte":
			/* R�cup�ration pu article */
			$req="SELECT * FROM pos_val WHERE pos_val.posval_id=".$id;
			$result=mysql_query($req,$link);
			$ligne=mysql_fetch_assoc($result);
			$id_art = $ligne["posval_idart"];
			$id_cht = $ligne["posval_idcht"];
			$req="SELECT * FROM pos_article WHERE pos_article.posarticle_id=".$id_art;
			$result=mysql_query($req,$link);
			$ligne=mysql_fetch_assoc($result);
			$pu = $ligne["posarticle_pu"];
			$tot = $val * $pu;
			$req2="UPDATE ".$table." SET ".$table."_tot='".$tot."' WHERE ".$table."_id=".$id;
			$result2=mysql_query($req2,$link);
			/* Modification de l'APD */
			$req2="SELECT sum(posval_tot) as totvalposte FROM pos_val WHERE posval_idcht=".$id_cht;
			$result2=mysql_query($req2,$link);
			$ligne=mysql_fetch_assoc($result2);
			$totvalposte = $ligne["totvalposte"];
			$req="SELECT apd.id FROM apd WHERE apd.id_cht=".$id_cht;
			$result=mysql_query($req,$link);
			$ligne=mysql_fetch_assoc($result);
			$id_apd = $ligne["id"];
			$req="SELECT tva.tva_taux FROM tva ORDER BY tva_datetva DESC LIMIT 1";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result)){
				$taux_tva = $ligne["tva_taux"];
			}
			$tva = $totvalposte * $taux_tva / 100;
			$totTTC = $totvalposte + $tva;
			$req2="UPDATE apd SET estimation_HT='".$totvalposte."',tva='".$tva."',tot_HT='".$totvalposte."',tot_TTC='".$totTTC."' WHERE id=".$id_apd;
			$result2=mysql_query($req2,$link);
			$req2="UPDATE val_apd SET tot='".$totvalposte."' WHERE id_apd=".$id_apd;
			$result2=mysql_query($req2,$link);
			break;
	}
}
function del_specif($idcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);

	$req2="DELETE FROM pos_val WHERE posval_idcht=".$idcht;
	$result2=mysql_query($req2,$link);
	$req2="DELETE FROM pos WHERE pos_idcht=".$idcht;
	$result2=mysql_query($req2,$link);
}
function del_inspecif($id,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM pos_val WHERE pos_val.posval_id=".$id;
	$result=mysql_query($req,$link);
	$ligne=mysql_fetch_assoc($result);
	$id_cht = $ligne["posval_idcht"];

	$req2="DELETE FROM ".$table." WHERE ".$table."_id=".$id;
	$result2=mysql_query($req2,$link);

	/* Modification de l'APD */
	$req="SELECT * FROM pos_val WHERE pos_val.posval_idcht=".$id_cht;
	$result=mysql_query($req,$link);
	$rows = mysql_num_rows($result);
	if ($rows==0){
		$req="SELECT apd.id FROM apd WHERE apd.id_cht=".$id_cht;
		$result=mysql_query($req,$link);
		$ligne=mysql_fetch_assoc($result);
		$id_apd = $ligne["id"];
		/* Suppression des valorisations APD */
		$req2="DELETE FROM val_apd WHERE val_apd.id_apd=".$id_apd;
		$result2=mysql_query($req2,$link);
		/* Reset des montants APD */
		$req2="UPDATE apd SET estimation_HT='0',tva='0',tot_HT='0',tot_TTC='0' WHERE id=".$id_apd;
		$result2=mysql_query($req2,$link);
	}else{
		$req2="SELECT sum(posval_tot) as totvalposte FROM pos_val WHERE posval_idcht=".$id_cht;
		$result2=mysql_query($req2,$link);
		$ligne=mysql_fetch_assoc($result2);
		$totvalposte = $ligne["totvalposte"];
		$req="SELECT apd.id FROM apd WHERE apd.id_cht=".$id_cht;
		$result=mysql_query($req,$link);
		$ligne=mysql_fetch_assoc($result);
		$id_apd = $ligne["id"];
		$req="SELECT tva.tva_taux FROM tva ORDER BY tva_datetva DESC LIMIT 1";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result)){
			$taux_tva = $ligne["tva_taux"];
		}
		$tva = $totvalposte * $taux_tva / 100;
		$totTTC = $totvalposte + $tva;
		$req2="UPDATE apd SET estimation_HT='".$totvalposte."',tva='".$tva."',tot_HT='".$totvalposte."',tot_TTC='".$totTTC."' WHERE id=".$id_apd;
		$result2=mysql_query($req2,$link);
		$req2="UPDATE val_apd SET tot='".$totvalposte."' WHERE id_apd=".$id_apd;
		$result2=mysql_query($req2,$link);
	}
}
function add_specif($idcht,$idart,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO ".$table." (id_cht,id_art) VALUES (".$idcht.",".$idart.")";
	$result2=mysql_query($req2,$link);

	/* Modification de l'APD */
	$req="SELECT apd.id FROM apd WHERE apd.id_cht=".$idcht;
	$result=mysql_query($req,$link);
	$ligne=mysql_fetch_assoc($result);
	$id_apd = $ligne["id"];
	$req="SELECT * FROM val_apd WHERE val_apd.id_apd=".$id_apd;
	$result=mysql_query($req,$link);
	$rowapd = mysql_num_rows($result);
	if ($rowapd==0){
		/* Recherche de la nature du poste */
		$req="SELECT pos.pos_nature FROM pos WHERE pos.idcht=".$idcht;
		$result=mysql_query($req,$link);
		$ligne=mysql_fetch_assoc($result);
		$nature = $ligne["pos_nature"];

		switch ($nature){
			case "PSS A":
			case "PSS B":
			case "PC 3UF":
			case "PC 4UF":
			case "PC 5UF":
				$req="SELECT articles.id FROM articles JOIN bordereau ON articles.id_bord=bordereau.id WHERE bordereau.libelle='2/ POSTE' AND articles.libelle='Pose cabine'";
				$result=mysql_query($req,$link);
				$ligne=mysql_fetch_assoc($result);
				$id_artapd = $ligne["id"];
				break;
			case "H61":
				$req="SELECT articles.id FROM articles JOIN bordereau ON articles.id_bord=bordereau.id WHERE bordereau.libelle='2/ POSTE' AND articles.libelle='Pose sur poteau'";
				$result=mysql_query($req,$link);
				$ligne=mysql_fetch_assoc($result);
				$id_artapd = $ligne["id"];
				break;
			case "PRCS":
				$req="SELECT articles.id FROM articles JOIN bordereau ON articles.id_bord=bordereau.id WHERE bordereau.libelle='2/ POSTE' AND articles.libelle='Pose sur socle'";
				$result=mysql_query($req,$link);
				$ligne=mysql_fetch_assoc($result);
				$id_artapd = $ligne["id"];
				break;
		}
		$req2="INSERT INTO val_apd (id_apd,id_art) VALUES (".$id_apd.",".$id_artapd.")";
		$result2=mysql_query($req2,$link);
	}
}
function select_specif($idbord,$idcht,$nat,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM pos_article WHERE pos_article.posarticle_idbord=".$idbord;
	$result=mysql_query($req,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='5%'></th><th>Libell�</th><th width='10%'>P.U.</th></tr></thead>";
	echo "<tbody>";
	$rowart = mysql_num_rows($result);
	if ($rowart!=0){
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo '<tr><td width="5%" class="text-center"><button data-idart="'.$ligne["posarticle_id"].'" data-table="pos_val" data-idcht="'.$idcht.'" class="btn btn-sm btn-default add_valposte"><i class="fa fa-plus-circle"></i></button></td><td>'.$ligne["posarticle_libelle"].'</td><td width="10%" class="text-center">'.$ligne["posarticle_pu"].'</td></tr>';
		}
	}else{
		echo '<tr><td class="text-center" colspan="3"><b>Aucun article</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function lst_val_poste($idcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT pos_val.posval_id,pos_article.posarticle_libelle,pos_article.posarticle_pu,pos_article.posarticle_codearticle,pos_val.posval_qte,pos_val.posval_tot,pos_bord.posbord_libelle as bord_libelle FROM pos_val JOIN pos_article ON pos_article.posarticle_id=pos_val.posval_idart JOIN pos_bord ON pos_bord.posbord_id=pos_article.posarticle_idbord WHERE pos_val.posval_idcht=".$idcht;
	$result=mysql_query($req,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='5%'></th><th>Bordereau</th><th>Code article</th><th>Libell�</th><th>Qte</th><th width='10%'>P.U.</th><th>Montant</th></tr></thead>";
	echo "<tbody>";
	$rowvalapd = mysql_num_rows($result);
	if ($rowvalapd!=0){
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo '<tr><td width="5%" class="text-center"><button data-idvalposte="'.$ligne["posval_id"].'" data-idcht="'.$idcht.'" data-table="pos_val" class="btn btn-sm btn-default del_valposte"><i class="fa fa-trash"></i></button></td><td>'.$ligne["bord_libelle"].'</td><td width="5%">'.$ligne["posarticle_codearticle"].'</td><td>'.$ligne["posarticle_libelle"].'</td><td width="10%"><input data-chp="qte" data-idvalapd="'.$ligne["id"].'" data-idapd="'.$idapd.'" class="form-control input-sm text-center update_specif" data-id="'.$ligne["posval_id"].'" data-idcht="'.$idcht.'" data-table="pos_val" data-type="" data-nat="pos" data-chp="qte" value="'.$ligne["posval_qte"].'"></td><td width="10%" class="text-center">'.number_format($ligne["posarticle_pu"], 2, ',', ' ').'</td><td width="10%"><input class="form-control input-sm text-right update_specif" data-id="'.$ligne["posval_id"].'" data-idcht="'.$idcht.'" data-table="pos_val" data-type="" data-nat="pos" data-chp="posval_tot" value="'.$ligne["posval_tot"].'" disabled></td></tr>';
		}
	}else{
		echo '<tr><td class="text-center" colspan="10"><b>Aucune valorisation</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}