<?php

/* Tables concern�es
ami
ami_prlv
*/

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(isset($_POST['action']) && !empty($_POST['action'])) {
	$action = $_POST['action'];
	switch($action) {
		case 'detailspecif' : detail_specif($_POST["idcht"],$_POST["nat"]);break;
		case 'updatespecif' : update_specif($_POST["id"],$_POST["chp"],$_POST["val"],$_POST["nat"],$_POST["table"]);break;
		case 'addspecif' : add_specif($_POST["idcht"],$_POST["table"]);break;
		case 'delinspecif' : del_inspecif($_POST["id"],$_POST["table"]);break;

		/* Bordereau */
		case 'lstarticlebpu' : lst_article_bpu($_POST["idbord"],$_POST["idcht"],$_POST["nat"]);break;
		case 'lstvalcht' : lst_val_cht($_POST["idcht"],$_POST["nat"]);break;
		case 'addvalcht' : add_valcht($_POST["idcht"],$_POST["idart"]);break;
		case 'delvalcht' : del_valcht($_POST["idvalcht"]);break;
		case 'updatevalcht' : update_valcht($_POST["idvalcht"],$_POST["chp"],$_POST["val"]);break;

		case 'blah' : blah();break;
		// ...etc...
	}
}

function detail_specif($idcht,$nat){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	/* Bordereau de prix unitaire */
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-book"></i> Bordereau de prix unitaire</div>';
	echo '<div class="panel-body" id="detail_specif_bord">';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-2" for="lstbordcht">Bordereaux:</label>';
		echo '<div class="col-sm-6">';
		echo '<select class="form-control input-sm" id="lstbordcht" name="lstbordcht" data-nat="'.$nat.'" data-idcht="'.$idcht.'">';
		echo '<option value="-1">S�lectionnez un bordereau</option>';
		$req="SELECT id,libelle,date_cloture FROM bordereau WHERE type='' AND nature LIKE '%".$nat."%'";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["date_cloture"]=="0000-00-00"){$title = $ligne["libelle"];}else{$title = $ligne["libelle"]." [CLOTURE]";}
			echo '<option value="'.$ligne["id"].'" data-idcht="'.$idcht.'">'.$title.'</option>';
		}
		echo '</select>';
		echo '</div>';
		echo '</div>';
		/* Liste des articles du bordereau selectionn� */
		echo '<div class="panel panel-default">';
		echo '<div class="panel-heading">Articles</div>';
		echo '<div class="panel-body" id="lstarticlescht" style="max-height: 200px;overflow-y: scroll;">';
			lst_article_bpu("-1",$idcht,$nat);
		echo '</div>';
		echo '</div>';
		/* Liste des articles valoris�s */
		echo '<div class="panel panel-default">';
		echo '<div class="panel-heading">Valorisation</div>';
		echo '<div class="panel-body" id="lstvalcht">';
			lst_val_cht($idcht,$nat);
		echo '</div>';
		echo '</div>';
	echo '</div>';
	echo '</div>';

	$req="SELECT * FROM ami WHERE ami_idcht=".$idcht;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$id = $ligne["ami_id"];
	}

	/* Liste des pr�l�vements */
	$req="SELECT * FROM ami_prlv WHERE amiprlv_idcht=".$idcht;
	$result=mysql_query($req,$link);
	$row = mysql_num_rows($result);
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-syringe"></i> Liste des pr�l�vements</div>';
	echo '<div class="panel-body" id="detail_specif_room">';
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='8%'><button data-tooltip='Nouveau pr�l�vement' id='add_specif' data-idcht='".$idcht."' data-table='ami_prlv' data-nat='".$nat."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th align='center' width='20%'>N� Pr�l�vement</th><th align='center' width='8%'>Pr�sence amiante</th><th align='center' width='20%'>Type d'amiante</th></tr></thead>";
	echo "<tbody>";
	if ($row!=0){
		while ($data=mysql_fetch_assoc($result))
		{
			if ($data["amiprlv_presence"]==0){$disabledlist="disabled";}else{$disabledlist="";}
			if ($data["amiprlv_presence"]==1){$checked="checked";}else{$checked="";}
			echo '<tr><td align="center" width="8%"><button data-tooltip="Supprimer" data-id="'.$data["amiprlv_id"].'" data-idcht="'.$idcht.'" data-nat="'.$nat.'" data-table="ami_prlv" class="btn btn-sm btn-default del_specif"><i class="fa fa-trash"></i></button></td>';
			echo '<td class="text-center" width="20%"><input class="form-control input-sm text-center update_specif" data-id="'.$data["amiprlv_id"].'" data-chp="amiprlv_numero" data-idcht="'.$idcht.'" data-table="ami_prlv" data-type="" data-nat="'.$nat.'" id="ami_numero" name="ami_numero" value="'.$data["amiprlv_numero"].'"></td>';
			echo '<td class="text-center" width="8%"><input type="checkbox" class="update_specif" data-id="'.$data["amiprlv_id"].'" data-chp="amiprlv_presence" data-idcht="'.$idcht.'" data-table="ami_prlv" data-type="chk" data-nat="'.$nat.'" id="ami_presence" name="ami_presence" '.$checked.'></td>';
			echo '<td>';
			echo '<select class="form-control input-sm text-right update_specif" data-id="'.$data["amiprlv_id"].'" data-chp="amiprlv_type" data-idcht="'.$idcht.'" data-table="ami_prlv" data-type="" data-nat="'.$nat.'" id="tel_montant" name="tel_montant" '.$disabledlist.'>';
			echo '<option value="">Choisir un type d\'amiante</option>';
			$reqtype="SELECT lex_id,lex_libelle FROM lexique WHERE lex_codelexique='TYPE_AMI'";
			$resulttype=mysql_query($reqtype,$link);
			while ($lignetype=mysql_fetch_assoc($resulttype))
			{
				if ($lignetype["lex_libelle"]==$data["amiprlv_type"]){$selected = "selected";}else{$selected = "";}
				echo '<option value="'.$lignetype["lex_libelle"].'" '.$selected.'>'.$lignetype["lex_libelle"].'</option>';
			}
			echo '</select>';
			echo '</td>';
			echo '</tr>';
		}
	}else{
		echo '<tr><td align="center" colspan="9"><b>Aucun pr�l�vement</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
	echo '</div>';
	echo '</div>';
}
function update_specif($id,$chp,$val,$nat,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE ".$table." SET ".$chp."='".$val."' WHERE id=".$id;
	$result2=mysql_query($req2,$link);
	if ($chp=="amiprlv_presence"){
		if ($val==0){
			$req2="UPDATE ".$table." SET type='' WHERE ".$table."_id=".$id;
			$result2=mysql_query($req2,$link);
		}
	}
}
function del_specif($idcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM ami WHERE ami_idcht=".$idcht;
	$result2=mysql_query($req2,$link);	
}
function del_inspecif($id,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="DELETE FROM ".$table." WHERE ".$table."_id=".$id;
	$result2=mysql_query($req2,$link);	
}
function add_specif($idcht,$table){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="INSERT INTO ".$table." (".$table."_idcht) VALUES (".$idcht.")";
	$result2=mysql_query($req2,$link);	
}

/* Bordereau */
function lst_article_bpu($idbord,$idcht,$nat){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM articles WHERE articles.id_bord=".$idbord;
	$result=mysql_query($req,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='5%'></th><th>Libell�</th><th width='10%'>P.U.</th></tr></thead>";
	echo "<tbody>";
	$rowart = mysql_num_rows($result);
	if ($rowart!=0){
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo '<tr><td width="5%" class="text-center"><button data-tooltip="Nouvel article" data-idarticle="'.$ligne["id"].'" data-nat="'.$nat.'" data-idcht="'.$idcht.'" class="btn btn-sm btn-default add_valcht"><i class="fa fa-plus-circle"></i></button></td><td>'.$ligne["libelle"].'</td><td width="10%" class="text-center">'.$ligne["pu"].'</td></tr>';
		}
	}else{
		echo '<tr><td class="text-center" colspan="3"><b>Aucun article</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function lst_val_cht($idcht,$nat){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT val_cht.id,articles.libelle,articles.pu,val_cht.qte,val_cht.tot FROM val_cht JOIN articles ON articles.id=val_cht.id_art WHERE val_cht.id_cht=".$idcht;
	$result=mysql_query($req,$link);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='5%'></th><th>Libell�</th><th width='10%'>Qte</th><th width='10%'>P.U.</th><th width='10%'>Montant</th></tr></thead>";
	echo "<tbody>";
	$rowvalapd = mysql_num_rows($result);
	if ($rowvalapd!=0){
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo '<tr><td width="5%" class="text-center"><button data-tooltip="Supprimer" data-idvalcht="'.$ligne["id"].'" data-nat="'.$nat.'" data-idcht="'.$idcht.'" class="btn btn-sm btn-default del_valcht"><i class="fa fa-trash"></i></button></td><td>'.$ligne["libelle"].'</td><td width="10%"><input data-chp="qte" data-idvalcht="'.$ligne["id"].'" data-nat="'.$nat.'" data-idcht="'.$idcht.'" class="form-control input-sm text-center update_valcht" value="'.$ligne["qte"].'"></td><td width="10%" class="text-center">'.number_format($ligne["pu"], 2, ',', ' ').'</td><td width="10%"><input data-chp="tot" data-idvalcht="'.$ligne["id"].'" data-nat="'.$nat.'" data-idcht="'.$idcht.'" class="form-control input-sm text-right update_valcht" value="'.$ligne["tot"].'" disabled></td></tr>';
		}
	}else{
		echo '<tr><td class="text-center" colspan="10"><b>Aucune valorisation</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";
}
function add_valcht($idcht,$idart){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="INSERT INTO val_cht (id_cht,id_art) VALUES (".$idcht.",".$idart.")";
	$result=mysql_query($req,$link);
}
function del_valcht($idvalcht){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="DELETE FROM val_cht WHERE val_cht.id=".$idvalcht;
	$result=mysql_query($req,$link);
}
function update_valcht($idvalcht,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	/* R�cup�ration du PU de l'article */
	$req="SELECT articles.pu FROM val_cht JOIN articles ON articles.id=val_cht.id_art WHERE val_cht.id=".$idvalcht;
	$result=mysql_query($req,$link);
	$row = mysql_fetch_assoc($result);
	$pu = $row["pu"];
	$tot = $val * $pu;
	$req2="UPDATE val_cht SET ".$chp."='".$val."',tot='".$tot."' WHERE id=".$idvalcht;
	$result2=mysql_query($req2,$link);	
}