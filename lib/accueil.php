<!DOCTYPE html>
<html lang="fr">
  <?php require("dashboard.php");$rowsnotif = nbrnotif_dashboard();?>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-15" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link id="page_favicon" href="../images/favicon.ico" rel="icon" type="image/x-icon" />

    <title>Travaux 2.0</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <!--<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>-->
    <link href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" rel="stylesheet">
    <link href="../css/simple-sidebar.css" rel="stylesheet">
    <link rel='stylesheet' href='../css/jquery.qtip.min.css' type='text/css'>
    <link rel='stylesheet' href='../css/bootstrap-datepicker3.min.css' type='text/css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/css/fileinput.min.css' type='text/css'>
    <link rel='stylesheet' href='../css/travaux.css' type='text/css'>
    <link rel='stylesheet' href='../css/progress-step.css' type='text/css'>
    <link rel='stylesheet' href='../css/sweetalert.css' type='text/css'>
    <link rel='stylesheet' href='../css/tooltip.css' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Travaux 2.0</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-collapse">
       <!--<ul class="nav navbar-nav">
        <!--<li><a href="#" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
        <form class="navbar-form navbar-left" role="search">
          <div class="input-group">
            <select class="form-control" placeholder="Communes" id="lstcom"></select>
            <span class="input-group-btn"><button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
        </form>
      </ul>-->
      <ul class="nav navbar-nav navbar-right">
        <!--<li><a href="#"><span class="fa-layers fa-fw fa-2x"><i class="fa fa-comment"></i><span class="fa-layers-counter fa-2x" style="background:Tomato">0</span></span></a></li>-->
        <li><a href="#" id="btnbar_notif"><span class="fa-layers fa-fw fa-lg"><i class="fa fa-bell"></i></span></a></li>
        <li><a href="#" id="btnbar_task"><span class="fa-layers fa-fw fa-lg"><i class="fa fa-tasks"></i></span></a></li>
        <li class="dropdown">
          <a href="#" id="identite" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#" id="settings"><span class="fa fa-cogs"></span> Pr�f�rences</a></li>
            <li class="divider"></li>
            <li><a href="#" class="quitter"><span class="fa fa-power-off"></span> Se d�connecter</a></li>
          </ul>
        </li>
      </ul>      
      <ul class="nav navbar-nav sider-navbar" id="wrapper">
        <li id="profile">
          <figure class="profile-userpic">
            <!--<img src="https://www.notesmate.in/images/default-user.png" class="img-responsive" alt="Profile Picture">-->
            <img src="../images/user2.png" class="img-responsive" alt="Profil utilisateur">
          </figure>
          <div class="profile-usertitle">
            <div class="profile-usertitle-name"></div>
            <div class="profile-usertitle-title">Charg� d'affaire</div>
          </div>
        </li>
        <li class="sider-menu">
          <ul>
            <li class="active"><a href="#" id="menu_dashboard"><span class="fa fa-fw fa-tachometer-alt"></span> Tableau de bord</a></li>
            <li><a href="#" id="menu_projet"><span class="fa fa-cogs"></span> Projets</a></li>
            <li><a href="#" id="menu_chantier"><span class="fa fa-bolt"></span> Travaux</a></li>
            <li><a href="#" id="menu_instruction"><span class="fas fa-plug"></span> Instructions</a></li>
            <li><a href="#" id="menu_maintenance"><span class="fa fa-wrench"></span> Maintenance</a></li>
            <li><a href="#" id="menu_marche"><span class="fas fa-calculator"></span> March�s</a></li>
            <li><a href="#" id="menu_facturier"><span class="fas fa-file-invoice-dollar"></span> Facturier</a></li>
            <li><a href="#" id="menu_subvention"><span class="far fa-money-bill-alt"></span> Subventions</a></li>
            <li><a href="#" id="menu_requete"><span class="fas fa-database"></span> Requ�te</a></li>
            <li><a href="#" id="menu_print"><span class="fa fa-print"></span> Impression</a></li>
            <li><a href="#" class="quitter"><span class="fa fa-power-off"></span> Quitter</a></li>
          </ul>
       </li>
      </ul>
    </div>
  </div>
</nav>
<section id="page-keeper">
  <div class="container-fluid">
    <div class="row" id="contentpage">
    </div>
  </div>
</section>
 
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src='http://code.jquery.com/ui/1.12.0/jquery-ui.min.js'></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js"></script>
    <script src='../js/jquery.qtip.min.js'></script>
    <script src='../js/cookies.js'></script>
    <script src='../js/filter.js'></script>
    <script src='../js/bootstrap-datepicker.min.js'></script>
    <script src='../js/bootstrap-datepicker.fr.min.js'></script>
    <script src='../js/bootbox.min.js'></script>
    <script src='../js/highcharts.js'></script>
    <script src='../js/jquery.highchartTable.js'></script>
    <script src='../js/jquery.maskedinput.js'></script>
    <script src='../js/sweetalert.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.5/js/fileinput.min.js'></script>
    <script src='../js/fileinput_locale_fr.js'></script>
    <script src='../js/push_notification.min.js'></script>
    <script src='../js/validation.js'></script>
    <script src='../js/jquery.table2excel.js'></script>
    <script src='../js/travaux.js'></script>
    <script src='../js/chantier.js'></script>
    <script src='../js/projet.js'></script>
    <script src='../js/facturier.js'></script>
    <script src='../js/instruction.js'></script>
    <script src='../js/marche.js'></script>
    <script src='../js/print.js'></script>
    <script src='../js/requete.js'></script>
  </body>
</html>
