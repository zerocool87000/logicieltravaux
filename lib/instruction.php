<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

if(isset($_POST['action']) && !empty($_POST['action'])) {
	$action = $_POST['action'];
	switch($action) {
		case 'verifaccesinstruction' : verifacces_instruction();break;
		case 'verifdemanderaccgeo' : verif_demanderaccgeo();break;
		case 'demraccgeotoinst' : demraccgeotoinst($_POST["id"]);break;
		case 'lstinstruction' : lst_instruction($_POST["page"],$_POST["login"],$_POST["chp"],$_POST["val"],$_POST["criteres"],$_POST["tabcrit"]);break;
		case 'lstdemanderaccgeo' : lst_demanderaccgeo();break;
		case 'addinstruction' : add_instruction();break;
		case 'delinstruction' : del_instruction($_POST["idinst"]);break;
		case 'updateinstruction' : update_instruction($_POST["idinst"],$_POST["chp"],$_POST["val"]);break;
		case 'addinstparcelle' : add_instparcelle($_POST["idinst"]);break;
		case 'delinstparcelle' : del_instparcelle($_POST["id"]);break;
		case 'updateinstparcelle' : update_instparcelle($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		case 'addinstbeneficiaire' : add_instbeneficiaire($_POST["idinst"]);break;
		case 'delinstbeneficiaire' : del_instbeneficiaire($_POST["id"]);break;
		case 'updateinstbeneficiaire' : update_instbeneficiaire($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		case 'lstcollectivite' : lst_collectivite();break;
		case 'lstentreprise' : lst_entreprise();break;
		case 'lstcategorie' : lst_categorie();break;
		case 'lstnature' : lst_nature();break;
		case 'lsthistory' : lst_history($_POST["idope"]);break;
		case 'detailinstruction' : detail_instruction($_POST["id"]);break;
		case 'detailinstlocalisation' : detailinst_localisation($_POST["idope"],$_POST["insee"],$_POST["entite"]);break;
		case 'detailgeneinstruction' : detailgene_instruction($_POST["idinst"]);break;
		case 'detailurbanisme' : detail_urbanisme($_POST["idinst"]);break;
		case 'detailbeneficiaire' : detail_beneficiaire($_POST["idinst"]);break;
		case 'detailestimation' : detail_estimation($_POST["idinst"]);break;
		case 'updateinstaps' : update_instaps($_POST["id"],$_POST["chp"],$_POST["val"]);break;
		case 'blah' : blah();break;
		// ...etc...
	}
}

function verifacces_instruction(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT user_instructeur FROM user WHERE user_login='".$_COOKIE["login"]."'";
	$result2=mysql_query($req2,$link);
	while ($ligne2=mysql_fetch_assoc($result2))
	{
		if ($ligne2["user_instructeur"]==1){echo "Ok";}else{echo "erreur";}
	}	
}
function verif_demanderaccgeo(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT * FROM demande_raccgeo WHERE demraccgeo_idinst=0";
	$result2=mysql_query($req2,$link);
	$nbraccgeo=mysql_num_rows($result2);
	if ($nbraccgeo!=0){
		echo '<div class="alert alert-info text-center"><strong><i class="fas fa-exclamation-triangle"></i> Info!</strong> '.$nbraccgeo.' nouvelle(s) demande(s) de raccordement en attentes.</div>';
	}
}
function demraccgeotoinst($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);

	$req2="SELECT inst_numero FROM instructions ORDER BY inst_numero DESC LIMIT 1";
	$result2=mysql_query($req2,$link);
	while ($ligneinst=mysql_fetch_assoc($result2))
	{
		$tabnuminst = explode("-",$ligneinst["inst_numero"]);
	}
	$newref = date("y")."-INS-";
	if ($tabnuminst[0]==date("y")){
		$compteur = $tabnuminst[2]+1;
	}else{
		$compteur = 1;
	}
	$compt = strlen($compteur);
	$diff = 4 - $compt;
	for ($i=0;$i<$diff;$i++){
		$newref .= "0";
	}
	$newref .= $compteur;
	/* Insert l'op�ration */
	$req2="SELECT * FROM demande_raccgeo WHERE demraccgeo_id=".$id;
	$result2=mysql_query($req2,$link);
	$lignedemraccgeo=mysql_fetch_assoc($result2);
	$section = substr($lignedemraccgeo["demraccgeo_secnumproj"],0,2);
	$parcelle = substr($lignedemraccgeo["demraccgeo_secnumproj"],-4);
	$codeinsee = $lignedemraccgeo["demraccgeo_codeinsee"];
	$datefe = $lignedemraccgeo["demraccgeo_datefe"];
	$type_urba = $lignedemraccgeo["demraccgeo_typedu"];
	$ref_urba = $lignedemraccgeo["demraccgeo_numdu"];
	$nom = $lignedemraccgeo["demraccgeo_nompetition"]." ".$lignedemraccgeo["demraccgeo_prenompetition"];
	$adresse = $lignedemraccgeo["demraccgeo_adressepetition"];
	$codepostal = $lignedemraccgeo["demraccgeo_cppetition"];
	$ville = $lignedemraccgeo["demraccgeo_villepetition"];
	$mail = $lignedemraccgeo["demraccgeo_mailpetition"];
	$req2="INSERT INTO instructions (inst_numero,inst_datecreate,inst_datedemande,inst_datereponse,inst_entite,inst_codeinsee,inst_categorie,inst_chargeaffaire,inst_libelle,inst_typeurbanisme,inst_refurbanisme) VALUES ('".$newref."',NOW(),'".$datefe."',NOW(),'COMMUNE',".$codeinsee.",'Instruction','".$_COOKIE["login"]."','Demande G�oSeHV','".$type_urba."','".$ref_urba."')";
	$result2=mysql_query($req2,$link);
	$idinst = mysql_insert_id($link);
	$req2="INSERT INTO inst_urbanisme (insturba_idinst,insturba_numerosection,insturba_numeroparcelle) VALUES ('".$idinst."','".$section."','".$parcelle."')";
	$result2=mysql_query($req2,$link);
	$req2="INSERT INTO inst_beneficiaire (instbenef_idinst,instbenef_nom,instbenef_adresse,instbenef_codepostal,instbenef_ville,instbenef_mail) VALUES ('".$idinst."','".$nom."','".$adresse."','".$codepostal."','".$ville."','".$mail."')";
	$result2=mysql_query($req2,$link);
	$req2="INSERT INTO inst_aps (instaps_idinst) VALUES ('".$idinst."')";
	$result2=mysql_query($req2,$link);
	$req2="UPDATE demande_raccgeo SET demraccgeo_idinst='".$idinst."' WHERE demraccgeo_id=".$id;
	$result2=mysql_query($req2,$link);
	$req2="INSERT INTO historique (hist_idocc,hist_dateevent,hist_libelleevent,hist_user,hist_event,hist_natureevent) VALUES ('".$idinst."',NOW(),'Cr�ation instruction','".$_COOKIE["login"]."','create','INS')";
	$result2=mysql_query($req2,$link);
}
function update_instruction($idinst,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	switch ($chp){
		case "inst_datedemande":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "inst_datereponse":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "inst_dateretourdevissigne":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "inst_dateemissiontitre":
			$valeur = explode("/",$val);
			$val = $valeur[2]."-".$valeur[1]."-".$valeur[0];
			break;
		case "inst_entite":
			$req2="UPDATE instructions SET inst_codeepci=0, inst_codeinsee='' WHERE id=".$idinst;
			$result2=mysql_query($req2,$link);
			break;
	}
	$req2="UPDATE instructions SET ".$chp."='".utf8_decode($val)."' WHERE inst_id=".$idinst;
	$result2=mysql_query($req2,$link);
	calcul_forfait($idinst,"epu");
}
function lst_collectivite(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT coll_nom, coll_codeinsee, coll_type FROM collectivites WHERE coll_type in ('COMMUNE','EPCI') ORDER BY coll_type,coll_nom ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une collectivit�</option>';
	if ($row2!=0)
	{
		$c=0;
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			if ($c==0){echo '<option class="lstcomoptionfilter" disabled><b>-------- '.$ligne2["coll_type"].' --------</b></option>';$type_coll=$ligne2["coll_type"];}
			if ($type_coll!=$ligne2["coll_type"]){echo '<option class="lstcomoptionfilter" disabled><b>-------- '.$ligne2["coll_type"].' --------</b></option>';$type_coll=$ligne2["coll_type"];}
			echo '<option value="'.$ligne2["coll_codeinsee"].'">'.$ligne2["coll_nom"].'</option>';
			$c++;
		}
	}
}
function lst_entreprise(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT ent_nom, ent_id FROM entreprises ORDER BY ent_nom ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une entrepise</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["ent_id"].'">'.$ligne2["ent_nom"].'</option>';
		}
	}
}
function lst_categorie(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT cat_libelle, cat_id FROM categorie ORDER BY cat_libelle ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une cat�gorie</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["cat_id"].'">'.$ligne2["cat_libelle"].'</option>';
		}
	}
}
function lst_nature(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT nat_libelle, nat_id FROM nature ORDER BY nat_libelle ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo '<option value="0">Choisir une nature</option>';
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<option value="'.$ligne2["nat_id"].'">'.$ligne2["nat_libelle"].'</option>';
		}
	}
}

function lst_demanderaccgeo(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT collectivites.coll_nom,demande_raccgeo.demraccgeo_secnumproj,demande_raccgeo.demraccgeo_datefe,demande_raccgeo.demraccgeo_id FROM demande_raccgeo JOIN collectivites ON collectivites.coll_codeinsee=demande_raccgeo.demraccgeo_codeinsee WHERE demande_raccgeo.demraccgeo_idinst=0 ORDER BY demande_raccgeo.demraccgeo_datefe ASC";
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th class='text-center' width='10%'></th><th class='text-center'>Communes</th><th class='text-center'>Section/Num�ro</th><th class='text-center'>Date</th></tr></thead>";
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			echo '<tr><td class="text-center"><button data-id="'.$ligne2["demraccgeo_id"].'" data-tooltip="Traiter" class="btn btn-default btn-sm demraccgeotoinst"><i class="fa fa-cog"></i></button></td><td class="text-center">'.$ligne2["coll_nom"].'</td><td class="text-center">'.$ligne2["demraccgeo_secnumproj"].'</td><td class="text-center">'.strftime("%d/%m/%Y",strtotime($ligne2["demraccgeo_datefe"])).'</td></tr>';
		}
	}else{
		echo '<tr><td class="text-center" colspan="4"><b>Aucune demande</b></td></tr>';
	}
	echo "</tbody>";
	echo "</table>";	
}
function lst_instruction($page,$loginca,$chp,$val,$criteres,$tabcrit){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	
	$messagesParPage=10;
	if ($criteres!=""){
		$retour_total=mysql_query('SELECT COUNT(*) AS total FROM instructions where '.stripslashes(utf8_decode($criteres)));
		//$retour_total=mysql_query('SELECT COUNT(*) AS total FROM instructions where '.$chp.' like "%'.$val.'%"');
		$donnees_total=mysql_fetch_assoc($retour_total);
	}else{
		$retour_total=mysql_query('SELECT COUNT(*) AS total FROM instructions where instructions.inst_chargeaffaire="'.$loginca.'"');
		$donnees_total=mysql_fetch_assoc($retour_total);
	}
	$total=$donnees_total['total'];
	$nombreDePages=ceil($total/$messagesParPage);
	if(isset($page))
	{
		$pageActuelle=intval($page);

		if($pageActuelle>$nombreDePages)
		{
			$pageActuelle=$nombreDePages;
		}
	}else{
		$pageActuelle=1;
	}

	$premiereEntree=($pageActuelle-1)*$messagesParPage;

	if ($criteres!=""){
		$delimiter = ",";
		$tab_crit = explode($delimiter,$tabcrit);
		for ($i=0;$i<count($tab_crit);$i++){
			$tabtemp = explode(":",$tab_crit[$i]);
			switch($tabtemp[0]){
				case "inst_numero":
					$numero = $tabtemp[1];
					break;
				case "inst_typeurbanisme":
					$type_urbanisme = $tabtemp[1];
					break;
				case "inst_refurbanisme":
					$ref_urbanisme = $tabtemp[1];
					break;
				case "inst_codeinsee":
					$codeinsee = $tabtemp[1];
					break;
			}
		}
		$req2="SELECT instructions.inst_numero, instructions.inst_libelle, instructions.inst_categorie, instructions.inst_id, instructions.inst_typeurbanisme, instructions.inst_refurbanisme, collectivites.coll_nom FROM instructions LEFT JOIN collectivites ON collectivites.coll_codeinsee=instructions.inst_codeinsee WHERE ".stripslashes(utf8_decode($criteres))." ORDER BY instructions.inst_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
		//$req2="SELECT instructions.inst_numero, instructions.inst_libelle, instructions.inst_categorie, instructions.inst_id, collectivites.coll_nom FROM instructions LEFT JOIN collectivites ON collectivites.coll_codeinsee=instructions.inst_codeinsee WHERE ".$chp." LIKE '%".$val."%' ORDER BY operations.ope_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
	}else{
		$req2="SELECT instructions.inst_numero, instructions.inst_libelle, instructions.inst_categorie, instructions.inst_id, instructions.inst_typeurbanisme, instructions.inst_refurbanisme, collectivites.coll_nom FROM instructions LEFT JOIN collectivites ON collectivites.coll_codeinsee=instructions.inst_codeinsee WHERE instructions.inst_chargeaffaire='".$loginca."' ORDER BY instructions.inst_datecreate DESC limit ".$premiereEntree.",".$messagesParPage." ";
	}
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	$compteope = 1;
	if ($pageActuelle>1){
		echo "<div><div data-tooltip='Pr�c�dent' class='forward btnpager'><i class='fa fa-caret-left' aria-hidden='true'></i></div>";
	}
	echo "<div class='boxpager'>".$pageActuelle." / ".$nombreDePages."</div>";
	if ($pageActuelle<$nombreDePages){
		echo "<div data-tooltip='Suivant' class='next btnpager'><i class='fa fa-caret-right' aria-hidden='true'></i></div></div>";
	}
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th width='10%'></th><th class='text-center' width='10%'>Num�ro</th><th class='text-center' width='10%'>Type</th><th class='text-center'>R�f�rence</th><th class='text-center'>Sections/parcelles</th><th class='text-center' width='20%'>Commune</th></tr></thead>";
	echo "<tbody><tr><td width='10%'></td><td class='text-center' width='10%'><input data-chp='inst_numero' class='form-control input-sm text-center chpinst_filter' value='".$numero."'></td>";
	echo "<td class='text-center' width='10%'><select data-chp='inst_typeurbanisme' class='form-control input-sm chpinst_filter'><option value=''>-- Type --</option>";
	$req="SELECT * FROM lexique WHERE lex_codelexique='TYPE_URBA'";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["lex_libelle"]==$type_urbanisme){$selected="selected";}else{$selected="";}
		echo '<option value="'.$ligne["lex_libelle"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
	}		
	echo "</select></td>";
	echo "<td class='text-center' width='10%'><input data-chp='inst_refurbanisme' class='form-control input-sm text-center chpinst_filter' value='".$ref_urbanisme."'></td>";
	echo "<td></td>";
	echo "<td><select data-chp='inst_codeinsee' class='form-control input-sm chpinst_filter'><option value=''>-- Collectivit�s --</option>";
	$reqcat="SELECT coll_nom, coll_codeinsee FROM collectivites WHERE coll_type='COMMUNE'";
	$resultcat=mysql_query($reqcat,$link);
	while ($lignecat=mysql_fetch_assoc($resultcat))
	{
		if ($codeinsee==$lignecat["coll_codeinsee"]){$selected="selected";}else{$selected="";}
		echo "<option value='".$lignecat["coll_codeinsee"]."' ".$selected.">".$lignecat["coll_nom"]."</option>";
	}
	echo "</select></td>";
	echo "</tr></tbody>";
	echo "<tbody>";
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{
			$sectionparelle = "";
			$req="SELECT * FROM inst_urbanisme WHERE insturba_idinst=".$ligne2["inst_id"];
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				$sectionparelle .= ' <i class="fas fa-home"></i> '.$ligne["insturba_numerosection"]." ".$ligne["insturba_numeroparcelle"];
			}
			echo "<tr><td align='center' width='10%'><button data-tooltip='Visualiser' data-id='".$ligne2["inst_id"]."' class='btn btn-sm btn-default visuinst'><i class='fa fa-eye'></i></button> <button data-tooltip='Supprimer' data-id='".$ligne2["inst_id"]."' class='btn btn-sm btn-default delinst'><i class='fa fa-trash'></i></button></td><td align='center' width='10%'>".$ligne2["inst_numero"]."</td><td align='center' width='10%'>".$ligne2["inst_typeurbanisme"]."</td><td align='center' width='10%'>".$ligne2["inst_refurbanisme"]."</td><td align='center'>".$sectionparelle."</td><td width='20%'>".$ligne2["coll_nom"]."</td></tr>";
		}
	}else{
		echo "<tr><td colspan='6' align='center'><b>Aucune instruction</b></td></tr>";
	}
	echo "</tbody>";
	echo "</table>";
}

function detail_instruction($id){
	require("./compte.php");
	require("./print.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="SELECT inst_numero, inst_libelle, inst_categorie, inst_id, inst_typeurbanisme,inst_dateretourdevissigne FROM instructions WHERE inst_id=".$id;
	$result2=mysql_query($req2,$link);
	$row2=mysql_num_rows($result2);
	if ($row2!=0)
	{
		while ($ligne2=mysql_fetch_assoc($result2))
		{

			$reqcat="SELECT cat_libelle, cat_codenumero, cat_AUT FROM categorie WHERE cat_libelle='".$ligne2["inst_categorie"]."'";
			$resultcat=mysql_query($reqcat,$link);
			while ($lignecat=mysql_fetch_assoc($resultcat))
			{
				$aut_cat = $lignecat["cat_AUT"];
			}
			$panel=1;
			echo "<div class='col-md-12'>";
			echo "<div class='portlet'><div class='portlet-title'><i class='fas fa-plug' aria-hidden='true'></i> <b>Instruction</b> ".$ligne2["inst_numero"]." - ".$ligne2["inst_libelle"]."</div><div class='portlet-content' id='detinstruction'>";
			echo "<div class='row'>";

			echo '<div class="col-sm-12">';
			echo "<button id='back_lstinstructions' class='btn btn-default btn-sm' data-idinst='".$ligne2["inst_id"]."'><i class='fa fa-reply'></i> Instructions</button>";
			if ($ligne2["inst_dateretourdevissigne"]!="0000-00-00") {
				echo "<button id='btn_raccmagic' class='btn btn-default btn-sm pull-right' data-idinst='".$ligne2["inst_id"]."'><i class='fa fa-magic'></i> Raccordement</button>";
			}
			echo '</div><br><br>';

			echo '<div class="col-sm-12">';
			echo '<div class="panel-group" id="accordion">';
			/* G�n�ralit�s */
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h2 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-file" aria-hidden="true"></i> G�n�ralit�s</a>';
			echo '</h2>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detailgene_instruction">';
				detailgene_instruction($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Urbanisme */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fas fa-road" aria-hidden="true"></i> Urbanisme</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="deturbanisme">';
				detail_urbanisme($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* B�n�ficiaires */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-users" aria-hidden="true"></i> B�n�ficiaires</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detbeneficiaire">';
				detail_beneficiaire($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Chiffrage / Estimations */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fas fa-calculator" aria-hidden="true"></i> Chiffrage / Estimations</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detestimation">';
				detail_estimation($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Documents */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="far fa-file-alt" aria-hidden="true"></i> Documents</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="detdocoperation">';
				lstreportope($id);
				lst_print_contextuel("Travaux/INST",$id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			/* Historique */
			$panel++;
			echo '<div class="panel panel-default">';
			echo '<div class="panel-heading">';
			echo '<h4 class="panel-title">';
			echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$panel.'"><i class="fa fa-history" aria-hidden="true"></i> Historique</a>';
			echo '</h4>';
			echo '</div>';
			echo '<div id="collapse'.$panel.'" class="panel-collapse collapse">';
			echo '<div class="panel-body" id="lsthistory">';
				lst_history($id);
			echo '</div>';
			echo '</div>';
			echo '</div>';
			
			echo '</div>';
			echo '</div>';
		}
	}
}

function lstreportope($id){
	echo '<div class="col-sm-8">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-file"></i> Fichiers</div>';
	echo '<div class="panel-body" id="detailope_document">';
		echo "<table class='table table-responsive table-bordered table-striped'>";
		echo "<thead><tr><th></th><th>Nom du fichier</th><th>Action(s)</th></tr></thead>";
		echo "<tbody>";
		$root = scandir("../report/INST/");
		foreach($root as $value)
		{
			if($value === '.' || $value === '..') {continue;}
			echo '<tr><td width="5%" class="text-center"><i class="fa fa-file"></i></td><td>'.$value.'</td><td width="10%" class="text-center"><button class="btn btn-sm btn-default visurpt" data-type="OPE" data-file="'.$value.'" data-id="'.$id.'"><i class="fa fa-eye"></i></button></td></tr>';
		}
		echo "</tbody>";
		echo "</table>";
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-4">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fa fa-upload"></i> Nouveaux documents</div>';
	echo '<div class="panel-body">';

	echo '</div>';
	echo '</div>';
	echo '</div>';
}

function lst_history($idope){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$reqhist="SELECT hist_dateevent, hist_libelleevent, hist_event, user_nom, user_prenom FROM historique LEFT JOIN user ON historique.hist_user=user.user_login WHERE hist_idocc=".$idope." AND hist_natureevent='INS' ORDER BY hist_dateevent DESC";
	$resulthist=mysql_query($reqhist,$link);
	$rowhist=mysql_num_rows($resulthist);
	if ($rowhist!=0)
	{
		echo "<div class='col-sm-12'>";
		while ($lignehist=mysql_fetch_assoc($resulthist))
		{
			switch($lignehist["hist_event"]){
				case "create":
					$icon = "magic";
					break;
				case "delete":
					$icon = "trash";
					break;
			}
			//echo "<div class='row'><i class='fa fa-".$icon."'></i> ".$lignehist["hist_libelleevent"]." le ".strftime("%d/%m/%Y %T",strtotime($lignehist["hist_dateevent"]))." par ".$lignehist["hist_user"]."</div>";
			echo '<div class="media"><div class="media-left"><a href="#"><img src="../images/cogs_hist.png" class="media-object img-rounded" alt="Sample Image"></a></div>';
			echo '<div class="media-body"><h5 class="media-heading"><b>'.$lignehist["user_nom"].' '.$lignehist["user_prenom"].' </b><small><i>Effectu� le '.strftime("%d/%m/%Y %T",strtotime($lignehist["hist_dateevent"])).'</i></small></h5>';
			echo '<p><i class="fa fa-'.$icon.'"></i> '.$lignehist["hist_libelleevent"].'</p>';
			echo '</div></div>';
		}
		echo "</div>";
	}
}

function detailgene_instruction($idinst){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM instructions WHERE inst_id=".$idinst;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$datecreate = strftime("%d/%m/%Y",strtotime($ligne["inst_datecreate"]));
		$datedemande = strftime("%d/%m/%Y",strtotime($ligne["inst_datedemande"]));
		$datereponse = strftime("%d/%m/%Y",strtotime($ligne["inst_datereponse"]));
		$dateretourdevissigne = strftime("%d/%m/%Y",strtotime($ligne["inst_dateretourdevissigne"]));
		$dateemissiontitre = strftime("%d/%m/%Y",strtotime($ligne["inst_dateemissiontitre"]));
		$entite = $ligne["inst_entite"];
		$charge_affaire = $ligne["inst_chargeaffaire"];
		$epci = $ligne["inst_codeepci"];
		$insee = $ligne["inst_codeinsee"];
		$numero = $ligne["inst_numero"];
		$libelle = $ligne["inst_libelle"];
		$lieudit = $ligne["inst_lieudit"];
		$categorie = $ligne["inst_categorie"];
	}

	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">G�n�ralit�s</div>';
	echo '<div class="panel-body">';
		echo '<form>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="inst_datecreate">Date cr�ation:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="inst_datecreate" name="inst_datecreate" value="'.$datecreate.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="inst_datedemande">Date demande:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm date modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_datedemande" id="inst_datedemande" name="inst_datedemande" value="'.$datedemande.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="inst_datereponse">Date r�ponse:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm date modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_datereponse" id="inst_datereponse" name="inst_datereponse" value="'.$datereponse.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="inst_dateretourdevissigne">Date retour devis sign�:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm date modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_dateretourdevissigne" id="inst_dateretourdevissigne" name="inst_dateretourdevissigne" value="'.$dateretourdevissigne.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="inst_dateemissiontitre">Date �mission du titre:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm date modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_dateemissiontitre" id="inst_dateemissiontitre" name="inst_dateemissiontitre" value="'.$dateemissiontitre.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="inst_ca">Instructeur:</label>';
		echo '<div class="col-sm-7">';
		echo '<select class="form-control input-sm modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_chargeaffaire" id="inst_ca" name="inst_ca">';
		echo '<option value="-1">S�lectionnez un instructeur</option>';
		$req="SELECT user_login,user_nom,user_prenom FROM user WHERE user_instructeur=1";
		$result=mysql_query($req,$link);
		while ($ligne=mysql_fetch_assoc($result))
		{
			if ($ligne["user_login"]==$charge_affaire){$selected = "selected";}else{$selected = "";}
			echo '<option value="'.$ligne["user_login"].'" '.$selected.'>'.$ligne["user_nom"].' '.$ligne["user_prenom"].'</option>';
		}
		echo '</select>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="inst_categorie">Cat�gorie:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="inst_categorie" name="inst_categorie" value="'.$categorie.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="inst_refsehv">R�f�rence SEHV:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm" id="inst_refsehv" name="inst_refsehv" value="'.$numero.'" disabled>';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="inst_libelle">Libell�:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_libelle" id="inst_libelle" name="inst_libelle" value="'.$libelle.'" >';
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group row">';
		echo '<label class="control-label col-sm-5" for="inst_lieudit">Lieu-dit:</label>';
		echo '<div class="col-sm-7">';
		echo '<input type="text" class="form-control input-sm modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_lieudit" id="inst_lieudit" name="inst_lieudit" value="'.$lieudit.'" >';
		echo '</div>';
		echo '</div>';
		echo '</form>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-map-marker-alt"></i> Localisation</div>';
	echo '<div class="panel-body" id="detailope_localisation">';
		detailinst_localisation($idinst,$insee,$entite,$epci);
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function detailinst_localisation($idinst,$insee,$entite,$epci){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="inst_entite">Origine demande:</label>';
	echo '<div class="col-sm-7">';
	echo '<select class="form-control input-sm modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_entite" id="inst_entite" name="inst_entite">';
	echo '<option value="-1">S�lectionnez une entit�</option>';
	if ($entite=="EPCI"){echo '<option value="EPCI" selected>EPCI</option>';}else{echo '<option value="EPCI">EPCI</option>';}
	if ($entite=="COMMUNE"){echo '<option value="COMMUNE" selected>Commune</option>';}else{echo '<option value="COMMUNE">Commune</option>';}
	echo '</select>';
	echo '</div>';
	echo '</div>';
	switch ($entite){
		case "COMMUNE":
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="inst_commune">Commune:</label>';
			echo '<div class="col-sm-7">';
			echo '<select class="form-control input-sm modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_codeinsee" id="inst_commune" name="inst_commune">';
			echo '<option value="-1">S�lectionnez une commune</option>';
			if ($epci!=0){
				$req="SELECT * FROM collectivites WHERE coll_idpere=".$epci;
			}else{
				$req="SELECT * FROM collectivites WHERE coll_type='COMMUNE'";
			}
			$result=mysql_query($req,$link);
			$code_epci="";
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["coll_codeinsee"]==$insee){$selected="selected";$code_epci=$ligne["coll_idpere"];}else{$selected="";}
				echo '<option value="'.$ligne["coll_codeinsee"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="inst_epci">EPCI:</label>';
			echo '<div class="col-sm-7">';
			if ($entite!="EPCI"){$disabled="disabled";}else{$disabled="";}
			echo '<select class="form-control input-sm modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_codeepci" id="inst_epci" name="inst_epci" '.$disabled.'>';
			echo '<option value="-1">S�lectionnez un EPCI</option>';
			$req="SELECT * FROM collectivites WHERE coll_type='EPCI'";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($epci!=0){
					if ($ligne["coll_id"]==$epci){$selected="selected";}else{$selected="";}
				}else{
					if ($ligne["coll_id"]==$code_epci){$selected="selected";}else{$selected="";}
				}
				echo '<option value="'.$ligne["coll_id"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
		break;
		case "EPCI":
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="inst_epci">EPCI:</label>';
			echo '<div class="col-sm-7">';
			if ($entite!="EPCI"){$disabled="disabled";}else{$disabled="";}
			echo '<select class="form-control input-sm modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_codeepci" id="inst_epci" name="inst_epci" '.$disabled.'>';
			echo '<option value="-1">S�lectionnez un EPCI</option>';
			$req="SELECT * FROM collectivites WHERE coll_type='EPCI'";
			$result=mysql_query($req,$link);
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($epci!=0){
					if ($ligne["coll_id"]==$epci){$selected="selected";}else{$selected="";}
				}else{
					if ($ligne["coll_id"]==$code_epci){$selected="selected";}else{$selected="";}
				}
				echo '<option value="'.$ligne["coll_id"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
			echo '<div class="form-group row">';
			echo '<label class="control-label col-sm-4" for="inst_commune">Commune:</label>';
			echo '<div class="col-sm-7">';
			echo '<select class="form-control input-sm modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_codeinsee" id="inst_commune" name="inst_commune">';
			echo '<option value="-1">S�lectionnez une commune</option>';
			if ($epci!=0){
				$req="SELECT * FROM collectivites WHERE coll_idpere=".$epci;
			}else{
				$req="SELECT * FROM collectivites WHERE coll_type='COMMUNE'";
			}
			$result=mysql_query($req,$link);
			$code_epci="";
			while ($ligne=mysql_fetch_assoc($result))
			{
				if ($ligne["coll_codeinsee"]==$insee){$selected="selected";$code_epci=$ligne["coll_idpere"];}else{$selected="";}
				echo '<option value="'.$ligne["coll_codeinsee"].'" '.$selected.'>'.$ligne["coll_nom"].' ('.$ligne["coll_codeinsee"].')</option>';
			}
			echo '</select>';
			echo '</div>';
			echo '</div>';
		break;
	}
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="email">STE:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="email" name="email" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="ope_insee">Code insee:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="ope_insee" name="ope_insee" value="'.$insee.'" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="email">Code postal:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="email" name="email" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="email">Statut collectivit� (U/R):</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="email" name="email" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="email">Eligibilit� FACE:</label>';
	echo '<div class="col-sm-7">';
	echo '<input type="text" class="form-control input-sm" id="email" name="email" disabled>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="email">Adh�rente ECL:</label>';
	echo '<div class="col-sm-2">';
	echo '<input type="text" class="form-control input-sm" id="email" name="email" disabled>';
	echo '</div>';
	echo '<label class="control-label col-sm-3" for="email">Adh�rente ESP:</label>';
	echo '<div class="col-sm-2">';
	echo '<input type="text" class="form-control input-sm" id="email" name="email" disabled>';
	echo '</div>';
	echo '</div>';
	echo '</form>';
}

function detail_urbanisme($idinst){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM instructions WHERE inst_id=".$idinst;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$type_urbanisme = $ligne["inst_typeurbanisme"];
		$ref_urbanisme = $ligne["inst_refurbanisme"];
		$obs_urbanisme = $ligne["inst_obs"];
	}
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="inst_refurbanisme">R�f�rence urbanisme:</label>';
	echo '<div class="col-md-2 col-sm-3">';
	echo '<select class="form-control input-sm modifchpinstruction"  data-idinst="'.$idinst.'" data-chp="inst_typeurbanisme" id="inst_typeurbanisme" name="inst_typeurbanisme">';
	echo '<option value="0">Choisir un type</option>';
	$req="SELECT * FROM lexique WHERE lex_codelexique='TYPE_URBA'";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["lex_libelle"]==$type_urbanisme){$selected="selected";}else{$selected="";}
		echo '<option value="'.$ligne["lex_libelle"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
	}		
	echo '</select>';
	echo '</div>';
	echo '<div class="col-sm-4">';
	echo '<input type="text" class="form-control input-sm modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_refurbanisme" id="inst_refurbanisme" name="inst_refurbanisme" value="'.$ref_urbanisme.'" >';
	echo '</div>';
	echo '</div>';
	echo '</form>';
	/* Parcelles de l'instruction */
	echo '<div class="col-md-6 col-sm-12" style="margin-bottom:20px;">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">Parcelles</div>';
	echo '<div class="panel-body">';
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th width='10%' class='text-center'><button data-tooltip='Ajouter une parcelle' id='add_instparc' data-idinst='".$idinst."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th class='text-center' width='40%'>Section</th><th  width='40%' class='text-center'>Parcelle</th></tr></thead>";
	echo "<tbody>";
	$req="SELECT * FROM inst_urbanisme WHERE insturba_idinst=".$idinst;
	$result=mysql_query($req,$link);
	$row = mysql_num_rows($result);
	$totlot=0;
	if ($row!=0)
	{
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo "<tr><td align='center' width='10%'><button data-tooltip='Supprimer' data-id='".$ligne["insturba_id"]."' data-idinst='".$idinst."' class='btn btn-sm btn-default delinstparc'><i class='fa fa-trash'></i></button></td><td class='text-center' width='40%'><input type='text' class='form-control input-sm text-center modifchpinstparcelle' data-id='".$ligne["insturba_id"]."' data-idinst='".$idinst."' data-chp='insturba_numerosection' value='".$ligne["insturba_numerosection"]."'></td><td class='text-center' width='40%'><input type='text' class='form-control input-sm text-center modifchpinstparcelle' data-id='".$ligne["insturba_id"]."' data-idinst='".$idinst."' data-chp='insturba_numeroparcelle' value='".$ligne["insturba_numeroparcelle"]."'></td></tr>";
		}
	}else{
		echo "<tr><td colspan='6' class='text-center'><b>Aucune parcelle</b></td></tr>";
	}
	echo "</tbody>";
	echo "</table>";
	echo '</div>';
	echo '</div>';
	echo '</div>';

	echo '<div class="col-md-12">';
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-3" for="inst_obs">Observation:</label>';
	echo '<div class="col-md-9">';
	echo '<textarea class="form-control input-sm modifchpinstruction"  data-idinst="'.$idinst.'" data-chp="inst_obs" id="inst_obs" name="inst_obs">'.$obs_urbanisme.'</textarea>';
	echo '</div>';
	echo '</div>';
	echo '</form>';
	echo '</div>';
}
function add_instparcelle($idinst){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql="INSERT INTO inst_urbanisme (insturba_idinst) VALUES(".$idinst.")";	
	$result=mysql_query($sql,$link);
}
function del_instparcelle($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql="DELETE FROM inst_urbanisme WHERE insturba_id=".$id;	
	$result=mysql_query($sql,$link);
}
function update_instparcelle($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE inst_urbanisme SET ".$chp."='".utf8_decode($val)."' WHERE insturba_id=".$id;
	$result2=mysql_query($req2,$link);	
}
function detail_beneficiaire($idinst){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM instructions WHERE inst_id=".$idinst;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$puissance = $ligne["inst_puissance"];
		$raccordement = $ligne["inst_raccordement"];
		$longueur_epu = $ligne["inst_longueurepu"];
		$regime_alim_epu = $ligne["inst_regimealimepu"];
	}
	/* Partie commune aux deux entit�s */
	echo '<div class="col-md-12">';
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="inst_puissance">Puissance:</label>';
	echo '<div class="col-sm-4">';
	echo '<input type="text" class="form-control input-sm modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_puissance" id="inst_puissance" name="inst_puissance" value="'.$puissance.'">';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="inst_raccordement">Raccordement:</label>';
	echo '<div class="col-md-2 col-sm-3">';
	echo '<select class="form-control input-sm modifchpinstruction"  data-idinst="'.$idinst.'" data-chp="inst_raccordement" id="inst_raccordement" name="inst_raccordement">';
	echo '<option value="0">Choisir un type</option>';
	$req="SELECT * FROM lexique WHERE lex_codelexique='RACC_URBA'";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["lex_libelle"]==$raccordement){$selected="selected";}else{$selected="";}
		echo '<option value="'.$ligne["lex_libelle"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
	}		
	echo '</select>';
	echo '</div>';
	echo '</div>';
	echo '</form>';
	echo '</div>';
	/* Mairie */
	echo '<div class="col-md-12" style="margin-bottom:20px;">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-university"></i> Equipement Public</div>';
	echo '<div class="panel-body">';
	echo '<form>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="inst_regime_alim_epu">R�gime d\'alimentation:</label>';
	echo '<div class="col-md-2 col-sm-3">';
	echo '<select class="form-control input-sm modifchpinstruction"  data-idinst="'.$idinst.'" data-chp="inst_regimealimepu" id="inst_regime_alim_epu" name="inst_regime_alim_epu">';
	echo '<option value="0">Choisir un type</option>';
	$req="SELECT * FROM lexique WHERE lex_codelexique='REG_ALIM' AND lex_typelexique='COM'";
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		if ($ligne["lex_libelle"]==$regime_alim_epu){$selected="selected";}else{$selected="";}
		echo '<option value="'.$ligne["lex_libelle"].'" '.$selected.'>'.$ligne["lex_libelle"].'</option>';
	}		
	echo '</select>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group row">';
	echo '<label class="control-label col-sm-4" for="inst_longueurepu">Longueur:</label>';
	echo '<div class="col-sm-4">';
	echo '<input type="text" class="form-control input-sm modifchpinstruction" data-idinst="'.$idinst.'" data-chp="inst_longueurepu" id="inst_longueurepu" name="inst_longueurepu" value="'.$longueur_epu.'">';
	echo '</div>';
	echo '</div>';
	echo '</form>';

	echo '</div>';
	echo '</div>';
	echo '</div>';
	/* B�n�ficiaires */
	echo '<div class="col-md-12" style="margin-bottom:20px;">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading"><i class="fas fa-user"></i> Equipement Propre</div>';
	echo '<div class="panel-body">';
	echo "<table class='table table-responsive table-bordered table-striped table-hover table-sm'>";
	echo "<thead><tr><th width='10%' class='text-center'><button data-tooltip='Ajouter un b�n�ficiaire' id='add_instbeneficiaire' data-idinst='".$idinst."' class='btn btn-sm btn-default'><i class='fa fa-plus-circle'></i></button></th><th class='text-center' width='15%'>Nom</th><th width='15%' class='text-center'>Adresse</th><th class='text-center' width='5%'>Code postal</th><th class='text-center' width='15%'>Ville</th><th class='text-center' width='15%'>Mail</th><th class='text-center' width='5%'>Longueur</th><th class='text-center' width='15%'>R�gime alimentation</th></tr></thead>";
	echo "<tbody>";
	$req="SELECT * FROM inst_beneficiaire WHERE instbenef_idinst=".$idinst;
	$result=mysql_query($req,$link);
	$row = mysql_num_rows($result);
	$totlot=0;
	if ($row!=0)
	{
		while ($ligne=mysql_fetch_assoc($result))
		{
			echo "<tr><td align='center' width='10%'><button data-tooltip='Supprimer' data-id='".$ligne["instbenef_id"]."' data-idinst='".$idinst."' class='btn btn-sm btn-default delinstbeneficiaire'><i class='fa fa-trash'></i></button> <button data-tooltip='Devis' data-id='".$ligne["instbenef_id"]."' data-idinst='".$idinst."' class='btn btn-sm btn-default devisinstbeneficiaire'><i class='fa fa-edit'></i></button></td><td class='text-center' width='15%'><input type='text' class='form-control input-sm modifchpinstbeneficiaire' data-id='".$ligne["instbenef_id"]."' data-idinst='".$idinst."' data-chp='instbenef_nom' value='".$ligne["instbenef_nom"]."'></td><td class='text-center' width='15%'><input type='text' class='form-control input-sm modifchpinstbeneficiaire' data-id='".$ligne["instbenef_id"]."' data-idinst='".$idinst."' data-chp='instbenef_adresse' value='".$ligne["instbenef_adresse"]."'></td><td class='text-center' width='5%'><input type='text' class='form-control input-sm text-center modifchpinstbeneficiaire' data-id='".$ligne["instbenef_id"]."' data-idinst='".$idinst."' data-chp='instbenef_codepostal' value='".$ligne["instbenef_codepostal"]."'></td><td class='text-center' width='15%'><input type='text' class='form-control input-sm text-center modifchpinstbeneficiaire' data-id='".$ligne["instbenef_id"]."' data-idinst='".$idinst."' data-chp='instbenef_ville' value='".$ligne["instbenef_ville"]."'></td><td class='text-center' width='15%'><input type='text' class='form-control input-sm text-center modifchpinstbeneficiaire' data-id='".$ligne["instbenef_id"]."' data-idinst='".$idinst."' data-chp='instbenef_mail' value='".$ligne["instbenef_mail"]."'></td><td class='text-center' width='5%'><input type='text' class='form-control input-sm text-center modifchpinstbeneficiaire' data-id='".$ligne["instbenef_id"]."' data-idinst='".$idinst."' data-chp='instbenef_longueur' value='".$ligne["instbenef_longueur"]."'></td>";
			echo '<td class="text-center" width="15%"><select class="form-control input-sm modifchpinstbeneficiaire" data-id="'.$ligne["instbenef_id"].'" data-idinst="'.$idinst.'" data-chp="instbenef_regimealim">';
			echo '<option value="0">Choisir un type</option>';
			$req2="SELECT * FROM lexique WHERE lex_codelexique='REG_ALIM' AND lex_typelexique!='COM'";
			$result2=mysql_query($req2,$link);
			while ($ligne2=mysql_fetch_assoc($result2))
			{
				if ($ligne2["lex_libelle"]==$ligne["instbenef_regimealim"]){$selected="selected";}else{$selected="";}
				echo '<option value="'.$ligne2["lex_libelle"].'" '.$selected.'>'.$ligne2["lex_libelle"].'</option>';
			}		
			echo '</select></td>';
			echo "</tr>";
		}
	}else{
		echo "<tr><td colspan='8' class='text-center'><b>Aucun b�n�ficiaire</b></td></tr>";
	}
	echo "</tbody>";
	echo "</table>";

	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function add_instbeneficiaire($idinst){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql="INSERT INTO inst_beneficiaire (instbenef_idinst) VALUES(".$idinst.")";	
	$result=mysql_query($sql,$link);
}
function del_instbeneficiaire($id){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$sql="DELETE FROM inst_beneficiaire WHERE instbenef_id=".$id;	
	$result=mysql_query($sql,$link);
}
function update_instbeneficiaire($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE inst_beneficiaire SET ".$chp."='".utf8_decode($val)."' WHERE instbenef_id=".$id;
	$result2=mysql_query($req2,$link);
	/* calcul du forfait */
	calcul_forfait($id,"epo");
}
function calcul_forfait($id,$type){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$r1 = range(3, 12);
	$r2 = range(15, 36);
	$r3 = range(37, 118);
	$r4 = range(120, 250);
	/* calcul du forfait */
	if ($type=="epo"){
		$req="SELECT * FROM inst_beneficiaire WHERE instbenef_id=".$id;
		$result=mysql_query($req,$link);
		$row=mysql_fetch_assoc($result);
		$req2="SELECT * FROM instructions WHERE inst_id=".$row["instbenef_idinst"];
		$result2=mysql_query($req2,$link);
		$row2=mysql_fetch_assoc($result2);
		$v = $row2["inst_puissance"];
		switch (true) {
			case in_array($v, $r1) :
				if ($row["instbenef_longueur"]<=200){
					$forfait = 1074.02+($row["instbenef_longueur"]*29.83);
				}else{
					$forfait = 1074.02+(200*29.83)+(($row["instbenef_longueur"]-200)*59.67);
				}
				break;
			case in_array($v, $r2) :
				if ($row["instbenef_longueur"]<=200){
					$forfait = 1432.02+($row["instbenef_longueur"]*29.83);
				}else{
					$forfait = 1432.02+(200*29.83)+(($row["instbenef_longueur"]-200)*59.67);
				}
				break;
			case in_array($v, $r3) :
				$forfait = 1598.67+($row["instbenef_longueur"]*29.83);
				break;
			case in_array($v, $r4) :
				$forfait = 1598.7+($row["instbenef_longueur"]*29.83);
				break;
		}
		$req2="UPDATE inst_beneficiaire SET instbenef_forfaitHT='".$forfait."' WHERE instbenef_id=".$id;
		$result2=mysql_query($req2,$link);	
	}else{
		$req2="SELECT * FROM instructions WHERE inst_id=".$id;
		$result2=mysql_query($req2,$link);
		$row2=mysql_fetch_assoc($result2);
		$v = $row2["inst_puissance"];
		switch (true) {
			case in_array($v, $r1) :
				if ($row2["inst_longueurepu"]<=200){
					$forfait = 1074.02+($row2["inst_longueurepu"]*29.83);
				}else{
					$forfait = 1074.02+(200*29.83)+(($row2["inst_longueurepu"]-200)*59.67);
				}
				break;
			case in_array($v, $r2) :
				if ($row2["inst_longueurepu"]<=200){
					$forfait = 1432.02+($row2["inst_longueurepu"]*29.83);
				}else{
					$forfait = 1432.02+(200*29.83)+(($row2["inst_longueurepu"]-200)*59.67);
				}
				break;
			case in_array($v, $r3) :
				$forfait = 1598.67+($row2["inst_longueurepu"]*29.83);
				break;
			case in_array($v, $r4) :
				$forfait = 1598.7+($row2["inst_longueurepu"]*29.83);
				break;
		}
		$req2="UPDATE instructions SET inst_forfaitHTepu='".$forfait."' WHERE inst_id=".$id;
		$result2=mysql_query($req2,$link);	
	}
}
function del_instruction($idinst){
	require("./compte.php");
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	/* Suppression des historiques */
	$sql = "DELETE FROM historique WHERE hist_idocc=".$idinst;
	$resul=mysql_query($sql,$link);
	/* Suppression de l'instruction */
	$sqlinst = "DELETE FROM inst_urbanisme WHERE insturba_idinst=".$idinst;
	$resulinst=mysql_query($sqlinst,$link);
	$sqlinst = "DELETE FROM inst_beneficiaire WHERE instbenef_idinst=".$idinst;
	$resulinst=mysql_query($sqlinst,$link);
	$sqlinst = "DELETE FROM inst_aps WHERE instaps_idinst=".$idinst;
	$resulinst=mysql_query($sqlinst,$link);
	$sqlinst = "DELETE FROM instructions WHERE inst_id=".$idinst;
	$resulinst=mysql_query($sqlinst,$link);
}
function add_instruction(){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);

	$req2="SELECT inst_numero FROM instructions ORDER BY inst_numero DESC LIMIT 1";
	$result2=mysql_query($req2,$link);
	while ($ligneinst=mysql_fetch_assoc($result2))
	{
		$tabnuminst = explode("-",$ligneinst["inst_numero"]);
	}
	$newref = date("y")."-INS-";
	if ($tabnuminst[0]==date("y")){
		$compteur = $tabnuminst[2]+1;
	}else{
		$compteur = 1;
	}
	$compt = strlen($compteur);
	$diff = 4 - $compt;
	for ($i=0;$i<$diff;$i++){
		$newref .= "0";
	}
	$newref .= $compteur;
	/* Insert l'op�ration */
	$req2="INSERT INTO instructions (inst_numero,inst_datecreate,inst_datereponse,inst_categorie,inst_chargeaffaire) VALUES ('".$newref."',NOW(),NOW(),'Instruction','".$_COOKIE["login"]."')";
	$result2=mysql_query($req2,$link);
	$idinst = mysql_insert_id($link);	
	$req2="INSERT INTO inst_aps (instaps_idinst) VALUES ('".$idinst."')";
	$result2=mysql_query($req2,$link);
	$req2="INSERT INTO historique (hist_idocc,hist_dateevent,hist_libelleevent,hist_user,hist_event,hist_natureevent) VALUES ('".$idinst."',NOW(),'Cr�ation instruction','".$_COOKIE["login"]."','create','INS')";
	$result2=mysql_query($req2,$link);
}
function detail_estimation($idinst){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra'); 
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req="SELECT * FROM inst_aps JOIN instructions ON instructions.inst_id = inst_aps.instaps_idinst WHERE inst_aps.instaps_idinst=".$idinst;
	$result=mysql_query($req,$link);
	while ($ligne=mysql_fetch_assoc($result))
	{
		$id = $ligne["instaps_id"];
		$montantETU = $ligne["instaps_montantETU"];
		$puHTA = $ligne["instaps_puHTA"];
		$qteHTA = $ligne["instaps_qteHTA"];
		$montantHTA = $ligne["instaps_montantHTA"];
		$puBT = $ligne["instaps_puBT"];
		$qteBT = $ligne["instaps_qteBT"];
		$montantBT = $ligne["instaps_montantBT"];
		$montantBranch = $ligne["instaps_montantBranch"];
		$montantPoste = $ligne["instaps_montantPoste"];
		$montantTVXEnedis = $ligne["instaps_montantTVXEnedis"];
		$montantTVXAnnexe = $ligne["instaps_montantTVXAnnexe"];
		$soustotal = $ligne["instaps_soustotal"];
		$tauxMOO = $ligne["instaps_tauxMOO"];
		$montantMOO = $ligne["instaps_montantMOO"];
		$montantPCT = $ligne["instaps_montantPCT"];
		$montantRestant = $ligne["instaps_montantRestant"];
	}
	/* Cout r�el */
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">Co�t R�el</div>';
	echo '<div class="panel-body">';
		echo '<table class="table table-sm table-responsive table-striped table-bordered table-hover">';
		echo '<thead><tr><th></th><th class="text-center">Ratio</th><th class="text-center">Qte/ml</th><th class="text-center" width="15%">Montant (�)</th></tr></thead>';
		echo '<tbody>';
		echo '<tr><td>ETUDE</td><td></td><td></td><td width="20%"><input data-id="'.$id.'" data-chp="instaps_montantETU" data-idinst="'.$idinst.'" class="form-control input-sm text-right modifchpinstaps" value="'.$montantETU.'"></td></tr>';
		echo '<tr><td>RESEAU HTA</td><td width="12%"><input data-id="'.$id.'" data-chp="instaps_puHTA" data-idinst="'.$idinst.'" class="form-control input-sm text-center modifchpinstaps" value="'.$puHTA.'"></td><td width="12%"><input data-id="'.$id.'" data-chp="instaps_qteHTA" data-idinst="'.$idinst.'" class="form-control input-sm text-center modifchpinstaps" value="'.$qteHTA.'"></td><td width="20%"><input data-id="'.$id.'" data-chp="instaps_montantHTA" data-idinst="'.$idinst.'" class="form-control input-sm text-right modifchpinstaps" value="'.$montantHTA.'"></td></tr>';
		echo '<tr><td>RESEAU BT</td><td width="12%"><input data-id="'.$id.'" data-chp="instaps_puBT" data-idinst="'.$idinst.'" class="form-control input-sm text-center modifchpinstaps" value="'.$puBT.'"></td><td width="12%"><input data-id="'.$id.'" data-chp="instaps_qteBT" data-idinst="'.$idinst.'" class="form-control input-sm text-center modifchpinstaps" value="'.$qteBT.'"></td><td width="20%"><input data-id="'.$id.'" data-chp="instaps_montantBT" data-idinst="'.$idinst.'" class="form-control input-sm text-right modifchpinstaps" value="'.$montantBT.'"></td></tr>';
		echo '<tr><td>BRANCHEMENT</td><td></td><td></td><td width="20%"><input data-id="'.$id.'" data-chp="instaps_montantBranch" data-idinst="'.$idinst.'" class="form-control input-sm text-right modifchpinstaps" value="'.$montantBranch.'"></td></tr>';
		echo '<tr><td>POSE D\'UN POSTE HTA/BT</td><td></td><td></td><td width="20%"><input data-id="'.$id.'" data-chp="instaps_montantPoste" data-idinst="'.$idinst.'" class="form-control input-sm text-right modifchpinstaps" value="'.$montantPoste.'"></td></tr>';
		echo '<tr><td>TRAVAUX ENEDIS</td><td></td><td></td><td width="20%"><input data-id="'.$id.'" data-chp="instaps_montantTVXEnedis" data-idinst="'.$idinst.'" class="form-control input-sm text-right modifchpinstaps" value="'.$montantTVXEnedis.'"></td></tr>';
		echo '<tr><td>TRAVAUX ANNEXE</td><td></td><td></td><td width="20%"><input data-id="'.$id.'" data-chp="instaps_montantTVXAnnexe" data-idinst="'.$idinst.'" class="form-control input-sm text-right modifchpinstaps" value="'.$montantTVXAnnexe.'"></td></tr>';
		echo '<tr><td><b>Sous Total</b></td><td></td><td></td><td class="text-right" width="20%"><b>'.$soustotal.'</b></td></tr>';
		echo '<tr><td><b>Taux Maitrise d\'Oeuvre</b></td><td></td><td class="text-center">'.$tauxMOO.' %</td><td class="text-right" width="20%"><b>'.$montantMOO.'</b></td></tr>';
		echo '<tr><td><b>PCT</b></td><td></td><td></td><td class="text-right" width="20%"><b>'.$montantPCT.'</b></td></tr>';
		echo '<tr><td><b>Montant restant � charge</b></td><td></td><td></td><td class="text-right" width="20%"><b>'.$montantRestant.'</b></td></tr>';
		echo '</tbody>';
		echo '</table>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	/* Forfait */
	echo '<div class="col-sm-6">';
	echo '<div class="panel panel-default">';
	echo '<div class="panel-heading">Forfait</div>';
	echo '<div class="panel-body">';
		echo '<div class="panel panel-default">';
		echo '<div class="panel-heading"><i class="fas fa-university"></i> Equipement Public</div>';
		echo '<div class="panel-body">';
			echo '<table class="table table-sm table-bordered table-striped table-responsive table-hover">';
			echo '<thead><tr><th width="10%"></th><th align="center">Nom</th><th align="center" width="12%">Forfait (� HT)</th></tr></thead>';
			echo '<tbody>';
			$req2="SELECT * FROM instructions WHERE inst_id=".$idinst;
			$result2=mysql_query($req2,$link);
			while ($row = mysql_fetch_assoc($result2)){
				echo '<tr><td align="center" width="10%"><button data-tooltip="Estimation" class="btn btn-default btn-sm"><i class="fa fa-file"></i></button></td><td>Mairie</td><td width="12%" align="right">'.$row["inst_forfaitHTepu"].'</td></tr>';
			}
			echo '</tbody>';
			echo '</table>';
		echo '</div>';
		echo '</div>';
	echo '<div class="panel panel-default">';
		echo '<div class="panel-heading"><i class="fas fa-user"></i> Equipements Propres</div>';
		echo '<div class="panel-body">';
			echo '<table class="table table-sm table-bordered table-striped table-responsive table-hover">';
			echo '<thead><tr><th width="10%"></th><th align="center">Nom</th><th align="center" width="12%">Forfait (� HT)</th></tr></thead>';
			echo '<tbody>';
			$req2="SELECT * FROM inst_beneficiaire WHERE instbenef_idinst=".$idinst;
			$result2=mysql_query($req2,$link);
			$nbbenef = mysql_num_rows($result2);
			if ($nbbenef!=0){
				while ($row = mysql_fetch_assoc($result2)){
					echo '<tr><td align="center" width="10%"><button data-tooltip="Estimation" class="btn btn-default btn-sm"><i class="fa fa-file"></i></button></td><td>'.$row["instbenef_nom"].'</td><td width="12%" align="right">'.$row["instbenef_forfaitHT"].'</td></tr>';
				}
			}else{
				echo '<tr><td class="text-center" colspan="3"><b>Aucun b�n�ficiaire</b></td></tr>';
			}
			echo '</tbody>';
			echo '</table>';
		echo '</div>';
		echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}
function update_instaps($id,$chp,$val){
	require("./compte.php");
	setlocale (LC_TIME, 'fr_FR.utf8','fra');
	$link=mysql_connect($srvSYGALE,$logSYGALE,$pwdSYGALE) or die("Impossible de se connecter : " .mysql_error());
	mysql_select_db($baseSYGALE,$link);
	$req2="UPDATE inst_aps SET ".$chp."='".utf8_decode($val)."' WHERE instaps_id=".$id;
	$result2=mysql_query($req2,$link);
	/* Calcul des qte */
	$req2="SELECT * FROM inst_aps WHERE instaps_id=".$id;
	$result2=mysql_query($req2,$link);
	$row=mysql_fetch_assoc($result2);
	$montantHTA = $row["instaps_puHTA"] * $row["instaps_qteHTA"];
	$montantBT = $row["instaps_puBT"] * $row["instaps_qteBT"];
	$req2="UPDATE inst_aps SET instaps_montantHTA='".$montantHTA."',instaps_montantBT='".$montantBT."' WHERE instaps_id=".$id;
	$result2=mysql_query($req2,$link);

	/* Calcul des totaux */
	$req2="SELECT * FROM inst_aps WHERE instaps_id=".$id;
	$result2=mysql_query($req2,$link);
	$row=mysql_fetch_assoc($result2);
	$soustotal = $row["instaps_montantETU"]+$row["instaps_montantHTA"]+$row["instaps_montantBT"]+$row["instaps_montantBranch"]+$row["instaps_montantPoste"]+$row["instaps_montantTVXEnedis"]+$row["instaps_montantTVXAnnexe"];
	$montantMOO = $soustotal * $row["instaps_tauxMOO"] /100;
	$montantPCT = ($soustotal + $montantMOO) * $row["instaps_tauxPCT"] /100;
	$montantRestant = $soustotal + $montantMOO - $montantPCT;
	$req2="UPDATE inst_aps SET instaps_soustotal='".$soustotal."',instaps_montantMOO='".$montantMOO."',instaps_montantPCT='".$montantPCT."',instaps_montantRestant='".$montantRestant."' WHERE instaps_id=".$id;
	$result2=mysql_query($req2,$link);
}
function envoimail(){
	// Envoi du mail � l'entreprise
	require('../PHPMailer/class.phpmailer.php');
	$mail = new PHPMailer();
	$mail->Host = "cli-mail.devopsys.com";
	$mail->SMTPAuth   = false;
	$mail->Port = 25; // Par d�faut
	 
	// Exp�diteur
	$mail->SetFrom('sehv@sehv.fr', 'SEHV');
	$mail->Sender = 'sehv@sehv.fr';

	$ldaphost = "192.168.8.122";
	$ldapport = 389;
	$ldapuser = "administrateur@sehv87.fr";
	$ldappass = "147258";
	$login = $_COOKIE["login"];
	// Connexion LDAP
	$ldapconn = ldap_connect($ldaphost) or die("Impossible de se connecter au serveur LDAP $ldaphost");

	if($ldapconn) {
		$ldapbind = ldap_bind($ldapconn, $ldapuser, $ldappass);
		$ldaptree = "OU=Agents SEHV,DC=sehv87,DC=fr";
		$result = ldap_search($ldapconn,$ldaptree, "(samaccountname=".$login.")") or die ("Error in search query: ".ldap_error($ldapconn));

		$data = ldap_get_entries($ldapconn, $result);
	        for ($i=0; $i<$data["count"]; $i++) {
	            //echo "dn is: ". $data[$i]["dn"] ."<br />";
	            if(isset($data[$i]["mail"][0])) {
	                 $mailagent = $data[$i]["mail"][0];
	            }
	        }
	}
	ldap_close($ldapconn);

	// Re�evoir une confirmation de lecture
	$mail->AddReplyTo($mailagent, 'SEHV');
	$mail->addCustomHeader("X-Confirm-Reading-To: ".$mailagent);
	$mail->addCustomHeader("Disposition-notification-to: ".$mailagent);

	// Destinataire
	$mail->AddAddress('resptechnique@sehv.fr', 'PEYRICHON Alain');

	// Objet
	$mail->Subject = 'Demande BC ';
	 
	// Votre message
	$mail->MsgHTML('Test demande BC');

	// Ajouter une pi�ce jointe
	//$mail->AddAttachment(dirname(dirname(__DIR__)).'/PdfDepannage/'.$filename.'.pdf');

	if(!$mail->Send()) {
		echo 'Erreur : ' . $mail->ErrorInfo;
	} else {
		echo 'Message envoy� !';
	}
}