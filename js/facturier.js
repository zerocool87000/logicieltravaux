var tabcrit = [];var criteres="";
$(document).ready(function(){
	$(".fa").qtip({style: { classes: 'qtip-dark qtip-rounded qtip-shadow' }});
    $("#menu_facturier").click(function(){
		var elementmenu = $(this).parent();
		$('.sider-menu li').each(function(){
			$(this).removeClass("active");
		});
		elementmenu.addClass("active");
    	menu_facturier();
    });
	$("body").on("click",".visuproj",function(){
		var id = $(this).data("id");
		detail_projet(id);
	});
	$("body").on("click",".attrproj",function(){
		var idproj = $(this).data("id");
		swal({
			title: "Attributtion",
			text: "Etes vous s�r de vouloir attribuer le projet!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Valider",
			cancelButtonText: "Annuler",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
				type: "POST",
				url: "./projet.php",
				data: "action=attributionprojet&idproj="+idproj,
				success: function(resul){
					swal("Attribution!", "Le projet vous a bien �t� attribu�.", "success");
					lst_facturierfacture(1,getCookie("login"),"","","");
				}
			});
		});
	});
	$("body").on("click",".delproj",function(){
		var idproj = $(this).data("id");
		swal({
			title: "Suppression",
			text: "Etes vous s�r de vouloir supprimer!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Valider",
			cancelButtonText: "Annuler",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
				type: "POST",
				url: "./projet.php",
				data: "action=delprojet&idproj="+idproj,
				success: function(resul){
					swal("Supprim�!", "Le projet a bien �t� supprim�.", "success");
					lst_facturierfacture(1,getCookie("login"),"","","");
				}
			});
		});
	});
    $("body").on("change",".modifchpprojet",function(){
		var idproj = $(this).data("idproj");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_projet(idproj,chp,val);
    });
    $("body").on("change",".update_aps",function(){
		var idproj = $(this).data("idproj");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_aps_projet(idproj,chp,val);
    });
    $("body").on("change",".chpfact_filter",function(){
    	var compt=0;
    	criteres="";
    	tabcrit = [];
    	$(".chpfact_filter").each(function(){
			if ($(this).val()!=""){
				if (compt==0){
					criteres = $(this).data("chp")+" like '%"+$(this).val()+"%'";
				}else{
					criteres += ' AND '+$(this).data("chp")+" like '%"+$(this).val()+"%'";
				}
				tabcrit.push($(this).data("chp")+":"+$(this).val());
				compt = compt + 1;
			}
    	});
    	if (compt==0){
			lst_facturierfacture(1,getCookie("login"),"","","","");
    	}else{
			elements = tabcrit.join(',');
			lst_facturierfacture(1,"","","",encodeURIComponent(criteres),elements);
    	}
    });
});

function menu_facturier(){
	var box = "<div class='col-md-12'>";
	box += '<div class="well well-sm btn-toolbar">';
	box += '<div class="btn-group">';
	box += '<button data-position="bottom" data-tooltip="Nouvelle facture" type="button" id="add_facture" class="btn btn-sm btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Factures</button>';
	box += '</div>';
	box += '</div>';

	//box += "<div class='portlet'><div class='portlet-title'><i class='fas fa-file-invoice-dollar' aria-hidden='true'></i> Facturier </div><div class='portlet-content' id='lstfacturier'></div></div>";
	box += "<div class='portlet'><div class='portlet-title'><i class='fas fa-file-invoice-dollar' aria-hidden='true'></i> Module Facturier </div><div class='portlet-content' id='lstfacturier'>";
	box += "<div class='row'>";
	box += '<div class="col-sm-12">';
	box += '<div class="panel-group" id="accordion">';
	box +='<div class="panel panel-default">';
	box +='<div class="panel-heading">';
	box +='<h2 class="panel-title">';
	box +='<a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><i class="fas fa-edit" aria-hidden="true"></i> Devis</a>';
	box +='</h2>';
	box +='</div>';
	box +='<div id="collapse1" class="panel-collapse collapse">';
	box +='<div class="panel-body" id="lstfacturierdevis">';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='<div class="panel panel-default">';
	box +='<div class="panel-heading">';
	box +='<h2 class="panel-title">';
	box +='<a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><i class="fas fa-file-invoice-dollar" aria-hidden="true"></i> Factures</a>';
	box +='</h2>';
	box +='</div>';
	box +='<div id="collapse2" class="panel-collapse collapse in">';
	box +='<div class="panel-body" id="lstfacturierfact">';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box += "</div>";

	box += "</div>";
	$("#contentpage").html(box);
	lst_facturierfacture(1,getCookie("login"),"","","");
	$("#add_projet").click(function(){
		var textbox = '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Cat�gories</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectnouvproj_categorie" required><option></option></select></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./projet.php",
			data: "action=lstcategorie",
			success: function(resul){
				$("#selectnouvproj_categorie").html(resul);
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-cogs"></i> Nouveau projet',
			message: textbox,
			buttons: {
				ok: {
					label: "Cr�er",
					className: 'btn-success',
					callback: function(){
						fieldvalidate("selectnouvproj_categorie").then(function(answer){
							var ansbool = answer.toString();
							if (ansbool=="true"){
								$.ajax({
									type: "POST",
									url: "./projet.php",
									data: "action=addprojet&idcat="+$("#selectnouvproj_categorie").val(),
									success: function(resul){
										dialog.modal('hide');
										lst_facturierfacture(1,getCookie("login"),"","","","");
									}
								});
							}else{
							swal("Erreur!", "Cat�gorie absente.", "danger");								
							}
						});
					}
				},
				cancel: {
					label: "Annuler",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
}
function lst_facturierfacture(page,login,chp,val,criteres,tabcrit){
	$.ajax({
		type: "POST",
		url: "./facturier.php",
		data: "action=lstfacturierfacture&page="+page+"&login="+login+"&chp="+chp+"&val="+val+"&criteres="+criteres+"&tabcrit="+tabcrit,
		success: function(resul){
			$("#lstfacturierfact").html(resul);
			$('*[data-chp="proj_numero"]').mask("99-PRO-9999");
			$('*[data-chp="proj_anneeprog"]').mask("9999");
			$(".forward").click(function(){
				if(page>1){lst_facturierfacture(page-1,getCookie("login"),chp,val,criteres,tabcrit);}
			});
			$(".next").click(function(){
				lst_facturierfacture(page+1,getCookie("login"),chp,val,criteres,tabcrit);
			});

		}
	});
}
function update_projet(idproj,chp,val){
	$.ajax({
		type: "POST",
		url: "./projet.php",
		data: "action=updateprojet&idproj="+idproj+"&chp="+chp+"&val="+val,
		success: function(resul){
			detail_gene_projet(idproj);
			detail_programmation_projet(idproj);
		}
	});
}
function detail_projet(id){
	$.ajax({
		type: "POST",
		url: "./projet.php",
		data: "action=detailprojet&id="+id,
		success: function(resul){
			$("#contentpage").html(resul);
			//$("#annee_prog").mask("99/99/9999");
			$("#proj_datedemande").mask("99/99/9999");
			detail_programmation_projet(id);
			$("#back_lstprojets").click(function(){
				menu_projet();
			});
		}
	});
}
function detail_gene_projet(idproj){
	$.ajax({
		type: "POST",
		url: "./projet.php",
		data: "action=detailgeneprojet&idproj="+idproj,
		success: function(resul){
			$("#detailgene_projet").html(resul);
			$("#proj_datedemande").mask("99/99/9999");
		}
	});
}
function detail_programmation_projet(idproj){
	$.ajax({
		type: "POST",
		url: "./projet.php",
		data: "action=detailprogrammation&idproj="+idproj,
		success: function(resul){
			$("#detprogrammation").html(resul);
		}
	});
}
function update_aps_projet(idproj,chp,val){
	$.ajax({
		type: "POST",
		url: "./projet.php",
		data: "action=updateaps&idproj="+idproj+"&chp="+chp+"&val="+val,
		success: function(resul){
			detail_programmation_projet(idproj);
		}
	});
}