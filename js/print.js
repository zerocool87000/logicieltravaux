$(document).ready(function(){
	$(".fa").qtip({style: { classes: 'qtip-dark qtip-rounded qtip-shadow' }});
	$("#menu_print").click(function(){
		var elementmenu = $(this).parent();
		$('.sider-menu li').each(function(){
			$(this).removeClass("active");
		});
		elementmenu.addClass("active");
		menu_print();
	});
	$("body").on("click",".visureportcontext",function(){
		var id = $(this).data("id");
		var repos = $(this).data("repos");
		var file = $(this).data("file");
		//window.open("http://192.168.8.71:8080/pentaho/api/repos/%3Apublic%3A"+repos+"%3A"+file+"/viewer");
		window.open("http://192.168.8.108:8080/pentaho/api/repos/%3Apublic%3A"+repos+"%3A"+file+"/viewer?idx="+id,"Etat","fullscreen=yes");
	});
});
function menu_print(){
	$.ajax({
		type: "POST",
		url: "./print.php",
		data: "action=lstprintlisting",
		success: function(resul){
			$("#contentpage").html(resul);
			$("#print_apercureport").height($( document ).height()-200);
			$(".btn_export").click(function(){
				var repos = $(this).data("repos");
				var file = $(this).data("file");
				//window.open("http://192.168.8.71:8080/pentaho/api/repos/%3Apublic%3A"+repos+"%3A"+file+"/viewer");
				window.open("http://192.168.8.108:8080/pentaho/api/repos/%3Apublic%3A"+repos+"%3A"+file+"/viewer","Etat","menubar=no, status=no, scrollbars=no, menubar=no, width=200, height=100");
			});
			$(".visureport").click(function(){
				var repos = $(this).data("repos");
				var file = $(this).data("file");
				var url = "http://192.168.8.108:8080/pentaho/api/repos/%3Apublic%3A"+repos+"%3A"+file+"/viewer";
				$("#print_apercureport").attr("src", url);
			});
		}
	});
}
