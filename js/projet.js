var tabcrit = [];var criteres="";
$(document).ready(function(){
	$(".fa").qtip({style: { classes: 'qtip-dark qtip-rounded qtip-shadow' }});
    $("#menu_projet").click(function(){
		var elementmenu = $(this).parent();
		$('.sider-menu li').each(function(){
			$(this).removeClass("active");
		});
		elementmenu.addClass("active");
    	menu_projet();
    });
	$("body").on("click",".visuproj",function(){
		var id = $(this).data("id");
		detail_projet(id);
	});
	$("body").on("click",".attrproj",function(){
		var idproj = $(this).data("id");
		swal({
			title: "Attributtion",
			text: "Etes vous s�r de vouloir attribuer le projet!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Valider",
			cancelButtonText: "Annuler",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
				type: "POST",
				url: "./projet.php",
				data: "action=attributionprojet&idproj="+idproj,
				success: function(resul){
					swal("Attribution!", "Le projet vous a bien �t� attribu�.", "success");
					lst_projet(1,getCookie("login"),"","","");
				}
			});
		});
	});
	$("body").on("click",".delproj",function(){
		var idproj = $(this).data("id");
		swal({
			title: "Suppression",
			text: "Etes vous s�r de vouloir supprimer!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Valider",
			cancelButtonText: "Annuler",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
				type: "POST",
				url: "./projet.php",
				data: "action=delprojet&idproj="+idproj,
				success: function(resul){
					swal("Supprim�!", "Le projet a bien �t� supprim�.", "success");
					lst_projet(1,getCookie("login"),"","","");
				}
			});
		});
	});
    $("body").on("change",".modifchpprojet",function(){
		var idproj = $(this).data("idproj");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_projet(idproj,chp,val);
    });
    $("body").on("change",".update_aps",function(){
		var idproj = $(this).data("idproj");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_aps_projet(idproj,chp,val);
    });
    $("body").on("change",".chpproj_filter",function(){
    	var compt=0;
    	criteres="";
    	tabcrit = [];
    	$(".chpproj_filter").each(function(){
			if ($(this).val()!=""){
				if (compt==0){
					criteres = $(this).data("chp")+" like '%"+$(this).val()+"%'";
				}else{
					criteres += ' AND '+$(this).data("chp")+" like '%"+$(this).val()+"%'";
				}
				tabcrit.push($(this).data("chp")+":"+$(this).val());
				compt = compt + 1;
			}
    	});
    	if (compt==0){
			lst_projet(1,getCookie("login"),"","","","");
    	}else{
			elements = tabcrit.join(',');
			lst_projet(1,"","","",encodeURIComponent(criteres),elements);
    	}
    });
});

function menu_projet(){
	var box = "<div class='col-md-12'>";
	box += '<div class="well well-sm btn-toolbar">';
	box += '<div class="btn-group">';
	box += '<button data-position="bottom" data-tooltip="Nouveau projet" type="button" id="add_projet" class="btn btn-sm btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Projets</button>';
	box += '</div>';
	//box += '<button data-position="bottom" data-tooltip="Filtre" type="button" id="btn_filtreproj" class="btn btn-sm btn-default"><i class="fa fa-filter" aria-hidden="true"></i></button>';
	box += '</div>';

	box += '<div class="well well-sm" id="block_filter_projet">';
	box += '<div class="row"><label class="col-sm-2" for="filter_crit">Crit�res:</label><div class="col-sm-3">';
	box += '<select class="form-control input-sm" id="filter_crit" name="filter_crit">';
	box += '<option value="-1">Choisir un crit�re</option><option value="operations.code_insee">Commune</option><option value="operations.numero">Num�ro</option><option value="operations.numero">Ann�e</option><option value="operations.categorie">Cat�gorie</option>';
	box += '</select>';
	box += '</div><label class="col-sm-2" for="filter_val">Valeur:</label><div class="col-sm-3" id="filter_input"></div>';
	box += '<div class="col-sm-1"><button type="button" id="btn_filtrestart" class="btn btn-sm btn-default"><i class="fa fa-search" aria-hidden="true"></i></button></div>';
	box += '<div class="col-sm-1"><button type="button" id="btn_filtreerase" class="btn btn-sm btn-default"><i class="fa fa-trash" aria-hidden="true"></i></button></div>';
	box += '</div>';
	box += '</div>';

	box += "<div class='portlet'><div class='portlet-title'><i class='fa fa-cogs' aria-hidden='true'></i> Projets </div><div class='portlet-content' id='lstprojet'></div></div>";
	box += "</div>";
	$("#contentpage").html(box);
	$("#block_filter_projet").toggle();
	$("#btn_filtreproj").click(function(){
		$("#block_filter_projet").toggle("slow");
		if ($(this).hasClass("active")){$(this).removeClass("active");}else{$(this).addClass("active");}
	});
	$("#btn_filtrestart").click(function(){
		var chp = $("#filter_crit").val();
		var val = $("#filter_val").val();
		lst_projet(1,"",chp,val,"","");
	});
	$("#filter_crit").change(function(){
		$("#filter_val").val("");
		switch ($("#filter_crit option:selected").text()){
			case "Commune":
				$("#filter_input").html('<select id="filter_val" name="filter_val" class="form-control input-sm"></select>');
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstcollectivite",
					success: function(resul){
						$("#filter_val").html(resul);
						$("option.lstcomoptionfilter:disabled").css("color","#FAA21B");
					}
				});
				break;
			case "Num�ro":
				$("#filter_input").html('<input id="filter_val" name="filter_val" class="form-control input-sm">');
				$("#filter_val").mask("99-aaa-999");
				break;
			case "Ann�e":
				$("#filter_input").html('<input id="filter_val" name="filter_val" class="form-control input-sm">');
				$("#filter_val").mask("99");
				break;
			case "Cat�gorie":
				$("#filter_input").html('<input id="filter_val" name="filter_val" class="form-control input-sm">');
				break;
		}
	});
	$("#btn_filtreerase").click(function(){
		$("#filter_crit").val("-1");
		$("#filter_val").val("");
		$("#filter_input").html("");
		lst_projet(1,getCookie("login"),"","","","");
	});
	lst_projet(1,getCookie("login"),"","","");
	$("#add_projet").click(function(){
		var textbox = '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Cat�gories</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectnouvproj_categorie" required><option></option></select></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./projet.php",
			data: "action=lstcategorie",
			success: function(resul){
				$("#selectnouvproj_categorie").html(resul);
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-cogs"></i> Nouveau projet',
			message: textbox,
			buttons: {
				ok: {
					label: "Cr�er",
					className: 'btn-success',
					callback: function(){
						fieldvalidate("selectnouvproj_categorie").then(function(answer){
							var ansbool = answer.toString();
							if (ansbool=="true"){
								$.ajax({
									type: "POST",
									url: "./projet.php",
									data: "action=addprojet&idcat="+$("#selectnouvproj_categorie").val(),
									success: function(resul){
										dialog.modal('hide');
										lst_projet(1,getCookie("login"),"","","","");
									}
								});
							}else{
							swal("Erreur!", "Cat�gorie absente.", "danger");								
							}
						});
					}
				},
				cancel: {
					label: "Annuler",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
}
function lst_projet(page,login,chp,val,criteres,tabcrit){
	$.ajax({
		type: "POST",
		url: "./projet.php",
		data: "action=lstprojet&page="+page+"&login="+login+"&chp="+chp+"&val="+val+"&criteres="+criteres+"&tabcrit="+tabcrit,
		success: function(resul){
			$("#lstprojet").html(resul);
			$('*[data-chp="proj_numero"]').mask("99-PRO-9999");
			$('*[data-chp="proj_anneeprog"]').mask("9999");
			$(".forward").click(function(){
				if(page>1){lst_projet(page-1,getCookie("login"),chp,val,criteres,tabcrit);}
			});
			$(".next").click(function(){
				lst_projet(page+1,getCookie("login"),chp,val,criteres,tabcrit);
			});

		}
	});
}
function update_projet(idproj,chp,val){
	$.ajax({
		type: "POST",
		url: "./projet.php",
		data: "action=updateprojet&idproj="+idproj+"&chp="+chp+"&val="+val,
		success: function(resul){
			detail_gene_projet(idproj);
			detail_programmation_projet(idproj);
		}
	});
}
function detail_projet(id){
	$.ajax({
		type: "POST",
		url: "./projet.php",
		data: "action=detailprojet&id="+id,
		success: function(resul){
			$("#contentpage").html(resul);
			//$("#annee_prog").mask("99/99/9999");
			$("#proj_datedemande").mask("99/99/9999");
			detail_programmation_projet(id);
			$("#back_lstprojets").click(function(){
				menu_projet();
			});
		}
	});
}
function detail_gene_projet(idproj){
	$.ajax({
		type: "POST",
		url: "./projet.php",
		data: "action=detailgeneprojet&idproj="+idproj,
		success: function(resul){
			$("#detailgene_projet").html(resul);
			$("#proj_datedemande").mask("99/99/9999");
		}
	});
}
function detail_programmation_projet(idproj){
	$.ajax({
		type: "POST",
		url: "./projet.php",
		data: "action=detailprogrammation&idproj="+idproj,
		success: function(resul){
			$("#detprogrammation").html(resul);
		}
	});
}
function update_aps_projet(idproj,chp,val){
	$.ajax({
		type: "POST",
		url: "./projet.php",
		data: "action=updateaps&idproj="+idproj+"&chp="+chp+"&val="+val,
		success: function(resul){
			detail_programmation_projet(idproj);
		}
	});
}