/*!
 * FileInput French Translations
 *
 * This file must be loaded after 'fileinput.js'. Patterns in braces '{}', or
 * any HTML markup tags in the messages must not be converted or translated.
 *
 * @see http://github.com/kartik-v/bootstrap-fileinput
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
(function ($) {
    "use strict";

    $.fn.fileinputLocales['fr'] = {
        fileSingle: 'fichier',
        filePlural: 'fichiers',
        browseLabel: 'Parcourir&hellip;',
        removeLabel: 'Retirer',
        removeTitle: 'Retirer les fichiers s�lectionn�s',
        cancelLabel: 'Annuler',
        cancelTitle: "Annuler l'envoi en cours",
        uploadLabel: 'Transf�rer',
        uploadTitle: 'Transf�rer les fichiers s�lectionn�s',
        msgNo: 'Non',
        msgCancelled: 'Annul�',
        msgZoomTitle: 'Voir les d�tails',
        msgZoomModalHeading: 'Aper�u d�taill�',
        msgSizeTooLarge: 'Le fichier "{name}" (<b>{size} Ko</b>) d�passe la taille maximale autoris�e qui est de <b>{maxSize} Ko</b>.',
        msgFilesTooLess: 'Vous devez s�lectionner au moins <b>{n}</b> {files} � transmettre.',
        msgFilesTooMany: 'Le nombre de fichier s�lectionn� <b>({n})</b> d�passe la quantit� maximale autoris�e qui est de <b>{m}</b>.',
        msgFileNotFound: 'Le fichier "{name}" est introuvable !',
        msgFileSecured: "Des restrictions de s�curit� vous emp�chent d'acc�der au fichier \"{name}\".",
        msgFileNotReadable: 'Le fichier "{name}" est illisble.',
        msgFilePreviewAborted: 'Pr�visualisation du fichier "{name}" annul�e.',
        msgFilePreviewError: 'Une erreur est survenue lors de la lecture du fichier "{name}".',
        msgInvalidFileType: 'Type de document invalide pour "{name}". Seulement les documents de type "{types}" sont autoris�s.',
        msgInvalidFileExtension: 'Extension invalide pour le fichier "{name}". Seules les extensions "{extensions}" sont autoris�es.',
        msgUploadAborted: 'Le t�l�chargement du fichier a �t� interrompu',
        msgValidationError: 'Erreur de validation',
        msgLoading: 'Transmission du fichier {index} sur {files}&hellip;',
        msgProgress: 'Transmission du fichier {index} sur {files} - {name} - {percent}% faits.',
        msgSelected: '{n} {files} s�lectionn�(s)',
        msgFoldersNotAllowed: 'Glissez et d�posez uniquement des fichiers ! {n} r�pertoire(s) exclu(s).',
        msgImageWidthSmall: 'Largeur de fichier image "{name}" doit �tre d\'au moins {size} px.',
        msgImageHeightSmall: 'Hauteur de fichier image "{name}" doit �tre d\'au moins {size} px.',
        msgImageWidthLarge: 'Largeur de fichier image "{name}" ne peut pas d�passer {size} px.',
        msgImageHeightLarge: 'Hauteur de fichier image "{name}" ne peut pas d�passer {size} px.',
        msgImageResizeError: "Impossible d'obtenir les dimensions de l'image � redimensionner.",
        msgImageResizeException: "Erreur lors du redimensionnement de l'image.<pre>{errors}</pre>",
        dropZoneTitle: 'Glissez et d�posez votre image ici&hellip;',
        fileActionSettings: {
            removeTitle: 'Supprimer le fichier',
            uploadTitle: 'T�l�charger un fichier',
            indicatorNewTitle: 'Pas encore t�l�charg�',
            indicatorSuccessTitle: 'Post�',
            indicatorErrorTitle: 'Ajouter erreur',
            indicatorLoadingTitle: 'ajout ...'
        }
    };
})(window.jQuery);
