var tabcrit = [];var criteres="";
$(document).ready(function(){
	$(".fa").qtip({style: { classes: 'qtip-dark qtip-rounded qtip-shadow' }});
    $("#menu_instruction").click(function(){
		$.ajax({
			type: "POST",
			url: "./instruction.php",
			data: "action=verifaccesinstruction",
			success: function(resul){   
				if (resul=="Ok"){
					var elementmenu = $("#menu_instruction").parent();
					$('.sider-menu li').each(function(){
						$(this).removeClass("active");
					});
					elementmenu.addClass("active");
					menu_instruction();
				}else{
					var box = "<div class='alert alert-danger text-center'><i class='fas fa-exclamation-triangle fa-lg'></i> Vous n'�tes pas authoris�</div>";
					$("#contentpage").html(box);

				}
			}
		});
    });
	$("body").on("change",".modifchpinstruction",function(){
		var idinst = $(this).data("idinst");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_instruction(idinst,chp,val);
	});
    $("body").on("change",".chpinst_filter",function(){
    	var compt=0;
    	criteres="";
    	tabcrit = [];
    	$(".chpinst_filter").each(function(){
			if ($(this).val()!=""){
				if (compt==0){
					criteres = $(this).data("chp")+" like '%"+$(this).val()+"%'";
				}else{
					criteres += ' AND '+$(this).data("chp")+" like '%"+$(this).val()+"%'";
				}
				tabcrit.push($(this).data("chp")+":"+$(this).val());
				compt = compt + 1;
			}
    	});
    	if (compt==0){
			lst_instruction(1,getCookie("login"),"","","","");
    	}else{
			elements = tabcrit.join(',');
			lst_instruction(1,"","","",encodeURIComponent(criteres),elements);
    	}
    });
	$("body").on("click","#add_instparc",function(){
		var idinst = $(this).data("idinst");
		$.ajax({
			type: "POST",
			url: "./instruction.php",
			data: "action=addinstparcelle&idinst="+idinst,
			success: function(resul){
				detail_urbanisme(idinst);
			}
		});
	});
	$("body").on("click",".delinstparc",function(){
		var id = $(this).data("id");
		var idinst = $(this).data("idinst");
		swal({
			title: "Suppression",
			text: "Etes vous s�r de vouloir supprimer!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Valider",
			cancelButtonText: "Annuler",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
				type: "POST",
				url: "./instruction.php",
				data: "action=delinstparcelle&id="+id,
				success: function(resul){
					swal("Supprim�!", "La parcelle a bien �t� supprim�.", "success");
					detail_urbanisme(idinst);
				}
			});
		});
	});
	$("body").on("change",".modifchpinstparcelle",function(){
		var idinst = $(this).data("idinst");
		var id = $(this).data("id");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_instparcelle(id,idinst,chp,val);
	});
	$("body").on("click","#add_instbeneficiaire",function(){
		var idinst = $(this).data("idinst");
		$.ajax({
			type: "POST",
			url: "./instruction.php",
			data: "action=addinstbeneficiaire&idinst="+idinst,
			success: function(resul){
				detail_beneficiaire(idinst);
			}
		});
	});
	$("body").on("click",".delinstbeneficiaire",function(){
		var id = $(this).data("id");
		var idinst = $(this).data("idinst");
		swal({
			title: "Suppression",
			text: "Etes vous s�r de vouloir supprimer!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Valider",
			cancelButtonText: "Annuler",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
				type: "POST",
				url: "./instruction.php",
				data: "action=delinstbeneficiaire&id="+id,
				success: function(resul){
					swal("Supprim�!", "Le b�n�ficiaire a bien �t� supprim�.", "success");
					detail_beneficiaire(idinst);
					detail_estimation(idinst);
				}
			});
		});
	});
	$("body").on("change",".modifchpinstbeneficiaire",function(){
		var idinst = $(this).data("idinst");
		var id = $(this).data("id");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_instbeneficiaire(id,idinst,chp,val);
	});
	$("body").on("change",".modifchpinstaps",function(){
		var idinst = $(this).data("idinst");
		var id = $(this).data("id");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_instaps(id,idinst,chp,val);
	});
});
function menu_instruction(){
	var box = "<div class='col-md-12'>";
	box += '<div class="well well-sm btn-toolbar">';
	box += '<div class="btn-group">';
	box += '<div class="btn-group" role="group">';
	box += '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> Actions <span class="caret"></span></button>';
	box += '<ul class="dropdown-menu" style="font-size:9pt;">';
	box += '<li><a href="#" id="suivi_demanderaccgeo"><i class="fa fa-exclamation-circle"></i> Demandes de raccordement G�oSeHV</a></li>';
/*	box += '<li><a href="#" id="suivi_demandeosope"><i class="fa fa-exclamation-circle"></i> Suivi des demandes OS</a></li>';
*/	box += '</ul>';
	box += '</div>';
	box += '<button type="button" data-position="bottom" data-tooltip="Nouvelle instruction" id="add_instruction" class="btn btn-sm btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Instructions</button>';
	box += '</div>';
	//box += '<button type="button" data-position="bottom" data-tooltip="Filtre" id="btn_filtreope" class="btn btn-sm btn-default"><i class="fa fa-filter" aria-hidden="true"></i></button>';
	box += '</div>';

	box += '<div id="nouvdemanderaccgeo"></div>';

	box += "<div class='portlet'><div class='portlet-title'><i class='fas fa-plug' aria-hidden='true'></i> Instructions </div><div class='portlet-content' id='lstinstruction'></div></div>";
	box += "</div>";
	$("#contentpage").html(box);
	$("#block_filter").toggle();
	$("#btn_filtreope").click(function(){
		$("#block_filter").toggle("slow");
		if ($(this).hasClass("active")){$(this).removeClass("active");}else{$(this).addClass("active");}
	});
	$("#btn_filtrestart").click(function(){
		var chp = $("#filter_crit").val();
		var val = $("#filter_val").val();
		lst_instruction(1,"",chp,val,"","");
	});
	$("#filter_crit").change(function(){
		$("#filter_val").val("");
		switch ($("#filter_crit option:selected").text()){
			case "Commune":
				$("#filter_input").html('<select id="filter_val" name="filter_val" class="form-control input-sm"></select>');
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstcollectivite",
					success: function(resul){
						$("#filter_val").html(resul);
						$("option.lstcomoptionfilter:disabled").css("color","#FAA21B");
					}
				});
				break;
			case "Num�ro":
				$("#filter_input").html('<input id="filter_val" name="filter_val" class="form-control input-sm">');
				$("#filter_val").mask("99-aaa-999");
				break;
			case "Ann�e":
				$("#filter_input").html('<input id="filter_val" name="filter_val" class="form-control input-sm">');
				$("#filter_val").mask("99");
				break;
			case "Cat�gorie":
				$("#filter_input").html('<input id="filter_val" name="filter_val" class="form-control input-sm">');
				break;
		}
	});
	$("#btn_filtreerase").click(function(){
		$("#filter_crit").val("-1");
		$("#filter_val").val("");
		$("#filter_input").html("");
		lst_instruction(1,getCookie("login"),"","","","");
	});
	lst_instruction(1,getCookie("login"),"","","","");
	verif_nouvelledemanderaccgeo();
	$("#suivi_demandebcope").click(function(){
		var textbox = '<div class="row"><div class="col-md-12"><div id="tablstdemandebc"></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./chantier.php",
			data: "action=lstdemandebc",
			success: function(resul){
				$("#tablstdemandebc").html(resul);
				$('.showcht').each(function(){
					$(this).nextUntil('tr.showcht').slideToggle();
				});
				$('.showcht').click(function(){
					$(this).nextUntil('tr.showcht').slideToggle();
				});
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-exclamation-circle"></i> Suivi des demandes BC',
			message: textbox,
			buttons: {
				cancel: {
					label: "Fermer",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
	$("#add_instruction").click(function(){
		$.ajax({
			type: "POST",
			url: "./instruction.php",
			data: "action=addinstruction",
			success: function(resul){
				lst_instruction(1,getCookie("login"),"","","","");
			}
		});
	});
	$("#suivi_demanderaccgeo").click(function(){
		var myVar; var elements;
		var textbox = '<div class="row"><div class="col-md-12"><div id="tablstdemanderaccgeo"></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./instruction.php",
			data: "action=lstdemanderaccgeo",
			success: function(resul){
				$("#tablstdemanderaccgeo").html(resul);
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-bolt"></i> Demandes de raccordement G�oSeHV',
			message: textbox,
			className: "dialogSuividemande",
			buttons: {
				cancel: {
					label: "Fermer",
					className: 'btn-default'
				}
			}
		});
		$("body").on("click",".demraccgeotoinst",function(){
			var id = $(this).data("id");
			swal({
				title: "Attention",
				text: "Etes vous s�r de vouloir traiter la demande!",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-warning",
				confirmButtonText: "Valider",
				cancelButtonText: "Annuler",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
					type: "POST",
					url: "./instruction.php",
					data: "action=demraccgeotoinst&id="+id,
					success: function(resul){
						swal("F�licitation!", "La demande a bien �t� trait�e.", "success");
						$.ajax({
							type: "POST",
							url: "./instruction.php",
							data: "action=lstdemanderaccgeo",
							success: function(resul){
								$("#tablstdemanderaccgeo").html(resul);
							}
						});
						lst_instruction(1,getCookie("login"),"","","","");
					}
				});
			});
		});
	});
}

function verif_nouvelledemanderaccgeo(){
	$.ajax({
		type: "POST",
		url: "./instruction.php",
		data: "action=verifdemanderaccgeo",
		success: function(resul){
			$("#nouvdemanderaccgeo").html(resul);
		}
	});
}

function lst_instruction(page,login,chp,val,criteres,tabcrit){
	$.ajax({
		type: "POST",
		url: "./instruction.php",
		data: "action=lstinstruction&page="+page+"&login="+login+"&chp="+chp+"&val="+val+"&criteres="+criteres+"&tabcrit="+tabcrit,
		success: function(resul){
			$("#lstinstruction").html(resul);
			$('*[data-chp="inst_numero"]').mask("99-INS-9999");
			$(".forward").click(function(){
				if(page>1){lst_instruction(page-1,getCookie("login"),chp,val,criteres,tabcrit);}
			});
			$(".next").click(function(){
				lst_instruction(page+1,getCookie("login"),chp,val,criteres,tabcrit);
			});

			$(".visuinst").click(function(){
				var id = $(this).data("id");
				detail_instruction(id);
			});
			$(".delinst").click(function(){
				var idinst = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./instruction.php",
						data: "action=delinstruction&idinst="+idinst,
						success: function(resul){
							swal("Supprim�!", "L'instruction a bien �t� supprim�.", "success");
							lst_instruction(1,getCookie("login"),"","","","");
						}
					});
				});
			});
		}
	});
}
function update_instruction(idinst,chp,val){
	$.ajax({
		type: "POST",
		url: "./instruction.php",
		data: "action=updateinstruction&idinst="+idinst+"&chp="+chp+"&val="+val,
		success: function(resul){
			if (chp=="type_urbanisme"){
				detail_instruction(idinst);
			}else{
				$.ajax({
					type: "POST",
					url: "./instruction.php",
					data: "action=detailgeneinstruction&idinst="+idinst,
					success: function(resul){
						if (chp=="inst_dateretourdevissigne"){
							detail_instruction(idinst);
						}else{
							$("#detailgene_instruction").html(resul);
						}
						$(".date").mask("99/99/9999");
					}
				});
			}
			detail_estimation(idinst);
		}
	});
}

function detail_bcoperation(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailbcoperation&idope="+idope,
		success: function(resul){
			$("#detbcoperation").html(resul);
			$(".add_demandeos").click(function(){
				var tabdemoscht = [];
				var idbc = $(this).data("idbc");
				var myVar; var elements;
				var textbox = '<div class="row"><div class="col-md-12">';
				textbox += '<div class="col-md-3"><label>D�lai (sem.)</label></div><div class="col-md-2"><input class="form-control input-sm text-center" id="demandeos_delai"></div>';
				textbox += '</div></div>';
				textbox += '<br><div class="row"><div class="col-md-12">';
				textbox += '<div class="col-md-3"><label>Montant TTC (�)</label></div><div class="col-md-9"><input class="form-control input-sm text-right" id="demandeos_montant"></div>';
				textbox += '</div></div>';
				textbox += '<br><div class="row"><div class="col-md-12">';
				textbox += '<div class="col-md-3"><label>Entreprises</label></div><div class="col-md-9"><select class="form-control input-sm" id="demandeos_entreprise"></select></div>';
				textbox += '</div></div>';
				textbox += '<br><div class="row"><div class="col-md-12">';
				textbox += '<div class="col-md-3"><label>Type OS</label></div><div class="col-md-9"><select class="form-control input-sm" id="demandeos_typeos"></select></div>';
				textbox += '</div></div>';
				//textbox += '<br><div class="row"><div class="col-md-12"><div class="col-md-2"><label>Op�rations</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectdembc_operation"><option></option></select></div></div></div>';
				textbox += '<br><div class="row"><div class="col-md-12"><div class="col-md-3"><label>Op�rations</label></div><div class="col-md-9"><input class="form-control input-sm" id="selectdemos_operation"></div></div></div>';
				textbox += '<br><div class="row"><div class="col-md-12"><div id="lstchantierdemos"></div></div></div>';
				textbox += '<br><div class="row"><div class="col-md-12"><div id="lstdemos"></div></div></div>';
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstentreprise",
					success: function(resul){
						$("#demandeos_entreprise").html(resul);
					}
				});
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lsttypeos",
					success: function(resul){
						$("#demandeos_typeos").html(resul);
					}
				});
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstoperationdemos&idbc="+idbc,
					success: function(resul){
						$("#lstdemos").html(resul);
						$(".chkchtdemandeos").click(function(){
							if ( $( this ).prop( "checked" ) ){
								var id = $(this).data("id");
								tabdemoscht.push(id);
							}else{
								var id = $(this).data("id");
								index = tabdemoscht.indexOf(id);
								tabdemoscht.splice(index,1);
							}
						});
						$("#selectdemos_operation").change(function(){
							var idope = $(this).val();
							if (idope!='-1'){
								elements = tabdemoscht.join(',');
								$.ajax({
									type: "POST",
									url: "./chantier.php",
									data: "action=lstchantieropedemos&idope="+idope+"&tabcht="+elements,
									success: function(resul){
										$("#lstchantierdemos").html(resul);
										$(".chknouvchtdemandeos").click(function(){
											if ( $( this ).prop( "checked" ) ){
												var id = $(this).data("id");
												tabdemoscht.push(id);
											}else{
												var id = $(this).data("id");
												index = tabdemoscht.indexOf(id);
												tabdemoscht.splice(index,1);
											}
											console.log(tabdemoscht);
										});
									}
								});
							}
						});
					}
				});

				var dialog = bootbox.dialog({
					title: '<i class="fa fa-edit"></i> Nouvelle Demande OS',
					message: textbox,
					buttons: {
						ok: {
							label: "Envoyer",
							className: 'btn-success',
							callback: function(){
								elements = tabdemoscht.join(',');
								$.ajax({
									type: "POST",
									url: "./chantier.php",
									data: "action=adddemandeos&idope="+idope+"&idbc="+idbc+"&montantttc="+$("#demandeos_montant").val()+"&typeos="+$("#demandeos_typeos").val()+"&ident="+$("#demandeos_entreprise").val()+"&delai="+$("#demandeos_delai").val()+"&tabcht="+elements,
									success: function(resul){
										dialog.modal('hide');
										clearInterval(myVar);
									}
								});
							}
						},
						cancel: {
							label: "Annuler",
							className: 'btn-default',
							callback: function(){
								clearInterval(myVar);
								dialog.modal('hide');
							}
						}
					}
				});
				$("#selectdemos_operation").mask("99-aaa-999");
			});
		}
	});
}
function detail_instruction(id){
	$.ajax({
		type: "POST",
		url: "./instruction.php",
		data: "action=detailinstruction&id="+id,
		success: function(resul){
			$("#contentpage").html(resul);
			//$("#annee_prog").mask("99/99/9999");
			$(".date").mask("99/99/9999");
			detail_urbanisme(id);
			detail_beneficiaire(id);
			$("#back_lstinstructions").click(function(){
				menu_instruction();
			});
			$("#btn_raccmagic").click(function(){
				//menu_instruction();
				alert("En cours de construction !!!!");
			});
			$("#add_chantier").click(function(){
				var idope = $(this).data("idope");
				var textbox = '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Natures</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectnouvcht_nature"><option></option></select></div></div></div>';
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstnature",
					success: function(resul){
						$("#selectnouvcht_nature").html(resul);
					}
				});

				var dialog = bootbox.dialog({
					title: '<i class="fa fa-wrench"></i> Nouvelle prestation',
					message: textbox,
					buttons: {
						ok: {
							label: "Cr�er",
							className: 'btn-success',
							callback: function(){
								$.ajax({
									type: "POST",
									url: "./chantier.php",
									data: "action=addchantier&idope="+idope+"&idnat="+$("#selectnouvcht_nature").val(),
									success: function(resul){
										dialog.modal('hide');
										detail_operation(id);
									}
								});
							}
						},
						cancel: {
							label: "Annuler",
							className: 'btn-default',
							callback: function(){
								dialog.modal('hide');
							}
						}
					}
				});
			});
		}
	});
}
/* Urbanisme */
function update_instparcelle(id,idinst,chp,val){
	$.ajax({
		type: "POST",
		url: "./instruction.php",
		data: "action=updateinstparcelle&id="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			detail_urbanisme(idinst);
		}
	});
}
function detail_urbanisme(idinst){
	$.ajax({
		type: "POST",
		url: "./instruction.php",
		data: "action=detailurbanisme&idinst="+idinst,
		success: function(resul){
			$("#deturbanisme").html(resul);
		}
	});
}
/* Beneficiaires */
function update_instbeneficiaire(id,idinst,chp,val){
	$.ajax({
		type: "POST",
		url: "./instruction.php",
		data: "action=updateinstbeneficiaire&id="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			detail_beneficiaire(idinst);
			detail_estimation(idinst);
		}
	});
}
function detail_beneficiaire(idinst){
	$.ajax({
		type: "POST",
		url: "./instruction.php",
		data: "action=detailbeneficiaire&idinst="+idinst,
		success: function(resul){
			$("#detbeneficiaire").html(resul);
		}
	});
}
/* APS */
function update_instaps(id,idinst,chp,val){
	$.ajax({
		type: "POST",
		url: "./instruction.php",
		data: "action=updateinstaps&id="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			detail_estimation(idinst);
		}
	});
}
function detail_estimation(idinst){
	$.ajax({
		type: "POST",
		url: "./instruction.php",
		data: "action=detailestimation&idinst="+idinst,
		success: function(resul){
			$("#detestimation").html(resul);
		}
	});
}