$(document).ready(function(){
	$(".fa").qtip({style: { classes: 'qtip-dark qtip-rounded qtip-shadow' }});
    $("#menu_marche").click(function(){
		var elementmenu = $(this).parent();
		$('.sider-menu li').each(function(){
			$(this).removeClass("active");
		});
		elementmenu.addClass("active");
    	menu_marche();
    });
    $("body").on("click","#back_accueil",function(){
		menu_marche();
	});
	$("body").on("click","#back_bc",function(){
		detail_boncommande($(this).data("idbc"));
	});
    $("body").on("change",".modifchpmarche",function(){
		var idmarche = $(this).data("idmarche");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_marche(idmarche,chp,val);
    });
    $("body").on("change",".modifchplot",function(){
		var idlot = $(this).data("idlot");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_lot(idlot,chp,val);
    });
    $("body").on("change",".modifchppaiement",function(){
		var idpaie = $(this).data("idpaie");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_paiement(idpaie,chp,val);
    });
    $("body").on("change",".modifchpos",function(){
		var idos = $(this).data("idos");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_os(idos,chp,val);
    });
    $("body").on("change",".modifchpboncommande",function(){
		var idbc = $(this).data("idbc");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_boncommande(idbc,chp,val);
    });
    $("body").on("change",".modifchpchtbc",function(){
		var id = $(this).data("id");
		var idbc = $(this).data("idbc");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_chtbc(idbc,id,chp,val);
    });
	$("body").on("change","#lstprogbccht",function(){
		var id_prog = $(this).val();
		var id = $(this).data("id");
		var idbc = $(this).data("idbc");
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=financecht&idanneeprog=0&idprog="+id_prog+"&idbc="+idbc+"&idbccht="+id,
			success: function(resul){
				$("#detailfinancebccht").html(resul);
			}
		});
	});
    $("body").on("change",".modifchpvalventpaie",function(){
		var idventpaie = $(this).data("idventpaie");
		var id = $(this).data("idvalventpaie");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_valventpaie(idventpaie,id,chp,val);
    });
    $("body").on("change",".modifchpdatemarche",function(){
		var idmarche = $(this).data("idmarche");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_datemarche(idmarche,chp,val);
    });
    /* Lot */
    $("body").on("change",".modifchpdatelot",function(){
		var idlot = $(this).data("idlot");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_datelot(idlot,chp,val);
    });
	$("body").on("click","#add_indicelot",function(){
		var idlot = $(this).data("idlot");
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=addindicelot&idlot="+idlot,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./marche.php",
					data: "action=lstindicelot&idlot="+idlot,
					success: function(resul){
						$("#detindicelot").html(resul);
					}
				});
			}
		});
	});
	$("body").on("click","#add_formulelot",function(){
		var idlot = $(this).data("idlot");
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=addformulelot&idlot="+idlot,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./marche.php",
					data: "action=lstformulelot&idlot="+idlot,
					success: function(resul){
						$("#detformulelot").html(resul);
					}
				});
			}
		});
	});
	$("body").on("click","#add_entlot",function(){
		var idlot = $(this).data("idlot");
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=addentlot&idlot="+idlot,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./marche.php",
					data: "action=lstentlot&idlot="+idlot,
					success: function(resul){
						$("#detentlot").html(resul);
					}
				});
			}
		});
	});
    $("body").on("change",".modifchpentlot",function(){
		var idlot = $(this).data("idlot");
		var id = $(this).data("id");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_entlot(idlot,id,chp,val);
    });
    $("body").on("change",".modifchpindicelot",function(){
		var idlot = $(this).data("idlot");
		var id = $(this).data("id");
		var chp = $(this).data("chp");
		var val = encodeURIComponent($(this).val());
		update_indicelot(idlot,id,chp,val);
    });
    $("body").on("change",".modifchpformulelot",function(){
		var idlot = $(this).data("idlot");
		var id = $(this).data("id");
		var chp = $(this).data("chp");
		if (chp=="formule_actu"){var val = encodeURIComponent($(this).val());}else{var val = $(this).val();}
		update_formulelot(idlot,id,chp,val);
    });
    /* Marche */
	$("body").on("click","#add_indicemarche",function(){
		var idmarche = $(this).data("idmarche");
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=addindicemarche&idmarche="+idmarche,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./marche.php",
					data: "action=lstindicemarche&idmarche="+idmarche,
					success: function(resul){
						$("#detindicemarche").html(resul);
					}
				});
			}
		});
	});
	$("body").on("click","#add_formulemarche",function(){
		var idmarche = $(this).data("idmarche");
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=addformulemarche&idmarche="+idmarche,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./marche.php",
					data: "action=lstformulemarche&idmarche="+idmarche,
					success: function(resul){
						$("#detformulemarche").html(resul);
					}
				});
			}
		});
	});
	$("body").on("click","#add_entmarche",function(){
		var idmarche = $(this).data("idmarche");
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=addentmarche&idmarche="+idmarche,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./marche.php",
					data: "action=lstentmarche&idmarche="+idmarche,
					success: function(resul){
						$("#detentmarche").html(resul);
						$("option.lstcomoptionfilter:disabled").css("color","#FAA21B");
					}
				});
			}
		});
	});
    $("body").on("change",".modifchpentmarche",function(){
		var idmarche = $(this).data("idmarche");
		var id = $(this).data("id");
		var chp = $(this).data("chp");
		var val = $(this).val();
		if (chp=="marent_grpt"){
			if ( $( this ).prop( "checked" ) ){val=1;}else{val=0;}
		}
		update_entmarche(idmarche,id,chp,val);
    });
    $("body").on("change",".modifchpindicemarche",function(){
		var idmarche = $(this).data("idmarche");
		var id = $(this).data("id");
		var chp = $(this).data("chp");
		var val = encodeURIComponent($(this).val());
		update_indicemarche(idmarche,id,chp,val);
    });
    $("body").on("change",".modifchpformulemarche",function(){
		var idmarche = $(this).data("idmarche");
		var id = $(this).data("id");
		var chp = $(this).data("chp");
		if (chp=="marform_formuleactu"){var val = encodeURIComponent($(this).val());}else{var val = $(this).val();}
		update_formulemarche(idmarche,id,chp,val);
    });
	$("body").on("click","#add_paiebc",function(){
		var idbc = $(this).data("idbc");
		var textbox = '<form><div class="form-group row"><div class="col-md-12"><div class="col-md-3"><label for="nat_paiement">Nature</label></div><div class="col-md-9"><select class="form-control input-sm" id="nat_paiement"></select></div></div></div>';
		textbox += '</div></div></form>';
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=lstnaturepaiement",
			success: function(resul){
				$("#nat_paiement").html(resul);
			}
		});
		var dialog = bootbox.dialog({
			title: 'Nouveau paiement',
			message: textbox,
			buttons: {
				ok: {
					label: "Ajouter",
					className: 'btn-success',
					callback: function(){
						$.ajax({
							type: "POST",
							url: "./marche.php",
							data: "action=addpaiebc&idbc="+idbc+"&natpaie="+$("#nat_paiement").val(),
							success: function(resul){
								dialog.modal('hide');
								$.ajax({
									type: "POST",
									url: "./marche.php",
									data: "action=lstpaiebc&idbc="+idbc,
									success: function(resul){
										$("#detpaiebc").html(resul);
									}
								});
							}
						});
					}
				},
				cancel: {
					label: "Annuler",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
	$("body").on("click","#add_osbc",function(){
		var idbc = $(this).data("idbc");
		var textbox = '<form><div class="form-group row"><div class="col-md-12"><div class="col-md-3"><label for="type_os">Type</label></div><div class="col-md-9"><select class="form-control input-sm" id="type_os"></select></div></div></div>';
		textbox += '</div></div></form>';
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=lsttypeos",
			success: function(resul){
				$("#type_os").html(resul);
			}
		});
		var dialog = bootbox.dialog({
			title: 'Nouvel ordre de service',
			message: textbox,
			buttons: {
				ok: {
					label: "Ajouter",
					className: 'btn-success',
					callback: function(){
						$.ajax({
							type: "POST",
							url: "./marche.php",
							data: "action=addosbc&idbc="+idbc+"&typeos="+$("#type_os").val(),
							success: function(resul){
								dialog.modal('hide');
								$.ajax({
									type: "POST",
									url: "./marche.php",
									data: "action=lstosbc&idbc="+idbc,
									success: function(resul){
										$("#detosbc").html(resul);
									}
								});
							}
						});
					}
				},
				cancel: {
					label: "Annuler",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
});

function menu_marche(){
	var box = "<div class='col-md-12'>";
	box += '<div class="well well-sm btn-toolbar">';
	box += '<div class="btn-group">';
	box += '<div class="btn-group" role="group">';
	box += '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> Actions <span class="caret"></span></button>';
	box += '<ul class="dropdown-menu" style="font-size:9pt;">';
	box += '<li><a href="#" id="add_marche"><i class="fa fa-plus-circle"></i> Cr�er un march�</a></li>';
	box += '<li><a href="#" id="add_boncommande"><i class="fa fa-plus-circle"></i> Cr�er un bon de commande</a></li>';
	box += '<li><a href="#" id="suivi_demandebcmarche"><i class="fa fa-exclamation-circle"></i> Suivi demandes BC</a></li>';
	box += '<li><a href="#" id="suivi_demandeosmarche"><i class="fa fa-exclamation-circle"></i> Suivi demandes OS</a></li>';
	box += '</ul>';
	box += '</div>';

	box += '</div>';
	box += '<button data-position="bottom" data-tooltip="Filtre" type="button" id="btn_filtrebc" class="btn btn-sm btn-default"><i class="fa fa-filter" aria-hidden="true"></i></button>';
	box += '<button data-position="bottom" data-tooltip="Filien SEDIT" type="button" id="btn_exportsedit" class="btn btn-sm btn-default"><i class="fas fa-upload" aria-hidden="true"></i> SEDIT</button>';
	box += '</div>';

	box += '<div class="well well-sm" id="block_filter">';
	box += '<div class="row"><label class="col-sm-2" for="filter_crit">Crit�res:</label><div class="col-sm-3">';
	box += '<select class="form-control input-sm" id="filter_crit" name="filter_crit">';
	box += '<option value="-1">Choisir un crit�re</option><option value="boncommande.numero">Num�ro</option>';
	box += '</select>';
	box += '</div><label class="col-sm-2" for="filter_val">Valeur:</label><div class="col-sm-3"><input id="filter_val" name="filter_val" class="form-control input-sm"></div>';
	box += '<div class="col-sm-1"><button type="button" id="btn_filtrestart" class="btn btn-sm btn-default"><i class="fa fa-search" aria-hidden="true"></i></button></div>';
	box += '<div class="col-sm-1"><button type="button" id="btn_filtreerase" class="btn btn-sm btn-default"><i class="fa fa-trash" aria-hidden="true"></i></button></div>';
	box += '</div>';
	box += '</div>';

	box += "<div class='portlet'><div class='portlet-title'><i class='fa fa-university' aria-hidden='true'></i> Module March� </div><div class='portlet-content' id='lstmodulemarche'>";
	box += "<div class='row'>";
	box += '<div class="col-sm-12">';
	box += '<div class="panel-group" id="accordion">';
	box +='<div class="panel panel-default">';
	box +='<div class="panel-heading">';
	box +='<h2 class="panel-title">';
	box +='<a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><i class="fa fa-university" aria-hidden="true"></i> March�s</a>';
	box +='</h2>';
	box +='</div>';
	box +='<div id="collapse1" class="panel-collapse collapse">';
	box +='<div class="panel-body" id="lstmarche">';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='<div class="panel panel-default">';
	box +='<div class="panel-heading">';
	box +='<h2 class="panel-title">';
	box +='<a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Bons de commande</a>';
	box +='</h2>';
	box +='</div>';
	box +='<div id="collapse2" class="panel-collapse collapse in">';
	box +='<div class="panel-body" id="lstboncommande">';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box += "</div>";
	
	box += "</div></div>";
	$("#contentpage").html(box);
	$("#block_filter").toggle();
	$("#btn_filtrebc").click(function(){
		$("#block_filter").toggle("slow");
		if ($(this).hasClass("active")){$(this).removeClass("active");}else{$(this).addClass("active");}
	});
	$("#btn_filtrestart").click(function(){
		var chp = $("#filter_crit").val();
		var val = $("#filter_val").val();
		lst_boncommande(1,"",chp,val);
	});
	$("#filter_crit").change(function(){
		$("#filter_val").val("");
	});
	$("#btn_filtreerase").click(function(){
		$("#filter_crit").val("");
		$("#filter_val").val("");
		lst_boncommande(1,getCookie("login"),"","");
	});
	lst_marche(1,getCookie("login"),"","");
	lst_boncommande(1,getCookie("login"),"","");
	$("#suivi_demandebcmarche").click(function(){
		var textbox = '<div class="row"><div class="col-md-12"><div id="tablstdemandebcmarche"></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=lstdemandebc",
			success: function(resul){
				$("#tablstdemandebcmarche").html(resul);
				$('.showtrcht').each(function(){
					$(this).nextUntil('tr.showcht').slideToggle();
				});
				$('.showcht').click(function(){
					$(this).closest('tr').nextUntil('tr.showcht').slideToggle();
				});
				$(".convertdembc").click(function(){
					dialog.modal('hide');
					var id_dembc = $(this).data("iddembc");
					var numbc = $(".numbc"+id_dembc).val();
					var nummarche = $(".nummarche"+id_dembc).val();
					var numlot = $(".numlot"+id_dembc).val();
					dembctobc(id_dembc,numbc,nummarche,numlot);
				});
				$("body").on("change",".lstmarche",function(){
					var id_dembc = $(this).data("iddembc");
					var idmarche = $(this).val();
					$.ajax({
						type: "POST",
						url: "./marche.php",
						data: "action=lstlot&nummarche="+idmarche,
						success: function(resul){
							$(".numlot"+id_dembc).html(resul);
						}
					});
				});
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-exclamation-circle"></i> Suivi des demandes BC',
			className: "dialogSuividemande",
			message: textbox,
			buttons: {
				cancel: {
					label: "Fermer",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
	$("#btn_exportsedit").click(function(){
		var textbox = '<div class="panel panel-default"><div class="panel-heading"><i class="fa fa-cog"></i> Param�trages</div><div class="panel-body" id="">';
		textbox += '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Budget: </label></div><div class="col-md-5"><select class="form-control input-sm required" id="selectbudget_sedit"><option></option></select></div>';
		textbox += '<div class="col-md-2"><label>Exercice: </label></div><div class="col-md-3"><input class="form-control input-sm text-center required" id="inputexercice_sedit"></div></div></div><br>';
		textbox += '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Avancement: </label></div><div class="col-md-10"><select class="form-control input-sm required" id="selectavance_sedit"><option></option></select></div></div></div><br>';
		textbox += '</div></div>';
		textbox += '<div class="panel panel-default"><div class="panel-heading"><i class="far fa-file"></i> Mouvements � exporter</div><div class="panel-body" id="">';
		textbox += '<div id="divsedittabexport"></div><button id="btn_generefilien" class="btn btn-default btn-sm"><i class="fas fa-upload"></i> Exporter</button><br><br>';
		textbox += '<div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">0% Complete (success)</div></div>';
		textbox += '<div id="errorsedit" style="display:none" class="alert alert-danger text-center"></div>';
		textbox += '</div></div>';
		var dialog = bootbox.dialog({
			title: '<i class="fas fa-upload"></i> Export SEDIT (fichier filien)',
			message: textbox,
			buttons: {
				cancel: {
					label: "Fermer",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
		$("#inputexercice_sedit").mask("9999");
		$.ajax({
			type: "POST",
			url: "./sedit.php",
			data: "action=lstbudgetsedit",
			success: function(resul){
				$("#selectbudget_sedit").html(resul);
			}
		});
		$.ajax({
			type: "POST",
			url: "./sedit.php",
			data: "action=lstavancesedit",
			success: function(resul){
				$("#selectavance_sedit").html(resul);
			}
		});
		$.ajax({
			type: "POST",
			url: "./sedit.php",
			data: "action=lsttabexportsedit",
			beforeSend: function() {
				$("#divsedittabexport").html('<div class="well well-sm text-center"><b><i class="fas fa-spinner fa-spin"></i> Chargement en cours...</b></div>');
			},
			success: function(resul){
				$("#divsedittabexport").html(resul);
			}
		});
		$("#btn_generefilien").click(function(){
			var errorsedit = 0;
			$(".required").each(function(){
				if ($(this).val()=="" || $(this).val()==0){
					errorsedit = errorsedit+1;
				}
			});
			if (errorsedit==0){
				$("#errorsedit").css("display","none");
				valeur=20;
				$('.progress-bar').css('width', valeur+'%').attr('aria-valuenow', valeur);
				$('.progress-bar').html(valeur+" % Complete (success)");
				var tabdembccht = [];
				$(".chkbcsedit").each(function(){
					if ( $( this ).prop( "checked" ) ){
						var id = $(this).data("id");
						tabdembccht.push(id);
						valeur = valeur+10;
						$('.progress-bar').css('width', valeur+'%').attr('aria-valuenow', valeur);
						$('.progress-bar').html(valeur+" % Complete (success)");
					}
				});
				myVar = setInterval(function(){
					var elements = tabdembccht.join(',');
					valeur = valeur+10;
					$('.progress-bar').css('width', valeur+'%').attr('aria-valuenow', valeur);
					$('.progress-bar').html(valeur+" % Complete (success)");
					$.ajax({
						type: "POST",
						url: "./sedit.php",
						data: "action=exportsedit&bud="+$("#selectbudget_sedit").val()+"&annee="+$("#inputexercice_sedit").val()+"&avance="+$("#selectavance_sedit").val()+"&tab="+elements,
						success: function(resul){
							valeur = 100;
							$('.progress-bar').css('width', valeur+'%').attr('aria-valuenow', valeur);
							$('.progress-bar').html(valeur+" % Complete (success)");
						},
						complete: function(resul){
							myVar2 = setInterval(function(){
								clearInterval(myVar2);
								clearInterval(myVar);
								dialog.modal('hide');
								swal("F�licitation!", "Le Filien a bien �t� g�n�r�.", "success");
							}, 1000);
						}
					});
				}, 1500);
			}else{
				$("#errorsedit").css("display","block").html("<strong><i class='fas fa-exclamation-triangle'></i> Il manque des champs</strong>");
			}
		});
	});
	$("#suivi_demandeosmarche").click(function(){
		var textbox = '<div class="row"><div class="col-md-12"><div id="tablstdemandeosmarche"></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=lstdemandeos",
			success: function(resul){
				$("#tablstdemandeosmarche").html(resul);
				$('.showcht').each(function(){
					$(this).nextUntil('tr.showcht').slideToggle();
				});
				$('.showcht').click(function(){
					$(this).nextUntil('tr.showcht').slideToggle();
				});
				$(".convertdemos").click(function(){
					dialog.modal('hide');
					var id_demos = $(this).data("iddemos");
					demostoos(id_demos);
				});
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-exclamation-circle"></i> Suivi des demandes OS',
			message: textbox,
			buttons: {
				cancel: {
					label: "Fermer",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
	$("#add_boncommande").click(function(){
		var textbox = '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Num�ro</label></div><div class="col-md-10"><input class="form-control input-sm" id="numeronouv_bc"></div></div></div><br>';
		textbox += '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>March�s</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectnouvbc_marche"><option></option></select></div></div></div><br>';
		textbox += '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Lots</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectnouvbc_lot"><option></option></select></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=lstnouvbcmarche",
			success: function(resul){
				$("#selectnouvbc_marche").html(resul);
			}
		});
		$("body").on("change","#selectnouvbc_marche",function(){
			var idmarche = $(this).val();
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=lstlot&nummarche="+idmarche,
				success: function(resul){
					$("#selectnouvbc_lot").html(resul);
				}
			});
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-shopping-basket"></i> Nouveau Bon de commande',
			message: textbox,
			buttons: {
				ok: {
					label: "Cr�er",
					className: 'btn-success',
					callback: function(){
						$.ajax({
							type: "POST",
							url: "./marche.php",
							data: "action=addboncommande&numbc="+$("#numeronouv_bc").val()+"&nummarche="+$("#selectnouvbc_marche").val()+"&numlot="+$("#selectnouvbc_lot").val(),
							success: function(resul){
								dialog.modal('hide');
								lst_boncommande(1,getCookie("login"),"","");
							}
						});
					}
				},
				cancel: {
					label: "Annuler",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
	$("#add_marche").click(function(){
		var myVar; var elements;
		textbox = '<div class="row"><div class="col-md-3"><label>Ancien format</label></div><div class="col-md-9"><input id="chk_nummarche" name="chk_nummarche" type="checkbox"></div></div></div>';
		textbox += '<br><div class="row"><div class="col-md-2"><label>Num�ro</label></div><div class="col-md-10"><input class="form-control input-sm" id="input_nummarche"></div></div>';

		var dialog = bootbox.dialog({
			title: '<i class="fas fa-university"></i> Nouveau march�',
			message: textbox,
			buttons: {
				ok: {
					label: "Envoyer",
					className: 'btn-success',
					callback: function(){
						$.ajax({
							type: "POST",
							url: "./marche.php",
							data: "action=addmarche&nummarche="+$("#input_nummarche").val(),
							success: function(resul){
								dialog.modal('hide');
								clearInterval(myVar);
								lst_marche(1,"","","");
							}
						});
					}
				},
				cancel: {
					label: "Annuler",
					className: 'btn-default',
					callback: function(){
						clearInterval(myVar);
						dialog.modal('hide');
					}
				}
			}
		});
		$("#input_nummarche").mask("9999-9999999999-00");
		$("#chk_nummarche").change(function(){
			if(this.checked){
				$("#input_nummarche").unmask();
			}else{
				$("#input_nummarche").mask("9999-9999999999-00");
			}
		});

	});
}

function dembctobc(id_dembc,numbc,nummarche,numlot){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=convertdemandebc&iddembc="+id_dembc+"&numbc="+numbc+"&nummarche="+nummarche+"&numlot="+numlot,
		success: function(resul){
			lst_boncommande(1,getCookie("login"),"","");
		}
	});
}

function demostoos(id_demos){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=convertdemandeos&iddemos="+id_demos,
		success: function(resul){
		}
	});
}

function lst_marche(page,login,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=lstmarche&page="+page+"&login="+login+"&chp="+chp+"&val="+val,
		success: function(resul){
			$("#lstmarche").html(resul);
			$(".forward").click(function(){
				if(page>1){lst_marche(page-1,getCookie("login"),"","");}
			});
			$(".next").click(function(){
				lst_marche(page+1,getCookie("login"),"","");
			});
			$(".visumarche").click(function(){
				var idmarche = $(this).data("idmarche");
				detail_marche(idmarche);
			});
/*			$(".delbc").click(function(){
				var idbc = $(this).data("idbc");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					swal("Supprim�!", "Le bon de commande a bien �t� supprim�.", "success");
				});
			});
*/		}
	});
}
function lst_boncommande(page,login,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=lstboncommande&page="+page+"&login="+login+"&chp="+chp+"&val="+val,
		success: function(resul){
			$("#lstboncommande").html(resul);
			$(".forward").click(function(){
				if(page>1){lst_boncommande(page-1,getCookie("login"),"","");}
			});
			$(".next").click(function(){
				lst_boncommande(page+1,getCookie("login"),"","");
			});
			$(".visumarche").click(function(){
				var idmarche = $(this).data("idmarche");
				detail_marche(idmarche);
			});
			$(".visulot").click(function(){
				var idlot = $(this).data("idlot");
				detail_lot(idlot);
			});
			$(".visubc").click(function(){
				var idbc = $(this).data("idbc");
				detail_boncommande(idbc);
			});
			$(".delbc").click(function(){
				var idbc = $(this).data("idbc");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					swal("Supprim�!", "Le bon de commande a bien �t� supprim�.", "success");
				});
			});
		}
	});
}

/* Bon de commande */
function detail_boncommande(idbc){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=detailboncommande&idbc="+idbc,
		success: function(resul){
			$("#contentpage").html(resul);
			$(".chpdatebc").mask("99/99/9999");
			$("body").on("click",".del_chtbc",function(){
				var idchtbc = $(this).data("idchtbc");
				var id = $(this).data("id");
				$.ajax({
					type: "POST",
					url: "./marche.php",
					data: "action=delchtbc&id="+id,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./marche.php",
							data: "action=lstchtbc&idbc="+idbc,
							success: function(resul){
								$("#detchtbc").html(resul);
							}
						});
					}
				});
			});
			$("#add_chtbc").click(function(){
				var idbc = $(this).data("idbc");
				var textbox = '<form><div class="form-group row"><div class="col-md-12"><div class="col-md-3"><label for="cht_numero">Num�ro</label></div><div class="col-md-9"><input class="form-control input-sm" id="cht_numero"></div></div></div>';
				textbox += '</div></div></form>';
				var dialog = bootbox.dialog({
					title: 'Nouvel prestation',
					message: textbox,
					buttons: {
						ok: {
							label: "Lier",
							className: 'btn-success',
							callback: function(){
								$.ajax({
									type: "POST",
									url: "./marche.php",
									data: "action=addchtbc&idbc="+idbc+"&numero="+$("#cht_numero").val(),
									success: function(resul){
										dialog.modal('hide');
										$.ajax({
											type: "POST",
											url: "./marche.php",
											data: "action=lstchtbc&idbc="+idbc,
											success: function(resul){
												$("#detchtbc").html(resul);
											}
										});
									}
								});
							}
						},
						cancel: {
							label: "Annuler",
							className: 'btn-default',
							callback: function(){
								dialog.modal('hide');
							}
						}
					}
				});
			});
			$("#cht_numero").mask("99-aaa-999-aaa-9");
			$("body").on("click",".del_paiebc",function(){
				var idbc = $(this).data("idbc");
				var id = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./marche.php",
						data: "action=delpaiebc&id="+id,
						success: function(resul){
							$.ajax({
								type: "POST",
								url: "./marche.php",
								data: "action=lstpaiebc&idbc="+idbc,
								success: function(resul){
									swal("Supprim�!", "Le paiement a bien �t� supprim�.", "success");
									$("#detpaiebc").html(resul);
								}
							});
						}
					});
				});
			});
			$("body").on("click",".del_osbc",function(){
				var idbc = $(this).data("idbc");
				var id = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./marche.php",
						data: "action=delosbc&id="+id,
						success: function(resul){
							$.ajax({
								type: "POST",
								url: "./marche.php",
								data: "action=lstosbc&idbc="+idbc,
								success: function(resul){
									swal("Supprim�!", "L'ordre de service a bien �t� supprim�.", "success");
									$("#detosbc").html(resul);
								}
							});
						}
					});
				});
			});
			$("body").on("click",".visu_paiebc",function(){
				var idpaie = $(this).data("id");
				detail_paiement(idpaie);
			});
			$("body").on("click",".visu_osbc",function(){
				var idos = $(this).data("id");
				detail_os(idos);
			});
		}
	});
}
function update_boncommande(idbc,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updateboncommande&idbc="+idbc+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=detailgeneboncommande&idbc="+idbc,
				success: function(resul){
					$("#detailgene_boncommande").html(resul);
					$(".chpdatebc").mask("99/99/9999");
				}
			});
		}
	});
}
function update_chtbc(idbc,id,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updatechtbc&id="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=lstchtbc&idbc="+idbc,
				success: function(resul){
					if (chp!="programme"){
						$("#detchtbc").html(resul);
					}
				}
			});
		}
	});
}

/* March� */
function detail_marche(idmarche){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=detailmarche&idmarche="+idmarche,
		success: function(resul){
			$("#contentpage").html(resul);
			$("#marche_numero").mask("9999-9999999999-99");
			$(".modifchpdatemarche").mask("99/99/9999");
			$("option.lstcomoptionfilter:disabled").css("color","#FAA21B");
			$(".visulot").click(function(){
				var idlot = $(this).data("id");
				detail_lot(idlot);
			});
			$("body").on("click",".del_entmarche",function(){
				var idmarche = $(this).data("idmarche");
				var id = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./marche.php",
						data: "action=delentmarche&id="+id,
						success: function(resul){
							$.ajax({
								type: "POST",
								url: "./marche.php",
								data: "action=lstentmarche&idmarche="+idmarche,
								success: function(resul){
									swal("Supprim�!", "L'entreprise a bien �t� supprim�.", "success");
									$("#detentmarche").html(resul);
								}
							});
						}
					});
				});
			});
			$("body").on("click",".del_indicemarche",function(){
				var idmarche = $(this).data("idmarche");
				var id = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./marche.php",
						data: "action=delindicemarche&id="+id,
						success: function(resul){
							$.ajax({
								type: "POST",
								url: "./marche.php",
								data: "action=lstindicemarche&idmarche="+idmarche,
								success: function(resul){
									swal("Supprim�!", "L'indice a bien �t� supprim�.", "success");
									$("#detindicemarche").html(resul);
								}
							});
						}
					});
				});
			});
			$("body").on("click",".del_formulemarche",function(){
				var idmarche = $(this).data("idmarche");
				var id = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./marche.php",
						data: "action=delformulemarche&id="+id,
						success: function(resul){
							$.ajax({
								type: "POST",
								url: "./marche.php",
								data: "action=lstformulemarche&idmarche="+idmarche,
								success: function(resul){
									swal("Supprim�!", "La formule a bien �t� supprim�.", "success");
									$("#detformulemarche").html(resul);
								}
							});
						}
					});
				});
			});
			$(".dellot").click(function(){
				var idlot = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./marche.php",
						data: "action=dellot&idlot="+idlot,
						success: function(resul){
							swal("Supprim�!", "Le lot a bien �t� supprim�.", "success");
							detail_marche(idmarche);
						}
					});
				});
			});
			$("#add_lot").click(function(){
				var myVar; var elements;
				var textbox = '<form><div class="form-group row"><div class="col-md-12"><div class="col-md-3"><label for="lot_numero">Num�ro</label></div><div class="col-md-9"><input class="form-control input-sm" id="lot_numero"></div></div></div>';
				textbox += '<div class="form-group row"><div class="col-md-12">';
				textbox += '<div class="col-md-3"><label for="lot_montantmini">Montant mini:</label></div><div class="col-md-3"><input class="form-control input-sm text-center" id="lot_montantmini"></div>';
				textbox += '<div class="col-md-3"><label for="lot_montantmaxi">Montant maxi:</label></div><div class="col-md-3"><input class="form-control input-sm text-center" id="lot_montantmaxi"></div>';
				textbox += '</div></div></form>';
				var dialog = bootbox.dialog({
					title: 'Nouveau lot',
					message: textbox,
					buttons: {
						ok: {
							label: "Cr�er",
							className: 'btn-success',
							callback: function(){
								$.ajax({
									type: "POST",
									url: "./marche.php",
									data: "action=addlot&idmarche="+idmarche+"&numero="+$("#lot_numero").val()+"&montantmini="+$("#lot_montantmini").val()+"&montantmaxi="+$("#lot_montantmaxi").val(),
									success: function(resul){
										dialog.modal('hide');
										detail_marche(idmarche);
									}
								});
							}
						},
						cancel: {
							label: "Annuler",
							className: 'btn-default',
							callback: function(){
								dialog.modal('hide');
							}
						}
					}
				});
			});
			$("#add_tranche").click(function(){
				var myVar; var elements;
				var textbox = '<form><div class="form-group row"><div class="col-md-12"><div class="col-md-3"><label for="type_tranche">Type tranche</label></div><div class="col-md-9"><select class="form-control input-sm" id="type_tranche"></select></div></div></div>';
				textbox += '</form>';
				$.ajax({
					type: "POST",
					url: "./marche.php",
					data: "action=lsttypetranche",
					success: function(resul){
						$("#type_tranche").html(resul);
					}
				});
				var dialog = bootbox.dialog({
					title: '<i class="fa fa-bars"></i> Nouvelle tranche',
					message: textbox,
					buttons: {
						ok: {
							label: "Cr�er",
							className: 'btn-success',
							callback: function(){
								$.ajax({
									type: "POST",
									url: "./marche.php",
									data: "action=addtranche&idmarche="+idmarche+"&type="+$("#type_tranche").val(),
									success: function(resul){
										dialog.modal('hide');
										detail_marche(idmarche);
									}
								});
							}
						},
						cancel: {
							label: "Annuler",
							className: 'btn-default',
							callback: function(){
								dialog.modal('hide');
							}
						}
					}
				});
			});
			$(".deltranche").click(function(){
				var idtranche = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./marche.php",
						data: "action=deltranche&idtranche="+idtranche,
						success: function(resul){
							swal("Supprim�!", "La tranche a bien �t� supprim�.", "success");
							detail_marche(idmarche);
						}
					});
				});
			});
		}
	});
}
function update_marche(idmarche,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updatemarche&idmarche="+idmarche+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=detailgenemarche&idmarche="+idmarche,
				success: function(resul){
					$("#detailgene_marche").html(resul);
				}
			});
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=detailjalonmarche&idmarche="+idmarche,
				success: function(resul){
					$("#detjalon").html(resul);
					$(".modifchpdatemarche").mask("99/99/9999");
				}
			});
		}
	});
}
function update_entmarche(idmarche,id,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updateentmarche&id="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=lstentmarche&idmarche="+idmarche,
				success: function(resul){
					$("#detentmarche").html(resul);
				}
			});
		}
	});
}
function update_datemarche(idmarche,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updatedatemarche&idmarche="+idmarche+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=detailgenemarche&idmarche="+idmarche,
				success: function(resul){
					$("#detailgene_marche").html(resul);
				}
			});
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=detailjalonmarche&idmarche="+idmarche,
				success: function(resul){
					$("#detjalon").html(resul);
					$(".modifchpdatemarche").mask("99/99/9999");
				}
			});
		}
	});
}
function update_indicemarche(idmarche,id,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updateindicemarche&id="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=lstindicemarche&idmarche="+idmarche,
				success: function(resul){
					$("#detindicemarche").html(resul);
				}
			});
		}
	});
}
function update_formulemarche(idmarche,id,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updateformulemarche&id="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=lstformulemarche&idmarche="+idmarche,
				success: function(resul){
					$("#detformulemarche").html(resul);
				}
			});
		}
	});
}

/* Lot */
function detail_lot(idlot){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=detaillot&idlot="+idlot,
		success: function(resul){
			$("#contentpage").html(resul);
			$(".modifchpdatelot").mask("99/99/9999");
			$("#back_marche").click(function(){
				detail_marche($(this).data("idmarche"));
			});
			$("body").on("click",".del_entlot",function(){
				var idlot = $(this).data("idlot");
				var id = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./marche.php",
						data: "action=delentlot&id="+id,
						success: function(resul){
							$.ajax({
								type: "POST",
								url: "./marche.php",
								data: "action=lstentlot&idlot="+idlot,
								success: function(resul){
									swal("Supprim�!", "L'entreprise a bien �t� supprim�.", "success");
									$("#detentlot").html(resul);
								}
							});
						}
					});
				});
			});
			$("body").on("click",".del_indicelot",function(){
				var idlot = $(this).data("idlot");
				var id = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./marche.php",
						data: "action=delindicelot&id="+id,
						success: function(resul){
							$.ajax({
								type: "POST",
								url: "./marche.php",
								data: "action=lstindicelot&idlot="+idlot,
								success: function(resul){
									swal("Supprim�!", "L'indice a bien �t� supprim�.", "success");
									$("#detindicelot").html(resul);
								}
							});
						}
					});
				});
			});
			$("body").on("click",".del_formulelot",function(){
				var idlot = $(this).data("idlot");
				var id = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./marche.php",
						data: "action=delformulelot&id="+id,
						success: function(resul){
							$.ajax({
								type: "POST",
								url: "./marche.php",
								data: "action=lstformulelot&idlot="+idlot,
								success: function(resul){
									swal("Supprim�!", "La formule a bien �t� supprim�.", "success");
									$("#detformulelot").html(resul);
								}
							});
						}
					});
				});
			});
		}
	});
}
function update_lot(idlot,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updatelot&idlot="+idlot+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=detailgenelot&idlot="+idlot,
				success: function(resul){
					$("#detailgene_lot").html(resul);
				}
			});
		}
	});
}
function update_entlot(idlot,id,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updateentlot&id="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=lstentlot&idlot="+idlot,
				success: function(resul){
					$("#detentlot").html(resul);
				}
			});
		}
	});
}
function update_datelot(idlot,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updatedatelot&idlot="+idlot+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=detailgenelot&idlot="+idlot,
				success: function(resul){
					$("#detailgene_lot").html(resul);
				}
			});
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=detailjalonlot&idlot="+idlot,
				success: function(resul){
					$("#detjalon_lot").html(resul);
					$(".modifchpdatelot").mask("99/99/9999");
				}
			});
		}
	});
}
function update_indicelot(idlot,id,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updateindicelot&id="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=lstindicelot&idlot="+idlot,
				success: function(resul){
					$("#detindicelot").html(resul);
				}
			});
		}
	});
}
function update_formulelot(idlot,id,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updateformulelot&id="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=lstformulelot&idlot="+idlot,
				success: function(resul){
					$("#detformulelot").html(resul);
				}
			});
		}
	});
}

/* Paiement */
function detail_paiement(idpaie){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=detailpaiement&idpaie="+idpaie,
		success: function(resul){
			$("#contentpage").html(resul);
			$(".chpdatepaie").mask("99/99/9999");
		}
	});
	$("body").on("change","#lstbordapd",function(){
		var idbord = $(this).val();
		var idventpaie = $(this).data("idventpaie");
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=lstarticlebord&idbord="+idbord+"&idventpaie="+idventpaie,
			success: function(resul){
				$("#lstarticlesapd").html(resul);
			}
		});
	});
	$("body").on("click",".add_valventpaie",function(){
		var id_art = $(this).data("idarticle");
		var id_ventpaie = $(this).data("idventpaie");
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=addvalventpaie&idventpaie="+id_ventpaie+"&idart="+id_art,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./marche.php",
					data: "action=lstvalventpaie&idventpaie="+id_ventpaie,
					success: function(resul){
						$("#lstvalventpaie").html(resul);
					}
				});
			}
		});
	});
	$("body").on("click",".del_valventpaie",function(){
		var id_valventpaie = $(this).data("idvalventpaie");
		var id_ventpaie = $(this).data("idventpaie");
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=delvalventpaie&idvalventpaie="+id_valventpaie,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./marche.php",
					data: "action=lstvalventpaie&idventpaie="+id_ventpaie,
					success: function(resul){
						$("#lstvalventpaie").html(resul);
					}
				});
			}
		});
	});
	$("body").on("click",".visu_paieventcht",function(){
		$(".visu_paieventcht").each(function(){
			$(this).removeClass("active");
		});
		$(this).addClass("active");
		var idcht = $(this).data("idcht");
		var idpaie = $(this).data("idpaie");
		var id = $(this).data("id");
		$.ajax({
			type: "POST",
			url: "./marche.php",
			data: "action=apdventchtpaiement&idpaie="+idpaie+"&idcht="+idcht,
			success: function(resul){
				$("#detailventcht_paiement").html(resul);
			}
		});
	});
}
function update_valventpaie(idventpaie,id,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		dataType: 'json',
		data: "action=updatevalventpaie&idvalventpaie="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			var idpaie = resul[0];
			var idcht = resul[1];
			/* Mise � jour du tableau */
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=tabventchtpaiement&idpaie="+idpaie,
				success: function(resul){
					$("#tabventcht_paiement").html(resul);
				}
			});
			/* Mise � jour colonne gauche */
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=apdventchtpaiement&idpaie="+idpaie+"&idcht="+idcht,
				success: function(resul){
					$("#detailventcht_paiement").html(resul);
				}
			});
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=lstvalventpaie&idventpaie="+idventpaie,
				success: function(resul){
					$("#lstvalventpaie").html(resul);
				}
			});
		}
	});
}

function update_paiement(idpaie,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updatepaiement&idpaie="+idpaie+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=detailgenepaiement&idpaie="+idpaie,
				success: function(resul){
					$("#detailgene_paiement").html(resul);
					$(".chpdatepaie").mask("99/99/9999");
				}
			});
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=detailjalonpaiement&idpaie="+idpaie,
				success: function(resul){
					$("#detailjalon_paiement").html(resul);
					$(".chpdatepaie").mask("99/99/9999");
				}
			});
		}
	});
}
/* OS */
function detail_os(idos){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=detailos&idos="+idos,
		success: function(resul){
			$("#contentpage").html(resul);
			$(".chpdateos").mask("99/99/9999");
		}
	});
}
function update_os(idos,chp,val){
	$.ajax({
		type: "POST",
		url: "./marche.php",
		data: "action=updateos&idos="+idos+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./marche.php",
				data: "action=detailgeneos&idos="+idos,
				success: function(resul){
					$("#detailgene_os").html(resul);
					$(".chpdateos").mask("99/99/9999");
				}
			});
		}
	});
}