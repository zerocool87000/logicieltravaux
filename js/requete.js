var tabfieldrequete = [];var tabfilterrequete = [];var tabtrirequete = [];var tabgrouperequete = [];
$(document).ready(function(){
	$(".fa").qtip({style: { classes: 'qtip-dark qtip-rounded qtip-shadow' }});
    $("#menu_requete").click(function(){
		var elementmenu = $(this).parent();
		$('.sider-menu li').each(function(){
			$(this).removeClass("active");
		});
		elementmenu.addClass("active");
    	menu_requete();
    });
    $("body").on("click",".delfieldfilter_requete",function(){
    	var chp = $(this).data("field");
    	var dropzone = $(this).data("zone");
		index = tabfilterrequete.indexOf(chp);
		tabfilterrequete.splice(index,1);
		remplir_drop(dropzone);
    });
    $("body").on("click",".delfieldtri_requete",function(){
    	var chp = $(this).data("field");
    	var dropzone = $(this).data("zone");
		index = tabtrirequete.indexOf(chp);
		tabtrirequete.splice(index,1);
		remplir_drop(dropzone);
    });
    $("body").on("click",".delfieldgroupe_requete",function(){
    	var chp = $(this).data("field");
    	var dropzone = $(this).data("zone");
		index = tabgrouperequete.indexOf(chp);
		tabgrouperequete.splice(index,1);
		remplir_drop(dropzone);
    });
	$("body").on("change",".selfilter",function(){
		var chp = $(this).data("chp");
		var val = $(this).val();
		var val2 = $(this).parent().parent().children().children(".inputfilter").val();
		if (val!=""){
			switch (val){
				case "contient": where = chp + " LIKE %"+val2+"%";
					break;
				case "egale": where = chp + "="+val2;
					break;
			}
	    	//alert(where);
	    }
	    run_requete();
/*		var index = items.indexOf(3452);
		if (index !== -1) {
			items[index] = 1010;
		}
*/    
	});
	$("body").on("change",".inputfilter",function(){
		var chp = $(this).data("chp");
		var val = $(this).val();
		var val2 = $(this).parent().parent().children().children(".inputfilter").val();
		if (val!=""){
			switch (val){
				case "contient": where = chp + " LIKE %"+val2+"%";
					break;
				case "egale": where = chp + "="+val2;
					break;
			}
	    	//alert(where);
	    }
	    run_requete();
/*		var index = items.indexOf(3452);
		if (index !== -1) {
			items[index] = 1010;
		}
*/    
	});
	$('[data-toggle="toggle"]').change(function(){
		$(this).parents().next('.toto').toggle();
	});
});

function menu_requete(){
	var box = "<div class='col-md-12'>";
/*	box += '<div class="well well-sm btn-toolbar">';
	box += '<div class="btn-group">';
	box += '<div class="btn-group" role="group">';
	box += '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> Actions <span class="caret"></span></button>';
	box += '<ul class="dropdown-menu" style="font-size:9pt;">';
	box += '<li><a href="#" id="add_demandebc"><i class="fa fa-plus-circle"></i> Cr�er une demande BC</a></li>';
	box += '<li><a href="#" id="suivi_demandebcope"><i class="fa fa-exclamation-circle"></i> Suivi des demandes BC</a></li>';
	box += '<li><a href="#" id="suivi_demandeosope"><i class="fa fa-exclamation-circle"></i> Suivi des demandes OS</a></li>';
	box += '</ul>';
	box += '</div>';
	box += '<button type="button" id="add_operation" class="btn btn-sm btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Operations</button>';
	box += '</div>';
	box += '<button type="button" id="btn_filtreope" class="btn btn-sm btn-default"><i class="fa fa-filter" aria-hidden="true"></i></button>';
	box += '</div>';

	box += '<div class="well well-sm" id="block_filter">';
	box += '<div class="row"><label class="col-sm-2" for="filter_crit">Crit�res:</label><div class="col-sm-3">';
	box += '<select class="form-control input-sm" id="filter_crit" name="filter_crit">';
	box += '<option value="-1">Choisir un crit�re</option><option value="operations.code_insee">Commune</option><option value="operations.numero">Num�ro</option><option value="operations.categorie">Cat�gorie</option>';
	box += '</select>';
	box += '</div><label class="col-sm-2" for="filter_val">Valeur:</label><div class="col-sm-3"><input id="filter_val" name="filter_val" class="form-control input-sm"></div>';
	box += '<div class="col-sm-1"><button type="button" id="btn_filtrestart" class="btn btn-sm btn-default"><i class="fa fa-search" aria-hidden="true"></i></button></div>';
	box += '<div class="col-sm-1"><button type="button" id="btn_filtreerase" class="btn btn-sm btn-default"><i class="fa fa-trash" aria-hidden="true"></i></button></div>';
	box += '</div>';
	box += '</div>';
*/
	box += "<div class='portlet'><div class='portlet-title'><i class='fas fa-database' aria-hidden='true'></i> Requ�tes </div><div class='portlet-content' id='fenrequete'></div></div>";
	box += "</div>";
	$("#contentpage").html(box);
	tabfieldrequete = [];tabfilterrequete = [];tabtrirequete = [];tabgrouperequete = [];
	$.ajax({
		type: "POST",
		url: "./requete.php",
		data: "action=loadrequete",
		success: function(resul){
			tabfieldrequete = [];
			$("#fenrequete").html(resul);
			var dragItems = document.querySelectorAll('.dragfieldrequete');

			[].forEach.call(dragItems, function(dragItem) {
				dragItem.addEventListener('dragstart', handleDragStart, false);
			});
			var dropItems = document.querySelectorAll('.dropfieldrequete');

			[].forEach.call(dropItems, function(dropItem) {
				dropItem.addEventListener('dragenter', handleDragEnter, false);
				dropItem.addEventListener('dragleave', handleDragLeave, false);
				dropItem.addEventListener('dragover', handleDragOver, false);
				dropItem.addEventListener('drop', handleDrop, false);
			});

			$(".chkfieldrequete").click(function(){
				if ( $( this ).prop( "checked" ) ){
					var id = $(this).data("id");
					tabfieldrequete.push(id);
				}else{
					var id = $(this).data("id");
					index = tabfieldrequete.indexOf(id);
					tabfieldrequete.splice(index,1);
				}
				run_requete();
			});

			$("#btnexportresultat").click(function(){
				$("#tabresultatrequete").table2excel({
					exclude: ".noExl",
					name: "Excel Document Name",
					filename: "R�sultat",
					fileext: ".xls",
					exclude_img: true,
					exclude_links: true,
					exclude_inputs: true
				});
			});
		}
	});
}

function run_requete(){
	tabfield = tabfieldrequete.join(',');
	tabgroupe = tabgrouperequete.join(',');
	tabtri = tabtrirequete.join(',');
	var where="";
	$(".selfilter").each(function(){
		var chp = $(this).data("chp");
		var val = $(this).val();
		var val2 = $(this).parent().parent().children().children(".inputfilter").val();
		if (val!=""){
			switch (val){
				case "contient": if (where==""){where += chp + " LIKE '%"+val2+"%'";}else{where += " AND " + chp + " LIKE '%"+val2+"%'";};
					break;
				case "egale": if (where==""){where += chp + "='"+val2+"'";}else{where += " AND " + chp + "="+val2+"'";};
					break;
				case "in": if (where==""){where += chp + " IN ("+val2+")";}else{where += " AND " + chp + " IN ("+val2+")";};
					break;
			}
	    }
	});

	$.ajax({
		type: "POST",
		url: "./requete.php",
		data: "action=executerequete&tabfield="+tabfield+"&tabgroupe="+tabgroupe+"&tabtri="+tabtri+"&where="+where,
		beforeSend: function(){
			$("#detailview_requete").html('<div class="well well-sm text-center">Ex�cution en cours...</div>');
		},
		success: function(resul){
			$("#detailview_requete").html(resul);
		}
	});
}

var dragSrcEl = null;

function handleDragStart(e) {
  // Target (this) element is the source node.
  dragSrcEl = this;

  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text', $(this).data("chp"));
 }

 function handleDragEnter(e) {
 	this.style.borderStyle = 'dashed';
	// this / e.target is the current hover target.
}

function handleDragLeave(e) {
  this.style.borderStyle = 'solid';
}

function handleDragOver(e) {
	if (e.preventDefault) {
		e.preventDefault(); // Necessary. Allows us to drop.
	}
	return false;
}
function handleDrop(e) {
	e.preventDefault();
	//alert("Vous avez d�pos� votre champ!");
	var data = e.dataTransfer.getData("text");
	//alert(data);
	if (e.stopPropagation) {
		e.stopPropagation(); // Stops some browsers from redirecting.
	}
	//alert(this.id);
	var dropzone = this.id;
	switch (this.id){
		case "dropfieldfilter_requete":
			var index = tabfilterrequete.indexOf(data);
			if (index == -1){
				tabfilterrequete.push(data);
				remplir_drop(dropzone);
			}else{
				alert("Champs d�j� s�lectionn�");
			}
			break;
		case "dropfieldtri_requete":
			var index = tabtrirequete.indexOf(data);
			if (index == -1){
				tabtrirequete.push(data);
				remplir_drop(dropzone);
			}else{
				alert("Champs d�j� s�lectionn�");
			}
			break;
		case "dropfieldgroupe_requete":
			var index = tabgrouperequete.indexOf(data);
			if (index == -1){
				tabgrouperequete.push(data);
				remplir_drop(dropzone);
			}else{
				alert("Champs d�j� s�lectionn�");
			}
			break;
	}
	this.style.borderStyle = 'solid';
}

function remplir_drop(dropzone){
	switch (dropzone){
		case "dropfieldfilter_requete":
			var text="";
			tabfilterrequete.forEach(function(element) {
				text += '<div class="chpinrequete"><div class="row"><div class="col-sm-12">'+element+'</div>';
				text += '<div class="col-sm-5"><select data-chp="'+element+'" class="form-control input-sm selfilter">';
				text += '<option value="">Op�rateur...</option><option value="contient">Contient</option><option value="egale">Egale �</option><option value="in">Dans la liste</option>';
				text += '</select></div>';
				text += '<div class="col-sm-5"><input class="form-control input-sm inputfilter"></div><div class="col-sm-1"><button class="btn btn-default btn-sm delfieldfilter_requete" data-zone="'+dropzone+'" data-field="'+element+'"><i class="fa fa-times"></i></button></div></div></div>';
			});
			if (text==""){text="D�poser vos champs ici";}
			$("#"+dropzone).html(text);
			break;
		case "dropfieldtri_requete":
			var text="";
			tabtrirequete.forEach(function(element) {
				text += '<div class="chpinrequete"><div class="row"><div class="col-sm-5">'+element+'</div><div class="col-sm-5"><select class="form-control input-sm"><option>Croissant</option><option>D�croissant</option></select></div><div class="col-sm-1"><button class="btn btn-default btn-sm delfieldtri_requete" data-zone="'+dropzone+'" data-field="'+element+'"><i class="fa fa-times"></i></button></div></div></div>';
			});
			if (text==""){text="D�poser vos champs ici";}
			$("#"+dropzone).html(text);
			break;
		case "dropfieldgroupe_requete":
			var text="";
			tabgrouperequete.forEach(function(element) {
				text += '<div class="chpinrequete"><div class="row"><div class="col-sm-8">'+element+'</div><div class="col-sm-4"><button class="btn btn-default btn-sm"><i class="fa fa-caret-down"></i></button><button class="btn btn-default btn-sm delfieldgroupe_requete" data-zone="'+dropzone+'" data-field="'+element+'"><i class="fa fa-times"></i></button></div></div></div>';
			});
			if (text==""){text="D�poser vos champs ici";}
			$("#"+dropzone).html(text);
			break;
	}
	run_requete();
}