function formvalidate(form){
	var valid = $.Deferred();
	var nberror = 0;
	$('form#'+form).find('input').each(function(){
		if($(this).prop('required')){
			if($(this).val()==""){
				$(this).addClass("alert alert-danger");
				nberror++;
			}else{
				$(this).removeClass("alert alert-danger");
			}
		}
	});
	if (nberror>0){
		valid.resolve("false");	
	}else{
		valid.resolve("true");	
	}
	return valid.promise();
}

function fieldvalidate(chp){
	var valid = $.Deferred();
	var nberror = 0;
	var elem = $("#"+chp);
	if(elem.prop('required')){
		if(elem.val()=="" || elem.val()=="0"){
			elem.addClass("alert alert-danger");
			nberror++;
		}else{
			elem.removeClass("alert alert-danger");
		}
	}
	if (nberror>0){
		valid.resolve("false");	
	}else{
		valid.resolve("true");	
	}
	return valid.promise();	
}