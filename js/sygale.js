$(document).ready(function(){
	$(".fa").qtip({style: { classes: 'qtip-dark qtip-rounded qtip-shadow' }});
    //$("#menu-toggle").click(function(e) {
    //    e.preventDefault();
    //    $("#wrapper").toggleClass("nav navbar-nav sider-navbar active");
    //});
/*    if (!getCookie("id")){
		boxmodal = '<div class="modal fade" data-backdrop="static"  id="myModal" role="dialog">';
		boxmodal += '<div class="modal-dialog modal-lg">';
		boxmodal += '<div class="modal-content">';
		boxmodal += '<div class="modal-header" style="background-color:#eee;">';
		boxmodal += '<h4 class="modal-title">Erreur!!!</h4>';
		boxmodal += '</div>';
		boxmodal += '<div class="modal-body" id="content_visite"><p>';
		boxmodal += '<div class="alert alert-danger text-center"><h1><strong>Attention!</strong> Acc�s interdit.</h1></div>';
		boxmodal += '<div class="jumbotron text-center">';
		boxmodal += '<h1 style="font-size:100px;"><strong><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></strong></h1>';
		boxmodal += '<h1>Authentification requise !!</h1>';
		boxmodal += '<div><img width="20%" src="../images/logo.png"></div>';
		boxmodal += '</div>';
		boxmodal += '</p></div>';
		boxmodal += '</div>';
		boxmodal += '</div>';
		boxmodal += '</div>';
		$('body').remove("myModal");
		$('body').append(boxmodal);
		$("#myModal").modal('show');
    	var myVar = setInterval(function(){ window.location.replace("../index.php"); }, 5000);
    }
    loadCommune();
*/
    $("#lstcom").change(function(){
    	//alert($(this).val());
    	TBCom($(this).val());
    });
    dashboard();
    infoUser();
    $(".quitter").click(function(){
    	quitter();
    });
    $("#menu_chantier").click(function(){
    	menu_chantier();
    });
    $("#menu_marche").click(function(){
    	menu_marche();
    });
    $("#menu_print").click(function(){
    	menu_print();
    });
    $("#settings").click(function(){
    	settings();
    });
    $("#menu_dashboard").click(function(){
    	dashboard();
    });
    $("body").on("change",".modifchpoperation",function(){
		var idope = $(this).data("idope");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_operation(idope,chp,val);
    });
    /* fonctions onglet sp�cificit�*/
	$("body").on("click","#add_specif",function(){
		var idcht = $(this).data("idcht");
		var nat = $(this).data("nat");
		var table = $(this).data("table");
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=addspecif&idcht="+idcht+"&table="+table,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./"+nat+".php",
					data: "action=detailspecif&idcht="+idcht+"&nat="+nat,
					success: function(resul){
						$("#detspecif").html(resul);
					}
				});
			}
		});
	});
	$("body").on("change",".update_specif",function(){
		var chp = $(this).data("chp");
		var type = $(this).data("type");
		var id = $(this).data("id");
		var idcht = $(this).data("idcht");
		var nat = $(this).data("nat");
		var table = $(this).data("table");
		var val = $(this).val();
		if(type=="chk"){
			if ( $( this ).prop( "checked" ) ){val=1;}else{val=0;}					
		} 
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=updatespecif&id="+id+"&chp="+chp+"&val="+val+"&nat="+nat+"&table="+table,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./"+nat+".php",
					data: "action=detailspecif&idcht="+idcht+"&nat="+nat,
					success: function(resul){
						$("#detspecif").html(resul);
					}
				});
			}
		});
	});
	$("body").on("click",".del_specif",function(){
		var id = $(this).data("id");
		var idcht = $(this).data("idcht");
		var nat = $(this).data("nat");
		var table = $(this).data("table");
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=delinspecif&id="+id+"&table="+table,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./"+nat+".php",
					data: "action=detailspecif&idcht="+idcht+"&nat="+nat,
					success: function(resul){
						$("#detspecif").html(resul);
					}
				});
			}
		});
	});
});

function quitter(){
	//eraseCookie("id");
	eraseCookie("login");
	$(location).attr('href',"../index.php");	
}

function menu_print(){
	var uri;
	$.ajax({
		type: "POST",
		url: "./print.php",
		data: "action=lstprint",
		success: function(resul){
			$("#contentpage").html(resul);
			$(".btn_export").click(function(){
				var format = $(this).data("format");
				window.open("./exportreport.php?url="+uri+"&format="+format);
			});
			$("#print_lstdossier li").click(function(){
				$("#print_lstdossier li").each(function(){
					$(this).removeClass("active_report");
				});
				$(this).addClass("active_report");
				uri = $(this).data("url");
				$.ajax({
					type: "POST",
					url: "./print.php",
					data: "action=printaffichereport&url="+uri,
					beforeSend: function() {
						$("#print_apercureport").html("<div class='well well-sm text-center'>Chargement en cours...</div>");
					},
					success: function(resul){
						$("#print_apercureport").html(resul);
					}
				});
			});
		}
	});

}
function menu_chantier(){
	var box = "<div class='col-md-12'>";
	box += '<div class="well well-sm btn-toolbar">';
	box += '<div class="btn-group">';
	box += '<div class="btn-group" role="group">';
	box += '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> Demande BC <span class="caret"></span></button>';
	box += '<ul class="dropdown-menu" style="font-size:9pt;">';
	box += '<li><a href="#" id="add_demandebc"><i class="fa fa-plus-circle"></i> Cr�er une demande</a></li>';
	box += '<li><a href="#" id="suivi_demandebcope"><i class="fa fa-exclamation-circle"></i> Suivi des demandes</a></li>';
	box += '</ul>';
	box += '</div>';
	box += '<button type="button" id="add_operation" class="btn btn-sm btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Operations</button>';
	box += '</div>';
	box += '<button type="button" id="btn_filtreope" class="btn btn-sm btn-default"><i class="fa fa-filter" aria-hidden="true"></i></button>';
	box += '</div>';

	box += '<div class="well well-sm" id="block_filter">';
	box += '<div class="row"><label class="col-sm-2" for="filter_crit">Crit�res:</label><div class="col-sm-3">';
	box += '<select class="form-control input-sm" id="filter_crit" name="filter_crit">';
	box += '<option value="-1">Choisir un crit�re</option><option value="operations.code_insee">Commune</option><option value="operations.numero">Num�ro</option><option value="operations.categorie">Cat�gorie</option>';
	box += '</select>';
	box += '</div><label class="col-sm-2" for="filter_val">Valeur:</label><div class="col-sm-3"><input id="filter_val" name="filter_val" class="form-control input-sm"></div>';
	box += '<div class="col-sm-1"><button type="button" id="btn_filtrestart" class="btn btn-sm btn-default"><i class="fa fa-search" aria-hidden="true"></i></button></div>';
	box += '<div class="col-sm-1"><button type="button" id="btn_filtreerase" class="btn btn-sm btn-default"><i class="fa fa-trash" aria-hidden="true"></i></button></div>';
	box += '</div>';
	box += '</div>';

	box += "<div class='portlet'><div class='portlet-title'><i class='fa fa-wrench' aria-hidden='true'></i> Op�rations </div><div class='portlet-content' id='lstoperation'></div></div>";
	box += "</div>";
	$("#contentpage").html(box);
	$("#block_filter").toggle();
	$("#btn_filtreope").click(function(){
		$("#block_filter").toggle("slow");
		if ($(this).hasClass("active")){$(this).removeClass("active");}else{$(this).addClass("active");}
	});
	$("#btn_filtrestart").click(function(){
		var chp = $("#filter_crit").val();
		var val = $("#filter_val").val();
		lst_operation(1,"",chp,val);
	});
	$("#filter_crit").change(function(){
		$("#filter_val").val("");
	});
	$("#btn_filtreerase").click(function(){
		$("#filter_crit").val("");
		$("#filter_val").val("");
		lst_operation(1,getCookie("login"),"","");
	});
	lst_operation(1,getCookie("login"),"","");
	$("#suivi_demandebcope").click(function(){
		var textbox = '<div class="row"><div class="col-md-12"><div id="tablstdemandebc"></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./chantier.php",
			data: "action=lstdemandebc",
			success: function(resul){
				$("#tablstdemandebc").html(resul);
				$('.showcht').each(function(){
					$(this).nextUntil('tr.showcht').slideToggle();
				});
				$('.showcht').click(function(){
					$(this).nextUntil('tr.showcht').slideToggle();
				});
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-exclamation-circle"></i> Suivi des demandes BC',
			message: textbox,
			buttons: {
				cancel: {
					label: "Fermer",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
	$("#add_operation").click(function(){
		var textbox = '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Cat�gories</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectnouvope_categorie" required><option></option></select></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./chantier.php",
			data: "action=lstcategorie",
			success: function(resul){
				$("#selectnouvope_categorie").html(resul);
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-wrench"></i> Nouvelle op�ration',
			message: textbox,
			buttons: {
				ok: {
					label: "Cr�er",
					className: 'btn-success',
					callback: function(){
						fieldvalidate("selectnouvope_categorie").then(function(answer){
							var ansbool = answer.toString();
							if (ansbool=="true"){
								$.ajax({
									type: "POST",
									url: "./chantier.php",
									data: "action=addoperation&idcat="+$("#selectnouvope_categorie").val(),
									success: function(resul){
										dialog.modal('hide');
										lst_operation(1,getCookie("login"),"","");
									}
								});
							}else{
							swal("Erreur!", "Cat�gorie absente.", "danger");								
							}
						});
					}
				},
				cancel: {
					label: "Annuler",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
	$("#add_demandebc").click(function(){
		var myVar; var elements;
		var textbox = '<div class="row"><div class="col-md-12">';
		textbox += '<div class="col-md-3"><label>D�lai (sem.)</label></div><div class="col-md-2"><input class="form-control input-sm text-center" id="demandebc_delai"></div>';
		textbox += '<div class="col-md-3"><label>Entreprises</label></div><div class="col-md-4"><select class="form-control input-sm" id="demandebc_entreprise"></select></div>';
		textbox += '</div></div>';
		textbox += '<br><div class="row"><div class="col-md-12">';
		textbox += '<div class="col-md-3"><label>N� conv. EP</label></div><div class="col-md-3"><input class="form-control input-sm text-center" id="demandebc_numconvep"></div>';
		textbox += '<div class="col-md-3"><label>Date</label></div><div class="col-md-3"><input class="form-control input-sm text-center" id="demandebc_dateconvep"></div>';
		textbox += '</div></div>';
		//textbox += '<br><div class="row"><div class="col-md-12"><div class="col-md-2"><label>Op�rations</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectdembc_operation"><option></option></select></div></div></div>';
		textbox += '<br><div class="row"><div class="col-md-12"><div class="col-md-2"><label>Op�rations</label></div><div class="col-md-10"><input class="form-control input-sm" id="selectdembc_operation"></div></div></div>';
		textbox += '<br><div class="row"><div class="col-md-12"><div id="lstchantierdembc"></div></div></div>';
		textbox += '<br><div class="row"><div class="col-md-12"><div id="lstdembc"></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./chantier.php",
			data: "action=lstentreprise",
			success: function(resul){
				$("#demandebc_entreprise").html(resul);
			}
		});
		$.ajax({
			type: "POST",
			url: "./chantier.php",
			data: "action=lstoperationdembc",
			success: function(resul){
				//$("#selectdembc_operation").html(resul);
				var tabdembccht = [];
				myVar = setInterval(function(){
					elements = tabdembccht.join(',');
					$.ajax({
						type: "POST",
						url: "./chantier.php",
						data: "action=lstchantieropedembctemp&tabcht="+elements,
						success: function(resul){
							$("#lstdembc").html(resul);
						}
					});
				}, 1000);
				$("#selectdembc_operation").change(function(){
					var idope = $(this).val();
					if (idope!='-1'){
						elements = tabdembccht.join(',');
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstchantieropedembc&idope="+idope+"&tabcht="+elements,
							success: function(resul){
								$("#lstchantierdembc").html(resul);
								$(".chkchtdemandebc").click(function(){
									if ( $( this ).prop( "checked" ) ){
										var id = $(this).data("id");
										tabdembccht.push(id);
									}else{
										var id = $(this).data("id");
										index = tabdembccht.indexOf(id);
										tabdembccht.splice(index,1);
									}
								});
							}
						});
					}
				});
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-shopping-basket"></i> Nouvelle Demande de BC',
			message: textbox,
			buttons: {
				ok: {
					label: "Envoyer",
					className: 'btn-success',
					callback: function(){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=adddemandebc&numconvep="+$("#demandebc_numconvep").val()+"&dateconvep="+$("#demandebc_dateconvep").val()+"&ident="+$("#demandebc_entreprise").val()+"&delai="+$("#demandebc_delai").val()+"&tabcht="+elements,
							success: function(resul){
								dialog.modal('hide');
								clearInterval(myVar);
								lst_operation(1,getCookie("login"),"","");
							}
						});
					}
				},
				cancel: {
					label: "Annuler",
					className: 'btn-default',
					callback: function(){
						clearInterval(myVar);
						dialog.modal('hide');
					}
				}
			}
		});
		$("#selectdembc_operation").mask("99-aaa-999");
		$("#demandebc_dateconvep").mask("99/99/9999");

	});
}

function lst_operation(page,login,chp,val){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=lstoperation&page="+page+"&login="+login+"&chp="+chp+"&val="+val,
		success: function(resul){
			$("#lstoperation").html(resul);
			$(".forward").click(function(){
				if(page>1){lst_operation(page-1,getCookie("login"),"","");}
			});
			$(".next").click(function(){
				lst_operation(page+1,getCookie("login"),"","");
			});

			$(".visuope").click(function(){
				var id = $(this).data("id");
				detail_operation(id);
			});
			$(".delope").click(function(){
				var idope = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./chantier.php",
						data: "action=deloperation&idope="+idope,
						success: function(resul){
							swal("Supprim�!", "L'op�ration a bien �t� supprim�.", "success");
							lst_operation(1,getCookie("login"),"","");
						}
					});
				});
			});
		}
	});
}
function update_operation(idope,chp,val){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=updateoperation&idope="+idope+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./chantier.php",
				data: "action=detailgeneoperation&idope="+idope,
				success: function(resul){
					$("#detailgene_operation").html(resul);
				}
			});
		}
	});
}

function detail_bcoperation(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailbcoperation&idope="+idope,
		success: function(resul){
			$("#detbcoperation").html(resul);
		}
	});
}
function detail_operation(id){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailoperation&id="+id,
		success: function(resul){
			$("#contentpage").html(resul);
			//$("#annee_prog").mask("99/99/9999");
			$("#ope_datedemande").mask("99/99/9999");
			$("#ope_refconcess").mask("DC28/999999");
			detail_jalon(id);
			detail_coordination(id);
			detail_autorisation(id);
			detail_bcoperation(id);
			$("#back_lstoperations").click(function(){
				menu_chantier();
			});
			$(".visucht").click(function(){
				var idcht = $(this).data("id");
				detail_chantier(idcht);
			});
			$(".delcht").click(function(){
				var idcht = $(this).data("id");
				var idope = $(this).data("idope");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./chantier.php",
						data: "action=delchantier&idcht="+idcht,
						success: function(resul){
							swal("Supprim�!", "La prestation a bien �t� supprim�.", "success");
							detail_operation(idope);
						}
					});
				});
			});
			$("#add_chantier").click(function(){
				var idope = $(this).data("idope");
				var textbox = '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Natures</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectnouvcht_nature"><option></option></select></div></div></div>';
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstnature",
					success: function(resul){
						$("#selectnouvcht_nature").html(resul);
					}
				});

				var dialog = bootbox.dialog({
					title: '<i class="fa fa-wrench"></i> Nouvelle prestation',
					message: textbox,
					buttons: {
						ok: {
							label: "Cr�er",
							className: 'btn-success',
							callback: function(){
								$.ajax({
									type: "POST",
									url: "./chantier.php",
									data: "action=addchantier&idope="+idope+"&idnat="+$("#selectnouvcht_nature").val(),
									success: function(resul){
										dialog.modal('hide');
										detail_operation(id);
									}
								});
							}
						},
						cancel: {
							label: "Annuler",
							className: 'btn-default',
							callback: function(){
								dialog.modal('hide');
							}
						}
					}
				});
			});
		}
	});
}
function detail_jalon(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailjalon&idope="+idope,
		success: function(resul){
			$("#detjalon").html(resul);
			$(".chpjal").mask("99/99/9999");
			$(".chpjal").change(function(){
				var idope=$(this).data("idope");
				var idjal=$(this).data("idjal");
				var val=$(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatedatejalonoperation&idope="+idope+"&idjal="+idjal+"&val="+val,
					success: function(resul){
						detail_jalon(idope);
					}
				});
			});
		}
	});
}
function detail_coordination(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailcoordination&idope="+idope,
		success: function(resul){
			$("#detcoordination").html(resul);
			$("#annee_prog").mask("9999");
			$(".chpcoord").click(function(){
				var idope=$(this).data("idope");
				var idcoord=$(this).data("idcoord");
				if ( $( this ).prop( "checked" ) ){val=1;}else{val=0;}
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatecoordoperation&idope="+idope+"&idcoord="+idcoord+"&val="+val,
					success: function(resul){
						detail_coordination(idope);
					}
				});
			});
			$(".chpcritere").click(function(){
				var idope=$(this).data("idope");
				var idcritere=$(this).data("idcritere");
				if ( $( this ).prop( "checked" ) ){val=1;}else{val=0;}
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatecritereoperation&idope="+idope+"&idcritere="+idcritere+"&val="+val,
					success: function(resul){
						detail_coordination(idope);
					}
				});
			});
		}
	});
}
function detail_autorisation(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailautorisation&idope="+idope,
		success: function(resul){
			$("#detautorisation").html(resul);
			$(".datedestauto").mask("99/99/9999");
			$(".chkdestauto").click(function(){
				var idauto=$(this).data("idauto");
				var iddest=$(this).data("iddest");
				if ( $( this ).prop( "checked" ) ){val=1;}else{val=0;}
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=checkdestinataireauto&idauto="+idauto+"&iddest="+iddest+"&val="+val,
					success: function(resul){
						detail_autorisation(idope);
					}
				});
			});
			$(".chpmodifdestauto").change(function(){
				var idauto=$(this).data("idauto");
				var iddest=$(this).data("iddest");
				var chp=$(this).data("chp");
				var val=$(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatedestinataireauto&idauto="+idauto+"&iddest="+iddest+"&chp="+chp+"&val="+val,
					success: function(resul){
						detail_autorisation(idope);
					}
				});
			});
			$(".listbordvalauto").click(function(){
				var type = $(this).data("type");
				var idauto = $(this).data("idauto");
				$(".listbordvalauto").each(function(){
					$(this).removeClass("bg-success");
				});
				$(this).addClass("bg-success");
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstvalorisationauto&type="+type+"&idauto="+idauto,
					success: function(resul){
						$("#lstarticlevalorisationauto").html(resul);
					}
				});
			});
			$("body").on("change",".modifchpvalautoauto",function(){
				var idvalauto = $(this).data("idvalauto");
				var idauto = $(this).data("idauto");
				var val = $(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatevaleurvalorisationauto&idvalauto="+idvalauto+"&idauto="+idauto+"&val="+val,
					success: function(resul){
					}
				});
			});
		}
	});
}

function detail_chantier(idcht){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailchantier&id="+idcht,
		success: function(resul){
			$("#contentpage").html(resul);
			$("#back_operation").click(function(){
				var id_ope = $(this).data("idope");
				detail_operation(id_ope);
			});
			$("#lstbordapd").change(function(){
				var idbord = $(this).val();
				var idapd = $(this).data("idapd");
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstarticlebord&idbord="+idbord+"&idapd="+idapd,
					success: function(resul){
						$("#lstarticlesapd").html(resul);
						$(".add_valapd").click(function(){
							var id_art = $(this).data("idarticle");
							var id_apd = $(this).data("idapd");
							$.ajax({
								type: "POST",
								url: "./chantier.php",
								data: "action=addvalapd&idapd="+id_apd+"&idart="+id_art,
								success: function(resul){
									$.ajax({
										type: "POST",
										url: "./chantier.php",
										data: "action=lstvalapd&idapd="+id_apd,
										success: function(resul){
											$("#lstvalapd").html(resul);
										}
									});
								}
							});
						});
					}
				});
			});
			$("body").on("click",".del_valapd",function(){
				var id_apd = $(this).data("idapd");
				var id_valapd = $(this).data("idvalapd");
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=delvalapd&idvalapd="+id_valapd,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstvalapd&idapd="+id_apd,
							success: function(resul){
								$("#lstvalapd").html(resul);
							}
						});
					}
				});
			});
			$("body").on("change",".update_valapd",function(){
				var chp = $(this).data("chp");
				var id_valapd = $(this).data("idvalapd");
				var id_apd = $(this).data("idapd");
				var val = $(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatevalapd&idvalapd="+id_valapd+"&chp="+chp+"&val="+val,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstvalapd&idapd="+id_apd,
							success: function(resul){
								$("#lstvalapd").html(resul);
							}
						});
					}
				});
			});
			$("body").on("change",".update_tauxapd",function(){
				var chp = $(this).data("chp");
				var id_apd = $(this).data("idapd");
				var type = $(this).data("type");
				var val = $(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatetauxapd&idapd="+id_apd+"&chp="+chp+"&val="+val+"&type="+type,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstvalapd&idapd="+id_apd,
							success: function(resul){
								$("#lstvalapd").html(resul);
							}
						});
					}
				});
			});
			$("body").on("change",".update_chantier",function(){
				var chp = $(this).data("chp");
				var id_cht = idcht;
				var val = $(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatechantier&idcht="+id_cht+"&chp="+chp+"&val="+val,
					success: function(resul){
						//detail_chantier(id_cht);
						if (chp=="programme"){
							$.ajax({
								type: "POST",
								url: "./chantier.php",
								data: "action=detailgenechantier&idcht="+id_cht,
								success: function(resul){
									$("#detailgene_chantier").html(resul);
								}
							});
						}
					}
				});
			});
			$("body").on("click",".del_sstraitantcht",function(){
				var id = $(this).data("id");
				var id_cht = idcht;
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=delsstraitantcht&id="+id,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstsstraitantcht&idcht="+id_cht,
							success: function(resul){
								$("#detailsstraitantcht").html(resul);
							}
						});
					}
				});
			});
			$("body").on("change",".update_sstraitantcht",function(){
				var chp = $(this).data("chp");
				var id = $(this).data("id");
				var id_cht = idcht;
				var val = $(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatesstraitantcht&id="+id+"&chp="+chp+"&val="+val,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstsstraitantcht&idcht="+id_cht,
							success: function(resul){
								$("#detailsstraitantcht").html(resul);
							}
						});
					}
				});
			});
			$("body").on("click","#add_sstraitantcht",function(){
				var id_cht = idcht;
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=addsstraitantcht&idcht="+id_cht,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstsstraitantcht&idcht="+id_cht,
							success: function(resul){
								$("#detailsstraitantcht").html(resul);
							}
						});
					}
				});
			});
		}
	});
}

function settings(){
	var box = '<div class="well well-sm"><h3>PREFERENCES</h3></div>';
	box += '<div class="col-md-12">';
	box += '<form>';
	box += '<div class="form-group row"><div class="col-sm-1 col-md-2"><label>Identit�: </label></div><div class="col-sm-11 col-md-10"><input id="set_identite" class="form-control"></div></div>';
	box += '<div class="form-group row"><div class="col-sm-1 col-md-2"><label>Login: </label></div><div class="col-sm-11 col-md-10"><input id="set_login" class="form-control"></div></div>';
	box += '<div class="form-group row"><div class="col-sm-1 col-md-2"><label>Mot de passe: </label></div><div class="col-sm-11 col-md-10"><input type="password" class="form-control"></div></div>';
	box += '<div class="form-group row"><div class="col-sm-1 col-md-2"><label>Confirmer le mot de passe: </label></div><div class="col-sm-11 col-md-10"><input type="password" class="form-control"></div></div>';
	box += '<div class="form-group row"><div class="col-md-1"><button class="btn btn-default">Enregistrer</button></div><div class="col-md-1"><button id="set_annul" class="btn btn-danger">Annuler</button></div></div>';
	box += '</form>';
	box += '</div>';
	$("#contentpage").html(box);
	$("#set_annul").click(function(){
		dashboard();
	});
	$.ajax({
		type: "POST",
		url: "./user.php",
		dataType: "json",
		success: function(resul){
			$("#set_identite").val(resul.identite);
			$("#set_login").val(resul.login);
		}
	});
}
function infoUser(){
	$.ajax({
		type: "POST",
		url: "./user.php",
		dataType: "json",
		success: function(resul){
			$("#identite").html(resul.identite+' <span class="caret"></span>');
			$(".profile-usertitle-name").html(resul.identite);
		}
	});
}
function dashboard(){
	var box = "<div class='col-md-12'>";
	box += "<div class='portlet'><div class='portlet-title'><i class='fa fa-tachometer-alt' aria-hidden='true'></i> Tableau de bord</div><div class='portlet-content' id='contentdashboard'></div></div>";
	box += "</div>";

	$("#contentpage").html(box);
	$.ajax({
		type: "POST",
		url: "./dashboard.php",
		data: "action=loaddashboard",
		success: function(resul){
			$("#contentdashboard").html(resul);
			$("body").on("click",".lunotif",function(){
				var id_notif = $(this).data("idnotif");
				$.ajax({
					type: "POST",
					url: "./dashboard.php",
					data: "action=updatenotifdashboard&idnotif="+id_notif,
					success: function(resul){
						dashboard();
					}
				});	
			});
		}
	});	
}
