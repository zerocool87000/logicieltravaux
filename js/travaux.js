var nbtot = 0;
$(document).ready(function(){
	$("body").on("click",".btnsettings_deleg",function(){
		var deleg=$(this).data("deleg");
		var val=$("#settings_lstca").val();
		$.ajax({
			type: "POST",
			url: "./settings.php",
			data: "action=updatedelegation&type="+deleg+"&val="+val,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./settings.php",
					data: "action=loaddelegation",
					success: function(result){
						swal("F�licitation!", "La d�l�gation a bien �t� effectu�e.", "success");
						$("#detaildelegation_settings").html(result);
					}
				});
			}
		});
	});
	$("body").on("click","#update_usercompte",function(){
		var form = $("#formsetuser").serialize();
		$.ajax({
			type: "POST",
			url: "./settings.php",
			data: form,
			success: function(resul){
				swal("F�licitation!", "L'agent a bien �t� modifi�.", "success");
				$.ajax({
					type: "POST",
					url: "./settings.php",
					data: "action=loadcompte",
					success: function(result){
						$("#detailcompte_settings").html(result);
						//infoUser();
					}
				});
			}
		});
	});
	/* Gestion des collectivit�s */
	$("body").on("click",".visu_collectivite",function(){
		var id=$(this).data("id");
		$.ajax({
			type: "POST",
			url: "./settings.php",
			data: "action=loadinfocollectivite&id="+id,
			success: function(resul){
				$("#detailinfocollectivite_settings").html(resul);
			}
		});
	});
	$("body").on("click","#update_collectivite",function(){
		var form = $("#formsetcoll").serialize();
		$.ajax({
			type: "POST",
			url: "./settings.php",
			data: form,
			success: function(resul){
				swal("F�licitation!", "La collectivit� a bien �t� modifi�e.", "success");
				$.ajax({
					type: "POST",
					url: "./settings.php",
					data: "action=loadlstcollectivite",
					success: function(result){
						$("#detaillstcollectivite_settings").html(result);
					}
				});
			}
		});
	});
	$("body").on("click","#btnadd_collectivite",function(){
		$.ajax({
			type: "POST",
			url: "./settings.php",
			data: "action=addcollectivite",
			success: function(resul){
				swal("F�licitation!", "La collectivit� a bien �t� cr��e.", "success");
				$.ajax({
					type: "POST",
					url: "./settings.php",
					data: "action=loadlstcollectivite",
					success: function(result){
						$("#detaillstcollectivite_settings").html(result);
					}
				});
			}
		});
	});
	$("body").on("click",".del_collectivite",function(){
		var id = $(this).data("id");
		swal({
			title: "Suppression",
			text: "Etes vous s�r de vouloir supprimer!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Valider",
			cancelButtonText: "Annuler",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
				type: "POST",
				url: "./settings.php",
				data: "action=delcollectivite&id="+id,
				success: function(resul){
					if (resul!="erreur"){
						swal("Supprim�!", "La collectivit� a bien �t� supprim�.", "success");
						$.ajax({
							type: "POST",
							url: "./settings.php",
							data: "action=loadlstcollectivite",
							success: function(result){
								$("#detaillstcollectivite_settings").html(result);
							}
						});
					}else{
						swal("Supprim�!", "La collectivit� est rattach�e � des entit�s.", "error");
					}
				}
			});
		});
	});
	/* Gestion des comptes */
	$("body").on("click",".visu_comptes",function(){
		var id=$(this).data("id");
		$.ajax({
			type: "POST",
			url: "./settings.php",
			data: "action=loadinfocomptes&id="+id,
			success: function(resul){
				$("#detailinfocomptes_settings").html(resul);
			}
		});
	});
	$("body").on("click","#btnadd_setusers",function(){
	$.ajax({
			type: "POST",
			url: "./settings.php",
			data: "action=addcomptes",
			success: function(resul){
				swal("F�licitation!", "L'agent a bien �t� cr��.", "success");
				$.ajax({
					type: "POST",
					url: "./settings.php",
					data: "action=loadlstcomptes",
					success: function(result){
						$("#detaillstcomptes_settings").html(result);
					}
				});
			}
		});
	});
	$("body").on("click","#update_comptes",function(){
		var form = $("#formsetusers").serialize();
		$.ajax({
			type: "POST",
			url: "./settings.php",
			data: form,
			success: function(resul){
				swal("F�licitation!", "L'agent a bien �t� modifi�.", "success");
				$.ajax({
					type: "POST",
					url: "./settings.php",
					data: "action=loadlstcomptes",
					success: function(result){
						$("#detaillstcomptes_settings").html(result);
					}
				});
			}
		});
	});
	$("body").on("click",".del_comptes",function(){
		var id = $(this).data("id");
		swal({
			title: "Suppression",
			text: "Etes vous s�r de vouloir supprimer!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Valider",
			cancelButtonText: "Annuler",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
				type: "POST",
				url: "./settings.php",
				data: "action=delcomptes&id="+id,
				success: function(resul){
					swal("Supprim�!", "L'agent a bien �t� supprim�.", "success");
					$.ajax({
						type: "POST",
						url: "./settings.php",
						data: "action=loadlstcomptes",
						success: function(result){
							$("#detaillstcomptes_settings").html(result);
						}
					});
				}
			});
		});
	});
	/* PCT */
	$("body").on("click","#btnadd_pct",function(){
		$.ajax({
			type: "POST",
			url: "./settings.php",
			data: "action=addpct",
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./settings.php",
					data: "action=loadpct",
					success: function(result){
						$("#detailpct_settings").html(result);
						$(".datepct").mask("99/99/9999");
					}
				});
			}
		});
	});	
	$("body").on("click",".del_pct",function(){
		var id = $(this).data("id");
		swal({
			title: "Suppression",
			text: "Etes vous s�r de vouloir supprimer!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Valider",
			cancelButtonText: "Annuler",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
				type: "POST",
				url: "./settings.php",
				data: "action=delpct&id="+id,
				success: function(resul){
					swal("Supprim�!", "Le param�tre a bien �t� supprim�.", "success");
					$.ajax({
						type: "POST",
						url: "./settings.php",
						data: "action=loadpct",
						success: function(result){
							$("#detailpct_settings").html(result);
							$(".datepct").mask("99/99/9999");
						}
					});
				}
			});
		});
	});
	$("body").on("change",".update_pct",function(){
		var id = $(this).data("id");
		var chp = $(this).data("chp");
		var val = $(this).val();
		$.ajax({
			type: "POST",
			url: "./settings.php",
			data: "action=updatepct&id="+id+"&chp="+chp+"&val="+val,
			success: function(resul){
/*				$.ajax({
					type: "POST",
					url: "./settings.php",
					data: "action=loadpct",
					success: function(result){
						$("#detailpct_settings").html(result);
						$(".datepct").mask("99/99/9999");
					}
				});
*/			}
		});
	});


    dashboard();
    infoUser();
    $("#menu_dashboard").click(function(){
		var elementmenu = $(this).parent();
		$('.sider-menu li').each(function(){
			$(this).removeClass("active");
		});
		elementmenu.addClass("active");
    	dashboard();
    });
    $("#settings").click(function(){
    	settings();
    });
    $(".quitter").click(function(){
    	quitter();
    });
	myVar = setInterval(function(){
		nbtot = 0;
		$.ajax({
			type: "POST",
			url: "./dashboard.php",
			data: "action=nbrnotifdashboard",
			success: function(resul){
				if (resul!=0){
					$("#btnbar_notif").html('<span data-tooltip="Notifications" data-position="bottom" class="fa-layers fa-fw fa-lg"><i class="fa fa-bell"></i><span class="fa-layers-counter fa-2x" style="background:Tomato">'+resul+'</span></span>');
				}else{
					$("#btnbar_notif").html('<span data-tooltip="Notifications" data-position="bottom" class="fa-layers fa-fw fa-lg"><i class="fa fa-bell"></i></span>');
					resul=0;
				}
				nbtot = parseFloat(nbtot) + parseFloat(resul);
				$.ajax({
					type: "POST",
					url: "./dashboard.php",
					data: "action=nbrtaskdashboard",
					success: function(resul){
						if (resul!=0){
							$("#btnbar_task").html('<span data-tooltip="T�ches" data-position="bottom" class="fa-layers fa-fw fa-lg"><i class="fa fa-tasks"></i><span class="fa-layers-counter fa-2x" style="background:Tomato">'+resul+'</span></span>');
						}else{
							$("#btnbar_task").html('<span data-tooltip="T�ches" data-position="bottom" class="fa-layers fa-fw fa-lg"><i class="fa fa-tasks"></i></span>');
							resul=0;
						}
						nbtot = parseFloat(nbtot) + parseFloat(resul);
						if (nbtot!=0){$("title").html('Travaux 2.0 ('+nbtot+')');}else{$("title").html('Travaux 2.0');}
						loadpanelnotif();
					}
				});
			}
		});
	}, 5000);
	$.notify("Vous avez "+nbtot+" notifications", {
		title: "Travaux 2.0",
		icon: "../images/user2.png"
	});

});
function quitter(){
	//eraseCookie("id");
	eraseCookie("login");
	$(location).attr('href',"../index.php");	
}
function settings(){
	$.ajax({
		type: "POST",
		url: "./settings.php",
		data: "action=loadsettings",
		success: function(resul){
			$("#contentpage").html(resul);
			$("#set_annul").click(function(){
				dashboard();
			});
		}
	});
}
function infoUser(){
	$.ajax({
		type: "POST",
		url: "./user.php",
		dataType: "json",
		success: function(resul){
			if (resul.identite){
				$("#identite").html(resul.identite+' <span class="caret"></span>');
				$(".profile-usertitle-name").html(resul.identite);
			}else{
				var boxcontent = "<h2 class='text-center'>Authentification obligatoire</h2>";
				var dialog_auth = bootbox.dialog({title: '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Erreur Authentification', message: boxcontent ,closeButton: false});
				dialog_auth.find(".modal-header").addClass("bg-danger");
				dialog_auth.init(function(){
					setTimeout(function(){
						$(location).attr('href',"../index.php");
					}, 3000);
				});
			}
		}
	});
}
function dashboard(){
	var box = "<div class='col-md-12'>";
	box += "<div class='portlet'><div class='portlet-title'><i class='fa fa-tachometer-alt' aria-hidden='true'></i> Tableau de bord</div><div class='portlet-content' id='contentdashboard'></div></div>";
	box += "</div>";

	$("#contentpage").html(box);
	$.ajax({
		type: "POST",
		url: "./dashboard.php",
		data: "action=loaddashboard",
		success: function(resul){
			$("#contentdashboard").html(resul);
			myVar = setInterval(function(){
				loadpanelnotif();
			}, 5000);
			$("body").on("click",".lunotif",function(){
				var id_notif = $(this).data("idnotif");
				$.ajax({
					type: "POST",
					url: "./dashboard.php",
					data: "action=updatenotifdashboard&idnotif="+id_notif,
					success: function(resul){
						dashboard();
					}
				});	
			});
		}
	});	
}
function loadpanelnotif(){
	$.ajax({
		type: "POST",
		url: "./dashboard.php",
		data: "action=loaddashboardnotif",
		success: function(resul){
			$("#panel_notif").html(resul);
		}
	});	
}