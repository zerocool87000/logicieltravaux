$(document).ready(function(){
	$(".fa").qtip({style: { classes: 'qtip-dark qtip-rounded qtip-shadow' }});

    /* Gestion du bouton de login */
	$("#btnlog").click(function(event){
		event.preventDefault();
		formvalidate("formlog").then(function(answer){
			var ansbool = answer.toString();
			if (ansbool=="true"){
				$.ajax({
					type: "POST",
					url: "./lib/log.php",
					data: "log="+$("#log").val()+"&pwd="+$("#pwd").val(),
					dataType: 'json',
					success: function(resul){
						if (resul.erreur=="yes"){
							$("#login-alert").css("display","block");
						}else{
							//if (resul.droit==8){
							//	document.location.href="./lib/accueiladm.php";
							//}else{
								document.location.href="./lib/accueil.php";
							//}
						}
					}
				});
			}
		});
	});
});