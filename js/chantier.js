var tabdemoscht = [];var tabcrit = [];var criteres="";

$(document).ready(function(){
	$(".fa").qtip({style: { classes: 'qtip-dark qtip-rounded qtip-shadow' }});
    $("#menu_chantier").click(function(){
		var elementmenu = $(this).parent();
		$('.sider-menu li').each(function(){
			$(this).removeClass("active");
		});
		elementmenu.addClass("active");
    	menu_chantier();
    });
    $("body").on("change",".chpope_filter",function(){
    	var compt=0;
    	criteres="";
    	tabcrit = [];
    	$(".chpope_filter").each(function(){
			if ($(this).val()!=""){
				if (compt==0){
					criteres = $(this).data("chp")+" like '%"+$(this).val()+"%'";
				}else{
					criteres += ' AND '+$(this).data("chp")+" like '%"+$(this).val()+"%'";
				}
				tabcrit.push($(this).data("chp")+":"+$(this).val());
				compt = compt + 1;
			}
    	});
    	if (compt==0){
			lst_operation(1,getCookie("login"),"","","","");
    	}else{
			elements = tabcrit.join(',');
			lst_operation(1,"","","",encodeURIComponent(criteres),elements);
    	}
    });
    $("body").on("change",".modifchpoperation",function(){
		var idope = $(this).data("idope");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_operation(idope,chp,val);
    });
	$("body").on("change",".update_opeaps",function(){
		var idope = $(this).data("idope");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_aps_ope(idope,chp,val);
	});
	$("body").on("click","#addvalauto",function(){
		var idope = $(this).data("idope");
		$.ajax({
			type: "POST",
			url: "./chantier.php",
			data: "action=addvalauto&idope="+idope,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstvalorisationauto&idope="+idope,
					success: function(resul){
						$("#lstarticlevalorisationauto").html(resul);
					}
				});
			}
		});
	});
	$("body").on("click",".upload_xmldt",function(){
		//$("#dtconcept").click();
		var idope = $(this).data("idope");
		var textbox = '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Emprise</label></div><div class="col-md-10"><textarea class="form-control input-sm" rows="10" cols="50" id="dt_txtemprise"></textarea></div></div></div>';
		var dialog = bootbox.dialog({
			title: '<i class="far fa-map"></i> Emprise DT',
			message: textbox,
			buttons: {
				ok: {
					label: "D�poser",
					className: 'btn-success',
					callback: function(){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=createemprisedt&idope="+idope+"&emprise="+$("#dt_txtemprise").val(),
							success: function(resul){
								dialog.modal('hide');
								swal("Charg�!", "La DT a bien �t� charg�.", "success");
							}
						});
					}
				},
				cancel: {
					label: "Annuler",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
	$("body").on("click",".zimbracalendar",function(){
		//$("#dtconcept").click();
		var idope = $(this).data("idope");
		var textbox = '<div id="lstrdvzimbra"></div>';
		$.ajax({
			type: "POST",
			url: "./getzimbra.php",
			//data: "action=createemprisedt&idope="+idope+"&emprise="+$("#dt_txtemprise").val(),
			success: function(resul){
				$("#lstrdvzimbra").html(resul);
			}
		});
		var dialog = bootbox.dialog({
			title: '<i class="far fa-calendar-alt"></i> Rendez-vous Zimbra',
			message: textbox,
			buttons: {
				cancel: {
					label: "Fermer",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
	/* Beneficiaires de l'op�ration */
	$("body").on("click","#add_opebeneficiaire",function(){
		var idope = $(this).data("idope");
		$.ajax({
			type: "POST",
			url: "./chantier.php",
			data: "action=addopebeneficiaire&idope="+idope,
			success: function(resul){
				detail_opebeneficiaire(idope);
			}
		});
	});
	$("body").on("click",".delopebeneficiaire",function(){
		var id = $(this).data("id");
		var idope = $(this).data("idope");
		swal({
			title: "Suppression",
			text: "Etes vous s�r de vouloir supprimer!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Valider",
			cancelButtonText: "Annuler",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
				type: "POST",
				url: "./chantier.php",
				data: "action=delopebeneficiaire&id="+id,
				success: function(resul){
					swal("Supprim�!", "Le b�n�ficiaire a bien �t� supprim�.", "success");
					detail_opebeneficiaire(idope);
				}
			});
		});
	});
	$("body").on("change",".modifchpopebeneficiaire",function(){
		var idope = $(this).data("idope");
		var id = $(this).data("id");
		var chp = $(this).data("chp");
		var val = $(this).val();
		update_opebeneficiaire(id,idope,chp,val);
	});
    /* fonctions onglet sp�cificit�*/
	$("body").on("change","#lstbordcht",function(){
		var idbord = $(this).val();
		var nat = $(this).data("nat");
		var idcht = $(this).data("idcht");
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=lstarticlebpu&idbord="+idbord+"&idcht="+idcht+"&nat="+nat,
			success: function(resul){
				$("#lstarticlescht").html(resul);
			}
		});
	});
	$("body").on("click",".add_valcht",function(){
		var id_art = $(this).data("idarticle");
		var nat = $(this).data("nat");
		var id_cht = $(this).data("idcht");
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=addvalcht&idcht="+id_cht+"&idart="+id_art,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./"+nat+".php",
					data: "action=lstvalcht&idcht="+id_cht+"&nat="+nat,
					success: function(resul){
						$("#lstvalcht").html(resul);
					}
				});
			}
		});
	});
	$("body").on("click",".del_valcht",function(){
		var id_cht = $(this).data("idcht");
		var nat = $(this).data("nat");
		var id_valcht = $(this).data("idvalcht");
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=delvalcht&idvalcht="+id_valcht,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./"+nat+".php",
					data: "action=lstvalcht&idcht="+id_cht+"&nat="+nat,
					success: function(resul){
						$("#lstvalcht").html(resul);
					}
				});
			}
		});
	});
	$("body").on("change",".update_valcht",function(){
		var chp = $(this).data("chp");
		var id_valcht = $(this).data("idvalcht");
		var id_cht = $(this).data("idcht");
		var nat = $(this).data("nat");
		var val = $(this).val();
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=updatevalcht&idvalcht="+id_valcht+"&chp="+chp+"&val="+val,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./"+nat+".php",
					data: "action=lstvalcht&idcht="+id_cht+"&nat="+nat,
					success: function(resul){
						$("#lstvalcht").html(resul);
					}
				});
			}
		});
	});
	$("body").on("click","#add_specif",function(){
		var idcht = $(this).data("idcht");
		var nat = $(this).data("nat");
		var table = $(this).data("table");
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=addspecif&idcht="+idcht+"&table="+table,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./"+nat+".php",
					data: "action=detailspecif&idcht="+idcht+"&nat="+nat,
					success: function(result){
						$("#detspecif").html(result);
						$(".datespecif").mask("99/99/9999");
						$(".heurespecif").mask("99h99");
					}
				});
			}
		});
	});
	$("body").on("change",".update_specif",function(){
		var chp = $(this).data("chp");
		var type = $(this).data("type");
		var id = $(this).data("id");
		var idcht = $(this).data("idcht");
		var nat = $(this).data("nat");
		var table = $(this).data("table");
		var val = $(this).val();
		if(type=="chk"){
			if ( $( this ).prop( "checked" ) ){val=1;}else{val=0;}
		} 
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=updatespecif&id="+id+"&chp="+chp+"&val="+val+"&nat="+nat+"&table="+table,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./"+nat+".php",
					data: "action=detailspecif&idcht="+idcht+"&nat="+nat,
					success: function(resul){
						$("#detspecif").html(resul);
						$(".datespecif").mask("99/99/9999");
						$(".heurespecif").mask("99h99");
					}
				});
			}
		});
	});
	$("body").on("change",".update_specif input[type=radio]",function(){
		var chp = $(this).data("chp");
		var type = $(this).data("type");
		var id = $(this).data("id");
		var idcht = $(this).data("idcht");
		var nat = $(this).data("nat");
		var table = $(this).data("table");
		var val = this.value;
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=updatespecif&id="+id+"&chp="+chp+"&val="+val+"&nat="+nat+"&table="+table,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./"+nat+".php",
					data: "action=detailspecif&idcht="+idcht+"&nat="+nat,
					success: function(resul){
						$("#detspecif").html(resul);
						$(".datespecif").mask("99/99/9999");
						$(".heurespecif").mask("99h99");
					}
				});
			}
		});
	});
	$("body").on("change",".select_specif",function(){
		var id = $(this).data("id");
		var idcht = $(this).data("idcht");
		var nat = $(this).data("nat");
		var table = $(this).data("table");
		var val = $(this).val();
		var div = $(this).data("div");
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=selectspecif&val="+val+"&idcht="+idcht+"&nat="+nat+"&table="+table,
			success: function(resul){
				$("#"+div).html(resul);
			}
		});
	});
	$("body").on("click",".del_specif",function(){
		var id = $(this).data("id");
		var idcht = $(this).data("idcht");
		var nat = $(this).data("nat");
		var table = $(this).data("table");
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=delinspecif&id="+id+"&table="+table,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./"+nat+".php",
					data: "action=detailspecif&idcht="+idcht+"&nat="+nat,
					success: function(resul){
						$("#detspecif").html(resul);
						$(".datespecif").mask("99/99/9999");
						$(".heurespecif").mask("99h99");
					}
				});
			}
		});
	});
	$("body").on("click",".visu_specif",function(){
		var id = $(this).data("id");
		var idcht = $(this).data("idcht");
		var nat = $(this).data("nat");
		var table = $(this).data("table");
		var div = $(this).data("div");
		$(".visu_specif").each(function(){
			$(this).removeClass("active");
		});
		$(this).addClass("active");
		$.ajax({
			type: "POST",
			url: "./"+nat+".php",
			data: "action=visuinspecif&id="+id+"&table="+table+"&nat="+nat,
			success: function(resul){
				$("#"+div).html(resul);
			}
		});
	});
	/* Valorisation des postes dans sp�cifique */
	$("body").on("click",".add_valposte",function(){
		var idart = $(this).data("idart");
		var idcht = $(this).data("idcht");
		var table = $(this).data("table");
		var div = $(this).data("div");
		$.ajax({
			type: "POST",
			url: "./pos.php",
			data: "action=addspecif&idcht="+idcht+"&idart="+idart+"&table="+table,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./pos.php",
					data: "action=lstvalposte&idcht="+idcht,
					success: function(resul){
						$("#lstvalposte").html(resul);
					}
				});
			}
		});
	});
	$("body").on("click",".del_valposte",function(){
		var id = $(this).data("idvalposte");
		var idcht = $(this).data("idcht");
		var table = $(this).data("table");
		var div = $(this).data("div");
		$.ajax({
			type: "POST",
			url: "./pos.php",
			data: "action=delinspecif&id="+id+"&table="+table,
			success: function(resul){
				$.ajax({
					type: "POST",
					url: "./pos.php",
					data: "action=lstvalposte&idcht="+idcht,
					success: function(resul){
						$("#lstvalposte").html(resul);
					}
				});
			}
		});
	});

	/* Demande OS */
	$("body").on("click",".chkchtdemandeos",function(){
		alert("coucou");
		if ( $( this ).prop( "checked" ) ){
			var id = $(this).data("id");
			tabdemoscht.push(id);
		}else{
			var id = $(this).data("id");
			index = tabdemoscht.indexOf(id);
			tabdemoscht.splice(index,1);
		}
		console.log(tabdemoscht);
	});

	/* Devis ECL */
	$("body").on("change",".searchbpu",function(){
		var art = $(this).val();
		var nat = $(this).data("nat");
		if (art!=""){
			$.ajax({
				type: "POST",
				url: "./"+nat+".php",
				data: "action=searchbpu&art="+art,
				cache: false,
				dataType: 'json',
				success: function(resul){
					$(".searchbpu").removeClass(".has-error");
					$("#artnotinbord").html("");
					$("#artdevislibelle").html("");
					$("#artdevispu").html("");
					$("#artdevisqte").val("");
					$("#artdevisral").val("");
					$("#artdevismontant").html("");
					if (resul.trouve=="oui"){
						$("#artdevislibelle").html(resul.libelle);
						$("#artdevispu").html(resul.pu);
					}else{
						$(".searchbpu").addClass(".has-error");
						$("#artnotinbord").html("<div class='alert alert-danger text-center' role='alert'><strong>Erreur !</strong> Article inconnu</div>");
					}
				}
			});
		}else{
			$(".searchbpu").removeClass(".has-error");
			$("#artnotinbord").html("");
			$("#artdevislibelle").html("");
			$("#artdevispu").html("");
			$("#artdevisqte").val("");
			$("#artdevisral").val("");
			$("#artdevismontant").html("");			
		}
	});
	$("body").on("change","#artdevisqte",function(){
		var tot = Math.round(parseFloat($("#artdevispu").html()) * parseFloat($(this).val())*100)/100;
		$("#artdevismontant").html(tot);
	});
	$("body").on("click",".visurpt",function(){
		var id = $(this).data("id");
		var file = $(this).data("file");
		var type = $(this).data("type");
		window.open("./report.php?id="+id+"&type="+type+"&file="+file);
	});	
});

function menu_chantier(){
	var box = "<div class='col-md-12'>";
	box += '<div class="well well-sm btn-toolbar">';
	box += '<div class="btn-group">';
	box += '<div class="btn-group" role="group">';
	box += '<button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> Actions <span class="caret"></span></button>';
	box += '<ul class="dropdown-menu" style="font-size:9pt;">';
	box += '<li><a href="#" id="add_commandeprev"><i class="fa fa-plus-circle"></i> Commandes pr�visionnelles</a></li>';
	box += '<li><a href="#" id="add_demandebc"><i class="fa fa-plus-circle"></i> Cr�er une demande BC</a></li>';
	box += '<li><a href="#" id="suivi_demandebcope"><i class="fa fa-exclamation-circle"></i> Suivi des demandes BC</a></li>';
	box += '<li><a href="#" id="suivi_demandeosope"><i class="fa fa-exclamation-circle"></i> Suivi des demandes OS</a></li>';
	box += '</ul>';
	box += '</div>';
	box += '<button type="button" data-position="bottom" data-tooltip="Nouvelle op�ration" id="add_operation" class="btn btn-sm btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Operations</button>';
	box += '</div>';
	//box += '<button type="button" data-position="bottom" data-tooltip="Filtre" id="btn_filtreope" class="btn btn-sm btn-default"><i class="fa fa-filter" aria-hidden="true"></i></button>';
	box += '</div>';

	box += '<div class="well well-sm" id="block_filter">';
	box += '<div class="row"><label class="col-sm-2" for="filter_crit">Crit�res:</label><div class="col-sm-3">';
	box += '<select class="form-control input-sm" id="filter_crit" name="filter_crit">';
	box += '<option value="-1">Choisir un crit�re</option><option value="operations.ope_codeinsee">Commune</option><option value="operations.ope_numero">Num�ro</option><option value="operations.ope_numero">Ann�e</option><option value="operations.ope_categorie">Cat�gorie</option>';
	box += '</select>';
	box += '</div><label class="col-sm-2" for="filter_val">Valeur:</label><div class="col-sm-3" id="filter_input"></div>';
	box += '<div class="col-sm-1"><button type="button" id="btn_filtrestart" class="btn btn-sm btn-default"><i class="fa fa-search" aria-hidden="true"></i></button></div>';
	box += '<div class="col-sm-1"><button type="button" id="btn_filtreerase" class="btn btn-sm btn-default"><i class="fa fa-trash" aria-hidden="true"></i></button></div>';
	box += '</div>';
	box += '</div>';

	//box += "<div class='portlet'><div class='portlet-title'><i class='fa fa-bolt' aria-hidden='true'></i> Op�rations </div><div class='portlet-content' id='lstoperation'></div></div>";
	box += "<div class='portlet'><div class='portlet-title'><i class='fa fa-bolt' aria-hidden='true'></i> Module Travaux </div><div class='portlet-content' id='lstmodulemarche'>";
	box += "<div class='row'>";
	box += '<div class="col-sm-12">';
	box += '<div class="panel-group" id="accordion">';
	box +='<div class="panel panel-default">';
	box +='<div class="panel-heading">';
	box +='<h2 class="panel-title">';
	box +='<a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><i class="fa fa-bolt" aria-hidden="true"></i> Op�rations</a>';
	box +='</h2>';
	box +='</div>';
	box +='<div id="collapse1" class="panel-collapse collapse in">';
	box +='<div class="panel-body" id="lstoperation">';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='<div class="panel panel-default">';
	box +='<div class="panel-heading">';
	box +='<h2 class="panel-title">';
	box +='<a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><i class="fas fa-folder-open" aria-hidden="true"></i> PCT</a>';
	box +='</h2>';
	box +='</div>';
	box +='<div id="collapse2" class="panel-collapse collapse">';
	box +='<div class="panel-body" id="lstbordpct">';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box +='</div>';
	box += "</div>";
	box += "</div>";
	$("#contentpage").html(box);
	$("#block_filter").toggle();
	$("#btn_filtreope").click(function(){
		$("#block_filter").toggle("slow");
		if ($(this).hasClass("active")){$(this).removeClass("active");}else{$(this).addClass("active");}
	});
	$("#btn_filtrestart").click(function(){
		var chp = $("#filter_crit").val();
		var val = $("#filter_val").val();
		lst_operation(1,"",chp,val,"","");
	});
	$("#filter_crit").change(function(){
		$("#filter_val").val("");
		switch ($("#filter_crit option:selected").text()){
			case "Commune":
				$("#filter_input").html('<select id="filter_val" name="filter_val" class="form-control input-sm"></select>');
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstcollectivite",
					success: function(resul){
						$("#filter_val").html(resul);
						$("option.lstcomoptionfilter:disabled").css("color","#FAA21B");
					}
				});
				break;
			case "Num�ro":
				$("#filter_input").html('<input id="filter_val" name="filter_val" class="form-control input-sm">');
				$("#filter_val").mask("99-aaa-999");
				break;
			case "Ann�e":
				$("#filter_input").html('<input id="filter_val" name="filter_val" class="form-control input-sm">');
				$("#filter_val").mask("99");
				break;
			case "Cat�gorie":
				$("#filter_input").html('<input id="filter_val" name="filter_val" class="form-control input-sm">');
				break;
		}
	});
	$("#btn_filtreerase").click(function(){
		$("#filter_crit").val("-1");
		$("#filter_val").val("");
		$("#filter_input").html("");
		lst_operation(1,getCookie("login"),"","","","");
	});
	lst_operation(1,getCookie("login"),"","","","");
	lst_bordpct(1,getCookie("login"),"","");
	$("#suivi_demandebcope").click(function(){
		var textbox = '<div class="row"><div class="col-md-12"><div id="tablstdemandebc"></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./chantier.php",
			data: "action=lstdemandebc",
			success: function(resul){
				$("#tablstdemandebc").html(resul);
				$('.showcht').each(function(){
					$(this).nextUntil('tr.showcht').slideToggle();
				});
				$('.showcht').click(function(){
					$(this).nextUntil('tr.showcht').slideToggle();
				});
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-exclamation-circle"></i> Suivi des demandes BC',
			message: textbox,
			buttons: {
				cancel: {
					label: "Fermer",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
	$("#add_operation").click(function(){
		var textbox = '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Cat�gories</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectnouvope_categorie" required><option></option></select></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./chantier.php",
			data: "action=lstcategorie",
			success: function(resul){
				$("#selectnouvope_categorie").html(resul);
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-bolt"></i> Nouvelle op�ration',
			message: textbox,
			buttons: {
				ok: {
					label: "Cr�er",
					className: 'btn-success',
					callback: function(){
						fieldvalidate("selectnouvope_categorie").then(function(answer){
							var ansbool = answer.toString();
							if (ansbool=="true"){
								$.ajax({
									type: "POST",
									url: "./chantier.php",
									data: "action=addoperation&idcat="+$("#selectnouvope_categorie").val(),
									success: function(resul){
										dialog.modal('hide');
										lst_operation(1,getCookie("login"),"","","","");
									}
								});
							}else{
							swal("Erreur!", "Cat�gorie absente.", "danger");								
							}
						});
					}
				},
				cancel: {
					label: "Annuler",
					className: 'btn-default',
					callback: function(){
						dialog.modal('hide');
					}
				}
			}
		});
	});
	$("#add_demandebc").click(function(){
		var myVar; var elements;
		var textbox = '<div class="row"><div class="col-md-12">';
		textbox += '<div class="col-md-3"><label>D�lai (sem.)</label></div><div class="col-md-2"><input class="form-control input-sm text-center" id="demandebc_delai"></div>';
		textbox += '<div class="col-md-3"><label>Entreprises</label></div><div class="col-md-4"><select class="form-control input-sm" id="demandebc_entreprise"></select></div>';
		textbox += '</div></div>';
		textbox += '<br><div class="row"><div class="col-md-12">';
		textbox += '<div class="col-md-3"><label>N� conv. EP</label></div><div class="col-md-3"><input class="form-control input-sm text-center" id="demandebc_numconvep"></div>';
		textbox += '<div class="col-md-3"><label>Date</label></div><div class="col-md-3"><input class="form-control input-sm text-center" id="demandebc_dateconvep"></div>';
		textbox += '</div></div>';
		//textbox += '<br><div class="row"><div class="col-md-12"><div class="col-md-2"><label>Op�rations</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectdembc_operation"><option></option></select></div></div></div>';
		textbox += '<br><div class="row"><div class="col-md-12"><div class="col-md-2"><label>Op�rations</label></div><div class="col-md-10"><input class="form-control input-sm" id="selectdembc_operation"></div></div></div>';
		textbox += '<br><div class="row"><div class="col-md-12"><div id="lstchantierdembc"></div></div></div>';
		textbox += '<br><div class="row"><div class="col-md-12"><div id="lstdembc"></div></div></div>';
		$.ajax({
			type: "POST",
			url: "./chantier.php",
			data: "action=lstentreprise",
			success: function(resul){
				$("#demandebc_entreprise").html(resul);
			}
		});
		$.ajax({
			type: "POST",
			url: "./chantier.php",
			data: "action=lstoperationdembc",
			success: function(resul){
				//$("#selectdembc_operation").html(resul);
				var tabdembccht = [];
				myVar = setInterval(function(){
					elements = tabdembccht.join(',');
					$.ajax({
						type: "POST",
						url: "./chantier.php",
						data: "action=lstchantieropedembctemp&tabcht="+elements,
						success: function(resul){
							$("#lstdembc").html(resul);
						}
					});
				}, 1000);
				$("#selectdembc_operation").change(function(){
					var idope = $(this).val();
					if (idope!='-1'){
						elements = tabdembccht.join(',');
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstchantieropedembc&idope="+idope+"&tabcht="+elements,
							success: function(resul){
								$("#lstchantierdembc").html(resul);
								$(".chkchtdemandebc").click(function(){
									if ( $( this ).prop( "checked" ) ){
										var id = $(this).data("id");
										tabdembccht.push(id);
									}else{
										var id = $(this).data("id");
										index = tabdembccht.indexOf(id);
										tabdembccht.splice(index,1);
									}
								});
							}
						});
					}
				});
			}
		});

		var dialog = bootbox.dialog({
			title: '<i class="fa fa-shopping-basket"></i> Nouvelle Demande de BC',
			message: textbox,
			buttons: {
				ok: {
					label: "Envoyer",
					className: 'btn-success',
					callback: function(){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=adddemandebc&numconvep="+$("#demandebc_numconvep").val()+"&dateconvep="+$("#demandebc_dateconvep").val()+"&ident="+$("#demandebc_entreprise").val()+"&delai="+$("#demandebc_delai").val()+"&tabcht="+elements,
							success: function(resul){
								dialog.modal('hide');
								clearInterval(myVar);
								lst_operation(1,getCookie("login"),"","","","");
							}
						});
					}
				},
				cancel: {
					label: "Annuler",
					className: 'btn-default',
					callback: function(){
						clearInterval(myVar);
						dialog.modal('hide');
					}
				}
			}
		});
		$("#selectdembc_operation").mask("99-aaa-999");
		$("#demandebc_dateconvep").mask("99/99/9999");

	});
}

function lst_operation(page,login,chp,val,criteres,tabcrit){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=lstoperation&page="+page+"&login="+login+"&chp="+chp+"&val="+val+"&criteres="+criteres+"&tabcrit="+tabcrit,
		success: function(resul){
			$("#lstoperation").html(resul);
			$(".forward").click(function(){
				if(page>1){lst_operation(page-1,getCookie("login"),chp,val,criteres,tabcrit);}
			});
			$(".next").click(function(){
				lst_operation(page+1,getCookie("login"),chp,val,criteres,tabcrit);
			});

			$(".visuope").click(function(){
				var id = $(this).data("id");
				detail_operation(id);
			});
			$(".delope").click(function(){
				var idope = $(this).data("id");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./chantier.php",
						data: "action=deloperation&idope="+idope,
						success: function(resul){
							swal("Supprim�!", "L'op�ration a bien �t� supprim�.", "success");
							lst_operation(1,getCookie("login"),"","");
						}
					});
				});
			});
		}
	});
}
function update_operation(idope,chp,val){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=updateoperation&idope="+idope+"&chp="+chp+"&val="+val,
		success: function(resul){
			$.ajax({
				type: "POST",
				url: "./chantier.php",
				data: "action=detailgeneoperation&idope="+idope,
				success: function(resul){
					$("#detailgene_operation").html(resul);
					$("#ope_datedemande").mask("99/99/9999");
					$("#ope_refconcess").mask("DC28/999999");
					detail_pctoperation(idope);
				}
			});
		}
	});
}

function detail_bcoperation(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailbcoperation&idope="+idope,
		success: function(resul){
			$("#detbcoperation").html(resul);
			$(".add_demandeos").click(function(){
				tabdemoscht = [];
				var idbc = $(this).data("idbc");
				var myVar; var elements;
				var textbox = '<div class="row"><div class="col-md-12">';
				textbox += '<div class="col-md-3"><label>D�lai (sem.)</label></div><div class="col-md-2"><input class="form-control input-sm text-center" id="demandeos_delai"></div>';
				textbox += '</div></div>';
				textbox += '<br><div class="row"><div class="col-md-12">';
				textbox += '<div class="col-md-3"><label>Montant TTC (�)</label></div><div class="col-md-9"><input class="form-control input-sm text-right" id="demandeos_montant"></div>';
				textbox += '</div></div>';
				textbox += '<br><div class="row"><div class="col-md-12">';
				textbox += '<div class="col-md-3"><label>Entreprises</label></div><div class="col-md-9"><select class="form-control input-sm" id="demandeos_entreprise"></select></div>';
				textbox += '</div></div>';
				textbox += '<br><div class="row"><div class="col-md-12">';
				textbox += '<div class="col-md-3"><label>Type OS</label></div><div class="col-md-9"><select class="form-control input-sm" id="demandeos_typeos"></select></div>';
				textbox += '</div></div>';
				//textbox += '<br><div class="row"><div class="col-md-12"><div class="col-md-2"><label>Op�rations</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectdembc_operation"><option></option></select></div></div></div>';
				textbox += '<br><div class="row"><div class="col-md-12"><div class="col-md-3"><label>Op�rations</label></div><div class="col-md-9"><input class="form-control input-sm" id="selectdemos_operation"></div></div></div>';
				textbox += '<br><div class="row"><div class="col-md-12"><div id="lstchantierdemos"></div></div></div>';
				textbox += '<br><div class="row"><div class="col-md-12"><div id="lstdemos"></div></div></div>';
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstentreprise",
					success: function(resul){
						$("#demandeos_entreprise").html(resul);
					}
				});
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lsttypeos",
					success: function(resul){
						$("#demandeos_typeos").html(resul);
					}
				});
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstoperationdemos&idbc="+idbc,
					success: function(resul){
						$("#lstdemos").html(resul);
						myVar = setInterval(function(){
							elements = tabdemoscht.join(',');
							$.ajax({
								type: "POST",
								url: "./chantier.php",
								data: "action=lstchantieropedemostemp&tabcht="+elements,
								success: function(resul){
									//$("#lstdemos").html(resul);
								}
							});
						}, 1000);
						$("#selectdemos_operation").change(function(){
							var idope = $(this).val();
							if (idope!='-1'){
								elements = tabdemoscht.join(',');
								$.ajax({
									type: "POST",
									url: "./chantier.php",
									data: "action=lstchantieropedemos&idope="+idope+"&tabcht="+elements,
									success: function(resul){
										$("#lstchantierdemos").html(resul);
										$("body").on("click",".chkchtdemandeos",function(){
											alert("coucou");
											if ( $( this ).prop( "checked" ) ){
												var id = $(this).data("id");
												tabdemoscht.push(id);
											}else{
												var id = $(this).data("id");
												index = tabdemoscht.indexOf(id);
												tabdemoscht.splice(index,1);
											}
											console.log(tabdemoscht);
										});
									}
								});
							}
						});
					}
				});

				var dialog = bootbox.dialog({
					title: '<i class="fa fa-edit"></i> Nouvelle Demande OS',
					message: textbox,
					buttons: {
						ok: {
							label: "Envoyer",
							className: 'btn-success',
							callback: function(){
								elements = tabdemoscht.join(',');
								$.ajax({
									type: "POST",
									url: "./chantier.php",
									data: "action=adddemandeos&idope="+idope+"&idbc="+idbc+"&montantttc="+$("#demandeos_montant").val()+"&typeos="+$("#demandeos_typeos").val()+"&ident="+$("#demandeos_entreprise").val()+"&delai="+$("#demandeos_delai").val()+"&tabcht="+elements,
									success: function(resul){
										dialog.modal('hide');
										clearInterval(myVar);
									}
								});
							}
						},
						cancel: {
							label: "Annuler",
							className: 'btn-default',
							callback: function(){
								clearInterval(myVar);
								dialog.modal('hide');
							}
						}
					}
				});
				$("#selectdemos_operation").mask("99-aaa-999");
			});
		}
	});
}
function detail_operation(id){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailoperation&id="+id,
		success: function(resul){
			$("#contentpage").html(resul);
			//$("#annee_prog").mask("99/99/9999");
			$("#ope_datedemande").mask("99/99/9999");
			$("#ope_refconcess").mask("DC28/999999");
			detail_jalon(id);
			detail_programmation(id);
			detail_pctoperation(id);
			detail_coordination(id);
			detail_autorisation(id);
			detail_bcoperation(id);
			$("#back_lstoperations").click(function(){
				menu_chantier();
			});
			$(".visucht").click(function(){
				var idcht = $(this).data("id");
				detail_chantier(idcht);
			});
			$(".delcht").click(function(){
				var idcht = $(this).data("id");
				var idope = $(this).data("idope");
				swal({
					title: "Suppression",
					text: "Etes vous s�r de vouloir supprimer!",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Valider",
					cancelButtonText: "Annuler",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type: "POST",
						url: "./chantier.php",
						data: "action=delchantier&idcht="+idcht,
						success: function(resul){
							swal("Supprim�!", "La prestation a bien �t� supprim�.", "success");
							detail_operation(idope);
						}
					});
				});
			});
			$("body").on("change","#dtconcept",function(){
				var formData = new FormData();
				formData.append('file', $('#dtconcept')[0].files[0]);
				formData.append('numope', $('#ope_refsehv').val());
				$.ajax({
					type: "POST",
					url: "./xmldt_upload.php",
					data: formData,
					cache: false,
					dataType: 'json',
					processData: false,
					contentType: false,
					success: function(resul){
						swal("Charg�!", "La DT a bien �t� charg�.", "success");
						$(".upload_xmldt").addClass("btn-success");
					}
				});
			});
			$("#add_chantier").click(function(){
				var idope = $(this).data("idope");
				var textbox = '<div class="row"><div class="col-md-12"><div class="col-md-2"><label>Natures</label></div><div class="col-md-10"><select class="form-control input-sm" id="selectnouvcht_nature"><option></option></select></div></div></div>';
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstnature",
					success: function(resul){
						$("#selectnouvcht_nature").html(resul);
					}
				});

				var dialog = bootbox.dialog({
					title: '<i class="fa fa-wrench"></i> Nouvelle prestation',
					message: textbox,
					buttons: {
						ok: {
							label: "Cr�er",
							className: 'btn-success',
							callback: function(){
								$.ajax({
									type: "POST",
									url: "./chantier.php",
									data: "action=addchantier&idope="+idope+"&idnat="+$("#selectnouvcht_nature").val(),
									success: function(resul){
										dialog.modal('hide');
										detail_operation(id);
									}
								});
							}
						},
						cancel: {
							label: "Annuler",
							className: 'btn-default',
							callback: function(){
								dialog.modal('hide');
							}
						}
					}
				});
			});
		}
	});
}
function detail_jalon(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailjalon&idope="+idope,
		success: function(resul){
			$("#detjalon").html(resul);
			$(".chpjal").mask("99/99/9999");
			$(".chpjal").change(function(){
				var idope=$(this).data("idope");
				var idjal=$(this).data("idjal");
				var val=$(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatedatejalonoperation&idope="+idope+"&idjal="+idjal+"&val="+val,
					success: function(resul){
						detail_jalon(idope);
					}
				});
			});
		}
	});
}
function detail_programmation(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailprogrammation&idope="+idope,
		success: function(resul){
			$("#detprogrammation").html(resul);
		}
	});
}
function update_aps_ope(idope,chp,val){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=updateapsope&idope="+idope+"&chp="+chp+"&val="+val,
		success: function(resul){
			detail_programmation(idope);
		}
	});
}
function detail_coordination(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailcoordination&idope="+idope,
		success: function(resul){
			$("#detcoordination").html(resul);
			$("#annee_prog").mask("9999");
			$(".chpcoord").click(function(){
				var idope=$(this).data("idope");
				var idcoord=$(this).data("idcoord");
				if ( $( this ).prop( "checked" ) ){val=1;}else{val=0;}
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatecoordoperation&idope="+idope+"&idcoord="+idcoord+"&val="+val,
					success: function(resul){
						detail_coordination(idope);
					}
				});
			});
			$(".chpcritere").click(function(){
				var idope=$(this).data("idope");
				var idcritere=$(this).data("idcritere");
				if ( $( this ).prop( "checked" ) ){val=1;}else{val=0;}
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatecritereoperation&idope="+idope+"&idcritere="+idcritere+"&val="+val,
					success: function(resul){
						detail_coordination(idope);
					}
				});
			});
		}
	});
}

function detail_pctoperation(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailpctoperation&idope="+idope,
		success: function(resul){
			$("#detpct").html(resul);
			$("#pct_numau").mask("099 999 99a9999");
		}
	});
}

function detail_autorisation(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailautorisation&idope="+idope,
		success: function(resul){
			$("#detautorisation").html(resul);
			$(".datedestauto").mask("99/99/9999");
			$(".chkdestauto").click(function(){
				var idauto=$(this).data("idauto");
				var iddest=$(this).data("iddest");
				if ( $( this ).prop( "checked" ) ){val=1;}else{val=0;}
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=checkdestinataireauto&idauto="+idauto+"&iddest="+iddest+"&val="+val,
					success: function(resul){
						detail_autorisation(idope);
					}
				});
			});
			$(".chpmodifdestauto").change(function(){
				var idauto=$(this).data("idauto");
				var iddest=$(this).data("iddest");
				var chp=$(this).data("chp");
				var val=$(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatedestinataireauto&idauto="+idauto+"&iddest="+iddest+"&chp="+chp+"&val="+val,
					success: function(resul){
						detail_autorisation(idope);
					}
				});
			});
			$("body").on("click",".del_valauto",function(){
				var id_ope = $(this).data("idope");
				var id_valauto = $(this).data("id");
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=delvalauto&idvalauto="+id_valauto,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstvalorisationauto&idope="+id_ope,
							success: function(resul){
								$("#lstarticlevalorisationauto").html(resul);
							}
						});
					}
				});
			});
			$("body").on("change",".update_valauto",function(){
				var chp = $(this).data("chp");
				var id_valauto = $(this).data("id");
				var id_ope = $(this).data("idope");
				var val = $(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatevalauto&idvalauto="+id_valauto+"&chp="+chp+"&val="+val,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstvalorisationauto&idope="+id_ope,
							success: function(resul){
								$("#lstarticlevalorisationauto").html(resul);
							}
						});
					}
				});
			});
		}
	});
}

function detail_chantier(idcht){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailchantier&id="+idcht,
		success: function(resul){
			$("#contentpage").html(resul);
			$(".datedevisecl").mask("99/99/9999");
			$(".datespecif").mask("99/99/9999");
			$(".heurespecif").mask("99h99");
			$("#back_operation").click(function(){
				var id_ope = $(this).data("idope");
				detail_operation(id_ope);
			});
			$("#lstbordapd").change(function(){
				var idbord = $(this).val();
				var idapd = $(this).data("idapd");
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=lstarticlebord&idbord="+idbord+"&idapd="+idapd,
					success: function(resul){
						$("#lstarticlesapd").html(resul);
						$(".add_valapd").click(function(){
							var id_art = $(this).data("idarticle");
							var id_apd = $(this).data("idapd");
							$.ajax({
								type: "POST",
								url: "./chantier.php",
								data: "action=addvalapd&idapd="+id_apd+"&idart="+id_art,
								success: function(resul){
									$.ajax({
										type: "POST",
										url: "./chantier.php",
										data: "action=lstvalapd&idapd="+id_apd,
										success: function(resul){
											$("#lstvalapd").html(resul);
										}
									});
								}
							});
						});
					}
				});
			});
			$("body").on("click",".del_valapd",function(){
				var id_apd = $(this).data("idapd");
				var id_valapd = $(this).data("idvalapd");
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=delvalapd&idvalapd="+id_valapd,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstvalapd&idapd="+id_apd,
							success: function(resul){
								$("#lstvalapd").html(resul);
							}
						});
					}
				});
			});
			$("body").on("change",".update_valapd",function(){
				var chp = $(this).data("chp");
				var id_valapd = $(this).data("idvalapd");
				var id_apd = $(this).data("idapd");
				var val = $(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatevalapd&idvalapd="+id_valapd+"&chp="+chp+"&val="+val,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstvalapd&idapd="+id_apd,
							success: function(resul){
								$("#lstvalapd").html(resul);
							}
						});
					}
				});
			});
			$("body").on("change",".update_tauxapd",function(){
				var chp = $(this).data("chp");
				var id_apd = $(this).data("idapd");
				var type = $(this).data("type");
				var val = $(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatetauxapd&idapd="+id_apd+"&chp="+chp+"&val="+val+"&type="+type,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstvalapd&idapd="+id_apd,
							success: function(resul){
								$("#lstvalapd").html(resul);
							}
						});
					}
				});
			});
			$("body").on("change","#lstprogcht",function(){
				var id_prog = $(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=financecht&idanneeprog=0&idprog="+id_prog,
					success: function(resul){
						$("#detailfinancecht").html(resul);
					}
				});
			});
			$("body").on("change",".update_chantier",function(){
				var chp = $(this).data("chp");
				var id_cht = idcht;
				var val = $(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatechantier&idcht="+id_cht+"&chp="+chp+"&val="+val,
					success: function(resul){
						//detail_chantier(id_cht);
/*						if (chp=="programme"){
							$.ajax({
								type: "POST",
								url: "./chantier.php",
								data: "action=detailgenechantier&idcht="+id_cht,
								success: function(resul){
									$("#detailgene_chantier").html(resul);
								}
							});
						}
*/					}
				});
			});
			$("body").on("click",".del_sstraitantcht",function(){
				var id = $(this).data("id");
				var id_cht = idcht;
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=delsstraitantcht&id="+id,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstsstraitantcht&idcht="+id_cht,
							success: function(resul){
								$("#detailsstraitantcht").html(resul);
							}
						});
					}
				});
			});
			$("body").on("change",".update_sstraitantcht",function(){
				var chp = $(this).data("chp");
				var id = $(this).data("id");
				var id_cht = idcht;
				var val = $(this).val();
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=updatesstraitantcht&id="+id+"&chp="+chp+"&val="+val,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstsstraitantcht&idcht="+id_cht,
							success: function(resul){
								$("#detailsstraitantcht").html(resul);
							}
						});
					}
				});
			});
			$("body").on("click","#add_sstraitantcht",function(){
				var id_cht = idcht;
				$.ajax({
					type: "POST",
					url: "./chantier.php",
					data: "action=addsstraitantcht&idcht="+id_cht,
					success: function(resul){
						$.ajax({
							type: "POST",
							url: "./chantier.php",
							data: "action=lstsstraitantcht&idcht="+id_cht,
							success: function(resul){
								$("#detailsstraitantcht").html(resul);
							}
						});
					}
				});
			});
		}
	});
}

/* PCT */
function lst_bordpct(page,login,chp,val){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=lstbordpct&page="+page+"&login="+login+"&chp="+chp+"&val="+val,
		success: function(resul){
			$("#lstbordpct").html(resul);
			$(".forwardpct").click(function(){
				if(page>1){lst_bordpct(page-1,getCookie("login"),"","");}
			});
			$(".nextpct").click(function(){
				lst_bordpct(page+1,getCookie("login"),"","");
			});
			$(".visubordpct").click(function(){
				var id = $(this).data("id");
				detail_bordpct(id);
			});
		}
	});
}
function detail_bordpct(id){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailbordpct&id="+id,
		success: function(resul){
			$("#contentpage").html(resul);
			//$("#annee_prog").mask("99/99/9999");
			//detail_jalon(id);
			$("#back_lstbordpct").click(function(){
				menu_chantier();
				$('#collapse1').collapse();
				$('#collapse2').collapse();
			});
		}
	});
}
/* B�n�ficiaires */
function detail_opebeneficiaire(idope){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=detailopebeneficiaire&idope="+idope,
		success: function(resul){
			$("#detbeneficiaire").html(resul);
		}
	});
}
function update_opebeneficiaire(id,idope,chp,val){
	$.ajax({
		type: "POST",
		url: "./chantier.php",
		data: "action=updateopebeneficiaire&id="+id+"&chp="+chp+"&val="+val,
		success: function(resul){
			detail_opebeneficiaire(idope);
		}
	});
}