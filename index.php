<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link id="page_favicon" href="./images/favicon.ico" rel="icon" type="image/x-icon" />

    <title>Travaux 2.0</title>

    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'>
    <link rel='stylesheet' href='./css/jquery.qtip.min.css' type='text/css'>
    <link rel='stylesheet' href='./css/fileinput.min.css' type='text/css'>
    <link rel='stylesheet' href='./css/login.css' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <!--<h1 class="text-center login-title">SYGALE 2.0<h3 class="text-center login-stitle">Syst&egrave;me de Gestion et d'Analyse d'&Eacute;nergies des collectivit&eacute;s</h3></h1>-->
            <div class="account-wall">
                <img class="profile-img" src="./images/logo.png" alt="">
                <h3 class="text-center" style="color:#636466;">Travaux 2.0</h3>
                <form id="formlog" class="form-signin">
					<div id="login-alert" style="display:none" class="alert alert-danger col-sm-12"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>Erreur:</strong> identifiant incorrect</div> <!--  -->

					<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user" aria-hidden="true"></i></span>
					<input type="text" class="form-control" id="log" placeholder="Identifiant" aria-describedby="basic-addon1" required autofocus>
					</div>

					<div class="input-group">
					<span class="input-group-addon" id="basic-addon2"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
					<input type="password" class="form-control" id="pwd" placeholder="Mot de passe" aria-describedby="basic-addon2" required>
					</div>
					
					<button id="btnlog" class="btn btn-lg btn-primary btn-block">Me connecter</button>


					<!--<label class="checkbox pull-left"><input type="checkbox" value="remember-me">Se souvenir</label>
					<a href="#" class="pull-right need-help">Besoin d'aide? </a><span class="clearfix"></span>-->
                </form>
            </div>
            <!--<a href="#" class="text-center new-account">Cr&eacute;er un compte </a>-->
        </div>
    </div>
</div>
    <script src='https://code.jquery.com/jquery-3.2.1.min.js'></script>
    <script src='./js/bootstrap.min.js'></script>
    <script src='./js/jquery.qtip.min.js'></script>
    <script src='./js/validation.js'></script>
    <script src='./js/cookies.js'></script>
    <script src='./js/login.js'></script>
</body>
</html>